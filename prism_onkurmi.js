const prism_base = require('./prism_base');
prism_base.moduleMap.quickfeature = function($prism) {
    return (new (require("./prism_quickfeature"))($prism));
}

function prism_module(doConnect) {

    var $prism = prism_base.init();
    if (doConnect){
        $prism.api.connect();
    }
    return $prism;
}

module.exports = $prism;
