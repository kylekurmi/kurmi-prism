const prismpromise = require("./prism_promise");
const kurmifieldlist = require("./kurmi_fields");
const prism_workspace_base = function ($prism) {
    var $prism_workspace_base = this;

    const proxypromise = typeof Promise === "function" ? Promise : prismpromise;

    $prism_workspace_base.getDetailList = function () {
        return $prism.sql.getObject(["cfg_branch_id", "name","frozen","location","parent","revision"],"cfg_branch").then(function (listWorkspaceSqlResult) {
            return listWorkspaceSqlResult;
        });
    };

    $prism_workspace_base.getList = function () {
        function flattenWorkspaceList(masterList, currentWorkspace) {
            masterList.push(currentWorkspace.name);
            (currentWorkspace.childs || []).reduce(flattenWorkspaceList, masterList);
            return masterList;
        }
        return $prism.api.execute("listWorkspaces", {}).then(function (listWorkspaceResult) {
            return listWorkspaceResult && listWorkspaceResult.workspaces && JSON.parse(listWorkspaceResult.workspaces).reduce(flattenWorkspaceList, []);
        });
    };
    $prism_workspace_base.getDetailList = function(){
        return $prism.sql.getObject(["name","location","frozen","cfg_branch_id"],'cfg_branch');
    }

    $prism_workspace_base.createBranch = function createBranch(branchName,parentName){
        return $prism_workspace_base.getDetailList().then(function(list){
            var _workspace = list.filter(function(ws){
                return ws.name == parentName;
            })[0];
            if (_workspace){
                return $prism.api.execute('addWorkspace',{name:branchName,parent:parentName});
            }
            else {
                return $prism.api.execute('addWorkspace',{name:branchName});
            }
        });
    }

    $prism_workspace_base.listWorkspaceFiles = function (workspaceName, fileType) {
        return $prism.api.execute("listWorkspaceFiles", { workspace: workspaceName }).then(function (listWorkspaceFilesResult) {
            var files = JSON.parse(listWorkspaceFilesResult.files).map(function (file) { return file.path });
            if (fileType) {
                var rg = typeof fileType == typeof /.*/ ? fileType : new RegExp(fileType, "i");
                files = files.filter(function (f) { return rg.test(f); });
            }
            return files.sort();
        });
    },
        $prism_workspace_base.getWorkspaceFile = function (workspaceName, filePath) {
            return $prism.api.execute("getWorkspaceFile", { workspace: workspaceName, path: filePath }).then(function (getWorkspaceFileResult) {
                return getWorkspaceFileResult;
            });
        }

    $prism_workspace_base.listOfMailFileRelations = function (workspaceName) {
        return $prism.workspace.listWorkspaceFiles(workspaceName, "mail.js").then(function (mailfilenames) {
            var mailFileResultsPromise = prismpromise.all(mailfilenames.map(function (name) {
                return $prism.workspace.getWorkspaceFile(workspaceName, name);
            }));
            return mailFileResultsPromise.then(function (mailFileResults) {
                var mailFileIncludes = mailFileResults.map(function (item) { return { path: item.path, includes: (item.content.match(/(?=\#include )(.*\.mail.js)/g)  || []).map(function(mtch) { return mtch.replace("#include ","");   }) }; });
                return mailFileIncludes.reduce(function (p, c, i, a) {
                    c.mentions = a.filter(function (mfc) { return mfc.includes.indexOf(c.path) != -1; });
                    p.push(c);
                    return p;
                }, []);
            });
        });
    }
    $prism_workspace_base.mapOfMailfiles = function (workspaceName) {
        return $prism_workspace_base.listOfMailFileRelations(workspaceName).then(function (mailFileIncludes) {
            var map = {};

            function reduceMailFiles($item, list) {
                var item = JSON.parse(JSON.stringify($item));
                if (item.includes) {
                    item.includes.forEach(function (includedItemPath) {

                        var includedItem = list.filter(function(f) { return f.path == includedItemPath;})[0];
                        if (includedItem) {
                            item[includedItemPath] = reduceMailFiles(includedItem, list);
                        }
                    });

                }
                if (item.mentions && item.mentions.length == 0) {
                    map[item.path] = item;


                }
                delete item.path;
                delete item.includes;
                delete item.mentions;

                return item;
            }
            var mailTemplates = {};
            Object.keys(mailTemplates).reduce(function(p,k) {
                var v = mailTemplates[k];
              	p[k] = {};
                p[k][v] = tp1[v];
                
                return p;
            },{});

            mailFileIncludes.forEach(function (item, i, arr) { reduceMailFiles(item, arr); });
            return map;
        });
    }
    $prism_workspace_base.mapConfiguredMailFiles = function(optionalTenantName){
        //prism.api.getConfigTree({path:"/MailManagement"},"trainingpod1").then(function(res) { return prism.component.processConfigNode(res.configTree,{}); });
        
        function _mapConfiguredMailFiles(mapOfMailfiles,configuration_MailManagement){
            var templateKeys = Object.keys(configuration_MailManagement || {}).filter(function(key){ return /^template/i.test(key); });
            
            return templateKeys.reduce(function(p,c){
                p[c] = {};
                var templateFilePath = configuration_MailManagement[c];
                p[c][templateFilePath] = mapOfMailfiles[templateFilePath] || {};


                return p;
            },{});

        }
        if (!optionalTenantName){
            return $prism.api.execute("getConfigTree",{path:"/MailManagement"}).then(function(res) { return prism.component.processConfigNode(res.configTree,{}); }).then(function(configuration_MailModule){
                return $prism_workspace_base.getSystemWorkspaceName().then(function(workspaceName){
                    return $prism_workspace_base.mapOfMailfiles(workspaceName).then(function(momf){
                        return _mapConfiguredMailFiles(momf,configuration_MailModule);
                    });
                })
            });
            
        }
        else {
            return $prism.api.execute("getConfigTree",{path:"/MailManagement"},optionalTenantName).then(function(res) { 
                return $prism.component.processConfigNode(res.configTree,{}); 
            }).then(function(configuration_MailModule){
                return $prism_workspace_base.getTenantWorkspaceName(optionalTenantName).then(function(workspaceName){
                    return $prism_workspace_base.mapOfMailfiles(workspaceName).then(function(momf){
                        return _mapConfiguredMailFiles(momf,configuration_MailModule);
                    });
                });
            });
        }
    }

    $prism_workspace_base.getTenantWorkspaceName = function(tenantIdentifier){
        return $prism.api.getTenants().then(function(tenants){
            return tenants.filter(function(t){ return t.identifier == tenantIdentifier; })[0].configBranch.split('/').slice(-2,-1)[0];
        });
    }

    $prism_workspace_base.getSystemWorkspaceName = function(){
        return $prism.sql.read("select name from cfg_branch join cfg_main_branch on cfg_main_branch.cfg_branch_id = cfg_branch.cfg_branch_id;").then(function(result) {
            return result[0][0];
            });
    }

    $prism_workspace_base.getWorkspaceId = function(workspaceName){
        return $prism.sql.read("select cfg_branch_id from cfg_branch where name like '" + workspaceName + "';").then(function(result) {
            return result[0][0];
            });
    }

    $prism_workspace_base.getWorkspaceServicesAndQuickfeaturesDocument = function(workspaceName) {
        return $prism_workspace_base.listWorkspaceFiles(workspaceName,/(service|quickfeature).xml/).then(function(listServices){
            return listServices.map(function(filePath){
                return $prism_workspace_base.getWorkspaceFile(workspaceName,filePath)
            })
        }).then(function(fileContentPromises){
            return Promise.all(fileContentPromises).then(function(fileResults) { return $prism_workspace_base.parseFileResultstoDocument(fileResults); });
        });

    }

    $prism_workspace_base.Workspace = function(workspaceDocument){
        this.xmlDocument = workspaceDocument;
        function getParentsUntil(ele,tagName) {
            return ele.tagName != tagName ? getParentsUntil(ele.parentElement,tagName) : ele;
        }
		const $xmlDocument = this.xmlDocument;

        Object.defineProperty(this,
            "serviceQuickFeatureMapping", {
                get() {
                    return [...$xmlDocument.querySelectorAll('service')].reduce(function(list, service) { 
                        var qfs = [...$xmlDocument.querySelectorAll(`quickchange service-addon[identifier="${service.getAttribute('uid')}"], quickchange service[identifier="${service.getAttribute('uid')}"]`)].map(el=>getParentsUntil(el,'quickchange')).filter(function(item,i,arr) { return arr.findIndex(it=>it.getAttribute('uid')==item.getAttribute('uid')) == i;});
                        if (qfs.length){
                            list[service.getAttribute('identifier')] = qfs;
                        }
                        return list;
                    },{});
                }
            }
        );
        
    }

    $prism_workspace_base.parseFileResultstoDocument = function(arrFileResults) {
        var dp = new DOMParser();
        var doc = dp.parseFromString("<workspace></workspace>","application/xml");
        var workspace = doc.querySelector('workspace');
        arrFileResults.forEach(function(fileResult){
            var docparsed = dp.parseFromString(fileResult.content,"application/xml");
            var fileResultElement = document.createElement('file-result');
            ['id','path','revisionAuthor','revisionComment','revisionDate','title'].forEach(function(attribute){
                fileResultElement.setAttribute(attribute,fileResult[attribute]);
                
            });
            fileResultElement.appendChild(docparsed.documentElement);    
            workspace.appendChild(fileResultElement);         
        });
        return doc;
    }

    $prism_workspace_base.parseXMLtoDocument = function(stringXML) {
        return (new DOMParser()).parseFromString(stringXML, "application/xml");
    }

    

    $prism_workspace_base.Service = function(stringXMLOrElement){
        this.xmlElement = typeof stringXMLOrElement == "string" ? $prism_workspace_base.parseXMLtoDocument(stringXMLOrElement) : stringXMLOrElement ;

        var modules = [...this.xmlElement.querySelectorAll('module')].map(f=>f.textContent);
        var modulesAndFields = Object.entries(kurmifieldlist[0].modules).filter(([moduleName,fields])=>{ return modules.some(mod=> moduleName.indexOf(mod) != -1); });





        this.getAnchorPoint = function() {
            var modifyVariable = this.xmlElement.querySelector('modifyVariable[logic="anchorPoint"]');
            var addVariable = this.xmlElement.querySelector('addVariable[logic="anchorPoint"]');
            var variable = this.xmlElement.querySelector('variable[logic="anchorPoint"]');
            return modifyVariable || addVariable || variable;
        }

        function scoreTag(element){
            //scores the rule or variable based on whether it's an add, modify, delete or builtin rule;
            if (!element) return -2;
            var tagName = element.tagName;
            
            if (/add/i.test(tagName)) {
                return 1;
            }
            else if (/modify/i.test(tagName)){
                return 2;
            }
            else if (/remove/i.test(tagName)){
                return -1;
            }
            else { 
                return 0;
            }
        }
        function weightTag(elementA,elementB){
            //favors the more recent rule or variable and returns it;
            if (scoreTag(elementA)>= scoreTag(elementB)){
                return elementA;
            }
            else {
                return elementB;
            }
        }

        this.getBlocks = function(){
            var blocks = [...this.xmlElement.querySelectorAll('variable:not([logic="field"]),addVariable:not([logic="field"]),modifyVariable:not([logic="field"])')];

            return Object.values(blocks.reduce(function(p,c){
                var id = c.getAttribute('id');
                p[id] = weightTag(p[id], c);
                return p;
            },[]));
        }
        this.getServiceVariables = function(){
            return [...this.xmlElement.querySelectorAll('variable[logic="field"], addVariable[logic="field"],modifyVariable[logic="field"]')];
        }
        this.getFields = function(){
            
            var fieldMap = {};
            this.xmlElement.querySelectorAll('rule, addRule').forEach(function (rule) {
              rule.querySelectorAll('fields field').forEach(function (field) {
                var mappedField = fieldMap[`${rule.querySelector('expression').textContent}:${field.textContent}`] = fieldMap[`${rule.querySelector('expression').textContent}:${field.textContent}`] || {};
                mappedField.rules = mappedField.rules || [];
                mappedField.rules.push(rule.getAttribute('uid'))
                mappedField.expression = rule.querySelector('expression').textContent;
                mappedField.name = field.textContent;
              });
      
            });
            window.fieldMap = fieldMap;
            return Object.values(fieldMap);
        }

        

        this.getRulesByBlockAndField = function(block,fieldName){
			var blockIdentifier = typeof block == 'string' ? block : variable.getAttribute('logic') == "field" ? 'self' : block.getAttribute('id');
			return [...this.xmlElement.querySelectorAll('rule,addRule,modifyRule')].filter(function(rule) { return rule.querySelector('expression').textContent == blockIdentifier && [...rule.querySelectorAll('fields field')].find(f=>f.textContent == fieldName);  });
		}

        this.getRuleById = function(uid){
            var rules = this.getRulesById(uid);
            return rules.modifyRule || rules.addRule || rules.rule;
        }
        this.getRulesById = function(uid) {
            var rule = this.xmlElement.querySelector(`rule[uid="${uid}"]`);
            var addRule = this.xmlElement.querySelector(`addRule[uid="${uid}"]`);
            var modifyRule = this.xmlElement.querySelector(`modifyRule[uid="${uid}"]`);
            var removeRule = this.xmlElement.querySelector(`removeRule[uid="${uid}"]`);
            return {addRule,modifyRule,removeRule,rule};
          }
        this.getFieldsOnVariable = function(variable) {
            if (variable.getAttribute('logic') != "field"){
                var componentType = variable.querySelector('componentType') && variable.querySelector('componentType').getAttribute('id');
                var specificModule = modulesAndFields.find(m=> new RegExp(componentType,'i').test(m[0]));
                if (!specificModule) {
                    console.error("couldn't find module by name");
                }
                var moduleFields = specificModule && specificModule[1].map(function(field){ return {name:field,expression:variable.getAttribute('id'),rules:[]} }) || [];
                var knownFields = this.getFields().filter(function (field) { return  variable.getAttribute('logic') == "field" ? field.expression == 'self' : field.expression == variable.getAttribute('id'); })

                return moduleFields.reduce(function(p,c){
                    if (!p.find(field=>field.name == c.name)){
                        p.push(c);
                    }
                    return p;
                },knownFields);
                
            }
            if (!variable) {
              console.error("no variable");
              return [];
            }
            return this.getFields().filter(function (field) { return  variable.getAttribute('logic') == "field" ? field.expression == 'self' : field.expression == variable.getAttribute('id'); });
          }
    }
}

module.exports = prism_workspace_base;
