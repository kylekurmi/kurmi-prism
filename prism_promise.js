const PENDING = "pending";
const FULFILLED = "fulfilled";
const REJECTED = "rejected";

function prismpromise(executor) {
    var state=PENDING;
    var value = null;
    var handlers=[];
    var catchers = [];
	var $this = this;
  	function updateValue(result) {
      	if (result && result.constructor.name != "prismpromise") {
           	value = result; 
        }
      	else if (result && result.constructor.name == "prismpromise"){
          	$this = result;
        }
      
    }
  
    function resolve(result) {
        if(state!==PENDING) return;

        state=FULFILLED;
        value = result;

        handlers.forEach(function(h){ 
          updateValue(h(value));
          
          return result;
		 });
    }

    function reject(error) {
        if(state!==PENDING)return;

        state=REJECTED;
        value=error;
        catchers.forEach(function(c) { return c(value);});
    }

    this.then = function(successCallback) {
        if(state === FULFILLED) {
          	updateValue(successCallback(value));
        }else {
            handlers.push(successCallback);
        }
        return $this;
    }

    this.catch = function(failureCallback) {
        if(state===REJECTED){
            failureCallback(value)
        } else {
            catchers.push(value);
        }
      	return $this;
    }
    executor(resolve,reject);
}

prismpromise.all = function(promises) {
    
    return new prismpromise(function (resolve, reject) {
        var cnt = 0;
        var res = [];
        if (promises.length === 0) {
            resolve(promises);
            return;
        }
        promises.forEach(function(prom,i){
            prom.then(function(val) {
                done(val, i);
            }).catch(function(err) {
                reject(err);
                
            });
        })
        
        
        function done(val, i) {
            res[i] = val;
            cnt++;
            if (promises.length === cnt) {
                resolve(res);
            }
        }
    });
}

module.exports = prismpromise;