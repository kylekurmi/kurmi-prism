require=(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };

},{}],2:[function(require,module,exports){
(function (setImmediate,clearImmediate){(function (){
var nextTick = require('process/browser.js').nextTick;
var apply = Function.prototype.apply;
var slice = Array.prototype.slice;
var immediateIds = {};
var nextImmediateId = 0;

// DOM APIs, for completeness

exports.setTimeout = function() {
  return new Timeout(apply.call(setTimeout, window, arguments), clearTimeout);
};
exports.setInterval = function() {
  return new Timeout(apply.call(setInterval, window, arguments), clearInterval);
};
exports.clearTimeout =
exports.clearInterval = function(timeout) { timeout.close(); };

function Timeout(id, clearFn) {
  this._id = id;
  this._clearFn = clearFn;
}
Timeout.prototype.unref = Timeout.prototype.ref = function() {};
Timeout.prototype.close = function() {
  this._clearFn.call(window, this._id);
};

// Does not start the time, just sets up the members needed.
exports.enroll = function(item, msecs) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = msecs;
};

exports.unenroll = function(item) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = -1;
};

exports._unrefActive = exports.active = function(item) {
  clearTimeout(item._idleTimeoutId);

  var msecs = item._idleTimeout;
  if (msecs >= 0) {
    item._idleTimeoutId = setTimeout(function onTimeout() {
      if (item._onTimeout)
        item._onTimeout();
    }, msecs);
  }
};

// That's not how node.js implements it but the exposed api is the same.
exports.setImmediate = typeof setImmediate === "function" ? setImmediate : function(fn) {
  var id = nextImmediateId++;
  var args = arguments.length < 2 ? false : slice.call(arguments, 1);

  immediateIds[id] = true;

  nextTick(function onNextTick() {
    if (immediateIds[id]) {
      // fn.call() is faster so we optimize for the common use-case
      // @see http://jsperf.com/call-apply-segu
      if (args) {
        fn.apply(null, args);
      } else {
        fn.call(null);
      }
      // Prevent ids from leaking
      exports.clearImmediate(id);
    }
  });

  return id;
};

exports.clearImmediate = typeof clearImmediate === "function" ? clearImmediate : function(id) {
  delete immediateIds[id];
};
}).call(this)}).call(this,require("timers").setImmediate,require("timers").clearImmediate)
},{"process/browser.js":1,"timers":2}],3:[function(require,module,exports){
module.exports = [{
	"version": "7.4.0",
	"modules": {

		"AuraCommAddress": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"commAddressId",
			"type",
			"handle",
			"domainName"
		],

		"AuraCommProfileSet": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"commProfileSetName",
			"commProfileSetId"
		],

		"fr.niji.database.AuraCommProfileSetUser": [
			"prefHandleId"
		],
		"AuraMessaging": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"firstName",
			"lastName",
			"password",
			"mailboxNumber",
			"numericAddress",
			"voiceMailboxExtension",
			"cos",
			"mail",
			"commonName",
			"asciiName",
			"secondExt",
			"misc1",
			"misc2",
			"storageDestinationType",
			"exchEmailAddress",
			"exchFQDN",
			"site",
			"aaDirectory",
			"mwiEnabled",
			"pronounceableName",
			"forceChangeOfPassword",
			"voiceMessagingPasswordExpired",
			"language",
			"timeZone",
			"mobilePhoneNumber",
			"forwardFaxToEmailAddress",
			"playOnPhoneType",
			"playOnPhoneNumber",
			"personalAttendantNumber",
			"reachMePriorityCallers",
			"reachMePriorityCallersAction",
			"reachMePriorityCallersCallScreen",
			"reachMeStandardCallersAction",
			"reachMeStandardCallersCallScreen",
			"reachMeType1",
			"reachMeType2",
			"reachMeType3",
			"reachMeNumber1",
			"reachMeNumber2",
			"reachMeNumber3",
			"reachMeRings2",
			"reachMeRings3",
			"reachMeRings4",
			"reachMeUseSchedule",
			"reachMeStartTime",
			"reachMeEndTime",
			"reachMeDays",
			"notifyMeType",
			"notifyMeProvider",
			"notifyMeUrgentOnly",
			"notifyMeEmail",
			"notifyMeEmailAddress",
			"notifyMeEmailRecord",
			"playUnreadImportant",
			"playReadImportant",
			"playSavedImportant",
			"playbackSpeed",
			"announceDateTime",
			"voiceAddressing"
		],
		"fr.niji.database.AuraMessagingDirectoryNumbers": [
			"ordre"
		],

		"AuraOfficelinx": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"officelinxId",
			"officelinxName",
			"mailBoxNumber",
			"numericPassword",
			"applicationUserPassword",
			"company",
			"department",
			"featureGroup",
			"capability",
			"domainAccountName",
			"synchronizationUserName"
		],


		"AuraPresence": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"presenceId",
			"presenceSystem",
			"imGatewaySipEntity",
			"publishViaAESCollector"
		],


		"AuraSessionManager": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"sessionManagerId",
			"handleName",
			"handleType",
			"handleDomainName",
			"password",
			"primarySM",
			"secondarySM",
			"originationAppSequence",
			"terminationAppSequence",
			"confFactorySet",
			"survivabilityServer",
			"homeLocation",
			"maxSimultaneousDevices"
		],


		"AuraSystemManager": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"userId",
			"firstName",
			"lastName",
			"middleName",
			"description",
			"authenticationType",
			"password",
			"title",
			"language",
			"timeZone",
			"employeeId",
			"department",
			"company",
			"duplicatedLoginAllowed",
			"enabled",
			"virtualUser",
			"manager",
			"preferredGivenName",
			"suffix",
			"honorific",
			"userType",
			"roles",
			"commPassword"
		],

		"AvayaEC500": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"extension",
			"dialPrefix",
			"countryCode",
			"phoneNumber",
			"trunkSelection",
			"trunkGroup",
			"configSet",
			"callLimit",
			"mappingMode",
			"callsAllowed",
			"bridgeCalls",
			"ec500Key",
			"extendCallKey"
		],

		"AvayaLineGroup": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"groupId",
			"directoryNumber",
			"ddiNumber",
			"displayedNumber",
			"numberParam0",
			"numberParam1",
			"numberParam2",
			"directoryName",
			"groupType",
			"tn",
			"cor",
			"pin",
			"callerDisplay",
			"acd",
			"queue",
			"queueAcd",
			"vector",
			"vectorQueue",
			"vectorAcd",
			"vectorAcdQueue",
			"coveragePath",
			"nightServiceDestination",
			"mmEarlyAnswer",
			"queueLimit",
			"queueLimitSize",
			"callsWarningThreshold",
			"callsWarningThresholdType",
			"callsWarningThresholdPort",
			"callsWarningThresholdExtension",
			"timeWarningThreshold",
			"timeWarningThresholdType",
			"timeWarningThresholdPort",
			"timeWarningThresholdExtension",
			"aas",
			"controlingAdjunct",
			"adjunctCTILink",
			"expectedCallHandlingTime",
			"forcedEntry",
			"interruptibleAuxThreshold",
			"interruptibleAuxDesactivationThreshold",
			"measured",
			"multipleCallHandling",
			"redirectOnNoAnswer",
			"redirectOnNoAnswerToVDN",
			"retainActiveVDNContextOnNoAnswer",
			"redirectOnFailureToVDN",
			"retainActiveVDNContextOnNoFailure",
			"serviceLevelInterval",
			"serviceLevelTarget",
			"skill",
			"supervisorExtension",
			"timedACWinterval",
			"vuStatsObjective",
			"afterXfer",
			"audixName",
			"lwcReception",
			"messageCenter",
			"messageCenterAudixName",
			"primary",
			"callingPartyNumber",
			"firstAnnouncementDelay",
			"firstAnnouncementExtension",
			"messageCenterMSAName",
			"provideRingback",
			"routingDigits",
			"sendRerouteRequest",
			"tscInterrogation",
			"voiceMailNumber",
			"voiceMailExtension",
			"voiceMailHandle"
		],
		"fr.niji.database.AvayaLineGroupDirectoryNumbers": [
			"ordre"
		],
		"AvayaPickupGroup": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"groupId",
			"groupName"
		],
		"fr.niji.database.AvayaPickupGroupDirectoryNumbers": [
			"ordre"
		],
		"ccmclngptpattern": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"pattern",
			"ddiNumber",
			"displayedNumber",
			"numberParam0",
			"numberParam1",
			"numberParam2",
			"partition",
			"description",
			"callingPartyUsePhoneMask",
			"callingPartyDigitDiscard",
			"callingPartyTransformationMask",
			"callingPartyPrefixDigits",
			"callingPartyLinePresentationBit"
		],

		"ccmintercom": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"directoryNumber",
			"ddiNumber",
			"displayedNumber",
			"numberParam0",
			"numberParam1",
			"numberParam2",
			"partition",
			"description",
			"alertingName",
			"asciiAlertingName",
			"allowCTIControlFlag",
			"css",
			"presenceGroup",
			"autoAnswer",
			"defaultActivatedDevice",
			"display",
			"asciiDisplay",
			"label",
			"asciiLabel",
			"speedDial",
			"externalPhoneNumberMask",
			"callerName",
			"callerNumber"
		],
		"ccmroutepattern": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"ddiNumber",
			"displayedNumber",
			"numberParam0",
			"numberParam1",
			"numberParam2",
			"pattern",
			"routePartition",
			"description",
			"routeList",
			"blockEnable",
			"blockCause",
			"networkLocation",
			"allowDeviceOverride",
			"provideOutsideDialtone",
			"supportOverlapSending",
			"patternUrgency",
			"authorizationCodeRequired",
			"authorizationLevelRequired",
			"clientCodeRequired",
			"callingPartyUsePhoneMask",
			"callingPartyTransformationMask",
			"callingPartyPrefixDigitsOut",
			"callingPartyLinePresentationBit",
			"callingPartyNamePresentationBit",
			"connectedPartyLinePresentationBit",
			"connectedPartyNamePresentationBit",
			"calledPartyDigitDiscard",
			"calledPartyTransformationMask",
			"calledPartyPrefixDigitsOut"
		],
		"ccmtranslationpattern": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"ddiNumber",
			"displayedNumber",
			"numberParam0",
			"numberParam1",
			"numberParam2",
			"pattern",
			"partition",
			"description",
			"css",
			"blockEnable",
			"blockCause",
			"provideOutsideDialtone",
			"patternUrgency",
			"callingPartyUsePhoneMask",
			"callingPartyTransformationMask",
			"callingPartyPrefixDigitsOut",
			"callingPartyLinePresentationBit",
			"callingPartyNamePresentationBit",
			"connectedPartyLinePresentationBit",
			"connectedPartyNamePresentationBit",
			"calledPartyDigitDiscard",
			"calledPartyTransformationMask",
			"calledPartyPrefixDigitsOut"
		],

		"ciscodeviceprofile": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"name",
			"deviceProfileDescription",
			"enableCTIControlledDeviceProfile",
			"deviceSoftkeyTemplate",
			"deviceCallInfoPrivacyStatus",
			"deviceFeatureControlPolicy",
			"deviceProduct",
			"deviceModel",
			"deviceProtocol",
			"devicePhoneButtonTemplate",
			"deviceDndEnable",
			"deviceDndOption",
			"deviceDndRingSetting",
			"deviceEmccCss",
			"deviceUserHoldMohAudioSourceId",
			"deviceServices",
			"deviceApplicationUsers",
			"deviceExpansionModule1",
			"deviceExpansionModule2",
			"deviceExpansionModule3",
			"deviceMlppDomainId",
			"deviceMlppIndication",
			"deviceMlppPreemption",
			"userLanguage",
			"param1",
			"param2",
			"param3",
			"param4",
			"param5",
			"param6",
			"param7",
			"param8",
			"param9",
			"param10",
			"param11",
			"param12",
			"param13",
			"param14",
			"param15",
			"param16",
			"param17",
			"param18",
			"param19",
			"param20",
			"param21",
			"param22",
			"param23",
			"param24",
			"param25",
			"param26",
			"param27",
			"param28",
			"param29",
			"param30"
		],
		"fr.niji.database.CiscoDeviceProfileToLine": [
			"externalPhoneNumberMask",
			"mwiPolicy",
			"recordingMediaSource",
			"lineDisplay",
			"asciilineDisplay",
			"lineLabel",
			"asciilineLabel",
			"lineRingSetting",
			"lineRingSettingActive",
			"lineRingSettingActiveIdlePickupAlert",
			"lineRingSettingIdleActivePickupAlert",
			"lineMaxNumCalls",
			"lineBusyTrigger",
			"lineCallInfoDisplayCallerName",
			"lineCallInfoDisplayCallerNumber",
			"lineCallInfoDisplayRedirectedNumber",
			"lineCallInfoDisplayDialedNumber",
			"userAssociatedWithLine",
			"lineRecordingFlag",
			"lineRecordingProfile",
			"lineMonitoringCss",
			"missedCallLogging"
		],

		"ciscovgsip": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"internalIdentifier",
			"vgSipName",
			"slot",
			"subunit",
			"port",
			"description",
			"cptone",
			"signal",
			"ringFrequency",
			"stationId",
			"idDialPeer",
			"descriptionDialPeer",
			"destinationPattern"
		],


		"fr.niji.database.ConnectorBundle": [
			"version",
			"content"
		],
		"CUPS": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"userId",
			"enableCUPC",
			"unityProfile",
			"ctiGatewayProfile",
			"meetingPlaceProfile",
			"ldapProfile",
			"proxyProfile",
			"ccmcipProfile",
			"audioProfile",
			"preferredDevice",
			"mocEnabled",
			"enableCalendarPresence"
		],

		"CCM7_0 call server Device": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"idCore",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"ciscoName",
			"description",
			"disableTime",
			"operationPending",
			"creationPending",
			"param1",
			"param2",
			"param3",
			"param4",
			"param5",
			"param6",
			"param7",
			"param8",
			"param9",
			"param10",
			"param11",
			"param12",
			"param13",
			"param14",
			"param15",
			"param16",
			"param17",
			"param18",
			"param19",
			"param20",
			"param21",
			"param22",
			"param23",
			"param24",
			"param25",
			"param26",
			"param27",
			"param28",
			"param29",
			"param30",
			"unicityEnabledChild",
			"idCoreChild",
			"oid",
			"css",
			"devicePool",
			"commonDeviceConfig",
			"model",
			"product",
			"phoneButtonTemplate",
			"softkeyTemplate",
			"commonPhoneProfile",
			"userHoldMohAudioSourceId",
			"networkHoldMohAudioSourceId",
			"protocol",
			"location",
			"dndRingSetting",
			"userLanguage",
			"featureControlPolicy",
			"settingsAccess",
			"voiceVlanAccess",
			"webAccess",
			"ciscoCamera",
			"wifi",
			"rtcp",
			"videoCapability",
			"pcPort",
			"displayOnTime",
			"displayOnDuration",
			"lldpOnSwPort",
			"lldpOnPcPort",
			"presenceGroup",
			"subscribeCss",
			"aarCss",
			"aarNeighborhoodName",
			"mediaResourceGroupList",
			"lineDisplay",
			"asciilineDisplay",
			"lineLabel",
			"asciilineLabel",
			"lineRingSetting",
			"lineRingSettingActive",
			"lineRingSettingActiveIdlePickupAlert",
			"lineRingSettingIdleActivePickupAlert",
			"lineMaxNumCalls",
			"lineBusyTrigger",
			"lineCallInfoDisplayCallerName",
			"lineCallInfoDisplayCallerNumber",
			"lineCallInfoDisplayRedirectedNumber",
			"lineCallInfoDisplayDialedNumber",
			"services",
			"applicationUsers",
			"mobileIdentityEnabled",
			"mobileIdentityDestinationName",
			"mobileIdentityDestinationNumber",
			"mobileIdentityATSTimer",
			"mobileIdentityATLTimer",
			"mobileIdentityDBRTimer",
			"mobileIdentityProfileName",
			"mobileIdentityMobilePhoneEnabled",
			"mobileIdentityMobileConnectEnabled",
			"mobileIdentityTimeZone",
			"expansionModule1",
			"expansionModule2",
			"expansionModule3",
			"CAPFAuthModeHistory",
			"CAPFAuthStringHistory",
			"CAPFOperationHistory",
			"CAPFKeySizeHistory",
			"CAPFOperationDateHistory",
			"lineRecordingFlag",
			"lineRecordingProfile",
			"lineMonitoringCss",
			"deviceKind",
			"vgDomainName",
			"numPort",
			"numSlot",
			"numSubUnit",
			"unattendedPort",
			"portType",
			"portDirection",
			"numDigits",
			"prefixDN",
			"expectedDigits",
			"smdiPortNumber",
			"attendantDN",
			"ownerUserId",
			"mobilityUserId",
			"digestUserId",
			"userAssociatedWithLine",
			"securityProfile",
			"deviceMobilityMode",
			"builtInBridge",
			"callingPartyTransformation",
			"useCallingPartyTransformation",
			"reroutingCSS",
			"enableExtensionMobility",
			"primaryPhone",
			"requireDTMFReception",
			"useTRP",
			"enableCUMC",
			"sipProfile",
			"allowPresentationSharingUsingBFCP",
			"widebandHeadsetUIControl",
			"widebandHeadset",
			"cuetLevel",
			"disallowShakeToLock",
			"cucmCtiUserName",
			"cucmUseVoiceDialing",
			"cucmAddVoiceDialingNumberToFavorites",
			"vmUserName",
			"vmUnityServer",
			"vmMessageStoreUserName",
			"vmMessageStore",
			"ldapUseLDAPUserAuthentication",
			"ldapUserName",
			"ldapPassword",
			"ldapServer",
			"ldapUseSSL",
			"ldapSearchBase",
			"ldapFieldMapping",
			"ldapPhoto",
			"dialerEmergencyNumbers",
			"domainName",
			"wifiNetworks",
			"PreWifiNetworks",
			"ehookEnable",
			"ciscoSupportField",
			"disableSpeakerPhone",
			"disableSpeakerPhoneAndHeadset",
			"paramEditability",
			"countryCode",
			"iPhoneCountryCode",
			"cucmUseSipDigest",
			"cucmSipDigestUserName",
			"enableSipDigestAuthentication",
			"sipDigestUsername",
			"voiceDialingPhoneNumber",
			"cucmApplicationDialRulesURL",
			"vpnLaunchURL",
			"cucmDirectoryLookupRulesURL",
			"cucmGSMHandoffPreference",
			"idleURL",
			"idleTimeout",
			"enableDVOFeature",
			"dialViaOffice",
			"voicemailServerPri",
			"voicemailServerBkup",
			"presenceServerPri",
			"presenceServerType",
			"softkeyControl",
			"networkLocale",
			"roomName",
			"kemOneColumn",
			"enableCdpSwPort",
			"enableCdpPcPort",
			"multiUser",
			"emURL",
			"deviceUIProfile",
			"appInstallFromAndroidMarket",
			"enableCiscoUcmAppClient",
			"recordedStatus",
			"recordedStatusReason",
			"recordedIpAddress",
			"recordedAuditTime"
		], 
		"OXE call server Device": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"idCore",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"ciscoName",
			"description",
			"disableTime",
			"operationPending",
			"creationPending",
			"param1",
			"param2",
			"param3",
			"param4",
			"param5",
			"param6",
			"param7",
			"param8",
			"param9",
			"param10",
			"param11",
			"param12",
			"param13",
			"param14",
			"param15",
			"param16",
			"param17",
			"param18",
			"param19",
			"param20",
			"param21",
			"param22",
			"param23",
			"param24",
			"param25",
			"param26",
			"param27",
			"param28",
			"param29",
			"param30",
			"idCoreChild",
			"equipmentAddressRack",
			"equipmentAddressBoard",
			"equipmentAddressTerminal",
			"stationType",
			"softPhoneEmul",
			"internalKeyboard",
			"remoteExtensionNumber",
			"remoteExtensionDeactivation",
			"noeParamApplicationCosId",
			"noeParamPhoneCosId",
			"noeParamDefaultHomePage",
			"noeParamApplicationId",
			"setRole",
			"profileName",
			"keyModel",
			"publicNetworkCategoryId",
			"externalForwardingCategoryId",
			"facilityCategoryId",
			"connectionCategoryId",
			"meteringCategory",
			"robinetCategory",
			"teleservice",
			"multineAutomaticIncomingSeize",
			"multineAutomaticOutgoingSeize",
			"multineSelectiveFiltering",
			"delayOverflowForWaitingCall",
			"immediateOverflowForWaitingCall",
			"primaryLinePrio",
			"secondaryLinePrio",
			"addOnModule1",
			"addOnModule2",
			"addOnModule3",
			"renvoiPrincipal",
			"numeroRenvoiPrincipal",
			"renvoiSecond",
			"numeroRenvoiSecond",
			"nePasDeranger",
			"cadenas",
			"busyOnCamp",
			"overflowOnAssociate",
			"overflowBusy",
			"consistentWithIdentity",
			"acdType",
			"ghostZ",
			"ghostZFeature",
			"terminalCLIP",
			"analogType",
			"a4980"
		], 
		"AVAYA Device": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"idCore",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"ciscoName",
			"description",
			"disableTime",
			"operationPending",
			"creationPending",
			"param1",
			"param2",
			"param3",
			"param4",
			"param5",
			"param6",
			"param7",
			"param8",
			"param9",
			"param10",
			"param11",
			"param12",
			"param13",
			"param14",
			"param15",
			"param16",
			"param17",
			"param18",
			"param19",
			"param20",
			"param21",
			"param22",
			"param23",
			"param24",
			"param25",
			"param26",
			"param27",
			"param28",
			"param29",
			"param30",
			"idCoreChild",
			"model",
			"locationBuilding",
			"locationFloor",
			"locationRoom",
			"displayName",
			"nativeName",
			"keyModel",
			"activeStationRinging",
			"speakerphone",
			"personalizedRingingPattern",
			"messageLampExt",
			"muteButtonEnabled",
			"expansionModule",
			"extendedExpansionModule",
			"ipSoftphone",
			"ipVideoSoftphone",
			"customizableLabels",
			"perButtonRingControl",
			"bridgedCallAlerting",
			"autoSelectAnyIdleAppearance",
			"idleAppearancePreference",
			"bridgedIdleLinePreference",
			"restrictLastAppearance",
			"cpnSendCallingNumber",
			"selectLastUsedAppearance",
			"directIPAudioConnections",
			"confOnPrimaryAppearance",
			"bridgedOriginationRestriction",
			"timeOfDayLockTable",
			"lossGroup",
			"mediaComplexExt",
			"cdrPrivacy",
			"emuLoginAllowed",
			"multimediaEarlyAnswer",
			"callAppearanceDisplayFormat",
			"port",
			"voiceMailButton",
			"emergencyLocationExt",
			"sipTrunk"
		], 
		"Mitel Device": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"idCore",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"ciscoName",
			"description",
			"disableTime",
			"operationPending",
			"creationPending",
			"param1",
			"param2",
			"param3",
			"param4",
			"param5",
			"param6",
			"param7",
			"param8",
			"param9",
			"param10",
			"param11",
			"param12",
			"param13",
			"param14",
			"param15",
			"param16",
			"param17",
			"param18",
			"param19",
			"param20",
			"param21",
			"param22",
			"param23",
			"param24",
			"param25",
			"param26",
			"param27",
			"param28",
			"param29",
			"param30",
			"idCoreChild",
			"deviceType",
			"auxiliaryModule",
			"sipDeviceCapabilities",
			"cesid",
			"deviceId",
			"homeElement"
		], 
		"Mitel MasDevice": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"idCore",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"ciscoName",
			"description",
			"disableTime",
			"operationPending",
			"creationPending",
			"param1",
			"param2",
			"param3",
			"param4",
			"param5",
			"param6",
			"param7",
			"param8",
			"param9",
			"param10",
			"param11",
			"param12",
			"param13",
			"param14",
			"param15",
			"param16",
			"param17",
			"param18",
			"param19",
			"param20",
			"param21",
			"param22",
			"param23",
			"param24",
			"param25",
			"param26",
			"param27",
			"param28",
			"param29",
			"param30",
			"idCoreChild",
			"deviceType",
			"sipDeviceCapabilities",
			"cesid"
		], 
		"CCM9_0 call server Device": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"idCore",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"ciscoName",
			"description",
			"disableTime",
			"operationPending",
			"creationPending",
			"param1",
			"param2",
			"param3",
			"param4",
			"param5",
			"param6",
			"param7",
			"param8",
			"param9",
			"param10",
			"param11",
			"param12",
			"param13",
			"param14",
			"param15",
			"param16",
			"param17",
			"param18",
			"param19",
			"param20",
			"param21",
			"param22",
			"param23",
			"param24",
			"param25",
			"param26",
			"param27",
			"param28",
			"param29",
			"param30",
			"unicityEnabledChild",
			"idCoreChild",
			"oid",
			"css",
			"devicePool",
			"commonDeviceConfig",
			"model",
			"product",
			"phoneButtonTemplate",
			"softkeyTemplate",
			"commonPhoneProfile",
			"userHoldMohAudioSourceId",
			"networkHoldMohAudioSourceId",
			"protocol",
			"location",
			"mlppDomainId",
			"mlppIndication",
			"mlppPreemption",
			"dndRingSetting",
			"userLanguage",
			"featureControlPolicy",
			"settingsAccess",
			"voiceVlanAccess",
			"webAccess",
			"webAccessxAPI",
			"ciscoCamera",
			"videoCapability",
			"wifi",
			"rtcp",
			"pcPort",
			"displayOnTime",
			"displayOnDuration",
			"lldpOnSwPort",
			"lldpOnPcPort",
			"eapAuthentication",
			"presenceGroup",
			"subscribeCss",
			"aarCss",
			"aarNeighborhoodName",
			"mediaResourceGroupList",
			"lineDisplay",
			"asciilineDisplay",
			"lineLabel",
			"asciilineLabel",
			"lineRingSetting",
			"lineRingSettingActive",
			"lineRingSettingActiveIdlePickupAlert",
			"lineRingSettingIdleActivePickupAlert",
			"lineMaxNumCalls",
			"lineBusyTrigger",
			"lineCallInfoDisplayCallerName",
			"lineCallInfoDisplayCallerNumber",
			"lineCallInfoDisplayRedirectedNumber",
			"lineCallInfoDisplayDialedNumber",
			"services",
			"applicationUsers",
			"mobileIdentityEnabled",
			"mobileIdentityDestinationName",
			"mobileIdentityDestinationNumber",
			"mobileIdentityATSTimer",
			"mobileIdentityATLTimer",
			"mobileIdentityDBRTimer",
			"mobileIdentityProfileName",
			"mobileIdentityMobilePhoneEnabled",
			"mobileIdentityMobileConnectEnabled",
			"mobileIdentityTimeZone",
			"expansionModule1",
			"expansionModule2",
			"expansionModule3",
			"CAPFAuthModeHistory",
			"CAPFAuthStringHistory",
			"CAPFOperationHistory",
			"CAPFKeySizeHistory",
			"CAPFOperationDateHistory",
			"CAPFKeyOrderHistory",
			"CAPFEcKeySizeHistory",
			"lineRecordingFlag",
			"lineRecordingProfile",
			"lineMonitoringCss",
			"deviceKind",
			"vgDomainName",
			"numPort",
			"numSlot",
			"numSubUnit",
			"unattendedPort",
			"portType",
			"portDirection",
			"numDigits",
			"prefixDN",
			"expectedDigits",
			"smdiPortNumber",
			"attendantDN",
			"ownerUserId",
			"mobilityUserId",
			"digestUserId",
			"userAssociatedWithLine",
			"securityProfile",
			"callInfoPrivacyStatus",
			"deviceMobilityMode",
			"builtInBridge",
			"callingPartyTransformation",
			"useCallingPartyTransformation",
			"callingPartyTransformationCSSForInboundCalls",
			"useDevicePoolCallingPartyTransformationCSSForInboundCalls",
			"reroutingCSS",
			"enableExtensionMobility",
			"primaryPhone",
			"requireDTMFReception",
			"useTRP",
			"enableCUMC",
			"sipProfile",
			"allowPresentationSharingUsingBFCP",
			"widebandHeadsetUIControl",
			"widebandHeadset",
			"cuetLevel",
			"disallowShakeToLock",
			"cucmCtiUserName",
			"cucmUseVoiceDialing",
			"cucmAddVoiceDialingNumberToFavorites",
			"vmUserName",
			"vmUnityServer",
			"vmMessageStoreUserName",
			"vmMessageStore",
			"ldapUseLDAPUserAuthentication",
			"ldapUserName",
			"ldapPassword",
			"ldapServer",
			"ldapUseSSL",
			"ldapSearchBase",
			"ldapFieldMapping",
			"ldapPhoto",
			"dialerEmergencyNumbers",
			"domainName",
			"wifiNetworks",
			"PreWifiNetworks",
			"ehookEnable",
			"ciscoSupportField",
			"disableSpeakerPhone",
			"disableSpeakerPhoneAndHeadset",
			"paramEditability",
			"countryCode",
			"iPhoneCountryCode",
			"cucmUseSipDigest",
			"cucmSipDigestUserName",
			"enableSipDigestAuthentication",
			"sipDigestUsername",
			"voiceDialingPhoneNumber",
			"cucmApplicationDialRulesURL",
			"vpnLaunchURL",
			"cucmDirectoryLookupRulesURL",
			"cucmGSMHandoffPreference",
			"idleURL",
			"idleTimeout",
			"enableDVOFeature",
			"dialViaOffice",
			"voicemailServerPri",
			"voicemailServerBkup",
			"presenceServerPri",
			"presenceServerType",
			"softkeyControl",
			"informationUrl",
			"directoryUrl",
			"messagesUrl",
			"servicesUrl",
			"authenticationUrl",
			"proxyServerUrl",
			"secureAuthenticationUrl",
			"secureDirectoryUrl",
			"secureIdleUrl",
			"secureInformationUrl",
			"secureMessageUrl",
			"secureServicesUrl",
			"networkLocale",
			"roomName",
			"roomNamexAPI",
			"kemOneColumn",
			"enableCdpSwPort",
			"enableCdpPcPort",
			"deviceTrustedMode",
			"phoneLoadName",
			"multiUser",
			"emURL",
			"deviceUIProfile",
			"appInstallFromAndroidMarket",
			"enableCiscoUcmAppClient",
			"daysDisplayNotActive",
			"displayIdleTimeout",
			"displayOnWhenIncomingCall",
			"sshAccess",
			"sshAccessxAPI",
			"RingLocale",
			"lineMode",
			"allCallsOnPrimary",
			"bluetooth",
			"spanToPCPort",
			"lowerYourVoiceAlert",
			"phonebookServerUrl",
			"phonebookServerType",
			"adminUser",
			"adminPassword",
			"facilityServiceCallType1",
			"facilityServiceName1",
			"facilityServiceNumber1",
			"facilityServiceType1",
			"recordedStatus",
			"recordedStatusReason",
			"recordedIpAddress",
			"recordedActiveLoadID",
			"recordedAuditTime",
			"wirelessLANProfileGroup",
			"elinGroup",
			"dndEnable",
			"dndOption",
			"ringSettingIdleBlfAudibleAlert",
			"ringSettingBusyBlfAudibleAlert",
			"additionalField1",
			"additionalField2",
			"additionalField3",
			"additionalField4",
			"additionalField5",
			"enableActivationID",
			"activationCode",
			"activationCodeExpiry",
			"mraServiceDomain",
			"allowMraMode"
		],
		"CCM7_0 call server Line": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"idCore",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"directoryNumber",
			"ddiNumber",
			"numberParam0",
			"numberParam1",
			"numberParam2",
			"externalPhoneNumber",
			"dynamicServices",
			"description",
			"param1",
			"param2",
			"param3",
			"param4",
			"param5",
			"param6",
			"param7",
			"param8",
			"param9",
			"param10",
			"param11",
			"param12",
			"param13",
			"param14",
			"param15",
			"param16",
			"param17",
			"param18",
			"param19",
			"param20",
			"param21",
			"param22",
			"param23",
			"param24",
			"param25",
			"param26",
			"param27",
			"param28",
			"param29",
			"param30",
			"unicityEnabledChild",
			"idCoreChild",
			"directoryNumberChild",
			"presenceGroup",
			"alertingName",
			"asciialertingName",
			"css",
			"partition",
			"voiceMailProfile",
			"userHoldMohAudioSourceId",
			"networkHoldMohAudioSourceId",
			"autoAnswer",
			"aarNeighborhoodName",
			"aarDestinationMask",
			"aarKeepCallHistory",
			"aarVoiceMailEnable",
			"callPickupGroupName",
			"callForwardAllToVoiceMail",
			"callForwardAllCss",
			"callForwardAllSecondCss",
			"callForwardAllDest",
			"callForwardBusyToVoiceMail",
			"callForwardBusyCss",
			"callForwardBusyDest",
			"callForwardBusyIntToVoiceMail",
			"callForwardBusyIntCss",
			"callForwardBusyIntDest",
			"callForwardNoAnswerToVoiceMail",
			"callForwardNoAnswerCss",
			"callForwardNoAnswerDest",
			"callForwardNoAnswerDuration",
			"callForwardNoAnswerIntToVoiceMail",
			"callForwardNoAnswerIntCss",
			"callForwardNoAnswerIntDest",
			"callForwardNoAnswerIntDuration",
			"callForwardNoCoverageToVoiceMail",
			"callForwardNoCoverageCss",
			"callForwardNoCoverageDest",
			"callForwardNoCoverageIntToVoiceMail",
			"callForwardNoCoverageIntCss",
			"callForwardNoCoverageIntDest",
			"callForwardOnFailureToVoiceMail",
			"callForwardOnFailureCss",
			"callForwardOnFailureDest",
			"callForwardNotRegisteredToVoiceMail",
			"callForwardNotRegisteredCss",
			"callForwardNotRegisteredDest",
			"callForwardNotRegisteredIntToVoiceMail",
			"callForwardNotRegisteredIntCss",
			"callForwardNotRegisteredIntDest",
			"allowCTIControlFlag",
			"cssActivationPolicy"
		], 
		"OXE call server Line": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"idCore",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"directoryNumber",
			"ddiNumber",
			"numberParam0",
			"numberParam1",
			"numberParam2",
			"externalPhoneNumber",
			"dynamicServices",
			"description",
			"param1",
			"param2",
			"param3",
			"param4",
			"param5",
			"param6",
			"param7",
			"param8",
			"param9",
			"param10",
			"param11",
			"param12",
			"param13",
			"param14",
			"param15",
			"param16",
			"param17",
			"param18",
			"param19",
			"param20",
			"param21",
			"param22",
			"param23",
			"param24",
			"param25",
			"param26",
			"param27",
			"param28",
			"param29",
			"param30",
			"unicityEnabledChild",
			"idCoreChild",
			"directoryNumberChild",
			"multiKeyNumber",
			"associatedStationDN",
			"pickupGroupName",
			"voiceMailDN",
			"primaryMLADN"
		], 
		"AVAYA Line": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"idCore",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"directoryNumber",
			"ddiNumber",
			"numberParam0",
			"numberParam1",
			"numberParam2",
			"externalPhoneNumber",
			"dynamicServices",
			"description",
			"param1",
			"param2",
			"param3",
			"param4",
			"param5",
			"param6",
			"param7",
			"param8",
			"param9",
			"param10",
			"param11",
			"param12",
			"param13",
			"param14",
			"param15",
			"param16",
			"param17",
			"param18",
			"param19",
			"param20",
			"param21",
			"param22",
			"param23",
			"param24",
			"param25",
			"param26",
			"param27",
			"param28",
			"param29",
			"param30",
			"unicityEnabledChild",
			"idCoreChild",
			"directoryNumberChild",
			"callForwardAllExtStatus",
			"callForwardAllIntStatus",
			"callForwardBusyExtStatus",
			"callForwardBusyIntStatus",
			"callForwardNoAnswerExtStatus",
			"callForwardNoAnswerIntStatus",
			"callForwardAllExtDest",
			"callForwardAllIntDest",
			"callForwardBusyExtDest",
			"callForwardBusyIntDest",
			"callForwardNoAnswerExtDest",
			"callForwardNoAnswerIntDest",
			"autoAnswer",
			"multiKeyNumber",
			"coveragePath1",
			"coveragePath2",
			"huntToStation",
			"redirectNotification",
			"callWaitingIndication",
			"attCallWaitingIndication",
			"audibleMessageWaiting",
			"coverageAfterForwarding",
			"cor",
			"cos",
			"tn",
			"mwiServedUserType",
			"survivableNodeName"
		], 
		"Lync Line": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"idCore",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"directoryNumber",
			"ddiNumber",
			"numberParam0",
			"numberParam1",
			"numberParam2",
			"externalPhoneNumber",
			"dynamicServices",
			"description",
			"param1",
			"param2",
			"param3",
			"param4",
			"param5",
			"param6",
			"param7",
			"param8",
			"param9",
			"param10",
			"param11",
			"param12",
			"param13",
			"param14",
			"param15",
			"param16",
			"param17",
			"param18",
			"param19",
			"param20",
			"param21",
			"param22",
			"param23",
			"param24",
			"param25",
			"param26",
			"param27",
			"param28",
			"param29",
			"param30",
			"unicityEnabledChild",
			"idCoreChild",
			"directoryNumberChild"
		], 
		"Mitel Line": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"idCore",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"directoryNumber",
			"ddiNumber",
			"numberParam0",
			"numberParam1",
			"numberParam2",
			"externalPhoneNumber",
			"dynamicServices",
			"description",
			"param1",
			"param2",
			"param3",
			"param4",
			"param5",
			"param6",
			"param7",
			"param8",
			"param9",
			"param10",
			"param11",
			"param12",
			"param13",
			"param14",
			"param15",
			"param16",
			"param17",
			"param18",
			"param19",
			"param20",
			"param21",
			"param22",
			"param23",
			"param24",
			"param25",
			"param26",
			"param27",
			"param28",
			"param29",
			"param30",
			"unicityEnabledChild",
			"idCoreChild",
			"directoryNumberChild",
			"hotDeskUser",
			"acdEnabled",
			"lineType",
			"interconnectNumber",
			"tenantNumber",
			"lockDefaultConfiguration",
			"maxCallHistoryRecords",
			"homeElement",
			"secondaryElement",
			"userProtocol",
			"hduExternalDialingPrefix",
			"hduExternalNumber",
			"externalHduLicense",
			"htmlInfrastructureLicense",
			"htmlGuiApplication",
			"newPageApplication1",
			"newPageApplication2",
			"newPageApplication3",
			"notificationApplication1",
			"notificationApplication2",
			"notificationApplication3",
			"brandingApplication",
			"screenSaverApplication",
			"localOnlyDN",
			"serviceLevel",
			"primeName",
			"privacy",
			"clusterElementId",
			"pni",
			"guid",
			"interceptNumber",
			"cosDay",
			"cosNight1",
			"cosNight2",
			"corDay",
			"corNight1",
			"corNight2",
			"defaultAcctCode",
			"zoneId",
			"zoneAssignmentMethod",
			"keyModel",
			"callReroutingDay",
			"callReroutingNight1",
			"callReroutingNight2",
			"callReroutingDndType",
			"firstAlternative",
			"secondAlternative",
			"alwaysEnabled",
			"alwaysFwdDn",
			"busyIntEnabled",
			"busyIntFwdDn",
			"busyExtEnabled",
			"busyExtFwdDn",
			"noAnsIntEnabled",
			"noAnsIntFwdDn",
			"noAnsExtEnabled",
			"noAnsExtFwdDn"
		], 
		"Mitel MasLine": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"idCore",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"directoryNumber",
			"ddiNumber",
			"numberParam0",
			"numberParam1",
			"numberParam2",
			"externalPhoneNumber",
			"dynamicServices",
			"description",
			"param1",
			"param2",
			"param3",
			"param4",
			"param5",
			"param6",
			"param7",
			"param8",
			"param9",
			"param10",
			"param11",
			"param12",
			"param13",
			"param14",
			"param15",
			"param16",
			"param17",
			"param18",
			"param19",
			"param20",
			"param21",
			"param22",
			"param23",
			"param24",
			"param25",
			"param26",
			"param27",
			"param28",
			"param29",
			"param30",
			"unicityEnabledChild",
			"idCoreChild",
			"directoryNumberChild",
			"masServer",
			"type",
			"serviceTemplateId",
			"tenantNumber",
			"twinningNumber",
			"twinningPrefix",
			"enableTwinning",
			"privacy",
			"cosDay",
			"cosNight1",
			"cosNight2",
			"corDay",
			"corNight1",
			"corNight2",
			"zoneId",
			"keyModel",
			"didlist",
			"callReroutingDay",
			"callReroutingNight1",
			"callReroutingNight2",
			"callReroutingDndType",
			"firstAlternative",
			"secondAlternative",
			"voicemailPassCode",
			"voicemailName",
			"email"
		], 
		"CCM9_0 call server Line": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"idCore",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"directoryNumber",
			"ddiNumber",
			"numberParam0",
			"numberParam1",
			"numberParam2",
			"externalPhoneNumber",
			"dynamicServices",
			"description",
			"param1",
			"param2",
			"param3",
			"param4",
			"param5",
			"param6",
			"param7",
			"param8",
			"param9",
			"param10",
			"param11",
			"param12",
			"param13",
			"param14",
			"param15",
			"param16",
			"param17",
			"param18",
			"param19",
			"param20",
			"param21",
			"param22",
			"param23",
			"param24",
			"param25",
			"param26",
			"param27",
			"param28",
			"param29",
			"param30",
			"unicityEnabledChild",
			"idCoreChild",
			"directoryNumberChild",
			"urgentPriority",
			"presenceGroup",
			"alertingName",
			"asciialertingName",
			"css",
			"partition",
			"voiceMailProfile",
			"userHoldMohAudioSourceId",
			"networkHoldMohAudioSourceId",
			"autoAnswer",
			"aarNeighborhoodName",
			"aarDestinationMask",
			"aarKeepCallHistory",
			"aarVoiceMailEnable",
			"callPickupGroupName",
			"callForwardAllToVoiceMail",
			"callForwardAllCss",
			"callForwardAllSecondCss",
			"callForwardAllDest",
			"callForwardBusyToVoiceMail",
			"callForwardBusyCss",
			"callForwardBusyDest",
			"callForwardBusyIntToVoiceMail",
			"callForwardBusyIntCss",
			"callForwardBusyIntDest",
			"callForwardNoAnswerToVoiceMail",
			"callForwardNoAnswerCss",
			"callForwardNoAnswerDest",
			"callForwardNoAnswerDuration",
			"callForwardNoAnswerIntToVoiceMail",
			"callForwardNoAnswerIntCss",
			"callForwardNoAnswerIntDest",
			"callForwardNoAnswerIntDuration",
			"callForwardNoCoverageToVoiceMail",
			"callForwardNoCoverageCss",
			"callForwardNoCoverageDest",
			"callForwardNoCoverageIntToVoiceMail",
			"callForwardNoCoverageIntCss",
			"callForwardNoCoverageIntDest",
			"callForwardOnFailureToVoiceMail",
			"callForwardOnFailureCss",
			"callForwardOnFailureDest",
			"callForwardNotRegisteredToVoiceMail",
			"callForwardNotRegisteredCss",
			"callForwardNotRegisteredDest",
			"callForwardNotRegisteredIntToVoiceMail",
			"callForwardNotRegisteredIntCss",
			"callForwardNotRegisteredIntDest",
			"allowCTIControlFlag",
			"cssActivationPolicy",
			"entAltNumActive",
			"entAltNumMask",
			"entAltNumAddToLocalRoutePartition",
			"entAltNumRoutePartition",
			"entAltNumIsUrgent",
			"entAltNumAdvertiseGlobally",
			"e164AltNumActive",
			"e164AltNumMask",
			"e164AltNumAddToLocalRoutePartition",
			"e164AltNumRoutePartition",
			"e164AltNumIsUrgent",
			"e164AltNumAdvertiseGlobally",
			"externalCallControlProfile",
			"directoryUri1Uri",
			"directoryUri1Partition",
			"directoryUri1AdvertiseGloballyViaIls",
			"directoryUri1Primary",
			"directoryUri2Uri",
			"directoryUri2Partition",
			"directoryUri2AdvertiseGloballyViaIls",
			"directoryUri2Primary",
			"directoryUri3Uri",
			"directoryUri3Partition",
			"directoryUri3AdvertiseGloballyViaIls",
			"directoryUri3Primary",
			"directoryUri4Uri",
			"directoryUri4Partition",
			"directoryUri4AdvertiseGloballyViaIls",
			"directoryUri4Primary",
			"directoryUri5Uri",
			"directoryUri5Partition",
			"directoryUri5AdvertiseGloballyViaIls",
			"directoryUri5Primary",
			"parkMonForwardNoRetrieveVmEnabled",
			"parkMonForwardNoRetrieveDn",
			"parkMonForwardNoRetrieveCssName",
			"parkMonForwardNoRetrieveIntVmEnabled",
			"parkMonForwardNoRetrieveIntDn",
			"parkMonForwardNoRetrieveIntCssName",
			"parkMonReversionTimer",
			"hrDuration",
			"hrInterval",
			"partyEntranceTone",
			"additionalField1",
			"additionalField2",
			"additionalField3",
			"additionalField4",
			"additionalField5"
		],
		"exchange": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"identity",
			"userId",
			"mailboxDataBase",
			"retentionPolicy",
			"activeSyncMailboxPolicy",
			"addressBookPolicy",
			"umMailboxEnabled",
			"umMailboxPolicy",
			"pin",
			"extensions",
			"sipResourceIdentifier",
			"primarySmtpAddress",
			"linkedUser",
			"linkedMasterAccount"
		],
		"externalDirectory": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"pollerId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"status",
			"autofix",
			"exportstatus",
			"generatePhoneNumberType",
			"idCore",
			"nameProfil",
			"synchroMode",
			"userId",
			"firstName",
			"lastName",
			"password",
			"email",
			"userLanguage",
			"directoryNumber",
			"secondDirectoryNumber",
			"faxNumber",
			"department",
			"param1",
			"param2",
			"param3",
			"param4",
			"param5",
			"param6",
			"param7",
			"param8",
			"param9",
			"param10",
			"param11",
			"param12",
			"param13",
			"param14",
			"param15",
			"param16",
			"param17",
			"param18",
			"param19",
			"param20",
			"param21",
			"param22",
			"param23",
			"param24",
			"param25",
			"param26",
			"param27",
			"param28",
			"param29",
			"param30"
		],
		"ccmhuntlist": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"name",
			"description",
			"callManagerGroup",
			"enable",
			"voiceMailUsage"
		],
		"fr.niji.database.HuntListLineGroupLink": [
			"lg_ordre"
		],
		"ccmhuntpilot": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"directoryNumber",
			"ddiNumber",
			"displayedNumber",
			"numberParam0",
			"numberParam1",
			"numberParam2",
			"partition",
			"description",
			"blockEnable",
			"blockCause",
			"provideOutsideDialtone",
			"patternUrgency",
			"huntForwardNoAnswerUsePersonalPref",
			"huntForwardNoAnswerCss",
			"huntForwardNoAnswerDest",
			"huntForwardBusyUsePersonalPref",
			"huntForwardBusyCss",
			"huntForwardBusyDest",
			"huntForwardMaxHuntDuration",
			"callingPartyUsePhoneMask",
			"callingPartyTransformationMask",
			"callingPartyPrefixDigitsOut",
			"callingPartyLinePresentationBit",
			"callingPartyNamePresentationBit",
			"connectedPartyLinePresentationBit",
			"connectedPartyNamePresentationBit",
			"calledPartyDigitDiscard",
			"calledPartyTransformationMask",
			"calledPartyPrefixDigitsOut",
			"aarNeighborhoodName",
			"aarExternalNumberMask",
			"callPickupGroupName",
			"alertingName",
			"asciialertingName",
			"queueingEnabled",
			"networkHoldMohAudioSourceID",
			"queueFullMaxCallersInQueue",
			"queueFullCallControl",
			"queueFullDestination",
			"queueFullCSS",
			"maxWaitTimeInQueue",
			"maxWaitTimeCallControl",
			"maxWaitTimeDestination",
			"maxWaitTimeCSS",
			"noAgentCallControl",
			"noAgentDestination",
			"noAgentCSS"
		],

		"ics": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"login",
			"salutation",
			"firstName",
			"lastName",
			"externalLogin",
			"language",
			"timeZone",
			"department",
			"dialingRuleDisplayName",
			"presenceID",
			"misc1",
			"misc2",
			"misc3",
			"password",
			"officePhone",
			"fax",
			"mobile",
			"pager",
			"dect",
			"tandem",
			"colleague",
			"attendant",
			"sipUri",
			"email",
			"wsXmlTelephonyAllowed",
			"wsOneNumberAllowed",
			"wsMessagingAllowed",
			"wsTeamworkAllowed",
			"wsTelephonyAllowed",
			"myICDeviceRights",
			"oneNumberDeviceRights",
			"messagingDeviceRights",
			"teamworkDeviceRights",
			"telephonyDeviceRights",
			"voicemailId",
			"voicemailType",
			"voicemailLogin",
			"voicemailPassword",
			"voicemailDisplayName",
			"voicemailSystem",
			"voicemailProfile",
			"voicemailIMAPLogin",
			"voicemailIMAPPassword",
			"voicemailGreeting",
			"voicemail24HourMode",
			"voicemailAddressingByName",
			"voicemailAutomaticReading",
			"voicemailDeleteConfirmFlag",
			"voicemailAssistantMenu",
			"voicemailAnswerOnly",
			"voicemailFaxNumber",
			"tuiPassword",
			"tuiLanguage",
			"tuiForceEnrollmentActivated",
			"tuiSkipPasswordOnDirectCall",
			"tuiDtmfMapping",
			"tuiExpertMode",
			"tuiAddressingByName"
		],
		"fr.niji.database.ICSDirectoryNumbers": [
			"OTSMain",
			"OTSAutoAnswer",
			"OTSNomadicRemoteConnection",
			"OTSCellular",
			"OTSMultimedia",
			"OTSDynamicContext",
			"OTSStaticContext"
		],

		"KURMISERVER": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"identifier",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"description",
			"address",
			"heartBeatTime",
			"shutdownRequested",
			"disableSunrise"
		],

		"ldap": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"userId",
			"firstName",
			"lastName",
			"commonName",
			"password",
			"pwdNotRequired",
			"pwdNeverExpires",
			"deactivateUser",
			"email",
			"directoryNumber",
			"secondDirectoryNumber",
			"faxNumber",
			"param1",
			"param2",
			"param3",
			"param4",
			"param5",
			"param6",
			"param7",
			"param8",
			"param9",
			"param10",
			"param11",
			"param12",
			"param13",
			"param14",
			"param15",
			"param16",
			"param17",
			"param18",
			"param19",
			"param20",
			"param21",
			"param22",
			"param23",
			"param24",
			"param25",
			"param26",
			"param27",
			"param28",
			"param29",
			"param30"
		],


		"linegroup": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"name",
			"solicitationTime",
			"distributionType",
			"huntAlgorithmNoAnswer",
			"autoLogOffHunt",
			"huntAlgorithmBusy",
			"huntAlgorithmNotAvailable",
			"otherMembers"
		],
		"fr.niji.database.LineGroupDirectoryNumbers": [
			"ordre"
		],
		"fr.niji.database.ListCCMDevices": [
			"externalPhoneNumberMask",
			"mwiPolicy",
			"recordingMediaSource",
			"lineDisplay",
			"asciilineDisplay",
			"lineLabel",
			"asciilineLabel",
			"lineRingSetting",
			"lineRingSettingActive",
			"lineRingSettingActiveIdlePickupAlert",
			"lineRingSettingIdleActivePickupAlert",
			"lineMaxNumCalls",
			"lineBusyTrigger",
			"lineCallInfoDisplayCallerName",
			"lineCallInfoDisplayCallerNumber",
			"lineCallInfoDisplayRedirectedNumber",
			"lineCallInfoDisplayDialedNumber",
			"userAssociatedWithLine",
			"lineRecordingFlag",
			"lineRecordingProfile",
			"lineMonitoringCss",
			"missedCallLogging"
		],
		"fr.niji.database.ListCCMDirectoryNumbers": [
			"externalPhoneNumberMask",
			"mwiPolicy",
			"recordingMediaSource",
			"lineDisplay",
			"asciilineDisplay",
			"lineLabel",
			"asciilineLabel",
			"lineRingSetting",
			"lineRingSettingActive",
			"lineRingSettingActiveIdlePickupAlert",
			"lineRingSettingIdleActivePickupAlert",
			"lineMaxNumCalls",
			"lineBusyTrigger",
			"lineCallInfoDisplayCallerName",
			"lineCallInfoDisplayCallerNumber",
			"lineCallInfoDisplayRedirectedNumber",
			"lineCallInfoDisplayDialedNumber",
			"userAssociatedWithLine",
			"lineRecordingFlag",
			"lineRecordingProfile",
			"lineMonitoringCss",
			"missedCallLogging"
		],

		"fr.niji.database.ListDevices": [
			"extensionPresent"
		],
		"fr.niji.database.ListDeviceUser": [
			"extInfo"
		],
		"fr.niji.database.ListDirectoryNumbers": [
			"extensionPresent"
		],








		"LyncClient": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"identity",
			"userId",
			"pinCode",
			"name",
			"voicePolicy",
			"voicePolicyDefault",
			"voicePolicyAllowCallForwarding",
			"voicePolicyAllowPSTNReRouting",
			"voicePolicyAllowSimulRing",
			"voicePolicyEnableBWPolicyOverride",
			"voicePolicyEnableCallPark",
			"voicePolicyEnableCallTransfer",
			"voicePolicyEnableDelegation",
			"voicePolicyEnableMaliciousCallTracing",
			"voicePolicyEnableTeamCall",
			"voicePolicyEnableVoicemailEscapeTimer",
			"voicePolicyPreventPSTNTollBypass",
			"voicePolicyPSTNVoicemailEscapeTimer",
			"voicePolicyPstnUsages",
			"voicePolicyCallForwardingSimulRingUsageType",
			"voicePolicyCallForwardingSimulRingUsages",
			"conferencingPolicy",
			"locationPolicy",
			"clientPolicy",
			"clientVersionPolicy",
			"archivingPolicy",
			"pinPolicy",
			"externalAccessPolicy",
			"dialPlan",
			"registrarPool",
			"sipAddress",
			"sipDomain",
			"lineURI",
			"telephony",
			"lineServerURI",
			"presencePolicy",
			"mobilityPolicy",
			"persistentChatPolicy",
			"hostedVoiceMail",
			"hostedVoiceMailPolicy"
		],

		"LyncGroup": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"identity",
			"name",
			"description",
			"participationPolicy",
			"agentAlertTime",
			"routingMethod",
			"agents",
			"distributionGroupAddress"
		],
		"fr.niji.database.LyncGroupLyncClient": [
			"ordre"
		],
		"LyncQueue": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"identity",
			"name",
			"description",
			"timeOut",
			"timeOutThreshold",
			"timeOutAction",
			"timeOutUri",
			"overflow",
			"timeOutQueueName",
			"overflowThreshold",
			"overflowCandidate",
			"overflowAction",
			"overflowUri",
			"overflowQueueName"
		],
		"fr.niji.database.LyncQueueLyncGroup": [
			"ordre"
		],
		"LyncWorkflow": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"identity",
			"primaryUri",
			"name",
			"lineUri",
			"displayNumber",
			"description",
			"enabled",
			"enabledForFederation",
			"anonymous",
			"language",
			"welcomeMessage",
			"welcomePrompt",
			"nonBusinessMessage",
			"nonBusinessPrompt",
			"nonBusinessAction",
			"nonBusinessSipAddress",
			"timeZone",
			"businessHoursPlanning"
		],

		"meetingplace": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"firstName",
			"lastName",
			"email",
			"profileID",
			"userID",
			"password",
			"profilePassword",
			"language",
			"outdialPhone",
			"alternatePhone",
			"billCode",
			"isSyncWithAD"
		],


		"mitelhuntgroup": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"extension",
			"ddiNumber",
			"displayedNumber",
			"numberParam0",
			"numberParam1",
			"numberParam2",
			"mode",
			"name",
			"description",
			"firstRAD",
			"secondRAD",
			"nightRAD",
			"priority",
			"firstThreshold",
			"secondThreshold",
			"alertDeviceDN",
			"type",
			"phaseTimerRing",
			"cosDay",
			"cosNight1",
			"cosNight2",
			"homeElement",
			"secondaryElement",
			"serviceId",
			"localOnlyDN",
			"primeName",
			"privacy",
			"clusterElementId",
			"department",
			"location",
			"pni",
			"guid",
			"masServer"
		],

		"mitelipconsole": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"homeElement",
			"secondaryElement",
			"deviceId",
			"deviceType",
			"extension",
			"ddiNumber",
			"displayedNumber",
			"numberParam0",
			"numberParam1",
			"numberParam2",
			"localOnlyDN",
			"softkeyAppearance",
			"interconnectNumber",
			"language",
			"macaddress",
			"tenantNumber",
			"interceptNumber",
			"cosDay",
			"cosNight1",
			"cosNight2",
			"corDay",
			"corNight1",
			"corNight2",
			"defaultAcctCode",
			"zoneId",
			"zoneAssignmentMethod",
			"sipDeviceCapabilities",
			"name",
			"primeName",
			"privacy",
			"clusterElementId",
			"department",
			"location",
			"pni",
			"guid",
			"callReroutingDay",
			"callReroutingNight1",
			"callReroutingNight2",
			"callReroutingDndType",
			"firstAlternative",
			"secondAlternative",
			"cesid"
		],
		"mitelmasipdevice": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"firstname",
			"lastname",
			"directoryname",
			"extension",
			"masServer",
			"serviceTemplateId",
			"pin",
			"deviceType",
			"cesid",
			"macaddress",
			"tenantNumber",
			"type",
			"sipDeviceCapabilities",
			"department",
			"location",
			"privacy",
			"zoneId",
			"cosDay",
			"cosNight1",
			"cosNight2",
			"corDay",
			"corNight1",
			"corNight2",
			"ddiNumber",
			"displayedNumber",
			"numberParam0",
			"numberParam1",
			"numberParam2",
			"callReroutingDay",
			"callReroutingNight1",
			"callReroutingNight2",
			"callReroutingDndType",
			"firstAlternative",
			"secondAlternative"
		],
		"mitelpersonalringgroup": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"extension",
			"ddiNumber",
			"displayedNumber",
			"numberParam0",
			"numberParam1",
			"numberParam2",
			"name",
			"description",
			"oneBusyAllBusy",
			"localOnlyDN",
			"homeElement",
			"secondaryElement",
			"masServer"
		],

		"mitelpickupgroup": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"name",
			"pickupGroup",
			"autoPickup",
			"description",
			"clusterEnabled",
			"neGuidList",
			"neNamesList",
			"homeElement",
			"masServer"
		],

		"mitelringgroup": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"extension",
			"ddiNumber",
			"displayedNumber",
			"numberParam0",
			"numberParam1",
			"numberParam2",
			"name",
			"description",
			"mode",
			"callQueuedTimer",
			"callRingingTimer",
			"cascadeRingTimer",
			"overflowPoint",
			"cosDay",
			"cosNight1",
			"cosNight2",
			"homeElement",
			"secondaryElement",
			"masServer",
			"serviceId",
			"localOnlyDN",
			"primeName",
			"privacy",
			"clusterElementId",
			"department",
			"location",
			"pni",
			"guid"
		],

		"mitelspeedcall": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"homeElement",
			"speedCallNumber",
			"actualNumber",
			"overridesTollControl",
			"type",
			"withContact",
			"name",
			"clusterElementId",
			"department",
			"location",
			"ddiNumber",
			"displayedNumber",
			"numberParam0",
			"numberParam1",
			"numberParam2"
		],
		"MODULARMESSAGING": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"sn",
			"givenName",
			"password",
			"mailboxNumber",
			"numericaddress",
			"umVoiceMailboxExtension",
			"classNo",
			"community",
			"mail",
			"telephoneNumber",
			"cn",
			"asciiName",
			"umBackupOperatorMailbox",
			"ummessageorderingflags",
			"umintercompaging",
			"umvoicemailenabled",
			"secondExt",
			"misc1",
			"misc2",
			"misc3",
			"misc4",
			"umTUIPreferredLocale",
			"avumpersonaloperatorscheduleguid"
		],
		"fr.niji.database.ModularMessagingDirectoryNumbers": [
			"ordre"
		],

		"CCM7_0 call server User": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"idCore",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"userId",
			"firstName",
			"lastName",
			"password",
			"pin",
			"email",
			"userLanguage",
			"param1",
			"param2",
			"param3",
			"param4",
			"param5",
			"param6",
			"param7",
			"param8",
			"param9",
			"param10",
			"param11",
			"param12",
			"param13",
			"param14",
			"param15",
			"param16",
			"param17",
			"param18",
			"param19",
			"param20",
			"param21",
			"param22",
			"param23",
			"param24",
			"param25",
			"param26",
			"param27",
			"param28",
			"param29",
			"param30",
			"unicityEnabledChild",
			"idCoreChild",
			"oid",
			"telephoneNumber",
			"department",
			"nameDialing",
			"presenceGroup",
			"enableCti",
			"enableEmcc",
			"enableMobility",
			"enableMobilityVoiceAccess",
			"deviceProfile",
			"deviceProfileName",
			"enableCTIControlledDeviceProfile",
			"deviceProfileDescription",
			"defaultProfileName",
			"devicePhoneButtonTemplate",
			"deviceSoftkeyTemplate",
			"deviceProduct",
			"deviceModel",
			"deviceProtocol",
			"deviceDndRingSetting",
			"deviceEmccCss",
			"deviceFeatureControlPolicy",
			"lineDisplay",
			"asciilineDisplay",
			"lineLabel",
			"asciilineLabel",
			"lineRingSetting",
			"lineRingSettingActive",
			"lineRingSettingActiveIdlePickupAlert",
			"lineRingSettingIdleActivePickupAlert",
			"lineMaxNumCalls",
			"lineBusyTrigger",
			"lineCallInfoDisplayCallerName",
			"lineCallInfoDisplayCallerNumber",
			"lineCallInfoDisplayRedirectedNumber",
			"lineCallInfoDisplayDialedNumber",
			"lineRecordingFlag",
			"lineRecordingProfile",
			"lineMonitoringCss",
			"deviceUserHoldMohAudioSourceId",
			"deviceServices",
			"deviceApplicationUsers",
			"deviceExpansionModule1",
			"deviceExpansionModule2",
			"deviceExpansionModule3",
			"permissionInformation",
			"subscribeCss",
			"enableCUPS",
			"enableCUPC",
			"primaryExtDN",
			"primaryExtPartition",
			"IPCCExtDN",
			"IPCCExtPartition",
			"digestCredential",
			"userAssociatedWithLine",
			"primaryUserDevice",
			"remoteDestinationLimit",
			"middleName",
			"maxDeskPickupWaitTime"
		], 
		"OXE call server User": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"idCore",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"userId",
			"firstName",
			"lastName",
			"password",
			"pin",
			"email",
			"userLanguage",
			"param1",
			"param2",
			"param3",
			"param4",
			"param5",
			"param6",
			"param7",
			"param8",
			"param9",
			"param10",
			"param11",
			"param12",
			"param13",
			"param14",
			"param15",
			"param16",
			"param17",
			"param18",
			"param19",
			"param20",
			"param21",
			"param22",
			"param23",
			"param24",
			"param25",
			"param26",
			"param27",
			"param28",
			"param29",
			"param30",
			"idCoreChild",
			"entityNumber",
			"costCenterId",
			"callByNameMiniMessagingRight",
			"validForCallByName",
			"usePrivateCallingNumber",
			"privateCallingNumber"
		], 
		"AVAYA User": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"idCore",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"userId",
			"firstName",
			"lastName",
			"password",
			"pin",
			"email",
			"userLanguage",
			"param1",
			"param2",
			"param3",
			"param4",
			"param5",
			"param6",
			"param7",
			"param8",
			"param9",
			"param10",
			"param11",
			"param12",
			"param13",
			"param14",
			"param15",
			"param16",
			"param17",
			"param18",
			"param19",
			"param20",
			"param21",
			"param22",
			"param23",
			"param24",
			"param25",
			"param26",
			"param27",
			"param28",
			"param29",
			"param30",
			"idCoreChild",
			"abbreviatedDialingListType1",
			"abbreviatedDialingListType2",
			"abbreviatedDialingListType3",
			"abbreviatedDialingList1",
			"abbreviatedDialingList2",
			"abbreviatedDialingList3"
		], 
		"Lync User": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"idCore",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"userId",
			"firstName",
			"lastName",
			"password",
			"pin",
			"email",
			"userLanguage",
			"param1",
			"param2",
			"param3",
			"param4",
			"param5",
			"param6",
			"param7",
			"param8",
			"param9",
			"param10",
			"param11",
			"param12",
			"param13",
			"param14",
			"param15",
			"param16",
			"param17",
			"param18",
			"param19",
			"param20",
			"param21",
			"param22",
			"param23",
			"param24",
			"param25",
			"param26",
			"param27",
			"param28",
			"param29",
			"param30",
			"idCoreChild",
			"commonName",
			"displayName",
			"ou"
		], 
		"Mitel User": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"idCore",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"userId",
			"firstName",
			"lastName",
			"password",
			"pin",
			"email",
			"userLanguage",
			"param1",
			"param2",
			"param3",
			"param4",
			"param5",
			"param6",
			"param7",
			"param8",
			"param9",
			"param10",
			"param11",
			"param12",
			"param13",
			"param14",
			"param15",
			"param16",
			"param17",
			"param18",
			"param19",
			"param20",
			"param21",
			"param22",
			"param23",
			"param24",
			"param25",
			"param26",
			"param27",
			"param28",
			"param29",
			"param30",
			"unicityEnabledChild",
			"idCoreChild",
			"loginId",
			"department",
			"location",
			"mitelUserId",
			"userProfileLanguage",
			"desktop",
			"groupAdmin",
			"application",
			"systemAdmin",
			"systemAdminPolicyName",
			"homeElement"
		], 
		"Mitel MasUser": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"idCore",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"userId",
			"firstName",
			"lastName",
			"password",
			"pin",
			"email",
			"userLanguage",
			"param1",
			"param2",
			"param3",
			"param4",
			"param5",
			"param6",
			"param7",
			"param8",
			"param9",
			"param10",
			"param11",
			"param12",
			"param13",
			"param14",
			"param15",
			"param16",
			"param17",
			"param18",
			"param19",
			"param20",
			"param21",
			"param22",
			"param23",
			"param24",
			"param25",
			"param26",
			"param27",
			"param28",
			"param29",
			"param30",
			"idCoreChild",
			"department",
			"location",
			"directoryName",
			"bundle",
			"oriaInternalId"
		], 
		"CCM9_0 call server User": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"idCore",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"userId",
			"firstName",
			"lastName",
			"password",
			"pin",
			"email",
			"userLanguage",
			"param1",
			"param2",
			"param3",
			"param4",
			"param5",
			"param6",
			"param7",
			"param8",
			"param9",
			"param10",
			"param11",
			"param12",
			"param13",
			"param14",
			"param15",
			"param16",
			"param17",
			"param18",
			"param19",
			"param20",
			"param21",
			"param22",
			"param23",
			"param24",
			"param25",
			"param26",
			"param27",
			"param28",
			"param29",
			"param30",
			"unicityEnabledChild",
			"idCoreChild",
			"oid",
			"telephoneNumber",
			"department",
			"nameDialing",
			"presenceGroup",
			"enableCti",
			"enableEmcc",
			"enableMobility",
			"enableMobilityVoiceAccess",
			"deviceProfile",
			"deviceProfileName",
			"enableCTIControlledDeviceProfile",
			"deviceProfileDescription",
			"defaultProfileName",
			"devicePhoneButtonTemplate",
			"deviceSoftkeyTemplate",
			"deviceCallInfoPrivacyStatus",
			"deviceProduct",
			"deviceModel",
			"deviceProtocol",
			"deviceDndRingSetting",
			"deviceDndEnable",
			"deviceDndOption",
			"deviceEmccCss",
			"deviceFeatureControlPolicy",
			"lineDisplay",
			"asciilineDisplay",
			"lineLabel",
			"asciilineLabel",
			"lineRingSetting",
			"lineRingSettingActive",
			"lineRingSettingActiveIdlePickupAlert",
			"lineRingSettingIdleActivePickupAlert",
			"lineMaxNumCalls",
			"lineBusyTrigger",
			"lineCallInfoDisplayCallerName",
			"lineCallInfoDisplayCallerNumber",
			"lineCallInfoDisplayRedirectedNumber",
			"lineCallInfoDisplayDialedNumber",
			"lineRecordingFlag",
			"lineRecordingProfile",
			"lineMonitoringCss",
			"deviceUserHoldMohAudioSourceId",
			"deviceServices",
			"deviceApplicationUsers",
			"deviceExpansionModule1",
			"deviceExpansionModule2",
			"deviceExpansionModule3",
			"permissionInformation",
			"subscribeCss",
			"primaryExtDN",
			"primaryExtPartition",
			"IPCCExtDN",
			"IPCCExtPartition",
			"digestCredential",
			"userAssociatedWithLine",
			"primaryUserDevice",
			"remoteDestinationLimit",
			"middleName",
			"homeCluster",
			"imAndPresenceEnable",
			"enableCalendarPresence",
			"serviceProfile",
			"maxDeskPickupWaitTime",
			"directoryUri",
			"displayName",
			"enableConferenceNow",
			"conferenceNowAccessCode",
			"selfServiceUserId",
			"userProfile",
			"deviceMlppDomainId",
			"deviceMlppIndication",
			"deviceMlppPreemption",
			"additionalField1",
			"additionalField2",
			"additionalField3",
			"additionalField4",
			"additionalField5"
		],
		"NetwiseCMG": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"recordId",
			"listName",
			"searchName",
			"directoryName",
			"classExt",
			"classCordless",
			"messageWait",
			"voicemailCon",
			"secretary",
			"searchOffice",
			"telno",
			"telnopri",
			"pbxId",
			"showList",
			"custGroup",
			"lastName",
			"firstName",
			"depPri",
			"icp1",
			"icp2",
			"cordless",
			"misc1",
			"misc2",
			"misc3",
			"misc4",
			"password",
			"fixedPhoneExists",
			"misc5",
			"misc6",
			"misc7",
			"misc8",
			"misc9",
			"misc10",
			"misc11",
			"misc12",
			"misc13",
			"misc14",
			"misc15",
			"misc16",
			"misc17",
			"misc18",
			"subjects",
			"orgUnits",
			"msgSys1",
			"msgId1",
			"msgSecret1",
			"msgSys2",
			"msgId2",
			"msgSecret2",
			"msgSys3",
			"msgId3",
			"msgSecret3",
			"msgSys4",
			"msgId4",
			"msgSecret4",
			"phoneticFirstName",
			"phoneticLastName"
		],

		"numberrange": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"overlap",
			"operationPending",
			"creationPending",
			"firstNumber",
			"lastNumber",
			"domainName",
			"slot",
			"unit",
			"firstPort",
			"lastPort",
			"likePattern",
			"category",
			"generationAlgorithm",
			"externalNumStatus",
			"externalNum",
			"description",
			"displayedNumberHasList",
			"otherDisplayableNumbers",
			"displayedNumberDefaultChoice",
			"ddiNumStatus",
			"ddiNum",
			"ddiNumHasList",
			"ddiNumList",
			"ddiNumDefaultChoice",
			"custom0NumStatus",
			"custom0Num",
			"custom0NumHasList",
			"custom0NumList",
			"custom0NumDefaultChoice",
			"custom1NumStatus",
			"custom1Num",
			"custom1NumHasList",
			"custom1NumList",
			"custom1NumDefaultChoice",
			"custom2NumStatus",
			"custom2Num",
			"custom2NumHasList",
			"custom2NumList",
			"custom2NumDefaultChoice"
		],
		"fr.niji.database.NumberRangeGeneratedNumber": [
			"idEcosystem",
			"addressKind",
			"directoryNumber",
			"departmentPath",
			"nbInstance",
			"generationDate",
			"releaseDate"
		],
		"onexmobile": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"userID",
			"pbxUserName",
			"firstName",
			"lastName",
			"ext",
			"cos"
		],

		"OXEFREEDESKTOP": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"creationStatus",
			"directoryNumber",
			"user",
			"mobileRight",
			"sharedRight",
			"protectedRight",
			"autoLogonRight",
			"networkRight"
		],

		"oxelinegroup": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"directoryNumber",
			"ddiNumber",
			"displayedNumber",
			"numberParam0",
			"numberParam1",
			"numberParam2",
			"directoryName",
			"pbxType",
			"cyclicSearchMode",
			"releaseAfterTimedDelay",
			"overflowDirectoryNumber",
			"authorizedPercentOfWaitingCalls",
			"connectionCategoryId",
			"publicNetworkCategoryId",
			"robinetCategory",
			"withdrawalRight",
			"entityNumber",
			"priorityGroup",
			"routingFlag",
			"prioPreempt",
			"gfuListNumber",
			"gfuIncomingAccess",
			"mcduMevo",
			"mevoPassword",
			"mevoLangue",
			"rightToPickupPrivateCall",
			"callPickupByExternal",
			"oneCallByPbx",
			"usingSetCategory",
			"clip4620"
		],
		"fr.niji.database.OxeLineGroupDirectoryNumbers": [
			"ordre"
		],
		"oxemanagerset": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"name"
		],
		"fr.niji.database.OXEManagerSetUsers": [
			"role",
			"screeningKeys",
			"unscreeningKeys",
			"absentSecretary",
			"screeningSupervision",
			"bossMail",
			"managerDedicatedDN",
			"managerDedicatedKeys"
		],
		"oxeTandem": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"name",
			"description",
			"partialOcc",
			"ringingInPartialOcc",
			"specificSupervision"
		],
		"fr.niji.database.OxeTandemDirectoryNumbers": [
			"role"
		],
		"phonEXONE": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"userId",
			"directoryNumber",
			"firstName",
			"lastName",
			"param1",
			"param2",
			"param3",
			"param4",
			"param5",
			"param6",
			"param7",
			"param8",
			"param9",
			"param10",
			"param11",
			"param12",
			"param13",
			"param14",
			"param15",
			"param16",
			"param17",
			"param18",
			"param19",
			"param20",
			"param21",
			"param22",
			"param23",
			"param24",
			"param25",
			"param26",
			"param27",
			"param28",
			"param29",
			"param30"
		],
		"pickupgroup": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"name",
			"number",
			"ddiNumber",
			"displayedNumber",
			"numberParam0",
			"numberParam1",
			"numberParam2",
			"description",
			"partition",
			"notification",
			"timer",
			"callingPartyInfo",
			"calledPartyInfo"
		],



		"remoteDestinationProfile": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"uid",
			"name",
			"description",
			"css",
			"devicePool",
			"userHoldMohAudioSourceId",
			"networkHoldMohAudioSourceId",
			"rerouteCallingSearchSpace",
			"cgpnTransformationCSS",
			"ignorePresentationIndicators",
			"callInfoPrivacyStatus",
			"useDevicePoolCgpnTransformCSS",
			"userLocale",
			"dndStatus",
			"dndOption"
		],
		"fr.niji.database.RemoteDestinationProfileLine": [
			"lineDisplay",
			"asciilineDisplay",
			"externalPhoneNumberMask",
			"lineMaxNumCalls",
			"lineBusyTrigger",
			"lineCallInfoDisplayCallerName",
			"lineCallInfoDisplayCallerNumber",
			"lineCallInfoDisplayRedirectedNumber",
			"lineCallInfoDisplayDialedNumber",
			"userAssociatedWithLine"
		],

		"reservednumberrange": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"firstNumber",
			"lastNumber",
			"domainName",
			"slot",
			"unit",
			"firstPort",
			"lastPort",
			"description"
		],
		"salesforce": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"salesforceId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"userName",
			"password",
			"firstName",
			"lastName",
			"email",
			"alias",
			"active",
			"communityNickname",
			"userRole",
			"userLicense",
			"userProfile",
			"timeZoneSidKey",
			"localeSidKey",
			"emailEncodingKey",
			"languageLocaleKey",
			"userPermissionsCallCenterAutoLogin",
			"userPermissionsMarketingUser",
			"userPermissionsOfflineUser",
			"triggerUserEmail",
			"opportunities",
			"accounts"
		],

		"SBC": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"number",
			"routeTemplate",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending"
		],
		"fr.niji.database.SBCCustomField": [
			"customFieldValue"
		],


		"Selfcare": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"userId",
			"password",
			"language",
			"email",
			"selfcareProfil",
			"services",
			"disableTime",
			"operationPending",
			"creationPending",
			"searchFilter",
			"dialProfile",
			"escapeCode",
			"preferences"
		],
		"service": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"identifier",
			"script",
			"internal1",
			"internal2",
			"internal3",
			"internal4",
			"internal5",
			"internal6",
			"internal7",
			"internal8",
			"resources",
			"links",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending"
		],
		"fr.niji.database.ServiceDevices": [
			"role"
		],
		"fr.niji.database.ServiceDirectoryNumbers": [
			"role"
		],
		"fr.niji.database.ServiceOwnedComponent": [
			"serviceId"
		],
		"fr.niji.database.ServiceOwnedLink": [
			"serviceId"
		],
		"fr.niji.database.ServiceUsers": [
			"role"
		],
		"siu": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"siuId",
			"login",
			"firstName",
			"lastName",
			"siret",
			"ico01",
			"email",
			"company",
			"phoneNumber",
			"password"
		],

		"fr.niji.database.SnapwarePMProfiles": [
			"profileName",
			"description",
			"active",
			"utilization",
			"application",
			"timeConfiguration",
			"timeConfigurationPeriod",
			"timeConfigurationCyclic",
			"indexProfile",
			"snapwareComponentId"
		],
		"fr.niji.database.SnapwarePMRules": [
			"caller",
			"target",
			"compareCallerNumber",
			"forwardingType",
			"forwardingTypeTimeout",
			"indexRule"
		],
		"snapware5_1": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"userId",
			"directoryNumber",
			"fullPhone",
			"fullName",
			"email",
			"importNotDeleteUser",
			"snapwareType",
			"monitorGrp",
			"partnerGrp",
			"profileGrp"
		],
		"tango": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"lastName",
			"firstName",
			"deskNumber",
			"mobileNumber",
			"mdnCountryName",
			"pbxName",
			"serviceProfileName",
			"mpPermisionName",
			"homeTimeZoneName",
			"dialPlanName",
			"sipAddress",
			"lineGroupName",
			"carrierName",
			"did",
			"didCountryName",
			"routingRuleSetName",
			"screeningRuleSetName",
			"mobileAssistantPassword",
			"subscriberEnabled"
		],

		"TENANT": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"identifier",
			"lastTemplateIdentifier",
			"lastTemplateDate",
			"lastTemplateRevision",
			"lastTemplateVersion",
			"maintenanceMode",
			"maintenanceDisplayStandardMessage",
			"maintenanceDisplayReOpeningMessage",
			"maintenanceReOpeningTime",
			"maintenanceDisplayCustomMessage",
			"maintenanceCustomMessage",
			"sunriseMaintenanceMode",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"description",
			"databaseServer",
			"databaseDefaultConfiguration",
			"databaseLogin",
			"databasePassword",
			"databaseMaxActive",
			"databaseMaxIdle",
			"catalog",
			"collectDocument",
			"synchroDocument",
			"provisioning",
			"designProblem"
		],
		"TENANTTEMPLATE": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"identifier",
			"revision",
			"revisionDate",
			"version",
			"versionComment",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"description",
			"databaseServer",
			"databaseDefaultConfiguration",
			"databaseLogin",
			"databasePassword",
			"databaseMaxActive",
			"databaseMaxIdle",
			"catalog",
			"synchronizedTables"
		],

		"ucx7applicationuser": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"templateObjectId",
			"firstName",
			"lastName",
			"displayName",
			"smtpproxyaddress",
			"employeeId",
			"uid",
			"webAppPass",
			"appUserObjectId",
			"language",
			"billingId",
			"emailaddress",
			"ldapType",
			"userMustChangePasswordAtNextLogin",
			"locked",
			"cantChange",
			"doesNotExpire",
			"roleObjectId",
			"credentialPolicyObjectId"
		],
		"ucx7CallHandler": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"uid",
			"displayName",
			"dtmfAccessId",
			"templateObjectId",
			"partitionObjectId",
			"transferRuleStandardAction",
			"transferRuleStandardExtension",
			"transferRuleStandardPlayTransferPrompt",
			"greetingStandardPlayWhat",
			"greetingStandardPlayRecordMessagePrompt"
		],
		"ucx7ReroutingRule": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"uid",
			"displayName",
			"state",
			"useLanguageType",
			"language",
			"searchSpaceObjectId",
			"parameter",
			"operator",
			"operandValue"
		],
		"fr.niji.database.Ucx7ReroutingRuleCallHandler": [
			"targetConversation"
		],
		"unityconnection7": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"firstName",
			"lastName",
			"displayName",
			"employeeId",
			"ldapType",
			"dtmfPin",
			"webAppPass",
			"dtmfAccessId",
			"uid",
			"language",
			"templateIdValue",
			"mediaSwitchObjectId",
			"cosObjectId",
			"smtpNotificationDeviceEnabled",
			"smtpNotificationDeviceTo",
			"htmlNotificationDeviceEnabled",
			"htmlNotificationDeviceTo",
			"htmlNotificationDeviceTplName",
			"htmlNotificationDeviceCbkNumber",
			"subscriberObjectId",
			"alternateExtensionEnabled",
			"alternateExtensionNumber",
			"alternateExtensionPartition",
			"billingId",
			"corporatePhoneNumber",
			"emailaddress",
			"alternateGreetingAction",
			"smtpproxyaddress",
			"externalServiceObjectId",
			"externalServiceEmailAddressUseCorp",
			"externalServiceEmailAddress",
			"externalServiceEnableMailboxSynchCapability",
			"deliveryReceiptAction",
			"emailAction",
			"faxAction",
			"voicemailAction",
			"relayAddress",
			"htmlScheduledSummaryEnabled",
			"htmlScheduledSummaryTo",
			"htmlScheduledSummaryTplName",
			"htmlScheduledSummaryCbkNumber",
			"htmlMissedCallEnabled",
			"htmlMissedCallTo",
			"htmlMissedCallTplName",
			"htmlMissedCallCbkNumber",
			"alternateExtensionDisplayName",
			"alternateExtension1",
			"alternateExtensionDisplayName1",
			"alternateExtensionNumber1",
			"alternateExtensionPartition1",
			"alternateExtension2",
			"alternateExtensionDisplayName2",
			"alternateExtensionNumber2",
			"alternateExtensionPartition2",
			"alternateExtension3",
			"alternateExtensionDisplayName3",
			"alternateExtensionNumber3",
			"alternateExtensionPartition3",
			"alternateExtension4",
			"alternateExtensionDisplayName4",
			"alternateExtensionNumber4",
			"alternateExtensionPartition4",
			"alternateExtension5",
			"alternateExtensionDisplayName5",
			"alternateExtensionNumber5",
			"alternateExtensionPartition5",
			"alternateExtension6",
			"alternateExtensionDisplayName6",
			"alternateExtensionNumber6",
			"alternateExtensionPartition6",
			"alternateExtension7",
			"alternateExtensionDisplayName7",
			"alternateExtensionNumber7",
			"alternateExtensionPartition7",
			"alternateExtension8",
			"alternateExtensionDisplayName8",
			"alternateExtensionNumber8",
			"alternateExtensionPartition8",
			"enabledmwiExtension1",
			"mwiExtensionDisplayName1",
			"inheritmwiExtension1",
			"mwiExtension1",
			"mwiExtensionmediaSwitchObjectId1",
			"enabledmwiExtension2",
			"mwiExtensionDisplayName2",
			"inheritmwiExtension2",
			"mwiExtension2",
			"mwiExtensionmediaSwitchObjectId2",
			"enabledmwiExtension3",
			"mwiExtensionDisplayName3",
			"inheritmwiExtension3",
			"mwiExtension3",
			"mwiExtensionmediaSwitchObjectId3",
			"enabledmwiExtension4",
			"mwiExtensionDisplayName4",
			"inheritmwiExtension4",
			"mwiExtension4",
			"mwiExtensionmediaSwitchObjectId4",
			"enabledmwiExtension5",
			"mwiExtensionDisplayName5",
			"inheritmwiExtension5",
			"mwiExtension5",
			"mwiExtensionmediaSwitchObjectId5",
			"enabledmwiExtension6",
			"mwiExtensionDisplayName6",
			"inheritmwiExtension6",
			"mwiExtension6",
			"mwiExtensionmediaSwitchObjectId6",
			"enabledmwiExtension7",
			"mwiExtensionDisplayName7",
			"inheritmwiExtension7",
			"mwiExtension7",
			"mwiExtensionmediaSwitchObjectId7",
			"enabledmwiExtension8",
			"mwiExtensionDisplayName8",
			"inheritmwiExtension8",
			"mwiExtension8",
			"mwiExtensionmediaSwitchObjectId8",
			"enabledmwiExtension9",
			"mwiExtensionDisplayName9",
			"inheritmwiExtension9",
			"mwiExtension9",
			"mwiExtensionmediaSwitchObjectId9",
			"enabledmwiExtension10",
			"mwiExtensionDisplayName10",
			"inheritmwiExtension10",
			"mwiExtension10",
			"mwiExtensionmediaSwitchObjectId10"
		],


		"fr.niji.database.UsedReinitPasswordToken": [
			"token",
			"creationDate"
		],
		"vimAgent": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"internalId",
			"folder",
			"name",
			"agentDesktop",
			"skillGroups",
			"description",
			"peripheral",
			"peripheralNumber",
			"supervisor",
			"agentStateTrace"
		],
		"vimAgentTeam": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"internalId",
			"folder",
			"peripheral",
			"dialedNumber",
			"name",
			"description"
		],
		"fr.niji.database.VimAgentVimAgentTeam": [
			"supervisor",
			"primarySupervisor",
			"physicalMember"
		],
		"vimPerson": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"disableTime",
			"operationPending",
			"creationPending",
			"internalId",
			"loginName",
			"folder",
			"name",
			"firstName",
			"lastName",
			"passPhrase",
			"description"
		],


		"webex": [
			"unicityEnabled",
			"asyncDirty",
			"asyncDeletion",
			"equipmentId",
			"auditStatus",
			"auditTime",
			"auditAutoStartTime",
			"tc1",
			"tc2",
			"tc3",
			"tc4",
			"tc5",
			"tc6",
			"tc7",
			"tc8",
			"tc9",
			"tc10",
			"disableTime",
			"operationPending",
			"creationPending",
			"webExId",
			"firstName",
			"lastName",
			"phone",
			"mpProfileNumber",
			"email",
			"password",
			"language",
			"timezone",
			"teleConfCallIn",
			"teleConfCallOut",
			"teleConfCallOutInternational",
			"otherTelephony",
			"meetingCenterPro"
		],
		"fr.niji.database.WrongLoginAttempt": [
			"login",
			"creationDate",
			"ip"
		],
		"avayaRouting domains": [
			"id",
			"name",
			"domainType",
			"notes"
		],
		"avayaRouting locations": [
			"id",
			"name",
			"notes",
			"dpt_in_survivable_mode",
			"listed_directory_number",
			"sipEntity",
			"ManagedBandwidthUnitOfMeasurement",
			"ManagedBandwidth",
			"cac_normal_bwidth_video",
			"cac_can_audio_steal_from_video",
			"cac_max_bwidth_video_intraloc",
			"cac_max_bwidth_video_interloc",
			"cac_min_acceptable_bwidth_video",
			"AverageBandwidthPerCall",
			"AverageBandwidthPerCallUnitOfMeasurement",
			"cac_audio_alarm_threshold_percent",
			"cac_video_alarm_threshold_percent",
			"cac_audio_alarm_latency",
			"cac_video_alarm_latency",
			"locationpattern"
		],
		"avayaRouting locationpatterns": [
			"id",
			"ipaddresspattern",
			"notes"
		],
		"avayaRouting adaptations": [
			"id",
			"name",
			"adaptationmodule",
			"moduleParameterType",
			"moduleParameter",
			"moduleParameters",
			"egressuriparameters",
			"notes",
			"ingressadaptation",
			"egressadaptation"
		],
		"avayaRouting ingressadaptations": [
			"id",
			"matchingpattern",
			"mindigits",
			"maxdigits",
			"phoneContext",
			"deletedigits",
			"insertdigits",
			"addressToModify",
			"adaptationdata",
			"notes"
		],
		"avayaRouting egressadaptations": [
			"id",
			"matchingpattern",
			"mindigits",
			"maxdigits",
			"phoneContext",
			"deletedigits",
			"insertdigits",
			"addressToModify",
			"adaptationdata",
			"notes"
		],
		"avayaRouting sipentities": [
			"id",
			"name",
			"fqdnoripaddr",
			"entitytype",
			"notes",
			"adaptation",
			"location",
			"outboundproxy",
			"timezoneName",
			"timer_bf_secs",
			"credentialname",
			"securable",
			"cdrSetting",
			"commproftype",
			"loop_detect_mode",
			"loop_detect_threshold",
			"loop_detect_interval_msec",
			"do_monitoring",
			"monitor_proactive_secs",
			"monitor_reactive_secs",
			"monitor_retries",
			"cac_capable",
			"sharedBandwidthManager",
			"primaryBandwidthManager",
			"secondaryBandwidthManager",
			"userfc3263",
			"tcp_failover_port",
			"tls_failover_port",
			"listenport",
			"sipresponse"
		],
		"avayaRouting listenports": [
			"id",
			"portnumber",
			"transportprotocol",
			"sipdomain",
			"notes"
		],
		"avayaRouting sipresponses": [
			"id",
			"response",
			"type",
			"notes"
		],
		"avayaRouting entitylinks": [
			"id",
			"name",
			"sipentity1",
			"transportProtocol",
			"listenPortEntity1",
			"sipentity2",
			"DNSOverride",
			"listenPortEntity2",
			"connectionPolicy",
			"serviceState",
			"notes"
		],
		"avayaRouting timeranges": [
			"id",
			"name",
			"includesMonday",
			"includesTuesday",
			"includesWednesday",
			"includesThursday",
			"includesFriday",
			"includesSaturday",
			"includesSunday",
			"startTime",
			"stopTime",
			"notes"
		],
		"avayaRouting routingpolicies": [
			"id",
			"name",
			"disabled",
			"retries",
			"notes",
			"sipentity",
			"timeofday"
		],
		"avayaRouting dialpatterns": [
			"id",
			"digitpattern",
			"mindigits",
			"maxdigits",
			"treatasemergency",
			"emergency_order",
			"emergency_desc",
			"sipdomainall",
			"sipdomain",
			"notes",
			"deny",
			"routingpolicy",
			"locationall",
			"location"
		],
		"avayaRouting regularexpressions": [
			"id",
			"pattern",
			"rankorder",
			"deny",
			"notes",
			"routingpolicy"
		],
		"AvayaControlManager featureaccescode": [
			"featureAccessCodeIdentifier",
			"abbreviatedDialingList1AccessCode",
			"abbreviatedDialingList2AccessCode",
			"abbreviatedDialingList3AccessCode",
			"abbreviatedDialPrgmGroupListAccessCode",
			"announcementAccessCode",
			"answerBackAccessCode",
			"attendantAccessCode",
			"autoAlternateRoutingAARAccessCode",
			"autoRouteSelectionARSAccessCode1",
			"autoRouteSelectionARSAccessCode2",
			"automaticCallbackActivation",
			"automaticCallbackDeactivation",
			"callForwardingActivationBusyDA",
			"callForwardingActivationAll",
			"callForwardingActivationDeactivation",
			"callForwardingEnhancedStatus",
			"callForwardingEnhancedAct",
			"callForwardingEnhancedDeactivation",
			"callParkAccessCode",
			"callPickupAccessCode",
			"cASRemoteHoldAnswerHoldUnholdAccessCode",
			"cDRAccountCodeAccessCode",
			"changeCORAccessCode",
			"changeCoverageAccessCode",
			"conditionalCallExtendActivation",
			"conditionalCallExtendDeactivation",
			"contactClosureOpenCode",
			"contactClosureCloseCode",
			"contactClosurePulseCode",
			"dataOriginationAccessCode",
			"dataPrivacyAccessCode",
			"directedCallPickupAccessCode",
			"directedGroupCallPickupAccessCode",
			"emergencyAccesstoAttendantAccessCode",
			"ec500SelfAdministrationAccessCodes1",
			"ec500SelfAdministrationAccessCodes2",
			"ec500SelfAdministrationAccessCodes3",
			"ec500SelfAdministrationAccessCodes4",
			"enhancedEC500Activation",
			"enhancedEC500Deactivation",
			"enterpriseMobilityUserActivation",
			"enterpriseMobilityUserDeactivation",
			"extendedCallFwdActivateBusyDA",
			"extendedCallFwdActivateAll",
			"extendedCallFwdActivateDeactivation",
			"extendedGroupCallPickupAccessCode",
			"facilityTestCallsAccessCode",
			"flashAccessCode",
			"groupControlRestrictActivation",
			"groupControlRestrictDeactivation",
			"huntGroupBusyActivation",
			"huntGroupBusyDeactivation",
			"iSDNAccessCode",
			"lastNumberDialedAccessCode",
			"leaveWordCallingMessageRetrievalLock",
			"leaveWordCallingMessageRetrievalUnlock",
			"leaveWordCallingSendAMessage",
			"leaveWordCallingCancelAMessage",
			"limitNumberofConcurrentCallsActivation",
			"limitNumberofConcurrentCallsDeactivation",
			"maliciousCallTraceActivation",
			"maliciousCallTraceDeactivation",
			"meetMeConferenceAccessCodeChange",
			"messageSequenceTraceMSTDisable",
			"pasteDisplayPBXdataonPhoneAccessCode",
			"personalStationAccessPSAAssociateCode",
			"personalStationAccessPSADissociateCode",
			"perCallCPNBlockingCodeAccessCode",
			"perCallCPNUnblockingCodeAccessCode",
			"postedMessagesActivation",
			"postedMessagesDeactivation",
			"priorityCallingAccessCode",
			"programAccessCode",
			"refreshTerminalParametersAccessCode",
			"remoteSendAllCallsActivation",
			"remoteSendAllCallsDeactivation",
			"selfStationDisplayActivation",
			"sendAllCallsActivation",
			"sendAllCallsDeactivation",
			"stationFirmwareDownloadAccessCode",
			"stationLockActivation",
			"stationLockDeactivation",
			"stationSecurityCodeChangeAccessCode",
			"stationUserAdminofFBIAssign",
			"stationUserAdminofFBIRemove",
			"stationUserButtonRingControlAccessCode",
			"terminalDialUpTestAccessCode",
			"terminalTranslationInitializationMergeCode",
			"terminalTranslationInitializationSeparationCode",
			"transfertoVoiceMailAccessCode",
			"trunkAnswerAnyStationAccessCode",
			"userControlRestrictActivation",
			"userControlRestrictDeactivation",
			"voiceCoverageMessageRetrievalAccessCode",
			"voicePrincipalMessageRetrievalAccessCode",
			"whisperPageActivationAccessCode",
			"3PCCH323OverrideSIPStationActivation",
			"3PCCH323OverrideSIPStationDeactivation",
			"pinCheckingforPrivateCallsAccessCode",
			"pinCheckingforPrivateCallsUsingARSAccessCode",
			"pinCheckingforPrivateCallsUsingAARAccessCode",
			"agentWorkModesAfterCallWorkAccessCode",
			"agentWorkModesAssistAccessCode",
			"agentWorkModesAutoInAccessCode",
			"agentWorkModesAuxWorkAccessCode",
			"agentWorkModesLoginAccessCode",
			"agentWorkModesLogoutAccessCode",
			"agentWorkModesManualinAccessCode",
			"serviceObservingListenOnlyAccessCode",
			"serviceObservingListenTalkAccessCode",
			"serviceObservingNoTalkAccessCode",
			"serviceObservingNextCallListenOnlyAccessCode",
			"serviceObservingbyLocationListenOnlyAccessCode",
			"serviceObservingbyLocationListenTalkAccessCode",
			"restrictFirstConsultActivation",
			"restrictFirstConsultDeactivation",
			"restrictSecondConsultActivation",
			"restrictSecondConsultDeactivation",
			"forcedLogoutAuxForcedAgentLogoutbyLocationAccessCode",
			"forcedLogoutAuxForcedAgentLogoutbySkillAccessCode",
			"forcedLogoutAuxForcedAgentAuxWorkbyLocationAccessCode",
			"forcedLogoutAuxForcedAgentAuxWorkbySkillAccessCode",
			"addAgentSkillAccessCode",
			"removeAgentSkillAccessCode",
			"remoteLogoutofAgentAccessCode",
			"callVectoringPromptingFeaturesConverseDataReturnCode",
			"callVectoringPromptingFeaturesVectorVariable1VV1Code",
			"callVectoringPromptingFeaturesVectorVariable2VV2Code",
			"callVectoringPromptingFeaturesVectorVariable3VV3Code",
			"callVectoringPromptingFeaturesVectorVariable4VV4Code",
			"callVectoringPromptingFeaturesVectorVariable5VV5Code",
			"callVectoringPromptingFeaturesVectorVariable6VV6Code",
			"callVectoringPromptingFeaturesVectorVariable7VV7Code",
			"callVectoringPromptingFeaturesVectorVariable8VV8Code",
			"callVectoringPromptingFeaturesVectorVariable9VV9Code",
			"automaticWakeupCallAccessCode",
			"housekeepingStatusClientRoomAccessCode1",
			"housekeepingStatusClientRoomAccessCode2",
			"housekeepingStatusClientRoomAccessCode3",
			"housekeepingStatusClientRoomAccessCode4",
			"housekeepingStatusClientRoomAccessCode5",
			"housekeepingStatusClientRoomAccessCode6",
			"housekeepingStatusStationAccessCode1",
			"housekeepingStatusStationAccessCode2",
			"housekeepingStatusStationAccessCode3",
			"housekeepingStatusStationAccessCode4",
			"verifyWakeupAnnouncementAccessCode",
			"voiceDoNotDisturbAccessCode",
			"basicModeActivation",
			"enhancedModeActivation",
			"multimediaCallAccessCode",
			"multimediaDataConferenceActivation",
			"multimediaDataConferenceDeactivation",
			"multimediaMultiAddressAccessCode",
			"multimediaParameterAccessCode",
			"sA9014PINReleaseofTODLockAccessCode",
			"sA9014PINReleaseofTODLockRelock",
			"sA9105AuthorizationCodeDialingAccessCode"
		],
		"AvayaControlManager avayaacmvectorstep": [
			"number",
			"stepNumber",
			"longTitle",
			"0006ff",
			"7500ff",
			"0007ff",
			"8024ff",
			"6002ff",
			"0009ff",
			"000aff",
			"000bff",
			"8082ff",
			"7800ff",
			"f814ff",
			"f815ff",
			"8083ff",
			"8084ff",
			"ca40ff",
			"ca41ff",
			"e004ff",
			"4e21ff",
			"0010ff",
			"7002ff",
			"6003ff",
			"e000ff",
			"6004ff",
			"6005ff",
			"e001ff",
			"3e80ff",
			"3e81ff",
			"3e83ff",
			"3e85ff",
			"0011ff",
			"0012ff",
			"0013ff",
			"807eff",
			"7205ff",
			"0014ff",
			"4a3cff",
			"4a3dff",
			"4a3eff",
			"4a3fff",
			"ca3cff",
			"6007ff",
			"4a42ff",
			"4a43ff",
			"4650ff",
			"0015ff",
			"4a40ff",
			"0016ff",
			"4a38ff",
			"4a39ff",
			"4a46ff",
			"6008ff",
			"e002ff",
			"ca39ff",
			"c65dff",
			"8086ff",
			"8087ff",
			"8088ff",
			"ca42ff",
			"001bff",
			"001cff",
			"8089ff",
			"808aff",
			"001fff",
			"808bff",
			"808cff",
			"7206ff",
			"7207ff",
			"7209ff",
			"720aff",
			"0023ff",
			"0024ff",
			"6009ff",
			"0025ff",
			"0026ff",
			"0027ff",
			"0028ff",
			"4a44ff",
			"0029ff",
			"4a41ff",
			"002aff",
			"e003ff",
			"002bff",
			"7201ff",
			"7202ff",
			"7203ff",
			"7204ff",
			"808dff",
			"6200ff",
			"4a3bff"
		],
		"AvayaControlManager avayaacmvrt": [
			"number",
			"name",
			"digitString1",
			"digitString2",
			"digitString3",
			"digitString4",
			"digitString5",
			"digitString6",
			"digitString7",
			"digitString8",
			"digitString9",
			"digitString10",
			"digitString11",
			"digitString12",
			"digitString13",
			"digitString14",
			"digitString15",
			"digitString16",
			"digitString17",
			"digitString18",
			"digitString19",
			"digitString20",
			"digitString21",
			"digitString22",
			"digitString23",
			"digitString24",
			"digitString25",
			"digitString26",
			"digitString27",
			"digitString28",
			"digitString29",
			"digitString30",
			"digitString31",
			"digitString32",
			"digitString33",
			"digitString34",
			"digitString35",
			"digitString36",
			"digitString37",
			"digitString38",
			"digitString39",
			"digitString40",
			"digitString41",
			"digitString42",
			"digitString43",
			"digitString44",
			"digitString45",
			"digitString46",
			"digitString47",
			"digitString48",
			"digitString49",
			"digitString50",
			"digitString51",
			"digitString52",
			"digitString53",
			"digitString54",
			"digitString55",
			"digitString56",
			"digitString57",
			"digitString58",
			"digitString59",
			"digitString60",
			"digitString61",
			"digitString62",
			"digitString63",
			"digitString64",
			"digitString65",
			"digitString66",
			"digitString67",
			"digitString68",
			"digitString69",
			"digitString70",
			"digitString71",
			"digitString72",
			"digitString73",
			"digitString74",
			"digitString75",
			"digitString76",
			"digitString77",
			"digitString78",
			"digitString79",
			"digitString80",
			"digitString81",
			"digitString82",
			"digitString83",
			"digitString84",
			"digitString85",
			"digitString86",
			"digitString87",
			"digitString88",
			"digitString89",
			"digitString90",
			"digitString91",
			"digitString92",
			"digitString93",
			"digitString94",
			"digitString95",
			"digitString96",
			"digitString97",
			"digitString98",
			"digitString99",
			"digitString100"
		],
		"AvayaControlManager avayaacmvector": [
			"number",
			"name",
			"multiMedia",
			"lock",
			"attendant",
			"conf",
			"basic",
			"eas",
			"g3v4Enhanced",
			"aniiiDigits",
			"asaiRouting",
			"prompting",
			"lai",
			"g3v4AdvRoute",
			"cinfo",
			"bsr",
			"holidays",
			"variables",
			"enhanced30"
		],
		"AvayaControlManager avayaacmagent": [
			"loginId",
			"numPlanCategory",
			"numPlanAdvancedType",
			"numPlanCustomField1",
			"numPlanCustomField2",
			"numPlanCustomField3",
			"name",
			"tn",
			"cor",
			"coveragePath",
			"securityCode",
			"attribute",
			"aas",
			"audix",
			"checkSkillTnsToMatchAgentTn",
			"lwcReception",
			"lwcLogExternalCalls",
			"audixNameForMessaging",
			"loginIdForISDNDisplay",
			"autoAnswer",
			"auxAgentRemainsInLOAQueue",
			"miaAcrossSkills",
			"auxAgentConsideredIdleMIA",
			"acwAgentConsideredIdle",
			"workModeOnLogin",
			"auxWorkReasonCodeType",
			"logoutReasonCodeType",
			"maximumTimeAgentInACWBeforeLogout",
			"forcedAgentLogoutTimeHour",
			"forcedAgentLogoutTimeMinute",
			"directAgentSkill",
			"callHandlingPreference",
			"serviceObjective",
			"localCallPreference",
			"password",
			"portExtension",
			"includeTenantCallingPermissions",
			"sn1",
			"rl1",
			"sl1",
			"pa1",
			"sn2",
			"rl2",
			"sl2",
			"pa2",
			"sn3",
			"rl3",
			"sl3",
			"pa3",
			"sn4",
			"rl4",
			"sl4",
			"pa4",
			"sn5",
			"rl5",
			"sl5",
			"pa5",
			"sn6",
			"rl6",
			"sl6",
			"pa6",
			"sn7",
			"rl7",
			"sl7",
			"pa7",
			"sn8",
			"rl8",
			"sl8",
			"pa8",
			"sn9",
			"rl9",
			"sl9",
			"pa9",
			"sn10",
			"rl10",
			"sl10",
			"pa10",
			"sn11",
			"rl11",
			"sl11",
			"pa11",
			"sn12",
			"rl12",
			"sl12",
			"pa12",
			"sn13",
			"rl13",
			"sl13",
			"pa13",
			"sn14",
			"rl14",
			"sl14",
			"pa14",
			"sn15",
			"rl15",
			"sl15",
			"pa15",
			"sn16",
			"rl16",
			"sl16",
			"pa16",
			"sn17",
			"rl17",
			"sl17",
			"pa17",
			"sn18",
			"rl18",
			"sl18",
			"pa18",
			"sn19",
			"rl19",
			"sl19",
			"pa19",
			"sn20",
			"rl20",
			"sl20",
			"pa20",
			"sn21",
			"rl21",
			"sl21",
			"pa21",
			"sn22",
			"rl22",
			"sl22",
			"pa22",
			"sn23",
			"rl23",
			"sl23",
			"pa23",
			"sn24",
			"rl24",
			"sl24",
			"pa24",
			"sn25",
			"rl25",
			"sl25",
			"pa25",
			"sn26",
			"rl26",
			"sl26",
			"pa26",
			"sn27",
			"rl27",
			"sl27",
			"pa27",
			"sn28",
			"rl28",
			"sl28",
			"pa28",
			"sn29",
			"rl29",
			"sl29",
			"pa29",
			"sn30",
			"rl30",
			"sl30",
			"pa30",
			"sn31",
			"rl31",
			"sl31",
			"pa31",
			"sn32",
			"rl32",
			"sl32",
			"pa32",
			"sn33",
			"rl33",
			"sl33",
			"pa33",
			"sn34",
			"rl34",
			"sl34",
			"pa34",
			"sn35",
			"rl35",
			"sl35",
			"pa35",
			"sn36",
			"rl36",
			"sl36",
			"pa36",
			"sn37",
			"rl37",
			"sl37",
			"pa37",
			"sn38",
			"rl38",
			"sl38",
			"pa38",
			"sn39",
			"rl39",
			"sl39",
			"pa39",
			"sn40",
			"rl40",
			"sl40",
			"pa40",
			"sn41",
			"rl41",
			"sl41",
			"pa41",
			"sn42",
			"rl42",
			"sl42",
			"pa42",
			"sn43",
			"rl43",
			"sl43",
			"pa43",
			"sn44",
			"rl44",
			"sl44",
			"pa44",
			"sn45",
			"rl45",
			"sl45",
			"pa45",
			"sn46",
			"rl46",
			"sl46",
			"pa46",
			"sn47",
			"rl47",
			"sl47",
			"pa47",
			"sn48",
			"rl48",
			"sl48",
			"pa48",
			"sn49",
			"rl49",
			"sl49",
			"pa49",
			"sn50",
			"rl50",
			"sl50",
			"pa50",
			"sn51",
			"rl51",
			"sl51",
			"pa51",
			"sn52",
			"rl52",
			"sl52",
			"pa52",
			"sn53",
			"rl53",
			"sl53",
			"pa53",
			"sn54",
			"rl54",
			"sl54",
			"pa54",
			"sn55",
			"rl55",
			"sl55",
			"pa55",
			"sn56",
			"rl56",
			"sl56",
			"pa56",
			"sn57",
			"rl57",
			"sl57",
			"pa57",
			"sn58",
			"rl58",
			"sl58",
			"pa58",
			"sn59",
			"rl59",
			"sl59",
			"pa59",
			"sn60",
			"rl60",
			"sl60",
			"pa60",
			"sn61",
			"rl61",
			"sl61",
			"pa61",
			"sn62",
			"rl62",
			"sl62",
			"pa62",
			"sn63",
			"rl63",
			"sl63",
			"pa63",
			"sn64",
			"rl64",
			"sl64",
			"pa64",
			"sn65",
			"rl65",
			"sl65",
			"pa65",
			"sn66",
			"rl66",
			"sl66",
			"pa66",
			"sn67",
			"rl67",
			"sl67",
			"pa67",
			"sn68",
			"rl68",
			"sl68",
			"pa68",
			"sn69",
			"rl69",
			"sl69",
			"pa69",
			"sn70",
			"rl70",
			"sl70",
			"pa70",
			"sn71",
			"rl71",
			"sl71",
			"pa71",
			"sn72",
			"rl72",
			"sl72",
			"pa72",
			"sn73",
			"rl73",
			"sl73",
			"pa73",
			"sn74",
			"rl74",
			"sl74",
			"pa74",
			"sn75",
			"rl75",
			"sl75",
			"pa75",
			"sn76",
			"rl76",
			"sl76",
			"pa76",
			"sn77",
			"rl77",
			"sl77",
			"pa77",
			"sn78",
			"rl78",
			"sl78",
			"pa78",
			"sn79",
			"rl79",
			"sl79",
			"pa79",
			"sn80",
			"rl80",
			"sl80",
			"pa80",
			"sn81",
			"rl81",
			"sl81",
			"pa81",
			"sn82",
			"rl82",
			"sl82",
			"pa82",
			"sn83",
			"rl83",
			"sl83",
			"pa83",
			"sn84",
			"rl84",
			"sl84",
			"pa84",
			"sn85",
			"rl85",
			"sl85",
			"pa85",
			"sn86",
			"rl86",
			"sl86",
			"pa86",
			"sn87",
			"rl87",
			"sl87",
			"pa87",
			"sn88",
			"rl88",
			"sl88",
			"pa88",
			"sn89",
			"rl89",
			"sl89",
			"pa89",
			"sn90",
			"rl90",
			"sl90",
			"pa90",
			"sn91",
			"rl91",
			"sl91",
			"pa91",
			"sn92",
			"rl92",
			"sl92",
			"pa92",
			"sn93",
			"rl93",
			"sl93",
			"pa93",
			"sn94",
			"rl94",
			"sl94",
			"pa94",
			"sn95",
			"rl95",
			"sl95",
			"pa95",
			"sn96",
			"rl96",
			"sl96",
			"pa96",
			"sn97",
			"rl97",
			"sl97",
			"pa97",
			"sn98",
			"rl98",
			"sl98",
			"pa98",
			"sn99",
			"rl99",
			"sl99",
			"pa99",
			"sn100",
			"rl100",
			"sl100",
			"pa100",
			"sn101",
			"rl101",
			"sl101",
			"pa101",
			"sn102",
			"rl102",
			"sl102",
			"pa102",
			"sn103",
			"rl103",
			"sl103",
			"pa103",
			"sn104",
			"rl104",
			"sl104",
			"pa104",
			"sn105",
			"rl105",
			"sl105",
			"pa105",
			"sn106",
			"rl106",
			"sl106",
			"pa106",
			"sn107",
			"rl107",
			"sl107",
			"pa107",
			"sn108",
			"rl108",
			"sl108",
			"pa108",
			"sn109",
			"rl109",
			"sl109",
			"pa109",
			"sn110",
			"rl110",
			"sl110",
			"pa110",
			"sn111",
			"rl111",
			"sl111",
			"pa111",
			"sn112",
			"rl112",
			"sl112",
			"pa112",
			"sn113",
			"rl113",
			"sl113",
			"pa113",
			"sn114",
			"rl114",
			"sl114",
			"pa114",
			"sn115",
			"rl115",
			"sl115",
			"pa115",
			"sn116",
			"rl116",
			"sl116",
			"pa116",
			"sn117",
			"rl117",
			"sl117",
			"pa117",
			"sn118",
			"rl118",
			"sl118",
			"pa118",
			"sn119",
			"rl119",
			"sl119",
			"pa119",
			"sn120",
			"rl120",
			"sl120",
			"pa120"
		],
		"AvayaControlManager avayaacmcoverageag": [
			"number",
			"name"
		],
		"AvayaControlManager avayaacmannouncement": [
			"extension",
			"NumPlanCategory",
			"NumPlanAdvancedType",
			"NumPlanCustomField1",
			"NumPlanCustomField2",
			"NumPlanCustomField3",
			"name",
			"type",
			"source",
			"protected",
			"rate",
			"cor",
			"tn",
			"integAnnouncementQueue",
			"queueLength",
			"livestreamSource"
		],
		"AvayaControlManager avayaacmvariable": [
			"name",
			"description",
			"type",
			"scopeG",
			"scopeL",
			"scopeCollect",
			"length",
			"start",
			"assignment",
			"assignmentValue",
			"assignmentType",
			"vac",
			"assignmentEquipment",
			"lengthEquipment"
		],
		"AvayaControlManager avayaacmabbrdialgr": [
			"listNumber",
			"name",
			"size",
			"programExt",
			"priveleged",
			"dialCode1",
			"dialCode2",
			"dialCode3",
			"dialCode4",
			"dialCode5",
			"dialCode6",
			"dialCode7",
			"dialCode8",
			"dialCode9",
			"dialCode10",
			"dialCode11",
			"dialCode12",
			"dialCode13",
			"dialCode14",
			"dialCode15",
			"dialCode16",
			"dialCode17",
			"dialCode18",
			"dialCode19",
			"dialCode20",
			"dialCode21",
			"dialCode22",
			"dialCode23",
			"dialCode24",
			"dialCode25",
			"dialCode26",
			"dialCode27",
			"dialCode28",
			"dialCode29",
			"dialCode30",
			"dialCode31",
			"dialCode32",
			"dialCode33",
			"dialCode34",
			"dialCode35",
			"dialCode36",
			"dialCode37",
			"dialCode38",
			"dialCode39",
			"dialCode40",
			"dialCode41",
			"dialCode42",
			"dialCode43",
			"dialCode44",
			"dialCode45",
			"dialCode46",
			"dialCode47",
			"dialCode48",
			"dialCode49",
			"dialCode50",
			"dialCode51",
			"dialCode52",
			"dialCode53",
			"dialCode54",
			"dialCode55",
			"dialCode56",
			"dialCode57",
			"dialCode58",
			"dialCode59",
			"dialCode60",
			"dialCode61",
			"dialCode62",
			"dialCode63",
			"dialCode64",
			"dialCode65",
			"dialCode66",
			"dialCode67",
			"dialCode68",
			"dialCode69",
			"dialCode70",
			"dialCode71",
			"dialCode72",
			"dialCode73",
			"dialCode74",
			"dialCode75",
			"dialCode76",
			"dialCode77",
			"dialCode78",
			"dialCode79",
			"dialCode80",
			"dialCode81",
			"dialCode82",
			"dialCode83",
			"dialCode84",
			"dialCode85",
			"dialCode86",
			"dialCode87",
			"dialCode88",
			"dialCode89",
			"dialCode90",
			"dialCode91",
			"dialCode92",
			"dialCode93",
			"dialCode94",
			"dialCode95",
			"dialCode96",
			"dialCode97",
			"dialCode98",
			"dialCode99",
			"dialCode100"
		],
		"AvayaControlManager avayaacmabbrdialpe": [
			"listId",
			"listNumber",
			"name",
			"size",
			"dialCode1",
			"dialCode2",
			"dialCode3",
			"dialCode4",
			"dialCode5",
			"dialCode6",
			"dialCode7",
			"dialCode8",
			"dialCode9",
			"dialCode10",
			"dialCode11",
			"dialCode12",
			"dialCode13",
			"dialCode14",
			"dialCode15",
			"dialCode16",
			"dialCode17",
			"dialCode18",
			"dialCode19",
			"dialCode20",
			"dialCode21",
			"dialCode22",
			"dialCode23",
			"dialCode24",
			"dialCode25",
			"dialCode26",
			"dialCode27",
			"dialCode28",
			"dialCode29",
			"dialCode30",
			"dialCode31",
			"dialCode32",
			"dialCode33",
			"dialCode34",
			"dialCode35",
			"dialCode36",
			"dialCode37",
			"dialCode38",
			"dialCode39",
			"dialCode40",
			"dialCode41",
			"dialCode42",
			"dialCode43",
			"dialCode44",
			"dialCode45",
			"dialCode46",
			"dialCode47",
			"dialCode48",
			"dialCode49",
			"dialCode50",
			"dialCode51",
			"dialCode52",
			"dialCode53",
			"dialCode54",
			"dialCode55",
			"dialCode56",
			"dialCode57",
			"dialCode58",
			"dialCode59",
			"dialCode60",
			"dialCode61",
			"dialCode62",
			"dialCode63",
			"dialCode64",
			"dialCode65",
			"dialCode66",
			"dialCode67",
			"dialCode68",
			"dialCode69",
			"dialCode70",
			"dialCode71",
			"dialCode72",
			"dialCode73",
			"dialCode74",
			"dialCode75",
			"dialCode76",
			"dialCode77",
			"dialCode78",
			"dialCode79",
			"dialCode80",
			"dialCode81",
			"dialCode82",
			"dialCode83",
			"dialCode84",
			"dialCode85",
			"dialCode86",
			"dialCode87",
			"dialCode88",
			"dialCode89",
			"dialCode90",
			"dialCode91",
			"dialCode92",
			"dialCode93",
			"dialCode94",
			"dialCode95",
			"dialCode96",
			"dialCode97",
			"dialCode98",
			"dialCode99",
			"dialCode100"
		],
		"AvayaControlManager avayaacmabbrdialsy": [
			"listIdentifier",
			"size",
			"priveleged",
			"abrDialLang",
			"dialCode1",
			"label1",
			"dialCode2",
			"label2",
			"dialCode3",
			"label3",
			"dialCode4",
			"label4",
			"dialCode5",
			"label5",
			"dialCode6",
			"label6",
			"dialCode7",
			"label7",
			"dialCode8",
			"label8",
			"dialCode9",
			"label9",
			"dialCode10",
			"label10",
			"dialCode11",
			"label11",
			"dialCode12",
			"label12",
			"dialCode13",
			"label13",
			"dialCode14",
			"label14",
			"dialCode15",
			"label15",
			"dialCode16",
			"label16",
			"dialCode17",
			"label17",
			"dialCode18",
			"label18",
			"dialCode19",
			"label19",
			"dialCode20",
			"label20",
			"dialCode21",
			"label21",
			"dialCode22",
			"label22",
			"dialCode23",
			"label23",
			"dialCode24",
			"label24",
			"dialCode25",
			"label25",
			"dialCode26",
			"label26",
			"dialCode27",
			"label27",
			"dialCode28",
			"label28",
			"dialCode29",
			"label29",
			"dialCode30",
			"label30",
			"dialCode31",
			"label31",
			"dialCode32",
			"label32",
			"dialCode33",
			"label33",
			"dialCode34",
			"label34",
			"dialCode35",
			"label35",
			"dialCode36",
			"label36",
			"dialCode37",
			"label37",
			"dialCode38",
			"label38",
			"dialCode39",
			"label39",
			"dialCode40",
			"label40",
			"dialCode41",
			"label41",
			"dialCode42",
			"label42",
			"dialCode43",
			"label43",
			"dialCode44",
			"label44",
			"dialCode45",
			"label45",
			"dialCode46",
			"label46",
			"dialCode47",
			"label47",
			"dialCode48",
			"label48",
			"dialCode49",
			"label49",
			"dialCode50",
			"label50",
			"dialCode51",
			"label51",
			"dialCode52",
			"label52",
			"dialCode53",
			"label53",
			"dialCode54",
			"label54",
			"dialCode55",
			"label55",
			"dialCode56",
			"label56",
			"dialCode57",
			"label57",
			"dialCode58",
			"label58",
			"dialCode59",
			"label59",
			"dialCode60",
			"label60",
			"dialCode61",
			"label61",
			"dialCode62",
			"label62",
			"dialCode63",
			"label63",
			"dialCode64",
			"label64",
			"dialCode65",
			"label65",
			"dialCode66",
			"label66",
			"dialCode67",
			"label67",
			"dialCode68",
			"label68",
			"dialCode69",
			"label69",
			"dialCode70",
			"label70",
			"dialCode71",
			"label71",
			"dialCode72",
			"label72",
			"dialCode73",
			"label73",
			"dialCode74",
			"label74",
			"dialCode75",
			"label75",
			"dialCode76",
			"label76",
			"dialCode77",
			"label77",
			"dialCode78",
			"label78",
			"dialCode79",
			"label79",
			"dialCode80",
			"label80",
			"dialCode81",
			"label81",
			"dialCode82",
			"label82",
			"dialCode83",
			"label83",
			"dialCode84",
			"label84",
			"dialCode85",
			"label85",
			"dialCode86",
			"label86",
			"dialCode87",
			"label87",
			"dialCode88",
			"label88",
			"dialCode89",
			"label89",
			"dialCode90",
			"label90",
			"dialCode91",
			"label91",
			"dialCode92",
			"label92",
			"dialCode93",
			"label93",
			"dialCode94",
			"label94",
			"dialCode95",
			"label95",
			"dialCode96",
			"label96",
			"dialCode97",
			"label97",
			"dialCode98",
			"label98",
			"dialCode99",
			"label99",
			"dialCode100",
			"label100"
		],
		"AvayaControlManager avayaacmabbrdialen": [
			"listNumber",
			"dialCodeFirstDigit",
			"size",
			"priveleged",
			"dialCode00",
			"dialCode01",
			"dialCode02",
			"dialCode03",
			"dialCode04",
			"dialCode05",
			"dialCode06",
			"dialCode07",
			"dialCode08",
			"dialCode09",
			"dialCode10",
			"dialCode11",
			"dialCode12",
			"dialCode13",
			"dialCode14",
			"dialCode15",
			"dialCode16",
			"dialCode17",
			"dialCode18",
			"dialCode19",
			"dialCode20",
			"dialCode21",
			"dialCode22",
			"dialCode23",
			"dialCode24",
			"dialCode25",
			"dialCode26",
			"dialCode27",
			"dialCode28",
			"dialCode29",
			"dialCode30",
			"dialCode31",
			"dialCode32",
			"dialCode33",
			"dialCode34",
			"dialCode35",
			"dialCode36",
			"dialCode37",
			"dialCode38",
			"dialCode39",
			"dialCode40",
			"dialCode41",
			"dialCode42",
			"dialCode43",
			"dialCode44",
			"dialCode45",
			"dialCode46",
			"dialCode47",
			"dialCode48",
			"dialCode49",
			"dialCode50",
			"dialCode51",
			"dialCode52",
			"dialCode53",
			"dialCode54",
			"dialCode55",
			"dialCode56",
			"dialCode57",
			"dialCode58",
			"dialCode59",
			"dialCode60",
			"dialCode61",
			"dialCode62",
			"dialCode63",
			"dialCode64",
			"dialCode65",
			"dialCode66",
			"dialCode67",
			"dialCode68",
			"dialCode69",
			"dialCode70",
			"dialCode71",
			"dialCode72",
			"dialCode73",
			"dialCode74",
			"dialCode75",
			"dialCode76",
			"dialCode77",
			"dialCode78",
			"dialCode79",
			"dialCode80",
			"dialCode81",
			"dialCode82",
			"dialCode83",
			"dialCode84",
			"dialCode85",
			"dialCode86",
			"dialCode87",
			"dialCode88",
			"dialCode89",
			"dialCode90",
			"dialCode91",
			"dialCode92",
			"dialCode93",
			"dialCode94",
			"dialCode95",
			"dialCode96",
			"dialCode97",
			"dialCode98",
			"dialCode99"
		],
		"AvayaControlManager avayaacmservhrstab": [
			"number",
			"description",
			"timeAdjustments",
			"hourStartMon1",
			"minuteStartMon1",
			"hourEndMon1",
			"minuteEndMon1",
			"hourStartMon2",
			"minuteStartMon2",
			"hourEndMon2",
			"minuteEndMon2",
			"hourStartMon3",
			"minuteStartMon3",
			"hourEndMon3",
			"minuteEndMon3",
			"hourStartMon4",
			"minuteStartMon4",
			"hourEndMon4",
			"minuteEndMon4",
			"hourStartMon5",
			"minuteStartMon5",
			"hourEndMon5",
			"minuteEndMon5",
			"hourStartTue1",
			"minuteStartTue1",
			"hourEndTue1",
			"minuteEndTue1",
			"hourStartTue2",
			"minuteStartTue2",
			"hourEndTue2",
			"minuteEndTue2",
			"hourStartTue3",
			"minuteStartTue3",
			"hourEndTue3",
			"minuteEndTue3",
			"hourStartTue4",
			"minuteStartTue4",
			"hourEndTue4",
			"minuteEndTue4",
			"hourStartTue5",
			"minuteStartTue5",
			"hourEndTue5",
			"minuteEndTue5",
			"hourStartWed1",
			"minuteStartWed1",
			"hourEndWed1",
			"minuteEndWed1",
			"hourStartWed2",
			"minuteStartWed2",
			"hourEndWed2",
			"minuteEndWed2",
			"hourStartWed3",
			"minuteStartWed3",
			"hourEndWed3",
			"minuteEndWed3",
			"hourStartWed4",
			"minuteStartWed4",
			"hourEndWed4",
			"minuteEndWed4",
			"hourStartWed5",
			"minuteStartWed5",
			"hourEndWed5",
			"minuteEndWed5",
			"hourStartThu1",
			"minuteStartThu1",
			"hourEndThu1",
			"minuteEndThu1",
			"hourStartThu2",
			"minuteStartThu2",
			"hourEndThu2",
			"minuteEndThu2",
			"hourStartThu3",
			"minuteStartThu3",
			"hourEndThu3",
			"minuteEndThu3",
			"hourStartThu4",
			"minuteStartThu4",
			"hourEndThu4",
			"minuteEndThu4",
			"hourStartThu5",
			"minuteStartThu5",
			"hourEndThu5",
			"minuteEndThu5",
			"hourStartFri1",
			"minuteStartFri1",
			"hourEndFri1",
			"minuteEndFri1",
			"hourStartFri2",
			"minuteStartFri2",
			"hourEndFri2",
			"minuteEndFri2",
			"hourStartFri3",
			"minuteStartFri3",
			"hourEndFri3",
			"minuteEndFri3",
			"hourStartFri4",
			"minuteStartFri4",
			"hourEndFri4",
			"minuteEndFri4",
			"hourStartFri5",
			"minuteStartFri5",
			"hourEndFri5",
			"minuteEndFri5",
			"hourStartSat1",
			"minuteStartSat1",
			"hourEndSat1",
			"minuteEndSat1",
			"hourStartSat2",
			"minuteStartSat2",
			"hourEndSat2",
			"minuteEndSat2",
			"hourStartSat3",
			"minuteStartSat3",
			"hourEndSat3",
			"minuteEndSat3",
			"hourStartSat4",
			"minuteStartSat4",
			"hourEndSat4",
			"minuteEndSat4",
			"hourStartSat5",
			"minuteStartSat5",
			"hourEndSat5",
			"minuteEndSat5",
			"hourStartSun1",
			"minuteStartSun1",
			"hourEndSun1",
			"minuteEndSun1",
			"hourStartSun2",
			"minuteStartSun2",
			"hourEndSun2",
			"minuteEndSun2",
			"hourStartSun3",
			"minuteStartSun3",
			"hourEndSun3",
			"minuteEndSun3",
			"hourStartSun4",
			"minuteStartSun4",
			"hourEndSun4",
			"minuteEndSun4",
			"hourStartSun5",
			"minuteStartSun5",
			"hourEndSun5",
			"minuteEndSun5"
		],
		"AvayaControlManager avayaacmholidaytable": [
			"number",
			"name",
			"monthStart1",
			"dayStart1",
			"hourStart1",
			"minutesStart1",
			"monthEnd1",
			"dayEnd1",
			"hourEnd1",
			"minutesEnd1",
			"description1",
			"monthStart2",
			"dayStart2",
			"hourStart2",
			"minutesStart2",
			"monthEnd2",
			"dayEnd2",
			"hourEnd2",
			"minutesEnd2",
			"description2",
			"monthStart3",
			"dayStart3",
			"hourStart3",
			"minutesStart3",
			"monthEnd3",
			"dayEnd3",
			"hourEnd3",
			"minutesEnd3",
			"description3",
			"monthStart4",
			"dayStart4",
			"hourStart4",
			"minutesStart4",
			"monthEnd4",
			"dayEnd4",
			"hourEnd4",
			"minutesEnd4",
			"description4",
			"monthStart5",
			"dayStart5",
			"hourStart5",
			"minutesStart5",
			"monthEnd5",
			"dayEnd5",
			"hourEnd5",
			"minutesEnd5",
			"description5",
			"monthStart6",
			"dayStart6",
			"hourStart6",
			"minutesStart6",
			"monthEnd6",
			"dayEnd6",
			"hourEnd6",
			"minutesEnd6",
			"description6",
			"monthStart7",
			"dayStart7",
			"hourStart7",
			"minutesStart7",
			"monthEnd7",
			"dayEnd7",
			"hourEnd7",
			"minutesEnd7",
			"description7",
			"monthStart8",
			"dayStart8",
			"hourStart8",
			"minutesStart8",
			"monthEnd8",
			"dayEnd8",
			"hourEnd8",
			"minutesEnd8",
			"description8",
			"monthStart9",
			"dayStart9",
			"hourStart9",
			"minutesStart9",
			"monthEnd9",
			"dayEnd9",
			"hourEnd9",
			"minutesEnd9",
			"description9",
			"monthStart10",
			"dayStart10",
			"hourStart10",
			"minutesStart10",
			"monthEnd10",
			"dayEnd10",
			"hourEnd10",
			"minutesEnd10",
			"description10",
			"monthStart11",
			"dayStart11",
			"hourStart11",
			"minutesStart11",
			"monthEnd11",
			"dayEnd11",
			"hourEnd11",
			"minutesEnd11",
			"description11",
			"monthStart12",
			"dayStart12",
			"hourStart12",
			"minutesStart12",
			"monthEnd12",
			"dayEnd12",
			"hourEnd12",
			"minutesEnd12",
			"description12",
			"monthStart13",
			"dayStart13",
			"hourStart13",
			"minutesStart13",
			"monthEnd13",
			"dayEnd13",
			"hourEnd13",
			"minutesEnd13",
			"description13",
			"monthStart14",
			"dayStart14",
			"hourStart14",
			"minutesStart14",
			"monthEnd14",
			"dayEnd14",
			"hourEnd14",
			"minutesEnd14",
			"description14",
			"monthStart15",
			"dayStart15",
			"hourStart15",
			"minutesStart15",
			"monthEnd15",
			"dayEnd15",
			"hourEnd15",
			"minutesEnd15",
			"description15"
		],
		"AvayaControlManager avayaacmvdn": [
			"extension",
			"name",
			"destination",
			"number",
			"attendantVectoring",
			"meetMeConferencing",
			"allowVDNOverride",
			"cOR",
			"tN",
			"measured",
			"reportAdjunctCallsasACD",
			"acceptableServiceLevelsec",
			"vDNofOriginAnncExtension",
			"1stSkill",
			"2ndSkill",
			"3rdSkill",
			"sipUri",
			"aUDIXName",
			"returnDestination",
			"vDNTimedACWInterval",
			"afterXferorHeldCallDrops",
			"bSRApplication",
			"bSRAvailableAgentStrategy",
			"usedforBSRPolling",
			"bSRTieStrategy",
			"observeonAgentAnswer",
			"sendVDNasCalledRingingNameOverQSIG",
			"displayVDNforRouteToDAC",
			"vDNOverrideforASAIMessages",
			"bSRLocalTreatment",
			"reportingforPCorPOMCalls",
			"passPrefixedCPNtoVDNVector",
			"conferenceAccessCode",
			"conferenceController",
			"conferenceType",
			"routeToNumber",
			"v1",
			"v1Assignment",
			"v2",
			"v2Assignment",
			"v3",
			"v3Assignment",
			"v4",
			"v4Assignment",
			"v5",
			"v5Assignment",
			"v6",
			"v6Assignment",
			"v7",
			"v7Assignment",
			"v8",
			"v8Assignment",
			"v9",
			"v9Assignment",
			"vDNTimeZoneOffset",
			"vDNTimeZoneOffsetHour",
			"vDNTimeZoneOffsetMinute",
			"daylightSavingRule",
			"useVDNTimeZoneForHolidayVectoring",
			"applyRingbackforAutoAnswercalls"
		],
		"AvayaMediaServer auramediafiles": [
			"fileName",
			"nameSpaceContentGroup",
			"fileUpload",
			"override",
			"sizeBytes",
			"mimeType",
			"version",
			"timeReceived",
			"lastAccessTime",
			"fileUrl"
		],
		"CCM9Infrastructure timeschedule": [
			"name",
			"description",
			"timePeriods"
		],
		"CCM9Infrastructure routepartition": [
			"name",
			"description",
			"timeSchedules",
			"useOriginatingDeviceTimeZone",
			"timeZone",
			"notGeneralPartition"
		],
		"CCM9Infrastructure css": [
			"name",
			"description",
			"routePartitions"
		],
		"CCM9Infrastructure physicallocation": [
			"name",
			"description"
		],
		"CCM9Infrastructure transpattern": [
			"uuid",
			"pattern",
			"routePartitions",
			"description",
			"patternPrecedence",
			"routeClass",
			"callingSearchSpaceName",
			"useOriginatorCss",
			"callInterceptProfileName",
			"blockEnable",
			"releaseClause",
			"provideOutsideDialtone",
			"patternUrgency",
			"dontWaitForIDTOnSubsequentHops",
			"routeNextHopByCgpn",
			"useCallingPartyPhoneMask",
			"callingPartyTransformationMask",
			"callingPartyPrefixDigits",
			"callingLinePresentationBit",
			"callingNamePresentationBit",
			"callingPartyNumberType",
			"callingPartyNumberingPlan",
			"connectedLinePresentationBit",
			"connectedNamePresentationBit",
			"digitDiscardInstructionName",
			"calledPartyTransformationMask",
			"prefixDigitsOut",
			"calledPartyNumberType",
			"calledPartyNumberingPlan"
		],
		"CCM9Infrastructure phonentp": [
			"ipAddress",
			"description",
			"mode"
		],
		"CCM9Infrastructure datetimegroup": [
			"name",
			"timeZone",
			"separator",
			"dateformat",
			"timeFormat",
			"phoneNtpReferences"
		],
		"CCM9Infrastructure region": [
			"name",
			"relatedRegions"
		],
		"CCM9Infrastructure location": [
			"name",
			"betweenLocations",
			"withinAudioBandwidth",
			"withinVideoBandwidth",
			"withinImmersiveKbits",
			"relatedLocations"
		],
		"CCM9Infrastructure srst": [
			"name",
			"port",
			"ipAddress",
			"sipNetwork",
			"sipPort",
			"isSecure"
		],
		"CCM9Infrastructure devicepool": [
			"name",
			"callManagerGroupName",
			"autoSearchSpaceName",
			"adjunctCallingSearchSpace",
			"revertPriority",
			"localRouteGroupName",
			"dateTimeSettingName",
			"regionName",
			"mediaResourceListName",
			"locationName",
			"networkLocale",
			"srstName",
			"connectionMonitorDuration",
			"singleButtonBarge",
			"joinAcrossLines",
			"physicalLocationName",
			"deviceMobilityGroupName",
			"elinGroupName",
			"mobilityCssName",
			"automatedAlternateRoutingCssName",
			"aarGroupName",
			"cgpnTransformationCssName",
			"cdpnTransformationCssName",
			"geoLocationName",
			"geoLocationFilterName",
			"callingPartyNationalPrefix",
			"callingPartyNationalStripDigits",
			"callingPartyInternationalPrefix",
			"callingPartyInternationalStripDigits",
			"callingPartyUnknownPrefix",
			"callingPartyUnknownStripDigits",
			"callingPartySubscriberPrefix",
			"callingPartySubscriberStripDigits",
			"calledPartyNationalPrefix",
			"calledPartyNationalStripDigits",
			"calledPartyInternationalPrefix",
			"calledPartyInternationalStripDigits",
			"calledPartyUnknownPrefix",
			"calledPartyUnknownStripDigits",
			"calledPartySubscriberPrefix",
			"calledPartySubscriberStripDigits",
			"callingPartyTransformationCSS",
			"cntdPnTransformationCssName",
			"redirectingPartyTransformationCSS",
			"resetDevicePool"
		],
		"CCM9Infrastructure devicepoolroutegroup": [
			"name",
			"devicePool",
			"localRouteGroupName",
			"routeGroup"
		],
		"CCM9Infrastructure annunciator": [
			"name",
			"description",
			"serverName",
			"devicePoolName",
			"locationName",
			"useTrustedRelayPoint"
		],
		"CCM9Infrastructure conferencebridge": [
			"product",
			"name",
			"description",
			"serverName",
			"devicePoolName",
			"locationName",
			"useTrustedRelayPoint"
		],
		"CCM9Infrastructure transcoder": [
			"product",
			"description",
			"name",
			"devicePoolName",
			"isTrustedRelayPoint"
		],
		"CCM9Infrastructure mtp": [
			"mtpType",
			"name",
			"description",
			"serverName",
			"devicePoolName",
			"trustedRelayPoint"
		],
		"CCM9Infrastructure mohaudiosource": [
			"sourceId",
			"sourceFile",
			"name",
			"multicast",
			"initialAnnouncement",
			"initialAnnouncementPlayed",
			"periodicAnnouncement",
			"periodicAnnouncementInterval",
			"localeAnnouncement"
		],
		"CCM9Infrastructure mohserver": [
			"name",
			"description",
			"processNodeName",
			"devicePoolName",
			"locationName",
			"maxUnicastConnections",
			"maxMulticastConnections",
			"fixedAudioSourceDevice",
			"useTrustedRelayPoint",
			"runFlag",
			"isMultiCastEnabled",
			"baseMulticastIpaddress",
			"baseMulticastPort",
			"multicastIncrementOnIp"
		],
		"CCM9Infrastructure mediaresourcegroup": [
			"name",
			"description",
			"mohServers",
			"conferenceBridges",
			"mtps",
			"mohServers",
			"transcoders",
			"annunciators",
			"interactivevoiceresponses",
			"multicast"
		],
		"CCM9Infrastructure mediaresourcelist": [
			"name",
			"mediaResourceGroups"
		],
		"CCM9Infrastructure devicemobilitygroup": [
			"name",
			"description"
		],
		"CCM9Infrastructure devicemobility": [
			"name",
			"subNet",
			"subNetMaskSz",
			"devicePools"
		],
		"CCM9Infrastructure siptrunk": [
			"product",
			"protocol",
			"sipTrunkType",
			"name",
			"description",
			"devicePoolName",
			"networkLocation",
			"mediaResourceListName",
			"locationName",
			"aarGroupName",
			"tunneledProtocol",
			"qsigVariant",
			"asn1RoseOidEncoding",
			"packetCaptureMode",
			"packetCaptureDuration",
			"mtpRequired",
			"retryVideoCallAsAudio",
			"pathReplacementSupport",
			"transmitUtf8",
			"enableQsigUtf8",
			"unattendedPort",
			"srtpAllowed",
			"trunkTrafficSecure",
			"routeClassSignalling",
			"useTrustedRelayPoint",
			"pstnAccess",
			"runOnEveryNode",
			"isRpidEnabled",
			"isPaiEnabled",
			"sipAssertedType",
			"sipPrivacy",
			"value",
			"connectedPartyIdPresentation",
			"connectedNamePresentation",
			"callingSearchSpaceName",
			"prefixDn",
			"acceptInboundRdnis",
			"unknownPrefix",
			"unknownStripDigits",
			"cgpnTransformationUnknownCssName",
			"useDevicePoolCgpnTransformCssUnkn",
			"cntdPnTransformationCssName ",
			"useDevicePoolCntdPnTransformationCss",
			"cdpnTransformationCssName",
			"useDevicePoolCdpnTransformCss",
			"cgpnTransformationCssName",
			"useDevicePoolCgpnTransformCss",
			"callingPartySelection",
			"callingLineIdPresentation",
			"callingname",
			"callingAndCalledPartyInfoFormat",
			"acceptOutboundRdnis",
			"rdnTransformationCssName",
			"useDevicePoolRdnTransformCss",
			"callerName",
			"callerIdDn",
			"useCallerIdCallerNameinUriOutgoingRequest",
			"destAddrIsSrv",
			"destinations",
			"tkSipCodec",
			"presenceGroupName",
			"securityProfileName",
			"rerouteCallingSearchSpaceName",
			"referCallingSearchSpaceName",
			"subscribeCallingSearchSpaceName",
			"sipProfileName",
			"dtmfSignalingMethod",
			"sipNormalizationScript",
			"scriptTraceEnabled",
			"scriptParameters",
			"recordingInformation",
			"geoLocationName",
			"geoLocationFilterName",
			"sendGeoLocation",
			"resetSipTrunk",
			"status"
		],
		"CCM9Infrastructure siptrunkdestination": [
			"uuid",
			"addressIpv4",
			"addressIpv6",
			"port"
		],
		"CCM9Infrastructure aargroup": [
			"name",
			"aarGroup",
			"revertAarGroup"
		],
		"CCM9Infrastructure routegroup": [
			"name",
			"isLocalRouteGroup",
			"description",
			"distributionAlgorithm",
			"routeGroupMember"
		],
		"CCM9Infrastructure routegroupmember": [
			"uuid",
			"name",
			"routeGroup",
			"linkedObjectType",
			"sipTrunk",
			"gatewayh323",
			"gateway",
			"deviceName",
			"port"
		],
		"CCM9Infrastructure routelist": [
			"name",
			"description",
			"callManagerGroupName",
			"routeListEnabled",
			"runOnEveryNode",
			"routeGroups"
		],
		"CCM9Infrastructure routepattern": [
			"uuid",
			"pattern",
			"routePartition",
			"description",
			"patternPrecedence",
			"enableDccEnforcement",
			"blockedCallPercentage",
			"routeClass",
			"switchLink",
			"routeList",
			"sipTrunk",
			"gateway",
			"blockEnable",
			"releaseClause",
			"networkLocation",
			"externalCallControl",
			"allowDeviceOverride",
			"provideOutsideDialtone",
			"supportOverlapSending",
			"patternUrgency",
			"authorizationCodeRequired",
			"authorizationLevelRequired",
			"clientCodeRequired",
			"isEmergencyServiceNumber",
			"useCallingPartyPhoneMask",
			"callingPartyTransformationMask",
			"callingPartyPrefixDigits",
			"callingLinePresentationBit",
			"callingNamePresentationBit",
			"callingPartyNumberType",
			"callingPartyNumberingPlan",
			"connectedLinePresentationBit",
			"connectedNamePresentationBit",
			"digitDiscardInstructionName",
			"calledPartyTransformationMask",
			"prefixDigitsOut",
			"calledPartyNumberType",
			"calledPartyNumberingPlan"
		],
		"CCM9Infrastructure cgpntranspattern": [
			"uuid",
			"pattern",
			"routePartition",
			"description",
			"useCallingPartyPhoneMask",
			"digitDiscardInstructionName",
			"callingPartyTransformationMask",
			"callingPartyPrefixDigits",
			"callingLinePresentationBit",
			"callingPartyNumberType",
			"callingPartyNumberingPlan"
		],
		"CCM9Infrastructure cdpntranspattern": [
			"uuid",
			"pattern",
			"routePartition",
			"description",
			"digitDiscardInstructionName",
			"calledPartyTransformationMask",
			"calledPartyPrefixDigits",
			"calledPartyNumberType",
			"calledPartyNumberingPlan"
		],
		"CCM9Infrastructure entfeataccessconf": [
			"uuid",
			"pattern",
			"routePartition",
			"description",
			"isDefaultEafNumber"
		],
		"CCM9Infrastructure mobilityprofile": [
			"name",
			"description",
			"mobileClientCallingOption",
			"dvofServiceAccessNumber",
			"entFeatAccessConf",
			"dvorCallerId"
		],
		"CCM9Infrastructure geolocationpolicy": [
			"name",
			"description",
			"country",
			"nationalSubDivision",
			"district",
			"communityName",
			"cityDivision",
			"neighbourhood",
			"street",
			"leadingStreetDirection",
			"trailingStreetSuffix",
			"streetSuffix",
			"houseNumber",
			"houseNumberSuffix",
			"landmark",
			"location",
			"floor",
			"occupantName",
			"postalCode",
			"relatedPoliciesBorderBorder",
			"relatedPoliciesBorderInterior",
			"relatedPoliciesInteriorBorder",
			"relatedPoliciesInteriorInterior"
		],
		"CCM9Infrastructure callmanager": [
			"ctiid",
			"name",
			"processNodeName",
			"description",
			"manageAutoRegistrationInformation",
			"universalDeviceTemplate",
			"lineTemplate",
			"startDn",
			"endDn",
			"autoRegistrationEnabled",
			"ethernetPhonePort",
			"listen",
			"keepAlive",
			"sipPhonePort",
			"sipPhoneSecurePort"
		],
		"CCM9Infrastructure callmanagergroup": [
			"name",
			"tftpDefault",
			"callManagers"
		],
		"CCM9Infrastructure siptrunksecurityprofile": [
			"name",
			"description",
			"securityMode",
			"incomingTransport",
			"outgoingTransport",
			"digestAuthentication",
			"noncePolicyTime",
			"x509SubjectName",
			"incomingPort",
			"applLevelAuthentication",
			"acceptPresenceSubscription",
			"acceptOutOfDialogRefer",
			"acceptUnsolicitedNotification",
			"allowReplaceHeader",
			"transmitSecurityStatus",
			"sipV150OutboundSdpOfferFiltering"
		],
		"CCM9Infrastructure sipprofile": [
			"name",
			"description",
			"defaultTelephonyEventPayloadType",
			"gClear",
			"userAgentServerHeaderInfo",
			"cucmVersionInSipHeader",
			"dialStringInterpretation",
			"confidentialAccessLevelHeaders",
			"redirectByApplication",
			"ringing180",
			"t38Invite",
			"enableUriOutdialSupport",
			"isAssuredSipServiceEnabled",
			"sipBandwidthModifier",
			"sdpTransparency",
			"acceptAudioCodecPreferences",
			"inactiveSDPRequired",
			"timerInvite",
			"timerRegisterDelta",
			"timerRegister",
			"timerT1",
			"timerT2",
			"retryInvite",
			"retryNotInvite",
			"startMediaPort",
			"stopMediaPort",
			"callpickupUri",
			"callpickupListUri",
			"callpickupGroupUri",
			"meetmeServiceUrl",
			"userInfo",
			"dtmfDbLevel",
			"callHoldRingback",
			"anonymousCallBlock",
			"callerIdBlock",
			"dndControl",
			"telnetLevel",
			"resourcePriorityNamespace",
			"timerKeepAlive",
			"timerSubscribe",
			"timerSubscribeDelta",
			"maxRedirects",
			"timerOffHookToFirstDigit",
			"callForwardUri",
			"abbreviatedDialUri",
			"confJointEnable",
			"rfc2543Hold",
			"semiAttendedTransfer",
			"enableVad",
			"stutterMsgWaiting",
			"mlppUserAuthorization",
			"sipNormalizationScript",
			"isScriptTraceEnabled",
			"scriptParameters",
			"callerIdDn",
			"callerName",
			"rerouteIncomingRequest",
			"rsvpOverSip",
			"resourcePriorityNamespaceListName",
			"fallbackToLocalRsvp",
			"sipRe11XxEnabled",
			"videoCallTrafficClass",
			"callingLineIdentification",
			"sipSessionRefreshMethod",
			"earlyOfferSupportForVoiceCall",
			"enableAnatForEarlyOfferCalls",
			"deliverConferenceBridgeIdentifier",
			"useCallerIdCallerNameinUriOutgoingRequest",
			"rejectAnonymousIncomingCall",
			"rejectAnonymousOutgoingCall",
			"destRouteString",
			"enableOutboundOptionsPing",
			"optionsPingIntervalWhenStatusOK",
			"optionsPingIntervalWhenStatusNotOK",
			"sipOptionsRetryTimer",
			"sipOptionsRetryCount",
			"sendRecvSDPInMidCallInvite",
			"allowPresentationSharingUsingBfcp",
			"allowiXApplicationMedia",
			"allowMultipleCodecs"
		],
		"CCM9Infrastructure presencegroup": [
			"name",
			"description",
			"presenceGroups"
		],
		"CCM9Infrastructure processnode": [
			"nodeUsage",
			"processNodeRole",
			"name",
			"ipv6Name",
			"mac",
			"description",
			"cupDomain"
		],
		"CCM9Infrastructure facinfo": [
			"name",
			"code",
			"authorizationLevel"
		],
		"CCM9Infrastructure callpark": [
			"uuid",
			"pattern",
			"description",
			"routePartitionName",
			"callManagerName"
		],
		"CCM9Infrastructure voicemailpilot": [
			"uuid",
			"dirn",
			"cssName",
			"description",
			"isDefault"
		],
		"CCM9Infrastructure voicemailprofile": [
			"name",
			"description",
			"voiceMailPilot",
			"voiceMailboxMask",
			"isDefault"
		],
		"CCM9Infrastructure ctiroutepoint": [
			"name",
			"description",
			"devicePoolName",
			"callingSearchSpaceName",
			"locationName",
			"userLocale",
			"mediaResourceListName",
			"useTrustedRelayPoint",
			"cgpnTransformationCssName",
			"geoLocationName",
			"useDevicePoolCgpnTransformCss",
			"lineDn",
			"linePartition",
			"lineFromCucm"
		],
		"CCM9Infrastructure phonebuttontemplate": [
			"name",
			"basePhoneTemplateName",
			"buttons"
		],
		"CCM9Infrastructure buttonphonebuttontemp": [
			"uuid",
			"phoneButtonTemplate",
			"feature",
			"label",
			"buttonNumber"
		],
		"CCM9Infrastructure mlppdomain": [
			"domainName",
			"domainId"
		],
		"CCM9Infrastructure geolocation": [
			"name",
			"description",
			"country",
			"nationalSubDivision",
			"district",
			"communityName",
			"cityDivision",
			"neighbourhood",
			"street",
			"leadingStreetDirection",
			"trailingStreetSuffix",
			"streetSuffix",
			"houseNumber",
			"houseNumberSuffix",
			"landmark",
			"location",
			"floor",
			"occupantName",
			"postalCode"
		],
		"CCM9Infrastructure geolocationfilter": [
			"name",
			"description",
			"useCountry",
			"useNationalSubDivision",
			"useDistrict",
			"useCommunityName",
			"useCityDivision",
			"useNeighbourhood",
			"useStreet",
			"useLeadingStreetDirection",
			"useTrailingStreetSuffix",
			"useStreetSuffix",
			"useHouseNumber",
			"useHouseNumberSuffix",
			"useLandmark",
			"useLocation",
			"useFloor",
			"useOccupantName",
			"usePostalCode"
		],
		"CCM9Infrastructure commondeviceconfig": [
			"name",
			"softkeyTemplateName",
			"userHoldMohAudioSourceId",
			"networkHoldMohAudioSourceId",
			"userLocale",
			"ipAddressingMode",
			"ipAddressingModePreferenceControl",
			"useTrustedRelayPoint",
			"useImeForOutboundCalls",
			"allowAutoConfigurationForPhones",
			"allowDuplicateAddressDetection",
			"acceptRedirectMessages",
			"replyMulticastEchoRequest",
			"mlppIndicationStatus",
			"preemption",
			"mlppDomainId",
			"confidentialAccessMode",
			"confidentialAccessLevel"
		],
		"CCM9Infrastructure ucservice": [
			"serviceType",
			"productType",
			"name",
			"description",
			"hostnameorip",
			"port",
			"protocol",
			"ucServiceXml"
		],
		"CCM9Infrastructure serviceprofile": [
			"name",
			"description",
			"isDefault",
			"details"
		],
		"CCM9Infrastructure serviceprofiledetail": [
			"uuid",
			"serviceprofile",
			"profileName",
			"primary",
			"secondary",
			"tertiary",
			"serverCertificateVerification",
			"serviceProfileXml"
		],
		"CCM9Infrastructure softkeytemplate": [
			"name",
			"description",
			"isDefault",
			"baseSoftkeyTemplateName",
			"applications"
		],
		"CCM9Infrastructure softkeylayout": [
			"kurmiLabel",
			"callState",
			"softkeys",
			"softkeyTemplate"
		],
		"CCM9Infrastructure sipnormalizationscript": [
			"name",
			"description",
			"content",
			"scriptExecutionErrorRecoveryAction",
			"systemResourceErrorRecoveryAction",
			"maxMemoryThreshold",
			"maxLuaInstructionsThreshold",
			"isStandard"
		],
		"CCM9Infrastructure featurecontrolpolicy": [
			"name",
			"description",
			"features"
		],
		"CCM9Infrastructure featurecontrolconfig": [
			"uuid",
			"overrideDefault",
			"featureName",
			"enableSetting"
		],
		"CCM9Infrastructure gateway": [
			"product",
			"domainName",
			"protocol",
			"description",
			"callManagerGroupName",
			"vendorConfig"
		],
		"CCM9Infrastructure gatewayunit": [
			"uuid",
			"domainName",
			"index",
			"product"
		],
		"CCM9Infrastructure gatewaysubunit": [
			"uuid",
			"domainName",
			"unit",
			"index",
			"product",
			"beginPort"
		],
		"CCM9Infrastructure appuser": [
			"userid",
			"password",
			"digestCredentials",
			"presenceGroupName",
			"acceptPresenceSubscription",
			"acceptOutOfDialogRefer",
			"acceptUnsolicitedNotification",
			"allowReplaceHeader",
			"pwdCredLockedByAdministrator",
			"pwdCredUserCantChange",
			"pwdCredUserMustChange",
			"pwdCredDoesNotExpire",
			"device",
			"deviceProfile"
		],
		"CCM9Infrastructure usergroup": [
			"name",
			"appusers",
			"roles"
		],
		"CCM9Infrastructure ilsconfiguration": [
			"role",
			"registrationServer",
			"synchronizeClustersEvery",
			"useTls",
			"usePassword"
		],
		"CCM9Infrastructure featureconfig": [
			"uuid",
			"keyname",
			"value"
		],
		"CCM9Infrastructure interactivevoiceresponse": [
			"name",
			"description",
			"serverName",
			"devicePoolName",
			"locationName",
			"useTrustedRelayPoint"
		],
		"CCM9Infrastructure conferencenow": [
			"uuid",
			"conferenceNowNumber",
			"routePartitionName",
			"description",
			"maxWaitTimeForHost",
			"mohAudioSourceId"
		],
		"CCM9Infrastructure elingroup": [
			"name",
			"description",
			"elinNumbers"
		],
		"CCM9Infrastructure elinnumber": [
			"uuid",
			"pattern",
			"partition"
		],
		"CCM9Infrastructure meetme": [
			"uuid",
			"pattern",
			"description",
			"routePartition",
			"minimumSecurityLevel"
		],
		"CCM9Infrastructure credentialpolicy": [
			"name",
			"failedLogon",
			"resetFailedLogonAttempts",
			"lockoutDuration",
			"credChangeDuration",
			"credExpiresAfter",
			"minCredLength",
			"prevCredStoredNum",
			"inactiveDaysAllowed",
			"expiryWarningDays",
			"trivialCredCheck"
		],
		"CCM9Infrastructure timeperiod": [
			"name",
			"description",
			"startTime",
			"endTime",
			"startDay",
			"endDay",
			"monthOfYear",
			"dayOfMonth",
			"monthOfYearEnd",
			"dayOfMonthEnd"
		],
		"CCM9Infrastructure ipphoneservices": [
			"uuid",
			"serviceName",
			"serviceDescription",
			"serviceUrl",
			"secureServiceUrl",
			"serviceCategory",
			"serviceType",
			"serviceVendor",
			"serviceVersion",
			"enabled",
			"enterpriseSubscription"
		],
		"CCM9Infrastructure ipphoneservicesparameter": [
			"uuid",
			"ipPhoneServices",
			"name",
			"displayName",
			"default",
			"description",
			"paramRequired",
			"paramPassword"
		],
		"CCM9Infrastructure advertisedpatterns": [
			"uuid",
			"description",
			"pattern",
			"patternType",
			"hostedRoutePSTNRule",
			"pstnFailStrip",
			"pstnFailPrepend"
		],
		"CCM9Infrastructure routepartitionsflp": [
			"name",
			"partitionForEnterpriseANo",
			"markLearnedEntAltNumbers",
			"partitionForE164ANo",
			"markLearnedE164AltNumbers",
			"partitionForEnterprisePatterns",
			"markFixedLengthEntPatterns",
			"markVariableLengthEntPatterns",
			"partitionForE164Pattern",
			"markFixedLengthE164Patterns",
			"markVariableLengthE164Patterns"
		],
		"CCM9Infrastructure enterpriseparameter": [
			"name",
			"value"
		],
		"CCM9Infrastructure serviceparamclusterwide": [
			"uuid",
			"service",
			"name",
			"value"
		],
		"CCM9Infrastructure serviceparam": [
			"uuid",
			"processNodeName",
			"service",
			"name",
			"value"
		],
		"CCM9Infrastructure directedcallpark": [
			"uuid",
			"pattern",
			"description",
			"routePartitionName",
			"reversionPattern",
			"revertCssName",
			"retrievalPrefix"
		],
		"CCM9Infrastructure messagewaiting": [
			"uuid",
			"pattern",
			"routePartitionName",
			"description",
			"messageWaitingIndicator",
			"callingSearchSpaceName"
		],
		"CCM9Infrastructure defaultdeviceprofile": [
			"product",
			"protocol",
			"name",
			"description",
			"userHoldMohAudioSourceId",
			"userLocale",
			"phoneButtonTemplate",
			"softkeyTemplate",
			"privacy",
			"singleButtonBarge",
			"joinAcrossLines",
			"alwaysUsePrimeLine",
			"alwaysUsePrimeLineForVoiceMessage",
			"featureControlPolicy",
			"ignorePi",
			"dndStatus",
			"dndOption",
			"dndRingSetting",
			"emccCallingSearchSpace",
			"mlppDomainId",
			"mlppIndication",
			"preemption"
		],
		"CCM9Infrastructure credentialpolicydefault": [
			"uuid",
			"credentialUser",
			"credentialType",
			"credPolicyName",
			"credentials",
			"confirmCredentials",
			"credUserCantChange",
			"credUserMustChange",
			"credDoesNotExpire"
		],
		"CCM9Infrastructure voicemailport": [
			"name",
			"description",
			"devicePoolName",
			"commonDeviceConfigName",
			"callingSearchSpaceName",
			"automatedAlternateRoutingCssName",
			"locationName",
			"securityProfileName",
			"useTrustedRelayPoint",
			"geoLocationName",
			"dnPattern",
			"routePartition",
			"dnCallingSearchSpace",
			"aarNeighborhoodName",
			"callerIdDisplay",
			"callerIdDisplayAscii",
			"externalMask"
		],
		"CCM9Infrastructure mobilevoiceaccess": [
			"uuid",
			"pattern",
			"routePartitionName",
			"locales"
		],
		"CCM9Infrastructure presenceredundancygroup": [
			"name",
			"description",
			"server1",
			"server2",
			"haEnabled"
		],
		"CCM9Infrastructure functionrole": [
			"name",
			"description",
			"isstandard",
			"permissions"
		],
		"CCM9Infrastructure universaldevicetemplate": [
			"name",
			"deviceDescription",
			"devicePool",
			"sipProfile",
			"phoneButtonTemplate",
			"joinAcrossLines",
			"alwaysUsePrimeLine",
			"alwaysUsePrimeLineForVoiceMessage",
			"singleButtonBarge",
			"privacy",
			"builtInBridge",
			"allowControlOfDeviceFromCti",
			"hotlineDevice",
			"loggedIntoHuntGroup",
			"retryVideoCallAsAudio",
			"ignorePresentationIndicators",
			"enableExtensionMobility",
			"callingSearchSpace",
			"callingPartyTransformationCSSForInboundCalls",
			"callingPartyTransformationCSSForOutboundCalls",
			"reroutingCallingSearchSpace",
			"subscribeCallingSearchSpaceName",
			"useDevicePoolCallingPartyTransformationCSSforInboundCalls",
			"useDevicePoolCallingPartyTransformationCSSforOutboundCalls",
			"commonPhoneProfile",
			"commonDeviceConfiguration",
			"softkeyTemplate",
			"featureControlPolicy",
			"phonePersonalization",
			"mtpPreferredOriginatingCodec",
			"outboundCallRollover",
			"mediaTerminationPointRequired",
			"unattendedPort",
			"requiredDtmfReception",
			"rfc2833Disabled",
			"useTrustedRelayPoint",
			"information",
			"directory",
			"messages",
			"servicesUrl",
			"authenticationServer",
			"proxyServer",
			"idle",
			"idleTimer",
			"secureAuthenticationUrl",
			"secureDirUrl",
			"secureIdleUrl",
			"secureInformationUrl",
			"secureMessagesUrl",
			"secureServicesUrl",
			"servicesProvisioning",
			"packetCaptureMode",
			"packetCaptureDuration",
			"secureShellUser",
			"secureShellPassword",
			"userLocale",
			"networkLocale",
			"mlppIndication",
			"mlppPreemption",
			"doNotDisturb",
			"dndOption",
			"dndIncomingCallAlert",
			"aarGroup",
			"aarCallingSearchSpace",
			"blfPresenceGroup",
			"blfAudibleAlertSettingPhoneBusy",
			"blfAudibleAlertSettingPhoneIdle",
			"userHoldMohAudioSource",
			"networkHoldMohAudioSource",
			"location",
			"geoLocation",
			"deviceMobilityMode",
			"mediaResourceGroupList",
			"remoteDevice"
		],
		"CCM9Infrastructure universallinetemplate": [
			"uuid",
			"name",
			"urgentPriority",
			"lineDescription",
			"routePartition",
			"voiceMailProfile",
			"callingSearchSpace",
			"alertingName",
			"extCallControlProfile",
			"blfPresenceGroup",
			"callPickupGroup",
			"partyEntranceTone",
			"rejectAnonymousCall",
			"userHoldMohAudioSource",
			"networkHoldMohAudioSource",
			"aarDestinationMask",
			"aarGroup",
			"retainDestInCallFwdHistory",
			"forwardDestAllCalls",
			"primaryCssForwardingAllCalls",
			"secondaryCssForwardingAllCalls",
			"cssActivationPolicy",
			"busyIntCallsDestination",
			"busyIntCallsCss",
			"busyExtCallsDestination",
			"busyExtCallsCss",
			"noAnsIntCallsDestination",
			"noAnsIntCallsCss",
			"noAnsExtCallsDestination",
			"noAnsExtCallsCss",
			"noCoverageIntCallsDestination",
			"noCoverageIntCallsCss",
			"noCoverageExtCallsDestination",
			"noCoverageExtCallsCss",
			"unregisteredIntCallsDestination",
			"unregisteredIntCallsCss",
			"unregisteredExtCallsDestination",
			"unregisteredExtCallsCss",
			"ctiFailureDestination",
			"ctiFailureCss",
			"fwdDestExtCallsWhenNotRetrieved",
			"cssFwdExtCallsWhenNotRetrieved",
			"fwdDestInternalCallsWhenNotRetrieved",
			"cssFwdInternalCallsWhenNotRetrieved",
			"parkMonitorReversionTime",
			"target",
			"mlppCss",
			"mlppNoAnsRingDuration",
			"holdReversionRingDuration",
			"holdReversionNotificationInterval"
		],
		"CCM9Infrastructure userprofileprovision": [
			"name",
			"description",
			"deskPhones",
			"mobileDevices",
			"profile",
			"universalLineTemplate",
			"allowProvision",
			"limitProvision",
			"enableMra",
			"mraPolicyDesktop",
			"mraPolicyMobile"
		],
		"CCM9Infrastructure extcallcontrolprofile": [
			"name",
			"primaryUri",
			"secondaryUri",
			"enableLoadBalancing",
			"routingRequestTimer",
			"diversionReroutingCssName",
			"callTreatmentOnFailure"
		],
		"CCM9Infrastructure phonesecurityprofile": [
			"phoneType",
			"protocol",
			"name",
			"description",
			"nonceValidityTime",
			"deviceSecurityMode",
			"transportType",
			"enableDigestAuthentication",
			"tftpEncryptedConfig",
			"excludeDigestCredentials",
			"authenticationMode",
			"keySize",
			"sipPhonePort"
		],
		"CCM9Infrastructure siproutepattern": [
			"uuid",
			"usage",
			"pattern",
			"dnOrPatternIpv6",
			"description",
			"routePartitionName",
			"switchLink",
			"sipTrunkName",
			"routeListName",
			"huntListName",
			"blockEnable",
			"useCallingPartyPhoneMask",
			"callingPartyTransformationMask",
			"callingPartyPrefixDigits",
			"callingLinePresentationBit",
			"callingNamePresentationBit",
			"connectedLinePresentationBit",
			"connectedNamePresentationBit"
		],
		"CCM9Infrastructure ldapsystem": [
			"uuid",
			"syncEnabled",
			"ldapServer",
			"userIdAttribute"
		],
		"CCM9Infrastructure ldapdirectory": [
			"name",
			"ldapDn",
			"ldapPassword",
			"userSearchBase",
			"ldapFilter",
			"repeatable",
			"intervalValue",
			"scheduleUnit",
			"nextExecTime",
			"userId",
			"middleName",
			"managerId",
			"phoneNumber",
			"directoryUri",
			"firstName",
			"lastName",
			"department",
			"mailId",
			"ldapdircustomfields",
			"accessControlGroupInfo",
			"manageFeatureGroupTemplate",
			"featureGroupTemplate",
			"applyMask",
			"mask",
			"applyPoolList",
			"nbPools",
			"startDn1",
			"endDn1",
			"startDn2",
			"endDn2",
			"startDn3",
			"endDn3",
			"nbServers",
			"hostName1",
			"ldapPortNumber1",
			"sslEnabled1",
			"hostName2",
			"ldapPortNumber2",
			"sslEnabled2",
			"hostName3",
			"ldapPortNumber3",
			"sslEnabled3"
		],
		"CCM9Infrastructure ldapdircustomfieldname": [
			"pkid",
			"typecustomuserattribute",
			"customfieldattributename"
		],
		"CCM9Infrastructure ldapauthentication": [
			"uuid",
			"authenticateEndUsers",
			"distinguishedName",
			"ldapPassword",
			"userSearchBase",
			"nbServers",
			"hostName1",
			"ldapPortNumber1",
			"sslEnabled1",
			"hostName2",
			"ldapPortNumber2",
			"sslEnabled2",
			"hostName3",
			"ldapPortNumber3",
			"sslEnabled3"
		],
		"CCM9Infrastructure ldapfilter": [
			"name",
			"filter"
		],
		"CCM9Infrastructure ldapsearch": [
			"uuid",
			"enableDirectorySearch",
			"distinguishedName",
			"password",
			"userSearchBase1",
			"userSearchBase2",
			"userSearchBase3",
			"ldapFilterForUser",
			"enableRecursiveSearch",
			"ldapsearchudstags",
			"primary",
			"secondary",
			"tertiary"
		],
		"CCM9Infrastructure ldapsearchudstag": [
			"pkid",
			"ldapSearch",
			"tkldapserverattr",
			"dirobjectclassattrname"
		],
		"CCM9Infrastructure featuregrouptemplate": [
			"name",
			"description",
			"homeCluster",
			"imAndUcPresenceEnable",
			"meetingInformation",
			"serviceProfile",
			"userProfile",
			"allowCTIControl",
			"enableEMCC",
			"enableMobility",
			"enableMobileVoiceAccess",
			"maxDeskPickupWait",
			"remoteDestinationLimit",
			"blfPresenceGp",
			"subscribeCallingSearch",
			"userLocale"
		],
		"CCM9Infrastructure clientmattercode": [
			"code",
			"description"
		],
		"CCM9Infrastructure recordingprofile": [
			"name",
			"recordingCssName",
			"recorderDestination"
		],
		"CCM9Infrastructure gatewayh323": [
			"name",
			"description",
			"devicePoolName",
			"commonDeviceConfigName",
			"networkLocation",
			"mediaResourceListName",
			"packetCaptureMode",
			"packetCaptureDuration",
			"locationName",
			"aarNeighborhoodName",
			"tunneledProtocol",
			"qsigVariant",
			"asn1RoseOidEncoding",
			"useTrustedRelayPoint",
			"signalingPort",
			"mtpRequired",
			"retryVideoCallAsAudio",
			"waitForFarEndH245TerminalSet",
			"pathReplacementSupport",
			"transmitUtf8",
			"srtpAllowed",
			"allowH235PassThrough",
			"pstnAccess",
			"mlppDomainId",
			"confidentialAccessMode",
			"confidentialAccessLevel",
			"callingSearchSpaceName",
			"prefixDn",
			"redirectInboundNumberIe",
			"enableInboundFaststart",
			"cntdPnTransformationCssName",
			"useDevicePoolCntdPnTransformationCss",
			"callingPartySelection",
			"callingLineIdPresentation",
			"calledPartyIeNumberType",
			"callingPartyIeNumberType",
			"calledNumberingPlan",
			"callingNumberingPlan",
			"callerIdDn",
			"displayIeDelivery",
			"redirectOutboundNumberIe",
			"enableOutboundFaststart",
			"codecForOutboundFaststart",
			"cdpnTransformationCssName",
			"useDevicePoolCdpnTransformCss",
			"cgpnTransformationCssName",
			"useDevicePoolCgpnTransformCss",
			"geoLocationName",
			"geoLocationFilterName",
			"callingPartyNationalPrefix",
			"callingPartyNationalStripDigits",
			"callingPartyNationalTransformationCssName",
			"useDevicePoolCgpnTransformCssNatl",
			"callingPartyInternationalPrefix",
			"callingPartyInternationalStripDigits",
			"callingPartyInternationalTransformationCssName",
			"useDevicePoolCgpnTransformCssIntl",
			"callingPartyUnknownPrefix",
			"callingPartyUnknownStripDigits",
			"callingPartyUnknownTransformationCssName",
			"useDevicePoolCgpnTransformCssUnkn",
			"callingPartySubscriberPrefix",
			"callingPartySubscriberStripDigits",
			"callingPartySubscriberTransformationCssName",
			"useDevicePoolCgpnTransformCssSubs",
			"calledPartyNationalPrefix",
			"calledPartyNationalStripDigits",
			"calledPartyNationalTransformationCssName",
			"useDevicePoolCalledCssNatl",
			"calledPartyInternationalPrefix",
			"calledPartyInternationalStripDigits",
			"calledPartyInternationalTransformationCssName",
			"useDevicePoolCalledCssIntl",
			"calledPartyUnknownPrefix",
			"calledPartyUnknownStripDigits",
			"calledPartyUnknownTransformationCssName",
			"useDevicePoolCalledCssUnkn",
			"calledPartySubscriberPrefix",
			"calledPartySubscriberStripDigits",
			"calledPartySubscriberTransformationCssName",
			"useDevicePoolCalledCssSubs"
		],
		"CCM9Infrastructure devicedefault": [
			"uuid",
			"model",
			"protocol",
			"devicePoolName",
			"phoneButtonTemplate",
			"preferActCodeOverAutoReg"
		],
		"CCM9Infrastructure headsettemplate": [
			"name",
			"description",
			"userprofileprovisions"
		],
		"Cups9Infrastructure presencegateways": [
			"tkpebackendgateway",
			"description",
			"gateway"
		],
		"Cups9Infrastructure ucsystemcfg": [
			"tftphost_primary",
			"tftphost_backup1",
			"tftphost_backup2",
			"csfcertificatedirectory",
			"tkldapserver",
			"fkepaslistener"
		],
		"Cups9Infrastructure ucccmcipprofile": [
			"name",
			"description",
			"ccmciphost",
			"ccmciphostbackup",
			"tkccmcipcertverificationlevel"
		],
		"Cups9Infrastructure ctigwservicecfg": [
			"username",
			"userpassword",
			"hbi",
			"sessionexpires",
			"tkmocserver",
			"address",
			"addressfo",
			"address3",
			"address4",
			"address5",
			"address6",
			"address7",
			"address8"
		],
		"Cups9Infrastructure cupsdomains": [
			"domainname"
		],
		"Cups9Infrastructure cupsstaticroutes": [
			"destinationpattern",
			"description",
			"nexthop",
			"nexthopport",
			"tkcontactuser_type",
			"tkconnectprotocol",
			"priority",
			"weight",
			"tkstatus_allowlessspecificroute",
			"tkstatus_inservice",
			"routeblock"
		],
		"Cups9Infrastructure cupsincomingacl": [
			"address",
			"description"
		],
		"Cups9Infrastructure cupsxmppfederationpolicy": [
			"host",
			"incoming",
			"outgoing",
			"fkfederationadminpolicy"
		],
		"Cups9Infrastructure cupsconfig": [
			"pkid",
			"paramname",
			"paramvalue"
		],
		"Cups9Infrastructure cupsxmppconfig": [
			"pkid",
			"proxydomain",
			"tkimaddressscheme",
			"enablepartitionedfedwithocs",
			"tkpartitionedfedroutingmode"
		],
		"ExchangeV6 exchangeuser": [
			"Guid",
			"ID",
			"Identity",
			"Alias",
			"Domain",
			"OrganizationalUnit",
			"Database",
			"IsLinked",
			"LinkedMasterAccount",
			"HiddenFromAddressListsEnabled",
			"PrimarySmtpAddress",
			"EmailAddresses",
			"ForwardTo",
			"InternalUser",
			"InternalContact",
			"ForwardingSmtpAddress",
			"ArchiveEnabled",
			"ArchiveName",
			"ArchiveDatabase",
			"ArchiveQuota",
			"ArchiveWarningQuota",
			"IssueWarningQuota",
			"ProhibitSendQuota",
			"ProhibitSendReceiveQuota",
			"MaxSendSize",
			"MaxReceiveSize",
			"POP",
			"IMAP",
			"MAPI",
			"OWA",
			"ActiveSync",
			"GroupsSendOnBehalf",
			"GroupsSendAs",
			"RetentionPolicy",
			"ActiveSyncPolicy",
			"AddressBookPolicy",
			"CustomAttribute1",
			"CustomAttribute2",
			"CustomAttribute3",
			"CustomAttribute4",
			"CustomAttribute5",
			"CustomAttribute6",
			"CustomAttribute7",
			"CustomAttribute8",
			"CustomAttribute9",
			"CustomAttribute10",
			"CustomAttribute11",
			"CustomAttribute12",
			"CustomAttribute13",
			"CustomAttribute14",
			"CustomAttribute15"
		],
		"ExchangeV6 exchangeum": [
			"Guid",
			"Identity",
			"UserId",
			"User",
			"UMMailboxPolicy",
			"Extension",
			"SIPResourceIdentifier",
			"FaxEnabled",
			"MissedCallNotificationEnabled",
			"UMSMSNotificationOption",
			"PinlessAccessToVoiceMailEnabled",
			"AnonymousCallersCanLeaveMessages",
			"AutomaticSpeechRecognitionEnabled",
			"PlayOnPhoneEnabled",
			"CallAnsweringRulesEnabled",
			"CallAnsweringAudioCodec",
			"TUIAccessToCalendarEnabled",
			"TUIAccessToEmailEnabled",
			"UMExtensions",
			"PinCode",
			"DoExpirePinCode",
			"PinLockedOut"
		],
		"ExchangeV6 exchangeshared": [
			"Guid",
			"ID",
			"Identity",
			"Alias",
			"Domain",
			"OrganizationalUnit",
			"Database",
			"HiddenFromAddressListsEnabled",
			"PrimarySmtpAddress",
			"EmailAddresses",
			"ForwardTo",
			"IntUserSharedMailbox",
			"IntContactSharedMailbox",
			"ForwardingSmtpAddress",
			"ManagedBy",
			"ArchiveEnabled",
			"ArchiveName",
			"ArchiveDatabase",
			"ArchiveQuota",
			"ArchiveWarningQuota",
			"IssueWarningQuota",
			"ProhibitSendQuota",
			"ProhibitSendReceiveQuota",
			"MaxSendSize",
			"MaxReceiveSize",
			"RetentionPolicy",
			"ActiveSyncPolicy",
			"AddressBookPolicy",
			"CustomAttribute1",
			"CustomAttribute2",
			"CustomAttribute3",
			"CustomAttribute4",
			"CustomAttribute5",
			"CustomAttribute6",
			"CustomAttribute7",
			"CustomAttribute8",
			"CustomAttribute9",
			"CustomAttribute10",
			"CustomAttribute11",
			"CustomAttribute12",
			"CustomAttribute13",
			"CustomAttribute14",
			"CustomAttribute15"
		],
		"ExchangeV6 exchangegroup": [
			"Guid",
			"Identity",
			"DisplayName",
			"OrganizationalUnit",
			"Description",
			"Type",
			"Alias",
			"Domain",
			"RecipientFilter",
			"Members",
			"LinkedGroup",
			"MailUsers",
			"Contacts",
			"Shareds",
			"ManagedBy",
			"MemberJoinRestriction",
			"MemberDepartRestriction",
			"SendOnBehalf",
			"SendAS",
			"SendOnBehalf",
			"SendAs",
			"HiddenFromAddressListsEnabled",
			"RequireSenderAuthenticationEnabled"
		],
		"ExchangeV6 exchangeressource": [
			"Guid",
			"Identity",
			"UseExistingAccount",
			"SamAccountName",
			"DisplayName",
			"Description",
			"Type",
			"Alias",
			"Domain",
			"OrganizationalUnit",
			"Location",
			"Capacity",
			"PhoneNumber",
			"HiddenFromAddressListsEnabled",
			"AutomateProcessing",
			"AllowConflicts",
			"BookingWindowInDays",
			"MaximumDurationInMinutes",
			"AllowRecurringMeetings",
			"ScheduleOnlyDuringWorkHours",
			"AddOrganizerToSubject",
			"RemovePrivateProperty",
			"DeleteComments",
			"DeleteSubject",
			"AddAdditionalResponse",
			"AdditionalResponse",
			"ProcessExternalMeetingMessages",
			"AddMailTip",
			"MailTip"
		],
		"ExchangeV6 exchangemailuser": [
			"Guid",
			"Identity",
			"UseExistingMailUser",
			"SamAccountName",
			"FirstName",
			"LastName",
			"DisplayName",
			"Alias",
			"Domain",
			"OrganizationalUnit",
			"ExternalEmailAddress",
			"Password"
		],
		"ExchangeV6 exchangecontact": [
			"Guid",
			"Identity",
			"UseExistingContact",
			"SamAccountName",
			"FirstName",
			"LastName",
			"DisplayName",
			"OrganizationalUnit",
			"ExternalEmailAddress",
			"HiddenFromAddressListsEnabled",
			"Title",
			"Department",
			"Office",
			"Phone",
			"MobilePhone",
			"Fax",
			"StreetAddress",
			"City",
			"State",
			"PostalCode",
			"Country"
		],
		"ExchangeV6 exchangeemailaddpol": [
			"Guid",
			"Identity",
			"Name",
			"Priority",
			"EnabledEmailAddressTemplates",
			"DisabledEmailAddressTemplates",
			"UseCustomFilter",
			"IncludedRecipients",
			"RecipientContainer",
			"RecipientFilter",
			"ConditionalCompany",
			"ConditionalDepartment",
			"ConditionalStateOrProvince",
			"ConditionalCustomAttribute1",
			"ConditionalCustomAttribute2",
			"ConditionalCustomAttribute3",
			"ConditionalCustomAttribute4",
			"ConditionalCustomAttribute5",
			"ConditionalCustomAttribute6",
			"ConditionalCustomAttribute7",
			"ConditionalCustomAttribute8",
			"ConditionalCustomAttribute9",
			"ConditionalCustomAttribute10",
			"ConditionalCustomAttribute11",
			"ConditionalCustomAttribute12",
			"ConditionalCustomAttribute13",
			"ConditionalCustomAttribute14",
			"ConditionalCustomAttribute15"
		],
		"Office365 office365user": [
			"ObjectId",
			"UserId",
			"Domain",
			"FirstName",
			"LastName",
			"DisplayName",
			"PreferredLanguage",
			"IsSynced",
			"Title",
			"Department",
			"Office",
			"PhoneNumber",
			"MobilePhone",
			"Fax",
			"StreetAddress",
			"City",
			"State",
			"PostalCode",
			"Country",
			"Password",
			"PasswordNeverExpires",
			"ForceChangePassword",
			"AlternateEmailAddresses",
			"Licenses",
			"UsageLocation",
			"Roles",
			"BlockCredential"
		],
		"Office365 office365group": [
			"ObjectId",
			"DisplayName",
			"Description",
			"GroupType",
			"Alias",
			"Domain",
			"AccessType",
			"Queues",
			"Groups",
			"Mailbox",
			"Shareds",
			"MailUsers",
			"Contacts",
			"Guests",
			"Users"
		],
		"Office365 office365teams": [
			"GroupId",
			"TeamsOwner",
			"DisplayName",
			"Description",
			"AccessType",
			"Channel",
			"AllowGiphy",
			"GiphyContentRating",
			"AllowStickersAndMemes",
			"AllowCustomMemes",
			"AllowCreateUpdateChannels",
			"AllowDeleteChannels",
			"AllowAddRemoveApps",
			"AllowCreateUpdateRemoveTabs",
			"AllowCreateUpdateRemoveConnectors",
			"GuestAllowCreateUpdateChannels",
			"GuestAllowDeleteChannels",
			"AllowUserEditMessages",
			"AllowUserDeleteMessages",
			"AllowOwnerDeleteMessages",
			"AllowTeamMentions",
			"AllowChannelMentions"
		],
		"Office365 office365skype": [
			"ObjectId",
			"User",
			"UserPrincipalName",
			"SipProxyAddress",
			"DisplayName",
			"HostingServer",
			"AudioVideoDisabled",
			"ExternalAccessPolicy",
			"ClientPolicy",
			"MobilityPolicy",
			"EnterpriseVoiceEnabled",
			"EnterpriseVoiceType",
			"PhoneNumberType",
			"LineURI",
			"OnPremLineURI",
			"OnPremLineExt",
			"OnPremLineUriManuallySet",
			"InterpretedUserType",
			"PSTNConnectivity",
			"AnonymousCall",
			"HideUserLineNumber",
			"AlternateLineID",
			"PrivateLine",
			"PSTNUsageLocation",
			"HomeServer",
			"TargetRegistrarPool",
			"HybridPstnSite",
			"AllowInternationalCall",
			"DialPlan",
			"TenantDialPlan",
			"CallingLineIdentity",
			"VoicePolicy",
			"OnlineDialOutPolicy",
			"OnlineVoiceRoutingPolicy",
			"OnlineVoiceMailPolicy",
			"IPPhonePolicy",
			"NumPlanCategory",
			"NumPlanAdvancedType",
			"NumPlanCustomField1",
			"NumPlanCustomField2",
			"NumPlanCustomField3",
			"PstnConferencingLicenseState",
			"ConferencingPolicy",
			"CloudMeetingPolicy",
			"BroadcastMeetingPolicy",
			"ConferencingServiceName",
			"ConferencingServiceDomain",
			"ConferencingServiceNumber",
			"MicrosoftConferencingServiceNumber",
			"ConferencingServiceTollFreeNumber",
			"IdMode",
			"ResetId",
			"ConferencingServiceURL",
			"ResetPin",
			"TeamsMeetingPolicy",
			"TeamsMeetingBroadcastPolicy",
			"TeamsCallingPolicy",
			"TeamsCallParkPolicy",
			"TeamsEmergencyCallingPolicy",
			"TeamsEmergencyCallRoutingPolicy",
			"TeamsMessagingPolicy",
			"TeamsUpgradePolicy",
			"TeamsVideoInteropServicePolicy",
			"TeamsAppPermissionPolicy",
			"TeamsAppSetupPolicy",
			"TeamsChannelsPolicy",
			"TeamsWorkLoadPolicy",
			"TeamsComplianceRecordingPolicy",
			"TeamsIPPhonePolicy",
			"TeamsMobilityPolicy"
		],
		"Office365 office365exchange": [
			"ObjectId",
			"ExchangeUser",
			"UserPrincipalName",
			"PrimarySmtpAddress",
			"DisplayName",
			"UMMailboxPolicy",
			"FaxEnabled",
			"MissedCallNotificationEnabled",
			"UMSMSNotificationOption",
			"PinlessAccessToVoiceMailEnabled",
			"AnonymousCallersCanLeaveMessages",
			"AutomaticSpeechRecognitionEnabled",
			"PlayOnPhoneEnabled",
			"CallAnsweringRulesEnabled",
			"CallAnsweringAudioCodec",
			"TUIAccessToCalendarEnabled",
			"TUIAccessToEmailEnabled",
			"Extensions",
			"PinCode",
			"PinExpired",
			"LockedOut"
		],
		"Office365 office365queue": [
			"PrimaryUri",
			"Name",
			"Domain",
			"LineUri",
			"NumPlanCategory",
			"NumPlanAdvancedType",
			"NumPlanCustomField1",
			"NumPlanCustomField2",
			"NumPlanCustomField3",
			"RoutingMethod",
			"AllowOptOut",
			"AgentAlertTime",
			"TimeoutThreshold",
			"TimeoutAction",
			"TimeoutUri",
			"OverflowThreshold",
			"OverflowAction",
			"OverflowUri"
		],
		"Office365 office365workflow": [
			"PrimaryUri",
			"Name",
			"Description",
			"LineUri",
			"NumPlanCategory",
			"NumPlanAdvancedType",
			"NumPlanCustomField1",
			"NumPlanCustomField2",
			"NumPlanCustomField3",
			"OperatorType",
			"UserOperator",
			"QueueOperator",
			"Active",
			"EnableDialByName",
			"EnableVoiceResponse",
			"LanguageId",
			"TimeZoneId",
			"WelcomeMessage",
			"WelcomeMessageAudio",
			"WelcomePrompt",
			"BusinessHoursID",
			"BusinessHoursMessageAudio",
			"BusinessHoursPrompt",
			"NonBusinessHoursMessage",
			"NonBusinessHoursMessageAudio",
			"NonBusinessHoursPrompt",
			"NonBusinessHoursAction",
			"NonBusinessHoursTargetType",
			"NonBusinessHoursSipAddress"
		],
		"Office365 office365cscallqueue": [
			"Identity",
			"Name",
			"LanguageId",
			"RoutingMethod",
			"AllowOptOut",
			"PresenceBaseRouting",
			"ConferenceMode",
			"AgentAlertTime",
			"Office365User",
			"Office365Groups",
			"UseDefaultMusicOnHold",
			"MusicOnHoldResourceId",
			"WelcomeMusicResourceId",
			"TimeoutThreshold",
			"TimeoutAction",
			"TimeoutActionTarget",
			"OverflowThreshold",
			"OverflowAction",
			"OverflowActionTarget",
			"ApplicationInstances"
		],
		"Office365 office365appinstance": [
			"ObjectId",
			"DisplayName",
			"PhoneNumberType",
			"PhoneNumber",
			"UserPrincipalName",
			"ApplicationId",
			"NumPlanCategory",
			"NumPlanAdvancedType",
			"NumPlanCustomField1",
			"NumPlanCustomField2",
			"NumPlanCustomField3"
		],
		"Office365 office365closingdays": [
			"Identity",
			"Name",
			"AutoAttendant",
			"StartDate",
			"EndDate",
			"Message",
			"AudioMessage",
			"Prompt",
			"Action",
			"SIPType",
			"UserOperatorCD",
			"QueueOperatorCD",
			"AutoAttendantCD"
		],
		"Office365 office365meetingroom": [
			"ObjectId",
			"DisplayName",
			"Description",
			"MeetingRoom",
			"UseExistingRessourceMailbox",
			"RessourceMailbox",
			"Location",
			"Capacity",
			"PhoneNumber",
			"Password",
			"PasswordNeverExpires",
			"UsageLocation",
			"Licenses",
			"EnterpriseVoiceEnabled",
			"LineURI",
			"NumPlanCategory",
			"NumPlanAdvancedType",
			"NumPlanCustomField1",
			"NumPlanCustomField2",
			"NumPlanCustomField3",
			"ConferencingPolicy",
			"ActiveSyncMailboxPolicy",
			"AutomateProcessing",
			"AllowConflicts",
			"BookingWindowInDays",
			"MaximumDurationInMinutes",
			"AllowRecurringMeetings",
			"ScheduleOnlyDuringWorkHours",
			"AddOrganizerToSubject",
			"RemovePrivateProperty",
			"DeleteComments",
			"DeleteSubject",
			"AddAdditionalResponse",
			"AdditionalResponse",
			"ProcessExternalMeetingMessages",
			"AddMailTip",
			"MailTip"
		],
		"Office365 office365dialplan": [
			"Identity",
			"SimpleName",
			"Description",
			"ExternalAccessPrefix",
			"OptimizeDeviceDialing"
		],
		"Office365 office365normalizatnrule": [
			"Identity",
			"Name",
			"Description",
			"DialPlan",
			"Pattern",
			"Translation",
			"IsInternalExtension"
		],
		"Office365 o365emergencycall": [
			"Identity",
			"Description",
			"NotificationMode",
			"ExternalLocationLookupMode",
			"NotificationGroup",
			"NotificationDialOutNumber"
		],
		"Office365 o365emergencyroute": [
			"Identity",
			"Description",
			"AllowEnhancedEmergencyServices"
		],
		"Office365 office365guest": [
			"ObjectId",
			"FirstName",
			"LastName",
			"DisplayName",
			"EmailAddress",
			"UsageLocation",
			"PreferredLanguage",
			"Title",
			"Department",
			"Office",
			"PhoneNumber",
			"MobilePhone",
			"Fax",
			"StreetAddress",
			"City",
			"State",
			"PostalCode"
		],
		"Office365 office365pstnusage": [
			"Usage",
			"Description"
		],
		"Office365 office365voiceroute": [
			"Identity",
			"Description",
			"Priority",
			"NumberPattern",
			"PSTNUsages",
			"OnlinePstnGatewayList"
		],
		"Office365 office365voiceroutpolicy": [
			"Identity",
			"Description",
			"PSTNUsages"
		],
		"Office365 teamsmeetingconf": [
			"Identity",
			"EnableQoS",
			"DisableAnonymousJoin",
			"ClientMediaPortRangeEnabled",
			"ClientAudioPort",
			"ClientAudioPortRange",
			"ClientVideoPort",
			"ClientVideoPortRange",
			"ClientAppSharingPort",
			"ClientAppSharingPortRange",
			"CustomFooterText",
			"HelpURL",
			"LegalURL",
			"LogoURL"
		],
		"Office365 teamsmeetbroadcastconf": [
			"Identity",
			"SupportURL",
			"AllowSdnProviderForBroadcastMeeting",
			"SdnProviderName",
			"SdnApiTemplateUrl",
			"SdnApiToken",
			"SdnLicenseId"
		],
		"Office365 teamsguestmeetingconf": [
			"Identity",
			"AllowIPVideo",
			"AllowMeetNow",
			"ScreenSharingMode"
		],
		"Office365 teamsguestmessagingconf": [
			"Identity",
			"AllowUserEditMessage",
			"AllowUserDeleteMessage",
			"AllowUserChat",
			"AllowGiphy",
			"GiphyRatingType",
			"AllowMemes",
			"AllowStickers"
		],
		"Office365 teamsguestcallingconf": [
			"Identity",
			"AllowPrivateCalling"
		],
		"Office365 teamsclientconf": [
			"Identity",
			"AllowOrganizationTab",
			"AllowSkypeBusinessInterop",
			"AllowGuestUser",
			"AllowScopedPeopleSearchandAccess",
			"AllowEmailIntoChannel",
			"AllowResourceAccountSendMessage",
			"RestrictedSenderList",
			"AllowDropBox",
			"AllowBox",
			"AllowGoogleDrive",
			"AllowShareFile",
			"ContentPin",
			"ResourceAccountContentAccess"
		],
		"Office365 o365exchuser": [
			"ObjectId",
			"O365User",
			"Alias",
			"Domain",
			"HiddenFromAddressListsEnabled",
			"PrimarySmtpAddress",
			"EmailAddresses",
			"ForwardTo",
			"InternalUser",
			"InternalContact",
			"ForwardingSmtpAddress",
			"ArchiveEnabled",
			"ArchiveName",
			"IssueWarningQuota",
			"ProhibitSendQuota",
			"ProhibitSendReceiveQuota",
			"MaxSendSize",
			"MaxReceiveSize",
			"POP",
			"IMAP",
			"MAPI",
			"OWA",
			"ActiveSync",
			"GroupsSendOnBehalf",
			"GroupsSendAs",
			"RetentionPolicy",
			"ActiveSyncPolicy",
			"AddressBookPolicy",
			"CustomAttribute1",
			"CustomAttribute2",
			"CustomAttribute3",
			"CustomAttribute4",
			"CustomAttribute5",
			"CustomAttribute6",
			"CustomAttribute7",
			"CustomAttribute8",
			"CustomAttribute9",
			"CustomAttribute10",
			"CustomAttribute11",
			"CustomAttribute12",
			"CustomAttribute13",
			"CustomAttribute14",
			"CustomAttribute15"
		],
		"Office365 o365exchshared": [
			"ObjectId",
			"DisplayName",
			"Alias",
			"Domain",
			"HiddenFromAddressListsEnabled",
			"PrimarySmtpAddress",
			"EmailAddresses",
			"ForwardTo",
			"IntUserShrdMailbox",
			"IntContactShrdMailbox",
			"ForwardingSmtpAddress",
			"ManagedBy",
			"ArchiveEnabled",
			"ArchiveName",
			"IssueWarningQuota",
			"ProhibitSendQuota",
			"ProhibitSendReceiveQuota",
			"MaxSendSize",
			"MaxReceiveSize",
			"RetentionPolicy",
			"ActiveSyncPolicy",
			"AddressBookPolicy",
			"CustomAttribute1",
			"CustomAttribute2",
			"CustomAttribute3",
			"CustomAttribute4",
			"CustomAttribute5",
			"CustomAttribute6",
			"CustomAttribute7",
			"CustomAttribute8",
			"CustomAttribute9",
			"CustomAttribute10",
			"CustomAttribute11",
			"CustomAttribute12",
			"CustomAttribute13",
			"CustomAttribute14",
			"CustomAttribute15"
		],
		"Office365 o365exchmailuser": [
			"ObjectId",
			"FirstName",
			"LastName",
			"DisplayName",
			"Alias",
			"Domain",
			"ExternalEmailAddress",
			"Password",
			"HiddenFromAddressListsEnabled",
			"Title",
			"Department",
			"Office",
			"Phone",
			"MobilePhone",
			"Fax",
			"StreetAddress",
			"City",
			"State",
			"PostalCode"
		],
		"Office365 o365exchcontact": [
			"ObjectId",
			"FirstName",
			"LastName",
			"DisplayName",
			"ExternalEmailAddress",
			"HiddenFromAddressListsEnabled",
			"Title",
			"Department",
			"Office",
			"Phone",
			"MobilePhone",
			"Fax",
			"StreetAddress",
			"City",
			"State",
			"PostalCode"
		],
		"Office365 o365exchangeemailaddpol": [
			"Guid",
			"Name",
			"Priority",
			"EnabledEmailAddressTemplates",
			"DisabledEmailAddressTemplates",
			"UseCustomFilter",
			"IncludedRecipients",
			"IncludeUnifiedGroupRecipients",
			"RecipientContainer",
			"RecipientFilter",
			"ConditionalCompany",
			"ConditionalDepartment",
			"ConditionalStateOrProvince",
			"ConditionalCustomAttribute1",
			"ConditionalCustomAttribute2",
			"ConditionalCustomAttribute3",
			"ConditionalCustomAttribute4",
			"ConditionalCustomAttribute5",
			"ConditionalCustomAttribute6",
			"ConditionalCustomAttribute7",
			"ConditionalCustomAttribute8",
			"ConditionalCustomAttribute9",
			"ConditionalCustomAttribute10",
			"ConditionalCustomAttribute11",
			"ConditionalCustomAttribute12",
			"ConditionalCustomAttribute13",
			"ConditionalCustomAttribute14",
			"ConditionalCustomAttribute15"
		],
		"OracleECB oracleecbuser": [
			"AoR",
			"numberOrPattern",
			"description",
			"dialingContext",
			"agent",
			"policy",
			"tags"
		],
		"ciscoPLM plmproduct": [
			"id",
			"name",
			"description",
			"productType",
			"hostInfo",
			"userId",
			"password",
			"syncStatus",
			"registered",
			"forceRegister",
			"registerAtAdd",
			"lastPollTime",
			"lastConnectTime",
			"version",
			"licenseVersion",
			"licenseStatus"
		],
		"ciscoPLM plmlicensestatus": [
			"id",
			"name",
			"version",
			"licenseStatus",
			"productType",
			"complianceTime",
			"installed",
			"available",
			"overage",
			"required",
			"borrowed",
			"loanedOut",
			"free"
		],
		"ciscoPLM plmresource": [
			"registrationId",
			"version",
			"mode",
			"lastSyncTime",
			"licenseViolations",
			"productsCount"
		],
		"uccx uccxresource": [
			"userID",
			"userIdFromCucm",
			"firstName",
			"lastName",
			"extension",
			"resourceGroup",
			"autoAvailable",
			"type",
			"skillMap",
			"team",
			"alias"
		],
		"uccx uccxresourcegroup": [
			"id",
			"name"
		],
		"uccx uccxteam": [
			"teamId",
			"teamname",
			"primarySupervisor",
			"secondarySupervisors",
			"csqs"
		],
		"uccx uccxskill": [
			"skillId",
			"skillName"
		],
		"uccx uccxcsq": [
			"id",
			"name",
			"queueType",
			"queueAlgorithm",
			"autoWork",
			"wrapupTime",
			"resourcePoolType",
			"serviceLevel",
			"serviceLevelPercentage",
			"selectionCriteriaSkill",
			"skillCompetency",
			"selectionCriteriaResource",
			"resourceGroupNameUriPair"
		],
		"UnityConnection9Infrastructure coses": [
			"URI",
			"displayName",
			"canRecordName",
			"maxNameLength",
			"listInDirectoryStatus",
			"maxGreetingLength",
			"accessIMAP",
			"imapCanFetchMessageBody",
			"imapCanFetchPrivateMessageBody",
			"accessVmi",
			"accessAdvancedUserFeatures",
			"accessTts",
			"accessVui",
			"accessSTT",
			"sttType",
			"enableSTTSecureMessage",
			"playbackMessageAndGreetings",
			"outsideCallers",
			"accessCallRoutingRules",
			"personalAdministrator",
			"accessUnifiedClient",
			"uaAlternateExtensionAccess",
			"maxMsgLength",
			"canSendToPublicDl",
			"movetoDeleteFolder",
			"accessLiveReply",
			"accessOutsideLiveReply",
			"requireSecureMessages",
			"maxPrivateDlists",
			"maxMembersPVL",
			"callScreenAvailable",
			"callHoldAvailable",
			"outcallRestriction",
			"xferRestriction",
			"faxRestriction"
		],
		"UnityConnection9Infrastructure unityutcredentialpin": [
			"kurmiLabel",
			"URI",
			"locked",
			"cantChange",
			"credMustChange",
			"doesntExpire",
			"CredentialPolicyObjectId",
			"usertemplate"
		],
		"UnityConnection9Infrastructure unityutcredentialpwd": [
			"kurmiLabel",
			"URI",
			"locked",
			"cantChange",
			"credMustChange",
			"doesntExpire",
			"CredentialPolicyObjectId",
			"usertemplate"
		],
		"UnityConnection9Infrastructure menuentries": [
			"kurmiLabel",
			"URI",
			"TouchtoneKey",
			"locked",
			"action",
			"TransferNumber",
			"DisplayName",
			"TransferType",
			"TransferRings",
			"targetConversation",
			"TargetDirectoryHandlerObjectId"
		],
		"UnityConnection9Infrastructure callhandlerprimarytmp": [
			"URI",
			"Language",
			"DisplayName",
			"MaxMsgLen",
			"EditMsg",
			"UseDefaultLanguage",
			"UseCallLanguage",
			"SendUrgentMsg",
			"SendPrivateMsg",
			"SendSecureMsg",
			"PlayAfterMessage",
			"AfterMessageAction",
			"OneKeyDelay",
			"EnablePrependDigits",
			"PrependDigits",
			"PlayPostGreetingRecording",
			"ScheduleSetObjectId",
			"usertemplate"
		],
		"UnityConnection9Infrastructure unityutgreetings": [
			"kurmiLabel",
			"URI",
			"GreetingType",
			"Enabled",
			"TimeExpires",
			"PlayWhat",
			"PlayRecordMessagePrompt",
			"IgnoreDigits",
			"EnableTransfer",
			"Reprompts",
			"RepromptDelay",
			"AfterGreetingAction",
			"AfterGreetingTargetConversation",
			"AfterGreetingTargetHandlerObjectId",
			"AfterGreetingTargetDirectoryHandlerObjectId"
		],
		"UnityConnection9Infrastructure unityutmessageactions": [
			"kurmiLabel",
			"URI",
			"VoicemailAction",
			"EmailAction",
			"FaxAction",
			"DeliveryReceiptAction",
			"RelayAddress"
		],
		"UnityConnection9Infrastructure usertemplates": [
			"URI",
			"templateAlias",
			"alias",
			"displayName",
			"displayNameRule",
			"PartitionObjectId",
			"SearchByExtensionSearchSpaceObjectId",
			"MediaSwitchObjectId",
			"CosObjectId",
			"isVmEnrolled",
			"listInDirectory",
			"routeNDRToSender",
			"skipPasswordForKnownDevice",
			"useShortPollForCache",
			"address",
			"building",
			"city",
			"state",
			"postalCode",
			"country",
			"useDefaultTimeZone",
			"timeZone",
			"useDefaultLanguage",
			"language",
			"department",
			"manager",
			"billingId",
			"createSmtpProxyFromCorp",
			"sendReadReceipts",
			"MessageAgingPolicyObjectId",
			"warningQuota",
			"sendQuota",
			"receiveQuota",
			"MailboxStoreObjectId",
			"useBriefPrompts",
			"promptVolume",
			"promptSpeed",
			"isClockMode24Hour",
			"conversationTUI",
			"enableMessageLocator",
			"messageLocatorSortOrder",
			"repeatMenu",
			"firstDigitTimeout",
			"interdigitDelay",
			"commandDigitTimeout",
			"greetByName",
			"sayAltGreetWarning",
			"jumpToMessagesOnLogin",
			"volume",
			"speed",
			"sayTotalNew",
			"sayTotalNewVoice",
			"sayTotalNewEmail",
			"sayTotalNewFax",
			"sayTotalReceipts",
			"sayTotalSaved",
			"sayTotalDraftMsg",
			"messageTypeMenu",
			"saySender",
			"saySenderExtension",
			"sayAni",
			"sayMsgNumber",
			"sayTimestampBefore",
			"sayMessageLength",
			"skipForwardTime",
			"skipReverseTime",
			"enableMessageBookmark",
			"saveMessageOnHangup",
			"saySenderAfter",
			"saySenderExtensionAfter",
			"sayAniAfter",
			"sayMsgNumberAfter",
			"sayTimestampAfter",
			"sayMessageLengthAfter",
			"autoAdvanceMsgs",
			"confirmDeleteMessage",
			"sendBroadcastMsg",
			"updateBroadcastMsg",
			"addressMode",
			"nameConfirmation",
			"continuousAddMode",
			"useDynamicNameSearchWeight",
			"enableSaveDraft",
			"retainUrgentMessageFlag",
			"sendMessageOnHangup"
		],
		"UnityConnection9Infrastructure pagernotificationdevices": [
			"kurmiLabel",
			"URI",
			"active",
			"displayName",
			"InitialDelay",
			"RepeatNotify",
			"RepeatInterval",
			"OnNotificationFailure",
			"PagerFailDeviceObjectId",
			"HtmlFailDeviceObjectId",
			"PhoneFailDeviceObjectId",
			"SmtpFailDeviceObjectId",
			"EventList",
			"PhoneNumber",
			"AfterDialDigits",
			"DialDelay",
			"RingsToWait",
			"RetriesOnBusy",
			"BusyRetryInterval",
			"RetriesOnRna",
			"RnaRetryInterval",
			"RetriesOnSuccess",
			"SuccessRetryInterval",
			"PhoneSystem"
		],
		"UnityConnection9Infrastructure phonenotificationdevices": [
			"kurmiLabel",
			"URI",
			"active",
			"displayName",
			"InitialDelay",
			"RepeatNotify",
			"RepeatInterval",
			"OnNotificationFailure",
			"PagerFailDeviceObjectId",
			"HtmlFailDeviceObjectId",
			"PhoneFailDeviceObjectId",
			"SmtpFailDeviceObjectId",
			"EventList",
			"PhoneNumber",
			"AfterDialDigits",
			"DialDelay",
			"RingsToWait",
			"RetriesOnBusy",
			"BusyRetryInterval",
			"RetriesOnRna",
			"RnaRetryInterval",
			"PhoneSystem",
			"PromptForId"
		],
		"UnityConnection9Infrastructure htmlnotificationdevices": [
			"kurmiLabel",
			"URI",
			"active",
			"displayName",
			"eventList",
			"smtpAddress",
			"NotificationTemplateID",
			"callbackNumber",
			"disableMobileNumberFromPCA",
			"disableTemplateSelectionFromPCA"
		],
		"UnityConnection9Infrastructure smtpnotificationdevices": [
			"kurmiLabel",
			"URI",
			"active",
			"displayName",
			"InitialDelay",
			"RepeatNotify",
			"RepeatInterval",
			"eventList",
			"smtpAddress",
			"PhoneNumber",
			"HeaderText",
			"StaticText",
			"FooterText",
			"SendCallerId",
			"SendCount",
			"SendPcaLink"
		],
		"UnityConnection9Infrastructure unifiedmessagingaccount": [
			"kurmiLabel",
			"URI",
			"externalServiceObjectId",
			"emailAddressUseCorp",
			"enableTtsOfEmailCapability",
			"enableCalendarCapability",
			"enableMailboxSynchCapability"
		],
		"UnityConnection9Infrastructure restrictiontables": [
			"URI",
			"displayName",
			"maxDigits",
			"minDigits",
			"defaultBlocked"
		],
		"UnityConnection9Infrastructure restrictionpatterns": [
			"kurmiLabel",
			"URI",
			"numberPattern",
			"blocked"
		],
		"UnityConnection9Infrastructure schedulesets": [
			"URI",
			"displayName",
			"ownerLocationObjectId"
		],
		"UnityConnection9Infrastructure schedules": [
			"URI",
			"displayName",
			"ownerLocationObjectId"
		],
		"UnityConnection9Infrastructure scheduledetails": [
			"kurmiLabel",
			"URI",
			"subject",
			"startTime",
			"endOfDay",
			"endTime",
			"isActiveMonday",
			"isActiveTuesday",
			"isActiveWednesday",
			"isActiveThursday",
			"isActiveFriday",
			"isActiveSaturday",
			"isActiveSunday"
		],
		"UnityConnection9Infrastructure holidayschedules": [
			"URI",
			"displayName",
			"ownerLocationObjectId"
		],
		"UnityConnection9Infrastructure holidays": [
			"kurmiLabel",
			"URI",
			"subject",
			"startDate",
			"endDate",
			"startTime",
			"endOfDay",
			"endTime"
		],
		"UnityConnection9Infrastructure phonesystems": [
			"URI",
			"displayName",
			"defaultTRaPSwitch",
			"mwiAlwaysUpdate",
			"mwiPortMemory",
			"mwiForceOff",
			"callLoopSupervisedTransferDetect",
			"callLoopForwardNotificationDetect",
			"callLoopDTMF",
			"callLoopGuardTimeMs",
			"callLoopExtensionDetect",
			"restrictDialScheduled",
			"restrictDialUnconditional",
			"restrictDialStartTime",
			"restrictDialEndTime",
			"axlServers",
			"ManageAXLServers",
			"CcmAXLUser",
			"CcmAXLPassword",
			"CcmVersion"
		],
		"UnityConnection9Infrastructure distributionlists": [
			"URI",
			"alias",
			"displayName",
			"dtmfAccessId",
			"PartitionObjectId",
			"allowContacts",
			"allowForeignMessage"
		],
		"UnityConnection9Infrastructure routingrules": [
			"URI",
			"displayName",
			"type",
			"state",
			"useDefaultLanguage",
			"useCallLanguage",
			"languageCode",
			"SearchSpaceObjectId",
			"RouteTargetHandlerObjectId",
			"routeAction",
			"routeTargetConversation",
			"routeTargetHandlerObjectType",
			"routingRuleConditions"
		],
		"UnityConnection9Infrastructure routingruleconditions": [
			"kurmiLabel",
			"URI",
			"routingRule",
			"parameter",
			"operator",
			"operandValue",
			"MediaPortObjectId",
			"PhoneSystem",
			"Schedule"
		],
		"UnityConnection9Infrastructure partitions": [
			"URI",
			"name",
			"description"
		],
		"UnityConnection9Infrastructure searchspaces": [
			"URI",
			"name",
			"description"
		],
		"UnityConnection9Infrastructure callhandlers": [
			"URI",
			"templateObjectId",
			"displayName",
			"phoneSystem",
			"scheduleSet",
			"useDefaultTimeZone",
			"timeZone",
			"useDefaultLanguage",
			"UseCallLanguage",
			"language",
			"dtmfAccessId",
			"PartitionObjectId",
			"InheritSearchSpaceFromCall",
			"CallSearchSpaceObjectId",
			"OneKeyDelay",
			"EnablePrependDigits",
			"PrependDigits",
			"MaxMsgLen",
			"EditMsg",
			"SendUrgentMsg",
			"SendPrivateMsg",
			"SendSecureMsg",
			"RecipientType",
			"RecipientSubscriberObjectId",
			"RecipientDistributionListObjectId",
			"DispatchDelivery",
			"PlayAfterMessage",
			"AfterMessageAction",
			"AfterMessageTargetConversation",
			"AfterMessageTargetHandlerObjectId",
			"AfterMessageTargetDirectoryHandlerObjectId",
			"AfterMessageUserTargetConversation",
			"UserAlias"
		],
		"UnityConnection9Infrastructure callhandlerxferoptions": [
			"kurmiLabel",
			"URI",
			"TransferOptionType",
			"Enabled",
			"TimeExpires",
			"Action",
			"UsePrimaryExtension",
			"Extension",
			"TransferType",
			"TransferRings",
			"PlayTransferPrompt",
			"TransferHoldingMode",
			"TransferAnnounce",
			"TransferIntroduce",
			"TransferConfirm",
			"TransferScreening"
		],
		"UnityConnection9Infrastructure callhandlermenuentries": [
			"kurmiLabel",
			"URI",
			"TouchtoneKey",
			"locked",
			"action",
			"TransferNumber",
			"DisplayName",
			"TransferType",
			"TransferRings",
			"targetConversation",
			"TargetHandlerObjectId",
			"TargetDirectoryHandlerObjectId",
			"UserTargetConversation",
			"UserAlias"
		],
		"UnityConnection9Infrastructure callhandlergreetings": [
			"kurmiLabel",
			"URI",
			"GreetingType",
			"Enabled",
			"TimeExpires",
			"PlayWhat",
			"PlayRecordMessagePrompt",
			"IgnoreDigits",
			"EnableTransfer",
			"Reprompts",
			"RepromptDelay",
			"AfterGreetingAction",
			"AfterGreetingTargetConversation",
			"AfterGreetingTargetHandlerObjectId",
			"AfterGreetingTargetDirectoryHandlerObjectId",
			"AfterGreetingUserTargetConversation",
			"UserAlias"
		],
		"UnityConnection9Infrastructure callhandlertemplates": [
			"URI",
			"displayName",
			"phoneSystem",
			"scheduleSet",
			"useDefaultTimeZone",
			"timeZone",
			"useDefaultLanguage",
			"UseCallLanguage",
			"language",
			"PartitionObjectId",
			"InheritSearchSpaceFromCall",
			"CallSearchSpaceObjectId",
			"MaxMsgLen",
			"EditMsg",
			"SendUrgentMsg",
			"SendPrivateMsg",
			"SendSecureMsg",
			"MessageRecipient",
			"RecipientSubscriberObjectId",
			"recipientDistributionListObjectId",
			"DispatchDelivery",
			"PlayAfterMessage",
			"AfterMessageAction",
			"AfterMessageTargetConversation",
			"AfterMessageTargetHandlerObjectId",
			"AfterMessageTargetDirectoryHandlerObjectId",
			"AfterMessageUserTargetConversation",
			"UserAlias"
		],
		"UnityConnection9Infrastructure portgroups": [
			"URI",
			"phoneSystem",
			"MediaPortGroupTemplateObjectId",
			"TelephonyIntegrationMethodEnum",
			"displayName",
			"SkinnyDevicePrefix",
			"MwiOnCode",
			"MwiOffCode",
			"SipRegisterWithProxyServer",
			"SipDoAuthenticate",
			"SipAuthenticateAsUser",
			"SipAuthenticatePassword",
			"SipContactLineName",
			"MediaSipSecurityProfileObjectId",
			"SipTransportProtocolEnum",
			"SipEnableNextGenSecurity",
			"MediaCertificateObjectId",
			"SipTLSModeEnum",
			"SipDoSRTP",
			"EnableMWI",
			"MwiMinRequestIntervalMs",
			"MwiMaxConcurrentRequests",
			"MwiRetryCountOnSuccess",
			"MwiRetryIntervalOnSuccessMs",
			"Reset"
		],
		"UnityConnection9Infrastructure portgroupservers": [
			"kurmiLabel",
			"URI",
			"MediaRemoteServiceEnum",
			"MediaPortGroupObjectId",
			"Precedence",
			"HostOrIPAddress",
			"HostOrIPAddressV6",
			"Port",
			"TlsPort",
			"SkinnyStateMachineEnum"
		],
		"UnityConnection9Infrastructure portgroupcodecs": [
			"kurmiLabel",
			"URI",
			"MediaPortGroupObjectId",
			"RtpCodecDefObjectId",
			"PreferredPacketSizeMs",
			"Preference"
		],
		"UnityConnection9Infrastructure sipsecurityprofiles": [
			"URI",
			"Port",
			"DoTLS",
			"displayName"
		],
		"UnityConnection9Infrastructure ports": [
			"URI",
			"CapEnabled",
			"phoneSystem",
			"MediaPortGroupObjectId",
			"VmsServerObjectId",
			"CapAnswer",
			"CapNotification",
			"CapMWI",
			"CapTrapConnection",
			"SkinnySecurityModeEnum",
			"displayName"
		],
		"UnityConnection9Infrastructure usertemplateroles": [
			"kurmiLabel",
			"URI",
			"UserObjectId",
			"RoleObjectId"
		],
		"UnityConnection9Infrastructure externalservices": [
			"URI",
			"ServerType",
			"IsEnabled",
			"displayName",
			"AuthenticationMode",
			"SecurityTransportType",
			"ValidateServerCertificate",
			"ProxyServer",
			"ExchDoAutodiscover",
			"ExchOrgDomain",
			"ExchSite",
			"ExchDoAutodiscover2003",
			"LdapSecurityTransportType",
			"LdapValidateServerCertificate",
			"Server",
			"TransferExtensionDialString",
			"ExchServerType",
			"ServiceAlias",
			"ServicePassword",
			"SupportsTtsOfEmailCapability",
			"SupportsCalendarCapability",
			"SupportsMailboxSynchCapability",
			"MailboxSynchEmailAction",
			"MailboxSynchFaxAction",
			"SupportsMeetingCapability"
		],
		"UnityConnection9Infrastructure messageagingpolicies": [
			"URI",
			"DisplayName",
			"Enabled"
		],
		"UnityConnection9Infrastructure messageagingrules": [
			"kurmiLabel",
			"URI",
			"policy",
			"RuleDescription",
			"Enabled",
			"Days",
			"Secure",
			"SendNotification",
			"NotificationDays"
		],
		"UnityConnection9Infrastructure axlservers": [
			"kurmiLabel",
			"URI",
			"phoneSystem",
			"Precedence",
			"HostOrIPAddress",
			"Port",
			"CcmAXLUser",
			"CcmAXLPassword"
		],
		"UnityConnection9Infrastructure authenticationrules": [
			"URI",
			"DisplayName",
			"MaxHacks",
			"HackResetTime",
			"LockoutDuration",
			"MinDuration",
			"MaxDays",
			"ExpiryWarningDays",
			"MinLength",
			"PrevCredCount",
			"TrivialCredChecking"
		],
		"UnityConnection9Infrastructure directoryhandlers": [
			"URI",
			"DisplayName",
			"useDefaultLanguage",
			"UseCallLanguage",
			"language",
			"dtmfAccessId",
			"PartitionObjectId",
			"VoiceEnabled",
			"SpeechConfidenceThreshold",
			"PlayAllNames",
			"SearchScope",
			"SearchScopeObjectId",
			"ScopeObjectCosObjectId",
			"ScopeObjectDistributionListObjectId",
			"ScopeObjectSearchSpaceObjectId",
			"SearchByFirstName",
			"AutoRoute",
			"MenuStyle",
			"SayExtension",
			"MaxMatches",
			"StartDialDelay",
			"EndDialDelay",
			"Tries",
			"ExitAction",
			"ExitTargetConversation",
			"ExitCallHandlerObjectId",
			"ExitDirectoryHandlerObjectId",
			"NoInputAction",
			"NoInputTargetConversation",
			"NoInputCallHandlerObjectId",
			"NoInputDirectoryHandlerObjectId",
			"NoSelectionAction",
			"NoSelectionTargetConversation",
			"NoSelectionCallHandlerObjectId",
			"NoSelectionDirectoryHandlerObjectId",
			"ZeroAction",
			"ZeroTargetConversation",
			"ZeroCallHandlerObjectId",
			"ZeroDirectoryHandlerObjectId",
			"UseCustomGreeting"
		],
		"UnityConnection9Infrastructure mailboxstores": [
			"URI",
			"DisplayName",
			"MailDatabase",
			"Server",
			"Mounted",
			"TotalSizeOfMailbox",
			"TimeAtWhichSizeCalculated",
			"MaxSizeMB",
			"Undeletable"
		],
		"UnityConnection9Infrastructure unitytenants": [
			"URI",
			"CreationDate",
			"Alias",
			"Name",
			"SmtpDomain",
			"Description",
			"MailboxStoreObjectId",
			"TimeZone",
			"Language",
			"UsePilotNumberForRouting",
			"PilotNumber",
			"MediaSwitchObjectId"
		],
		"Box boxuser": [
			"name",
			"login",
			"id",
			"role",
			"language",
			"space_amount",
			"can_see_managed_users",
			"is_sync_enabled",
			"status",
			"job_title",
			"phone",
			"address",
			"is_exempt_from_device_limits",
			"is_exempt_from_login_verification"
		],
		"Box folders": [
			"name",
			"id"
		],
		"Box collaborations": [
			"id",
			"status",
			"role",
			"item"
		],
		"Directory directoryentry": [
			"commonName",
			"userId",
			"guid",
			"firstname",
			"lastName",
			"directoryEmail",
			"password",
			"directoryNumber",
			"secondDirectoryNumber",
			"faxNumber",
			"ou",
			"deactivateUser",
			"pwdNotRequired",
			"pwdNeverExpires"
		],
		"DirectoryOpenLDAP directoryentryopenldap": [
			"userId",
			"firstname",
			"lastName",
			"commonName",
			"directoryEmail",
			"password",
			"directoryNumber",
			"secondDirectoryNumber",
			"faxNumber"
		],
		"LyncAD lyncaduser": [
			"userId",
			"firstName",
			"lastName",
			"commonName",
			"displayName",
			"email",
			"password",
			"ou",
			"deactivateUser",
			"pwdNotRequired",
			"pwdNeverExpires"
		],
		"Broadsoft bs_enterprise": [
			"serviceProviderId",
			"isEnterprise",
			"defaultDomain",
			"serviceProviderName",
			"supportEmail"
		],
		"Broadsoft bs_ent_routingprofile": [
			"serviceProviderId",
			"routingProfile"
		],
		"Broadsoft bs_ent_domain": [
			"serviceProviderId",
			"domain"
		],
		"Broadsoft bs_ent_ncos": [
			"serviceProviderId",
			"networkClassOfService",
			"defaultNetworkClassOfService"
		],
		"Broadsoft bs_ent_number": [
			"serviceProviderId",
			"phoneNumber1",
			"minPhoneNumber",
			"maxPhoneNumber"
		],
		"Broadsoft bs_ent_servicepack": [
			"serviceProviderId",
			"servicePackName",
			"servicePackDescription",
			"isAvailableForUse",
			"servicePackQuantity",
			"serviceName"
		],
		"Broadsoft bs_ent_service": [
			"serviceProviderId",
			"serviceName",
			"authorizedQuantity",
			"assigned",
			"limited",
			"allocated",
			"licensed",
			"servicePackAllocation",
			"userAssignable",
			"servicePackAssignable"
		],
		"Broadsoft bs_group": [
			"serviceProviderId",
			"groupId",
			"defaultDomain",
			"userLimit",
			"groupName",
			"callingLineIdName",
			"timeZone",
			"locationDialingCode"
		],
		"Broadsoft bs_grp_routingprofile": [
			"serviceProviderId",
			"groupId",
			"routingProfile"
		],
		"Broadsoft bs_grp_domain": [
			"serviceProviderId",
			"groupId",
			"domain"
		],
		"Broadsoft bs_grp_ncos": [
			"serviceProviderId",
			"groupId",
			"networkClassOfService",
			"defaultNetworkClassOfService"
		],
		"Broadsoft bs_grp_numbers": [
			"serviceProviderId",
			"groupId",
			"phoneNumber1",
			"minPhoneNumber",
			"maxPhoneNumber",
			"activated"
		],
		"Broadsoft bs_grp_service": [
			"serviceProviderId",
			"groupId",
			"groupServiceName",
			"authorizedQuantityGroupService"
		],
		"Broadsoft bs_grp_service_sp": [
			"serviceProviderId",
			"groupId",
			"servicePackName",
			"authorizedQuantityServicePack"
		],
		"Broadsoft bs_grp_service_us": [
			"serviceProviderId",
			"groupId",
			"userServiceName",
			"assigned",
			"limited",
			"authorizedQuantityUserService",
			"usage",
			"licensed",
			"allowed",
			"userAssignable",
			"groupServiceAssignable"
		],
		"Broadsoft bs_grouptrunkgroup": [
			"serviceProviderId",
			"groupId",
			"name"
		],
		"Broadsoft bs_extensiondialing": [
			"serviceProviderId",
			"groupId",
			"minExtensionLength",
			"maxExtensionLength",
			"defaultExtensionLength"
		],
		"Broadsoft bs_sys_devicetypelist": [
			"deviceType",
			"allowConference",
			"allowMusicOnHold",
			"onlyConference",
			"onlyVideoCapable",
			"onlyOptionalIpAddress"
		],
		"Broadsoft bs_sys_domain": [
			"domain"
		],
		"Broadsoft bs_sys_language": [
			"language",
			"locale",
			"encoding"
		],
		"Broadsoft bs_sys_ncos": [
			"name",
			"description",
			"networkTranslationIndex"
		],
		"Broadsoft bs_sys_routingprofile": [
			"routingProfile"
		],
		"Broadsoft bs_sys_stateorpro": [
			"key",
			"displayName"
		],
		"Broadsoft bs_sys_timezone": [
			"key",
			"displayName"
		],
		"Broadsoft broadsoftdeviceprofile": [
			"serviceProviderId",
			"groupId",
			"deviceName",
			"deviceType",
			"protocol",
			"netAddress",
			"port",
			"transportProtocol",
			"outboundProxyServerNetAddress",
			"stunServerNetAddress",
			"macAddress",
			"serialNumber",
			"description",
			"physicalLocation",
			"numberOfPorts",
			"numberOfAssignedPorts",
			"status",
			"configurationMode",
			"mobilityManagerProvisioningURL",
			"mobilityManagerProvisioningUserName",
			"mobilityManagerProvisioningPassword",
			"mobilityManagerDefaultOriginatingServiceKey",
			"mobilityManagerDefaultTerminatingServiceKey",
			"useCustomUserNamePassword",
			"userName",
			"version"
		],
		"Broadsoft broadsoftuser": [
			"serviceProviderId",
			"groupId",
			"userId",
			"domain",
			"lastName",
			"firstName",
			"callingLineIdLastName",
			"callingLineIdFirstName",
			"callingLineIdPhoneNumber",
			"nameDialingLastName",
			"nameDialingFirstName",
			"hiraganaLastName",
			"hiraganaFirstName",
			"password",
			"pin",
			"language",
			"timeZone",
			"networkClassOfService",
			"title",
			"pagerPhoneNumber",
			"mobilePhoneNumber",
			"emailAddress",
			"yahooId",
			"addressLocation",
			"addressLine1",
			"addressLine2",
			"city",
			"stateOrProvince",
			"zipOrPostalCode",
			"country",
			"phoneNumber",
			"extension",
			"numPlanExtensionCategory",
			"numPlanExtensionAdvancedType",
			"bsftUserDeviceProfile",
			"bsftUserTrunk",
			"sipAlias1",
			"sipAlias2",
			"sipAlias3"
		],
		"Broadsoft bs_alternatenumbers": [
			"userId",
			"distinctiveRing",
			"phoneNumber01",
			"phoneNumber02",
			"phoneNumber03",
			"phoneNumber04",
			"phoneNumber05",
			"phoneNumber06",
			"phoneNumber07",
			"phoneNumber08",
			"phoneNumber09",
			"phoneNumber10"
		],
		"Broadsoft bs_callreturn": [
			"userId"
		],
		"Broadsoft bs_directedcallpickup": [
			"userId"
		],
		"Broadsoft bs_nwaycall": [
			"userId"
		],
		"Broadsoft bs_threewaycall": [
			"userId"
		],
		"Broadsoft bs_tpartyvm": [
			"userId",
			"isActive",
			"busyRedirectToVoiceMail",
			"noAnswerRedirectToVoiceMail",
			"serverSelection",
			"userServer",
			"mailboxIdType",
			"mailboxURL",
			"noAnswerNumberOfRings",
			"alwaysRedirectToVoiceMail",
			"outOfPrimaryZoneRedirectToVoiceMail"
		],
		"Broadsoft bs_useranonymouscallr": [
			"userId",
			"isActive"
		],
		"Broadsoft bs_useraoc": [
			"userId",
			"isActive",
			"aocType"
		],
		"Broadsoft bs_userauthentication": [
			"userId",
			"userName",
			"newPassword"
		],
		"Broadsoft bs_userautocallback": [
			"userId",
			"isActive"
		],
		"Broadsoft bs_userbasiccalllogs": [
			"userId"
		],
		"Broadsoft bs_userbtbcdesktopvideo": [
			"userId"
		],
		"Broadsoft bs_userbtbcmobilevideo": [
			"userId"
		],
		"Broadsoft bs_userbtbctabletvideo": [
			"userId"
		],
		"Broadsoft bs_usercallforwardalways": [
			"userId",
			"isActive",
			"forwardToPhoneNumber",
			"isRingSplashActive"
		],
		"Broadsoft bs_usercallforwdbusy": [
			"userId",
			"isActive",
			"forwardToPhoneNumber"
		],
		"Broadsoft bs_usercallforwdnoansw": [
			"userId",
			"isActive",
			"forwardToPhoneNumber",
			"numberOfRings"
		],
		"Broadsoft bs_usercallforwdnotreach": [
			"userId",
			"isActive",
			"forwardToPhoneNumber"
		],
		"Broadsoft bs_usercallingnamedeliv": [
			"userId",
			"isActiveForExternalCalls",
			"isActiveForInternalCalls"
		],
		"Broadsoft bs_usercallingnamer": [
			"userId",
			"isActive"
		],
		"Broadsoft bs_usercallingnumberd": [
			"userId",
			"isActiveForExternalCalls",
			"isActiveForInternalCalls"
		],
		"Broadsoft bs_usercallmanager": [
			"userId",
			"launchOnLogin"
		],
		"Broadsoft bs_usercallrec": [
			"userId",
			"recordingOption",
			"pauseResumeNotification",
			"enableCallRecordingAnnouncement",
			"enableRecordCallRepeatWarningTone",
			"recordCallRepeatWarningToneTimerSeconds",
			"enableVoiceMailRecording"
		],
		"Broadsoft bs_usercalltransfer": [
			"userId",
			"isRecallActive",
			"recallNumberOfRings",
			"useDiversionInhibitorForBlindTransfer",
			"useDiversionInhibitorForConsultativeCalls",
			"enableBusyCampOn",
			"busyCampOnSeconds"
		],
		"Broadsoft bs_usercallwaiting": [
			"userId",
			"isActive",
			"disableCallingLineIdDelivery"
		],
		"Broadsoft bs_usercbusercontrol": [
			"userId",
			"enableProfile",
			"oldPasscode",
			"newPasscode",
			"resetLockout"
		],
		"Broadsoft bs_userchargenumber": [
			"userId",
			"phoneNumber",
			"useChargeNumberForEnhancedTranslations",
			"sendChargeNumberToNetwork"
		],
		"Broadsoft bs_userclidb": [
			"userId",
			"isActive"
		],
		"Broadsoft bs_userclidblockoverr": [
			"userId",
			"isActive"
		],
		"Broadsoft bs_userclip": [
			"userId"
		],
		"Broadsoft bs_userclir": [
			"userId",
			"isActive"
		],
		"Broadsoft bs_usercollashar": [
			"userId"
		],
		"Broadsoft bs_usercustoriginatrace": [
			"userId"
		],
		"Broadsoft bs_userdonotdisturb": [
			"userId",
			"isActive",
			"ringSplash"
		],
		"Broadsoft bs_userexternalclid": [
			"userId",
			"isActive"
		],
		"Broadsoft bs_userintegratedimp": [
			"userId",
			"isActive"
		],
		"Broadsoft bs_userinternalclid": [
			"userId",
			"isActive"
		],
		"Broadsoft bs_userlastnumberr": [
			"userId"
		],
		"Broadsoft bs_userlbcr": [
			"userId"
		],
		"Broadsoft bs_usermca": [
			"userId"
		],
		"Broadsoft bs_usermusiconhold": [
			"userId",
			"isActive"
		],
		"Broadsoft bs_usernpa": [
			"userId",
			"enable"
		],
		"Broadsoft bs_userremoteoffice": [
			"userId",
			"isActive",
			"remoteOfficePhoneNumber"
		],
		"Broadsoft bs_userscalinedevice": [
			"linePort",
			"userId",
			"deviceName",
			"deviceNameSufix",
			"userscalinedevice",
			"isActive",
			"allowOrigination",
			"allowTermination"
		],
		"Broadsoft bs_userservicepack": [
			"userId",
			"servicePackName"
		],
		"Broadsoft bs_usersharedcallapp": [
			"userId",
			"alertAllAppearancesForClickToDialCalls",
			"alertAllAppearancesForGroupPagingCalls",
			"allowSCACallRetrieve",
			"multipleCallArrangementIsActive",
			"allowBridgingBetweenLocations",
			"enableCallParkNotification",
			"bridgeWarningTone"
		],
		"Broadsoft bs_userspeeddial100": [
			"userId"
		],
		"Broadsoft bs_userspeeddial8": [
			"userId"
		],
		"Broadsoft bs_uservoicemessagmanag": [
			"userId",
			"isActive",
			"processing",
			"voiceMessageDeliveryEmailAddress",
			"usePhoneMessageWaitingIndicator",
			"sendVoiceMessageNotifyEmail",
			"voiceMessageNotifyEmailAddress",
			"sendCarbonCopyVoiceMessage",
			"voiceMessageCarbonCopyEmailAddress",
			"transferOnZeroToPhoneNumber",
			"transferPhoneNumber",
			"alwaysRedirectToVoiceMail",
			"busyRedirectToVoiceMail",
			"noAnswerRedirectToVoiceMail",
			"outOfPrimaryZoneRedirectToVoiceMail"
		],
		"CiscoHCMF hcmf_customer": [
			"id",
			"shortName",
			"extendedName",
			"serviceProviderId"
		],
		"CiscoHCMF hcmf_cucmcluster": [
			"id",
			"name",
			"typeAppVersion"
		],
		"CiscoHCMF hcmf_cucm": [
			"id",
			"name",
			"id_0",
			"typeCredential_0",
			"userID_0",
			"password_CommunityString_0",
			"typeSNMPAccess_0",
			"id_1",
			"typeCredential_1",
			"userID_1",
			"password_CommunityString_1",
			"typeSNMPAccess_1",
			"id_2",
			"typeCredential_2",
			"userID_2",
			"password_CommunityString_2",
			"typeSNMPAccess_2",
			"id_3",
			"typeCredential_3",
			"userID_3",
			"password_CommunityString_3",
			"typeSNMPAccess_3",
			"id_4",
			"typeCredential_4",
			"userID_4",
			"password_CommunityString_4",
			"typeSNMPAccess_4",
			"id_net_0",
			"typeAddressSpace_net_0",
			"domain_net_0",
			"hostShortNameOnly_net_0",
			"iPAddrV4_net_0",
			"iPAddrV6_net_0",
			"isSRVAddress_net_0",
			"id_net_1",
			"typeAddressSpace_net_1",
			"domain_net_1",
			"hostShortNameOnly_net_1",
			"iPAddrV4_net_1",
			"iPAddrV6_net_1",
			"isSRVAddress_net_1",
			"id_net_2",
			"typeAddressSpace_net_2",
			"domain_net_2",
			"hostShortNameOnly_net_2",
			"iPAddrV4_net_2",
			"iPAddrV6_net_2",
			"isSRVAddress_net_2",
			"id_net_3",
			"typeAddressSpace_net_3",
			"domain_net_3",
			"hostShortNameOnly_net_3",
			"iPAddrV4_net_3",
			"iPAddrV6_net_3",
			"isSRVAddress_net_3",
			"id_net_4",
			"typeAddressSpace_net_4",
			"domain_net_4",
			"hostShortNameOnly_net_4",
			"iPAddrV4_net_4",
			"iPAddrV6_net_4",
			"isSRVAddress_net_4",
			"isUCPublisher",
			"typeCucmServer"
		],
		"CiscoHCMF hcmf_cucxncluster": [
			"id",
			"name",
			"typeAppVersion"
		],
		"CiscoHCMF hcmf_cucxn": [
			"id",
			"name",
			"id_0",
			"typeCredential_0",
			"userID_0",
			"password_CommunityString_0",
			"typeSNMPAccess_0",
			"id_1",
			"typeCredential_1",
			"userID_1",
			"password_CommunityString_1",
			"typeSNMPAccess_1",
			"id_2",
			"typeCredential_2",
			"userID_2",
			"password_CommunityString_2",
			"typeSNMPAccess_2",
			"id_3",
			"typeCredential_3",
			"userID_3",
			"password_CommunityString_3",
			"typeSNMPAccess_3",
			"id_4",
			"typeCredential_4",
			"userID_4",
			"password_CommunityString_4",
			"typeSNMPAccess_4",
			"id_net_0",
			"typeAddressSpace_net_0",
			"domain_net_0",
			"hostShortNameOnly_net_0",
			"iPAddrV4_net_0",
			"iPAddrV6_net_0",
			"isSRVAddress_net_0",
			"id_net_1",
			"typeAddressSpace_net_1",
			"domain_net_1",
			"hostShortNameOnly_net_1",
			"iPAddrV4_net_1",
			"iPAddrV6_net_1",
			"isSRVAddress_net_1",
			"id_net_2",
			"typeAddressSpace_net_2",
			"domain_net_2",
			"hostShortNameOnly_net_2",
			"iPAddrV4_net_2",
			"iPAddrV6_net_2",
			"isSRVAddress_net_2",
			"id_net_3",
			"typeAddressSpace_net_3",
			"domain_net_3",
			"hostShortNameOnly_net_3",
			"iPAddrV4_net_3",
			"iPAddrV6_net_3",
			"isSRVAddress_net_3",
			"id_net_4",
			"typeAddressSpace_net_4",
			"domain_net_4",
			"hostShortNameOnly_net_4",
			"iPAddrV4_net_4",
			"iPAddrV6_net_4",
			"isSRVAddress_net_4",
			"isUCPublisher"
		],
		"kurmicore coreuser": [
			"userId",
			"firstName",
			"lastName",
			"bouquet",
			"email",
			"directoryNumber",
			"pin",
			"password",
			"passwordLastUpdate",
			"pinLastUpdate"
		],
		"Imagicle imagiclequeue": [
			"id",
			"name",
			"phoneNumber",
			"priorityOverride",
			"priorityValue",
			"loginLogoutPhoneNumberOverride",
			"loginLogoutPhoneNumberValue",
			"maxWaitingCallsOverride",
			"maxWaitingCallsValue"
		],
		"Polycom polycomuser": [
			"userId",
			"callsPerLineKey",
			"callsPerLineKeyVVX101",
			"callsPerLineKeyVVX201",
			"address1",
			"authpassword1",
			"authuserId1",
			"authloginCredentialType1",
			"extension1",
			"insertOBPAddressInRoute1",
			"label1",
			"lineAddress1",
			"outboundProxyaddress1",
			"pin1",
			"useTelUriAsLineLabel1",
			"address2",
			"authpassword2",
			"authuserId2",
			"extension2",
			"insertOBPAddressInRoute2",
			"label2",
			"lineAddress2",
			"outboundProxyaddress2",
			"pin2",
			"useTelUriAsLineLabel2"
		],
		"Polycom polycomsite": [
			"siteName",
			"directedCallPickupString",
			"disableMobileCalls",
			"parkedCallRetrieveString",
			"parkedCallString",
			"stickyAutoLineSeize",
			"urlNumberModeToggling",
			"enabled",
			"key",
			"remoteActiveHoldAsActive",
			"onHookDialing",
			"showPrompt",
			"collapseDuplicates",
			"filterAll",
			"filterEnabled",
			"grouping",
			"logConsultationCalls",
			"size",
			"journal",
			"terminated",
			"corporateDirectoryenabled",
			"address",
			"port",
			"transport",
			"expires",
			"register",
			"retryTimeOut",
			"retryMaxCount",
			"expireslineSeize"
		],
		"Polycom polycomdevice": [
			"macAddress",
			"appFilePath",
			"configFiles",
			"miscFiles",
			"logFileDirectory",
			"overridesDirectory",
			"contactsDirectory",
			"licenseDirectory",
			"userProfilesDirectory",
			"callListsDirectory",
			"corefileDirectory",
			"appFilePathSPIP300",
			"configFilesSPIP300",
			"appFilePathSPIP500",
			"configFilesSPIP500",
			"appFilePathSPIP301",
			"configFilesSPIP301",
			"appFilePathSPIP320",
			"configFilesSPIP320",
			"appFilePathSPIP330",
			"configFilesSPIP330",
			"appFilePathSPIP430",
			"configFilesSPIP430",
			"appFilePathSPIP501",
			"configFilesSPIP501",
			"appFilePathSPIP600",
			"configFilesSPIP600",
			"appFilePathSPIP601",
			"configFilesSPIP601",
			"appFilePathSPIP670",
			"configFilesSPIP670",
			"appFilePathSSIP4000",
			"configFilesSSIP4000",
			"appFilePathSSIP6000",
			"configFilesSSIP6000",
			"appFilePathSSIP7000",
			"configFilesSSIP7000"
		],
		"Polycom polycomdevicephone": [
			"macAddress",
			"deviceBaseProfile",
			"deviceBaseProfileSet",
			"deviceSet",
			"httpdCfgEnabled",
			"httpdCfgSecureTunnelRequired",
			"httpdEnabled",
			"authLoginCredentialType"
		],
		"FilesServiceNumber servicenumber": [
			"fileName",
			"Monday_NumberOfRedirections",
			"Tuesday_NumberOfRedirections",
			"Wednesday_NumberOfRedirections",
			"Thursday_NumberOfRedirections",
			"Friday_NumberOfRedirections",
			"Saturday_NumberOfRedirections",
			"Sunday_NumberOfRedirections",
			"NumberOfDays",
			"Day1_Date",
			"Day1_Time",
			"Day1_Target",
			"Day2_Date",
			"Day2_Time",
			"Day2_Target",
			"Day3_Date",
			"Day3_Time",
			"Day3_Target"
		],
		"mysql table1": [
			"column1",
			"column2"
		],
		"testUCX distributionlistsuc": [
			"ObjectId",
			"Alias",
			"DisplayName",
			"LocationObjectId",
			"PartitionObjectId"
		],
		"LyncServer lyncuser": [
			"Identity",
			"Guid",
			"PinCode",
			"RegistrarPool",
			"SipAddress",
			"SipDomain",
			"Hybrid",
			"Telephony",
			"DirectoryNumber",
			"Extension",
			"NumPlanCategory",
			"NumPlanAdvancedType",
			"NumPlanDdiCategory",
			"NumPlanDdiAdvancedType",
			"NumPlanCustomField1",
			"NumPlanCustomField2",
			"NumPlanCustomField3",
			"PrivateLine",
			"PrivateLineExtension",
			"NumPlanPrivateCategory",
			"NumPlanPrivateAdvancedType",
			"NumPlanPrivateDdiCategory",
			"NumPlanPrivateDdiAdvancedType",
			"LineServerURI",
			"DialPlan",
			"VoicePolicy",
			"VoicePolicyDefault",
			"VoicePolicyName",
			"VoicePolicyAllowCallForwarding",
			"VoicePolicyAllowPSTNReRouting",
			"VoicePolicyAllowSimulRing",
			"VoicePolicyEnableBWPolicyOverride",
			"VoicePolicyEnableCallPark",
			"VoicePolicyEnableCallTransfer",
			"VoicePolicyEnableDelegation",
			"VoicePolicyEnableMaliciousCallTracing",
			"VoicePolicyEnableTeamCall",
			"VoicePolicyPreventPSTNTollBypass",
			"VoicePolicyEnableVoicemailEscapeTimer",
			"VoicePolicyPSTNVoicemailEscapeTimer",
			"VoicePolicyPstnUsages",
			"VoicePolicyCallForwardingSimulRingUsageType",
			"VoicePolicyCustomCallForwardingSimulRingUsages",
			"ArchivingPolicy",
			"ClientPolicy",
			"ClientVersionPolicy",
			"ExternalAccessPolicy",
			"LocationPolicy",
			"PinPolicy",
			"ConferencingPolicy",
			"PresencePolicy",
			"PersistentChatPolicy",
			"MobilityPolicy",
			"HostedVoiceMail",
			"HostedVoicemailPolicy",
			"CallForwardUpdate",
			"CallForwardType",
			"CallForwardSimultaneousUri",
			"CallForwardTeamRingDelay",
			"CallForwardTeamRingTarget",
			"CallForwardDelegateTarget",
			"CallForwardNoAnswerDelay",
			"CallForwardNoAnswerForward",
			"CallForwardTargetUri"
		],
		"LyncServer lyncgroup": [
			"Identity",
			"Name",
			"Description",
			"Service",
			"ParticipationPolicy",
			"AgentAlertTime",
			"RoutingMethod",
			"Agents",
			"DistributionGroupAddress",
			"Users"
		],
		"LyncServer lyncqueue": [
			"Identity",
			"Name",
			"Description",
			"Service",
			"Timeout",
			"TimeoutThreshold",
			"TimeoutAction",
			"TimeoutUri",
			"TimeoutQueue",
			"Overflow",
			"OverflowThreshold",
			"OverflowAction",
			"OverflowUri",
			"OverflowCandidate",
			"OverflowQueue"
		],
		"LyncServer lyncworkflow": [
			"Identity",
			"Name",
			"Description",
			"Service",
			"PrimaryUri",
			"Extension",
			"LineUri",
			"DisplayNumber",
			"NumPlanCategory",
			"NumPlanAdvancedType",
			"NumPlanDDICategory",
			"NumPlanDDIAdvancedType",
			"Active",
			"EnabledForFederation",
			"Anonymous",
			"Language",
			"TimeZone",
			"WelcomeMessage",
			"WelcomeMessageAudio",
			"fileUpload",
			"WelcomeMessageAudioFileName",
			"WelcomePrompt",
			"BusinessHoursID",
			"NonBusinessHoursMessage",
			"NonBusinessHoursMessageAudio",
			"NonBusinessHoursPrompt",
			"NonBusinessHoursAction",
			"NonBusinessHoursSipAddress"
		],
		"LyncServer lynccommonareaphone": [
			"Identity",
			"DisplayName",
			"PinCode",
			"RegistrarPool",
			"SipAddress",
			"SipDomain",
			"OU",
			"Description",
			"ExchangeArchivingPolicy",
			"EnterpriseVoiceEnabled",
			"Extension",
			"DirectoryNumber",
			"DisplayNumber",
			"NumPlanCategory",
			"NumPlanAdvancedType",
			"NumPlanDdiCategory",
			"NumPlanDdiAdvancedType",
			"NumPlanCustomField1",
			"NumPlanCustomField2",
			"NumPlanCustomField3",
			"DialPlan",
			"VoicePolicy",
			"VoicePolicyDefault",
			"VoicePolicyName",
			"VoicePolicyAllowCallForwarding",
			"VoicePolicyAllowPSTNReRouting",
			"VoicePolicyAllowSimulRing",
			"VoicePolicyEnableBWPolicyOverride",
			"VoicePolicyEnableCallPark",
			"VoicePolicyEnableCallTransfer",
			"VoicePolicyEnableDelegation",
			"VoicePolicyEnableMaliciousCallTracing",
			"VoicePolicyEnableTeamCall",
			"VoicePolicyPreventPSTNTollBypass",
			"VoicePolicyEnableVoicemailEscapeTimer",
			"VoicePolicyPSTNVoicemailEscapeTimer",
			"VoicePolicyPstnUsages",
			"VoicePolicyCallForwardingSimulRingUsageType",
			"VoicePolicyCustomCallForwardingSimulRingUsages",
			"ArchivingPolicy",
			"ClientPolicy",
			"ClientVersionPolicy",
			"ExternalAccessPolicy",
			"LocationPolicy",
			"ConferencingPolicy",
			"PresencePolicy",
			"PersistentChatPolicy",
			"MobilityPolicy",
			"HostedVoicemailPolicy"
		],
		"LyncServer lyncanalogdevice": [
			"Identity",
			"DisplayName",
			"RegistrarPool",
			"SipAddress",
			"SipDomain",
			"OU",
			"AnalogFax",
			"Gateway",
			"ExchangeArchivingPolicy",
			"EnterpriseVoiceEnabled",
			"Extension",
			"DirectoryNumber",
			"DisplayNumber",
			"NumPlanCategory",
			"NumPlanAdvancedType",
			"NumPlanDdiCategory",
			"NumPlanDdiAdvancedType",
			"NumPlanCustomField1",
			"NumPlanCustomField2",
			"NumPlanCustomField3",
			"DialPlan",
			"VoicePolicy",
			"VoicePolicyDefault",
			"VoicePolicyName",
			"VoicePolicyAllowCallForwarding",
			"VoicePolicyAllowPSTNReRouting",
			"VoicePolicyAllowSimulRing",
			"VoicePolicyEnableBWPolicyOverride",
			"VoicePolicyEnableCallPark",
			"VoicePolicyEnableCallTransfer",
			"VoicePolicyEnableDelegation",
			"VoicePolicyEnableMaliciousCallTracing",
			"VoicePolicyEnableTeamCall",
			"VoicePolicyPreventPSTNTollBypass",
			"VoicePolicyEnableVoicemailEscapeTimer",
			"VoicePolicyPSTNVoicemailEscapeTimer",
			"VoicePolicyPstnUsages",
			"VoicePolicyCallForwardingSimulRingUsageType",
			"VoicePolicyCustomCallForwardingSimulRingUsages",
			"ArchivingPolicy",
			"ClientPolicy",
			"ClientVersionPolicy",
			"ExternalAccessPolicy",
			"LocationPolicy",
			"ConferencingPolicy",
			"PresencePolicy",
			"PersistentChatPolicy",
			"MobilityPolicy",
			"HostedVoicemailPolicy"
		],
		"LyncServer lyncnormalizationrules": [
			"Identity",
			"Name",
			"Description",
			"DialPlan",
			"Pattern",
			"Translation",
			"Priority",
			"IsInternalExtension"
		],
		"LyncServer lyncdialplan": [
			"Identity",
			"SimpleName",
			"Description",
			"DialinConferencingRegion",
			"CountryCode",
			"State",
			"City",
			"ExternalAccessPrefix",
			"OptimizeDeviceDialing"
		],
		"LyncServer lyncvoicepolicy": [
			"Identity",
			"Description",
			"AllowCallForwarding",
			"AllowPSTNReRouting",
			"AllowSimulRing",
			"EnableBWPolicyOverride",
			"EnableCallPark",
			"EnableCallTransfer",
			"EnableDelegation",
			"EnableMaliciousCallTracing",
			"EnableTeamCall",
			"PreventPSTNTollBypass",
			"EnableVoicemailEscapeTimer",
			"PSTNVoicemailEscapeTimer",
			"PstnUsages",
			"CallForwardingSimulRingUsageType",
			"CustomCallForwardingSimulRingUsages"
		],
		"LyncServer lyncpstnusages": [
			"Identity",
			"Usage",
			"Description"
		],
		"LyncServer lyncvoiceroute": [
			"Identity",
			"Description",
			"Priority",
			"NumberPattern",
			"SuppressCallerId",
			"AlternateCallerId",
			"PstnUsages",
			"PstnGatewayList"
		],
		"mitel miteluserdevice": [
			"lastName",
			"firstName",
			"department",
			"location",
			"role",
			"userLanguage",
			"email",
			"idsManageable",
			"number",
			"hotDeskingUser",
			"preferredSet",
			"serviceLevel",
			"localonlyDn",
			"acdAgent",
			"directoryName",
			"primeName",
			"privacy",
			"homeElement",
			"secondaryElement",
			"macAddress",
			"cesid",
			"cosDay",
			"cosNight1",
			"cosNight2",
			"corDay",
			"corNight1",
			"corNight2",
			"externalHotDeskingEnabled",
			"externalHotDeskingDialingPrefix",
			"externalHotDeskingNumber",
			"sipDeviceCapabilities",
			"interconnectNumber",
			"tenantNumber",
			"lockDefaultConfiguration",
			"maxCallHistory",
			"nonBusyExtension",
			"userPin",
			"wirelessPin",
			"desktopAdmin",
			"loginId",
			"password"
		],
		"OpenTouch otcmsuser": [
			"id",
			"salutation",
			"login",
			"firstName",
			"lastName",
			"externalLogin",
			"guiLanguage",
			"tuiLanguage",
			"tuiFormat24h",
			"department",
			"timeZone",
			"dialingRule",
			"presenceId",
			"category",
			"site",
			"tuiPassword",
			"tuiForceEnrollmentOn",
			"guiPassword",
			"sipPassword",
			"personalPhone",
			"personalMobile",
			"companyPhone",
			"companyEmail",
			"nomadic",
			"nomadicSip",
			"nomadicGsm",
			"desktop",
			"tablet",
			"voiceMail",
			"offSiteMobility",
			"conferencing",
			"iceTelApiBasic",
			"iceTelApiAdvanced",
			"iceConferenceApi",
			"licenseVideo",
			"maxAudioLegs",
			"maxApplicationSharingLegs",
			"maxNumberOfLines",
			"telephonicProfile",
			"answerOnly",
			"notifMailRight",
			"notifMail",
			"canSetNotifMail",
			"mailNotifAddress",
			"canSetMailNotifAddress",
			"attachVoiceMessageFiles",
			"deactivateMwi",
			"smsRight",
			"notifSms",
			"notifPhone",
			"notificationNumber",
			"dtmfMapping",
			"addressingByLastNameFirstOn",
			"expertModeOn",
			"skipPasswordOnDC",
			"recordAnnouncement",
			"blockIMWhenBusy"
		],
		"OpenTouch otcmsvmblocalstorage": [
			"id",
			"displayName",
			"voiceMailSystem",
			"greeting",
			"extendedAbsenceOn",
			"alternativeGM",
			"addressingByName",
			"automaticReading",
			"deleteConfirmFlag",
			"assistantMenu",
			"answerOnly",
			"faxNumber",
			"voiceMailProfile"
		],
		"OpenTouch otcmsvmbum": [
			"id",
			"displayName",
			"voiceMailSystem",
			"greeting",
			"extendedAbsenceOn",
			"alternativeGM",
			"addressingByName",
			"automaticReading",
			"deleteConfirmFlag",
			"assistantMenu",
			"answerOnly",
			"faxNumber",
			"voiceMailProfile"
		],
		"OpenTouch otdevicevhe8082": [
			"id",
			"deviceId",
			"deviceComment",
			"directoryNumber",
			"type",
			"nodetype",
			"deviceModel",
			"SIPMaxCall",
			"sipPhoneProfile",
			"TimeZone",
			"MACAddress",
			"IdentificationKey",
			"IdentificationPasswd",
			"ResetMacAddress",
			"DmEnetcfgDns1",
			"DmEnetcfgDns2",
			"DmEnetcfgDns3",
			"HTTPProxyActivated",
			"HTTP_proxy",
			"SIPSrtpAuthentication",
			"SIPPolicy",
			"oxeProxy",
			"oxeProxySIPAuthenticationName",
			"oxeProxySIPAuthenticationPassword",
			"oxeProxySIPPolicy",
			"externalProxy",
			"externalProxyDeviceUri",
			"externalProxySIPAuthenticationName",
			"externalProxySIPAuthenticationPassword",
			"externalProxySIPPolicy",
			"DmEnetcfgSntp",
			"DmEnetcfgSntpRefreshPeriod",
			"TelephonyPresenceAvailable",
			"AudioToneCountry",
			"DateFormat",
			"TimeFormat",
			"DefaultKeyboard",
			"language",
			"BeepEnabled",
			"EnableRouting",
			"EnableUserDetails",
			"EnableUserPresence",
			"DmLldpcfgPowerPriority",
			"DmSecucfgSsh",
			"DmAdminPasswd",
			"DmSecucfgPcportVlanFilter",
			"DmWpa8021xcfgMode",
			"sbcLanEncryption",
			"VideoSupport",
			"SIPLocalVideoPort",
			"VideoCameraCountryFrequency",
			"VideoCameraRefreshCycle",
			"DoorCamDTMF",
			"DoorCamSipName",
			"Video8021p",
			"VideoCallEncodingProfile",
			"VideoCallPacketizationMode",
			"VideoCallProfileLevelId",
			"VideoDiffServ",
			"VideoEncodingProfileHigh",
			"VideoEncodingProfileLow",
			"VideoEncodingProfileMedium",
			"codec_enable_g729",
			"codec_framing_g729",
			"codec_enable_pcmu",
			"codec_framing_pcmu",
			"codec_enable_pcma",
			"codec_framing_pcma",
			"codec_enable_g722",
			"codec_framing_g722",
			"codec_enable_amrwb",
			"codec_framing_amrwb"
		],
		"OpenTouch otdevicevhe8088": [
			"id",
			"deviceId",
			"deviceComment",
			"directoryNumber",
			"type",
			"nodetype",
			"deviceModel",
			"SIPMaxCall",
			"sipPhoneProfile",
			"timezone",
			"MACAddress",
			"IdentificationKey",
			"IdentificationPasswd",
			"DmEnetcfgDns1",
			"DmEnetcfgDns2",
			"DmEnetcfgDns3",
			"SIPSrtpAuthentication",
			"SIPIPFilteringEnabled",
			"SIPPolicy",
			"oxeProxy",
			"oxeProxySIPAuthenticationName",
			"oxeProxySIPAuthenticationPassword",
			"oxeProxySIPPolicy",
			"externalProxy",
			"externalProxyDeviceUri",
			"externalProxySIPAuthenticationName",
			"externalProxySIPAuthenticationPassword",
			"externalProxySIPPolicy",
			"HTTPProxyActivated",
			"HTTP_proxy",
			"DmEnetcfgSntpRefreshPeriod",
			"sbcLanEncryption",
			"DmLldpcfgPowerPriority",
			"DmSecucfgSsh",
			"DmAdminPasswd",
			"DmSecucfgPcportVlanFilter",
			"DmWpa8021xcfgMode",
			"AudioToneCountry",
			"DateFormat",
			"TimeFormat",
			"DefaultKeyboard",
			"language",
			"BeepEnabled",
			"EnableRouting",
			"EnableUserDetails",
			"BacklightForceBrightnessAuto",
			"EnableDisplayToHDMI",
			"VideoSupport",
			"SIPLocalVideoPort",
			"VideoCameraCountryFrequency",
			"VideoCameraRefreshCycle",
			"DoorCamDTMF",
			"DoorCamSipName",
			"Video8021p",
			"VideoCallEncodingProfile",
			"codec_enable_g729",
			"codec_framing_g729",
			"codec_enable_pcmu",
			"codec_framing_pcmu",
			"codec_enable_pcma",
			"codec_framing_pcma",
			"codec_enable_g722",
			"codec_framing_g722",
			"codec_enable_amrwb",
			"codec_framing_amrwb"
		],
		"OpenTouch otdevicevle8012": [
			"id",
			"deviceId",
			"deviceComment",
			"directoryNumber",
			"type",
			"nodetype",
			"deviceModel",
			"SIPMaxCall",
			"sipPhoneProfile",
			"timezone",
			"MACAddress",
			"IdentificationKey",
			"IdentificationPasswd",
			"DmEnetcfgDns1",
			"DmEnetcfgDns2",
			"hostname",
			"outbound_proxy",
			"auth_tag",
			"SIPPolicy",
			"oxeProxy",
			"oxeProxySIPAuthenticationName",
			"oxeProxySIPAuthenticationPassword",
			"oxeProxySIPPolicy",
			"externalProxy",
			"externalProxySIPAuthenticationName",
			"externalProxySIPAuthenticationPassword",
			"externalProxySIPPolicy",
			"sntp_addr",
			"tone_country",
			"admin_password",
			"time_format",
			"supported_language",
			"language",
			"telnet_password",
			"codec_pcma",
			"codec_pcmu",
			"codec_g729a",
			"codec_g723_1"
		],
		"OpenTouch otdevicevle8002": [
			"id",
			"deviceId",
			"deviceComment",
			"directoryNumber",
			"type",
			"nodetype",
			"deviceModel",
			"SIPMaxCall",
			"sipPhoneProfile",
			"timezone",
			"MACAddress",
			"IdentificationKey",
			"IdentificationPasswd",
			"DmEnetcfgDns1",
			"DmEnetcfgDns2",
			"hostname",
			"outbound_proxy",
			"auth_tag",
			"SIPPolicy",
			"oxeProxy",
			"oxeProxySIPAuthenticationName",
			"oxeProxySIPAuthenticationPassword",
			"oxeProxySIPPolicy",
			"externalProxy",
			"externalProxySIPAuthenticationName",
			"externalProxySIPAuthenticationPassword",
			"externalProxySIPPolicy",
			"sntp_addr",
			"tone_country",
			"admin_password",
			"time_format",
			"supported_language",
			"language",
			"telnet_password",
			"codec_pcma",
			"codec_pcmu",
			"codec_g729a",
			"codec_g723_1"
		],
		"OpenTouch otdevice8001": [
			"id",
			"deviceId",
			"deviceComment",
			"directoryNumber",
			"type",
			"nodetype",
			"deviceModel",
			"SIPMaxCall",
			"sipPhoneProfile",
			"timezoneType",
			"DmEnetcfgDns1",
			"DmEnetcfgDns2",
			"sntp_addr",
			"admin_password",
			"time_format",
			"language",
			"codec_g722",
			"codec_pcma",
			"codec_pcmu",
			"codec_g729a"
		],
		"OpenTouch otdevice8038": [
			"id",
			"deviceId",
			"deviceComment",
			"directoryNumber",
			"type",
			"nodetype",
			"deviceModel",
			"SIPMaxCall",
			"sipPhoneProfile",
			"timezone",
			"MACAddress",
			"IdentificationKey",
			"IdentificationPasswd",
			"DmEnetcfgDns1",
			"DmEnetcfgDns2",
			"hostname",
			"auth_tag",
			"SIPPolicy",
			"oxeProxy",
			"oxeProxySIPAuthenticationName",
			"oxeProxySIPAuthenticationPassword",
			"oxeProxySIPPolicy",
			"externalProxy",
			"externalProxySIPAuthenticationName",
			"externalProxySIPAuthenticationPassword",
			"externalProxySIPPolicy",
			"sntp_addr",
			"tone_country",
			"admin_password",
			"time_format",
			"language",
			"telnet_password",
			"codec_g722",
			"codec_pcma",
			"codec_pcmu",
			"codec_g729a",
			"codec_g723_1"
		],
		"OpenTouch otdevicemr8068": [
			"id",
			"deviceId",
			"deviceComment",
			"directoryNumber",
			"type",
			"nodetype",
			"deviceModel",
			"SIPMaxCall",
			"sipPhoneProfile",
			"timezone",
			"MACAddress",
			"IdentificationKey",
			"IdentificationPasswd",
			"DmEnetcfgDns1",
			"DmEnetcfgDns2",
			"hostname",
			"auth_tag",
			"SIPPolicy",
			"oxeProxy",
			"oxeProxySIPAuthenticationName",
			"oxeProxySIPAuthenticationPassword",
			"oxeProxySIPPolicy",
			"externalProxy",
			"externalProxySIPAuthenticationName",
			"externalProxySIPAuthenticationPassword",
			"externalProxySIPPolicy",
			"sntp_addr",
			"tone_country",
			"admin_password",
			"time_format",
			"language",
			"telnet_password",
			"codec_g722",
			"codec_pcma",
			"codec_pcmu",
			"codec_g729a",
			"codec_g723_1"
		],
		"OpenTouch otdeviceicmmobile": [
			"id",
			"deviceId",
			"deviceComment",
			"directoryNumber",
			"type",
			"nodetype",
			"deviceModel",
			"SIPMaxCall",
			"sipPhoneProfile",
			"timezone",
			"typeofmobile",
			"GSMNumber",
			"pwdSIPDevice",
			"usedForSMSnotification",
			"userMobileCallRouting",
			"sipPhoneProfileWifi",
			"DmEnetcfgDns1",
			"DmEnetcfgDns2",
			"ICMfallBackNumber",
			"disaProgressIndication",
			"codec_enable_amrwb",
			"codec_framing_amrwb",
			"codec_bitrate_amrwb",
			"codec_payload_amrwb",
			"codec_sampling_amrwb",
			"codec_enable_g722",
			"codec_framing_g722",
			"codec_payload_g722",
			"codec_enable_pcma",
			"codec_framing_pcma",
			"codec_payload_pcma",
			"codec_sampling_pcma",
			"codec_enable_pcmu",
			"codec_framing_pcmu",
			"codec_payload_pcmu",
			"codec_sampling_pcmu",
			"codec_enable_g729",
			"codec_framing_g729",
			"codec_payload_g729",
			"codec_sampling_g729"
		],
		"OpenTouch otdeviceotctablet": [
			"id",
			"deviceId",
			"deviceComment",
			"directoryNumber",
			"type",
			"nodetype",
			"deviceModel",
			"SIPMaxCall",
			"sipPhoneProfile",
			"timezone",
			"DmEnetcfgDns1",
			"DmEnetcfgDns2",
			"outbound_proxy",
			"SIPPolicy",
			"oxeProxy",
			"oxeProxySIPAuthenticationName",
			"oxeProxySIPAuthenticationPassword",
			"oxeProxySIPPolicy",
			"externalProxy",
			"externalProxyDeviceUri",
			"externalProxySIPAuthenticationName",
			"externalProxySIPAuthenticationPassword",
			"externalProxySIPPolicy",
			"sbcWan",
			"codec_enable_amrwb",
			"codec_framing_amrwb",
			"codec_bitrate_amrwb",
			"codec_payload_amrwb",
			"codec_sampling_amrwb",
			"codec_enable_g722",
			"codec_framing_g722",
			"codec_payload_g722",
			"codec_enable_pcma",
			"codec_framing_pcma",
			"codec_payload_pcma",
			"codec_sampling_pcma",
			"codec_enable_pcmu",
			"codec_framing_pcmu",
			"codec_payload_pcmu",
			"codec_sampling_pcmu",
			"codec_enable_g729",
			"codec_framing_g729",
			"codec_payload_g729",
			"codec_sampling_g729",
			"VideoCapability",
			"VideoIpTos",
			"Direct_encryption"
		],
		"OpenTouch otdevicemyicsip": [
			"id",
			"deviceId",
			"deviceComment",
			"directoryNumber",
			"type",
			"nodetype",
			"deviceModel",
			"SIPMaxCall",
			"sipPhoneProfile",
			"timezone",
			"desktopType",
			"DmEnetcfgDns1",
			"DmEnetcfgDns2",
			"outbound_proxy",
			"SIPPolicy",
			"oxeProxy",
			"oxeProxySIPAuthenticationName",
			"oxeProxySIPAuthenticationPassword",
			"oxeProxySIPPolicy",
			"externalProxy",
			"externalProxyDeviceUri",
			"externalProxySIPAuthenticationName",
			"externalProxySIPAuthenticationPassword",
			"externalProxySIPPolicy",
			"sbcWan",
			"codec_enable_g722",
			"codec_framing_g722",
			"codec_payload_g722",
			"codec_sampling_g722",
			"codec_enable_amrwb",
			"codec_framing_amrwb",
			"codec_bitrate_amrwb",
			"codec_payload_amrwb",
			"codec_sampling_amrwb",
			"codec_enable_pcma",
			"codec_framing_pcma",
			"codec_payload_pcma",
			"codec_sampling_pcma",
			"codec_enable_pcmu",
			"codec_framing_pcmu",
			"codec_payload_pcmu",
			"codec_sampling_pcmu",
			"codec_enable_g729",
			"codec_framing_g729",
			"codec_payload_g729",
			"codec_sampling_g729",
			"VideoIpTos",
			"Direct_encryption"
		],
		"OpenTouch otdevicethirdpartyvideo": [
			"id",
			"deviceId",
			"label",
			"deviceComment",
			"directoryNumber",
			"type",
			"nodetype",
			"SIPMaxCall",
			"sipPhoneProfile",
			"icmNode",
			"acsAVPProfileNumber"
		],
		"OpenTouch otdeviceanymobile": [
			"id",
			"deviceId",
			"label",
			"deviceComment",
			"directoryNumber",
			"type",
			"nodetype",
			"SIPMaxCall",
			"sipPhoneProfile",
			"deviceName",
			"GSMNumber",
			"usedForSMSnotification",
			"userMobileCallRouting"
		],
		"OpenTouch otdeviceoxe": [
			"id",
			"deviceId",
			"deviceComment",
			"directoryNumber",
			"type",
			"nodetype",
			"deviceModel",
			"domiciliatedOXE",
			"AutoAnswer",
			"UseRemoteConnectionMode",
			"cellular",
			"multimedia",
			"synchronized"
		],
		"OpenTouch otoxesipsubscriber": [
			"id",
			"directoryNumber",
			"cmsCallServer",
			"sipLogin",
			"sipPassword",
			"sip_port",
			"protocol",
			"primaryDnsAddress",
			"primary_dns_type",
			"secondaryDnsAddress",
			"secondary_dns_type",
			"outbound_proxy",
			"use_autoanswer",
			"use_rport",
			"use_keepalive",
			"use_session_timer",
			"session_time",
			"session_time_refresher",
			"register_duration",
			"sip_ip_tos",
			"wideband_negociation",
			"RTP_ip_tos",
			"VAD",
			"DTMF_payload_type",
			"audio_ringing_file_path_name",
			"audio_waiting_file_path_name",
			"video_waiting_file_path_name",
			"ring_back_tone",
			"IP_Stat_Ticket",
			"play_audio_local",
			"RTP_TOS_enable",
			"IP_precedence",
			"TOS",
			"RTP_DSCP_enable",
			"DSP",
			"codec_enable_amrwb",
			"codec_framing_amrwb",
			"codec_bitrate_amrwb",
			"codec_payload_amrwb",
			"codec_sampling_amrwb",
			"codec_enable_g722",
			"codec_framing_g722",
			"codec_payload_g722",
			"codec_sampling_g722",
			"codec_enable_pcma",
			"codec_framing_pcma",
			"codec_payload_pcma",
			"codec_sampling_pcma",
			"codec_enable_pcmu",
			"codec_framing_pcmu",
			"codec_payload_pcmu",
			"codec_sampling_pcmu",
			"codec_enable_g729",
			"codec_framing_g729",
			"codec_payload_g729",
			"codec_sampling_g729",
			"codec_enable_g723",
			"codec_framing_g723",
			"codec_bitrate_g723",
			"codec_payload_g723",
			"codec_sampling_g723",
			"VideoCapability",
			"VideoIpTos",
			"Direct_encryption",
			"Direct_Protocol",
			"Direct_Security_level",
			"Direct_SRTP_authentication",
			"SIP_keep_alive_timeout",
			"Direct_uri_scheme",
			"Direct_SDP_BE_negotiation",
			"sbcWan"
		],
		"OpenTouch otmalink": [
			"id",
			"displayName"
		],
		"oxe oxeuser": [
			"firstName",
			"lastName",
			"email",
			"languageUpdate",
			"userLanguage",
			"userId",
			"password",
			"pin",
			"callByNameMiniMessagingRight",
			"costCenterId",
			"entityNumber",
			"usePrivateCallingNumber",
			"privateCallingNumber",
			"validForCallByName"
		],
		"oxe oxedevice": [
			"ciscoName",
			"description",
			"stationType",
			"keyModel",
			"equipmentAddressRack",
			"equipmentAddressBoard",
			"equipmentAddressTerminal",
			"acdType",
			"ghostZ",
			"publicNetworkCategoryId",
			"externalForwardingCategoryId",
			"facilityCategoryId",
			"connectionCategoryId",
			"meteringCategory",
			"robinetCategory",
			"multineAutomaticIncomingSeize",
			"multineAutomaticOutgoingSeize",
			"multineSelectiveFiltering",
			"primaryLinePrio",
			"secondaryLinePrio",
			"delayOverflowForWaitingCall",
			"immediateOverflowForWaitingCall",
			"addOnModule1",
			"addOnModule2",
			"addOnModule3",
			"callForwardUpdate",
			"callForwardRenvoiPrincipal",
			"callForwardNumeroRenvoiPrincipal",
			"callForwardRenvoiSecond",
			"callForwardNumeroRenvoiSecond",
			"callForwardCadenas",
			"callForwardNePasDeranger",
			"callForwardBusyOnCamp",
			"callForwardOverflowOnAssociate",
			"callForwardOverflowBusy",
			"callForwardAssociatedSetNo",
			"callForwardResetChargePulseCounter",
			"callForwardConsistentWithIdentity"
		],
		"oxe oxeline": [
			"directoryNumber",
			"description",
			"associatedStationDN",
			"dynamicServices",
			"multiKeyNumber",
			"pickupGroupName",
			"primaryMLADN",
			"voiceMailDN"
		],
		"Genesys genesysperson": [
			"DBID",
			"firstName",
			"lastName",
			"tenantDBID",
			"employeeID",
			"emailAddress",
			"userName",
			"password",
			"externalID",
			"state",
			"isAgent",
			"skills",
			"agentlogins"
		],
		"Genesys genesysskill": [
			"DBID",
			"name",
			"tenantDBID",
			"state"
		],
		"Genesys genesysaccessgroup": [
			"DBID",
			"name",
			"tenantDBID",
			"state",
			"members"
		],
		"Genesys genesysagentgroup": [
			"DBID",
			"name",
			"tenantDBID",
			"state",
			"agents"
		],
		"Genesys genesysdn": [
			"DBID",
			"number",
			"type",
			"tenantDBID",
			"switchDBID",
			"state",
			"name",
			"routeType"
		],
		"Genesys genesysagentlogin": [
			"DBID",
			"loginCode",
			"tenantDBID",
			"switchDBID",
			"state",
			"useOverride",
			"override",
			"switchSpecificType",
			"password"
		],
		"Genesys genesystransaction": [
			"DBID",
			"name",
			"tenantDBID",
			"type",
			"alias",
			"recordPeriod",
			"state",
			"description"
		],
		"Genesys genesysplace": [
			"DBID",
			"name",
			"tenantDBID",
			"state",
			"dns"
		],
		"peterConnects attendantgroup": [
			"id",
			"name",
			"comment",
			"shortCode",
			"tenant",
			"maximumNumberOfLoggedOnAttendants",
			"alternativeOutgoingCallPrefix",
			"shortDialPrefix",
			"registerUsersAutomatically",
			"f1Queue",
			"f2Queue"
		],
		"peterConnects queue": [
			"id",
			"mainLabel",
			"mainColor",
			"mainOverflowDestination",
			"mainPriority",
			"mainMaximumNumberOfIncomingCalls",
			"mainMaximumExpectedWaitingTime",
			"mainExpectedHandlingTimePerCall",
			"mainAcceptExternalCallsOnly",
			"mainMohResource",
			"mainAlternativeDestination",
			"mainAlternativeDestinationOverrule",
			"mainCtiDevice",
			"mainTimeZone",
			"mainIvrProfile",
			"mainClosedProfile",
			"mainOverflowIvrProfile",
			"applicationType",
			"comment",
			"platform",
			"routePointDisplayName",
			"dispatchingMethod",
			"recordingPIN",
			"externalMask"
		],
		"peterConnects ivrprofile": [
			"id",
			"name",
			"ivrProfileLineStartTime",
			"ivrProfileLineCallerPrefixes",
			"ivrProfileLineComment",
			"ivrProfileLineAttendantLanguage",
			"ivrProfileLineQueuePosSwitchOff",
			"ivrProfileLineQueuePosSuppressTurn",
			"ivrProfileLineInitialDelay",
			"ivrProfileLineMenuDelay",
			"ivrProfileLineQueuePosDelay",
			"ivrProfileLineHideUntilWaiting",
			"ivrProfileLineWelcomeWaveInternal",
			"ivrProfileLineWelcomeWaveExternal",
			"ivrProfileLineDestinationTypeInternal",
			"ivrProfileLineDestinationTypeExternal",
			"ivrProfileLineDestinationInternal",
			"ivrProfileLineDestinationExternal",
			"IvrMenuInternal",
			"IvrMenuExternal"
		],
		"peterConnects closedprofile": [
			"id",
			"name",
			"comment"
		],
		"ciscoSpark sparkroom": [
			"id",
			"title",
			"description",
			"created",
			"type"
		],
		"ciscoSpark sparkpeople": [
			"id",
			"emails",
			"displayName",
			"firstName",
			"lastName",
			"description",
			"orgId",
			"roles",
			"licenses",
			"created",
			"status",
			"invitePending",
			"loginEnabled"
		],
		"ciscoSpark sparkexternalpeople": [
			"id",
			"emails",
			"displayName",
			"firstName",
			"lastName",
			"orgId"
		],
		"ciscoSpark sparkteam": [
			"id",
			"name",
			"description",
			"created"
		],
		"ciscoSpark sparkrgmembership": [
			"id",
			"peopleDisplayName",
			"licenseId",
			"resourceGroupId",
			"status"
		]
	}
}];
},{}],4:[function(require,module,exports){
(function (global,setImmediate){(function (){
/*
 * Dexie.js - a minimalistic wrapper for IndexedDB
 * ===============================================
 *
 * By David Fahlander, david.fahlander@gmail.com
 *
 * Version 3.2.2, Wed Apr 27 2022
 *
 * https://dexie.org
 *
 * Apache License Version 2.0, January 2004, http://www.apache.org/licenses/
 */
 
(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
    typeof define === 'function' && define.amd ? define(factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, global.Dexie = factory());
})(this, (function () { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.
    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.
    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };
    function __spreadArray(to, from, pack) {
        if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
            if (ar || !(i in from)) {
                if (!ar) ar = Array.prototype.slice.call(from, 0, i);
                ar[i] = from[i];
            }
        }
        return to.concat(ar || Array.prototype.slice.call(from));
    }

    var _global = typeof globalThis !== 'undefined' ? globalThis :
        typeof self !== 'undefined' ? self :
            typeof window !== 'undefined' ? window :
                global;

    var keys = Object.keys;
    var isArray = Array.isArray;
    if (typeof Promise !== 'undefined' && !_global.Promise) {
        _global.Promise = Promise;
    }
    function extend(obj, extension) {
        if (typeof extension !== 'object')
            return obj;
        keys(extension).forEach(function (key) {
            obj[key] = extension[key];
        });
        return obj;
    }
    var getProto = Object.getPrototypeOf;
    var _hasOwn = {}.hasOwnProperty;
    function hasOwn(obj, prop) {
        return _hasOwn.call(obj, prop);
    }
    function props(proto, extension) {
        if (typeof extension === 'function')
            extension = extension(getProto(proto));
        (typeof Reflect === "undefined" ? keys : Reflect.ownKeys)(extension).forEach(function (key) {
            setProp(proto, key, extension[key]);
        });
    }
    var defineProperty = Object.defineProperty;
    function setProp(obj, prop, functionOrGetSet, options) {
        defineProperty(obj, prop, extend(functionOrGetSet && hasOwn(functionOrGetSet, "get") && typeof functionOrGetSet.get === 'function' ?
            { get: functionOrGetSet.get, set: functionOrGetSet.set, configurable: true } :
            { value: functionOrGetSet, configurable: true, writable: true }, options));
    }
    function derive(Child) {
        return {
            from: function (Parent) {
                Child.prototype = Object.create(Parent.prototype);
                setProp(Child.prototype, "constructor", Child);
                return {
                    extend: props.bind(null, Child.prototype)
                };
            }
        };
    }
    var getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;
    function getPropertyDescriptor(obj, prop) {
        var pd = getOwnPropertyDescriptor(obj, prop);
        var proto;
        return pd || (proto = getProto(obj)) && getPropertyDescriptor(proto, prop);
    }
    var _slice = [].slice;
    function slice(args, start, end) {
        return _slice.call(args, start, end);
    }
    function override(origFunc, overridedFactory) {
        return overridedFactory(origFunc);
    }
    function assert(b) {
        if (!b)
            throw new Error("Assertion Failed");
    }
    function asap$1(fn) {
        if (_global.setImmediate)
            setImmediate(fn);
        else
            setTimeout(fn, 0);
    }
    function arrayToObject(array, extractor) {
        return array.reduce(function (result, item, i) {
            var nameAndValue = extractor(item, i);
            if (nameAndValue)
                result[nameAndValue[0]] = nameAndValue[1];
            return result;
        }, {});
    }
    function tryCatch(fn, onerror, args) {
        try {
            fn.apply(null, args);
        }
        catch (ex) {
            onerror && onerror(ex);
        }
    }
    function getByKeyPath(obj, keyPath) {
        if (hasOwn(obj, keyPath))
            return obj[keyPath];
        if (!keyPath)
            return obj;
        if (typeof keyPath !== 'string') {
            var rv = [];
            for (var i = 0, l = keyPath.length; i < l; ++i) {
                var val = getByKeyPath(obj, keyPath[i]);
                rv.push(val);
            }
            return rv;
        }
        var period = keyPath.indexOf('.');
        if (period !== -1) {
            var innerObj = obj[keyPath.substr(0, period)];
            return innerObj === undefined ? undefined : getByKeyPath(innerObj, keyPath.substr(period + 1));
        }
        return undefined;
    }
    function setByKeyPath(obj, keyPath, value) {
        if (!obj || keyPath === undefined)
            return;
        if ('isFrozen' in Object && Object.isFrozen(obj))
            return;
        if (typeof keyPath !== 'string' && 'length' in keyPath) {
            assert(typeof value !== 'string' && 'length' in value);
            for (var i = 0, l = keyPath.length; i < l; ++i) {
                setByKeyPath(obj, keyPath[i], value[i]);
            }
        }
        else {
            var period = keyPath.indexOf('.');
            if (period !== -1) {
                var currentKeyPath = keyPath.substr(0, period);
                var remainingKeyPath = keyPath.substr(period + 1);
                if (remainingKeyPath === "")
                    if (value === undefined) {
                        if (isArray(obj) && !isNaN(parseInt(currentKeyPath)))
                            obj.splice(currentKeyPath, 1);
                        else
                            delete obj[currentKeyPath];
                    }
                    else
                        obj[currentKeyPath] = value;
                else {
                    var innerObj = obj[currentKeyPath];
                    if (!innerObj || !hasOwn(obj, currentKeyPath))
                        innerObj = (obj[currentKeyPath] = {});
                    setByKeyPath(innerObj, remainingKeyPath, value);
                }
            }
            else {
                if (value === undefined) {
                    if (isArray(obj) && !isNaN(parseInt(keyPath)))
                        obj.splice(keyPath, 1);
                    else
                        delete obj[keyPath];
                }
                else
                    obj[keyPath] = value;
            }
        }
    }
    function delByKeyPath(obj, keyPath) {
        if (typeof keyPath === 'string')
            setByKeyPath(obj, keyPath, undefined);
        else if ('length' in keyPath)
            [].map.call(keyPath, function (kp) {
                setByKeyPath(obj, kp, undefined);
            });
    }
    function shallowClone(obj) {
        var rv = {};
        for (var m in obj) {
            if (hasOwn(obj, m))
                rv[m] = obj[m];
        }
        return rv;
    }
    var concat = [].concat;
    function flatten(a) {
        return concat.apply([], a);
    }
    var intrinsicTypeNames = "Boolean,String,Date,RegExp,Blob,File,FileList,FileSystemFileHandle,ArrayBuffer,DataView,Uint8ClampedArray,ImageBitmap,ImageData,Map,Set,CryptoKey"
        .split(',').concat(flatten([8, 16, 32, 64].map(function (num) { return ["Int", "Uint", "Float"].map(function (t) { return t + num + "Array"; }); }))).filter(function (t) { return _global[t]; });
    var intrinsicTypes = intrinsicTypeNames.map(function (t) { return _global[t]; });
    arrayToObject(intrinsicTypeNames, function (x) { return [x, true]; });
    var circularRefs = null;
    function deepClone(any) {
        circularRefs = typeof WeakMap !== 'undefined' && new WeakMap();
        var rv = innerDeepClone(any);
        circularRefs = null;
        return rv;
    }
    function innerDeepClone(any) {
        if (!any || typeof any !== 'object')
            return any;
        var rv = circularRefs && circularRefs.get(any);
        if (rv)
            return rv;
        if (isArray(any)) {
            rv = [];
            circularRefs && circularRefs.set(any, rv);
            for (var i = 0, l = any.length; i < l; ++i) {
                rv.push(innerDeepClone(any[i]));
            }
        }
        else if (intrinsicTypes.indexOf(any.constructor) >= 0) {
            rv = any;
        }
        else {
            var proto = getProto(any);
            rv = proto === Object.prototype ? {} : Object.create(proto);
            circularRefs && circularRefs.set(any, rv);
            for (var prop in any) {
                if (hasOwn(any, prop)) {
                    rv[prop] = innerDeepClone(any[prop]);
                }
            }
        }
        return rv;
    }
    var toString = {}.toString;
    function toStringTag(o) {
        return toString.call(o).slice(8, -1);
    }
    var iteratorSymbol = typeof Symbol !== 'undefined' ?
        Symbol.iterator :
        '@@iterator';
    var getIteratorOf = typeof iteratorSymbol === "symbol" ? function (x) {
        var i;
        return x != null && (i = x[iteratorSymbol]) && i.apply(x);
    } : function () { return null; };
    var NO_CHAR_ARRAY = {};
    function getArrayOf(arrayLike) {
        var i, a, x, it;
        if (arguments.length === 1) {
            if (isArray(arrayLike))
                return arrayLike.slice();
            if (this === NO_CHAR_ARRAY && typeof arrayLike === 'string')
                return [arrayLike];
            if ((it = getIteratorOf(arrayLike))) {
                a = [];
                while ((x = it.next()), !x.done)
                    a.push(x.value);
                return a;
            }
            if (arrayLike == null)
                return [arrayLike];
            i = arrayLike.length;
            if (typeof i === 'number') {
                a = new Array(i);
                while (i--)
                    a[i] = arrayLike[i];
                return a;
            }
            return [arrayLike];
        }
        i = arguments.length;
        a = new Array(i);
        while (i--)
            a[i] = arguments[i];
        return a;
    }
    var isAsyncFunction = typeof Symbol !== 'undefined'
        ? function (fn) { return fn[Symbol.toStringTag] === 'AsyncFunction'; }
        : function () { return false; };

    var debug = typeof location !== 'undefined' &&
        /^(http|https):\/\/(localhost|127\.0\.0\.1)/.test(location.href);
    function setDebug(value, filter) {
        debug = value;
        libraryFilter = filter;
    }
    var libraryFilter = function () { return true; };
    var NEEDS_THROW_FOR_STACK = !new Error("").stack;
    function getErrorWithStack() {
        if (NEEDS_THROW_FOR_STACK)
            try {
                getErrorWithStack.arguments;
                throw new Error();
            }
            catch (e) {
                return e;
            }
        return new Error();
    }
    function prettyStack(exception, numIgnoredFrames) {
        var stack = exception.stack;
        if (!stack)
            return "";
        numIgnoredFrames = (numIgnoredFrames || 0);
        if (stack.indexOf(exception.name) === 0)
            numIgnoredFrames += (exception.name + exception.message).split('\n').length;
        return stack.split('\n')
            .slice(numIgnoredFrames)
            .filter(libraryFilter)
            .map(function (frame) { return "\n" + frame; })
            .join('');
    }

    var dexieErrorNames = [
        'Modify',
        'Bulk',
        'OpenFailed',
        'VersionChange',
        'Schema',
        'Upgrade',
        'InvalidTable',
        'MissingAPI',
        'NoSuchDatabase',
        'InvalidArgument',
        'SubTransaction',
        'Unsupported',
        'Internal',
        'DatabaseClosed',
        'PrematureCommit',
        'ForeignAwait'
    ];
    var idbDomErrorNames = [
        'Unknown',
        'Constraint',
        'Data',
        'TransactionInactive',
        'ReadOnly',
        'Version',
        'NotFound',
        'InvalidState',
        'InvalidAccess',
        'Abort',
        'Timeout',
        'QuotaExceeded',
        'Syntax',
        'DataClone'
    ];
    var errorList = dexieErrorNames.concat(idbDomErrorNames);
    var defaultTexts = {
        VersionChanged: "Database version changed by other database connection",
        DatabaseClosed: "Database has been closed",
        Abort: "Transaction aborted",
        TransactionInactive: "Transaction has already completed or failed",
        MissingAPI: "IndexedDB API missing. Please visit https://tinyurl.com/y2uuvskb"
    };
    function DexieError(name, msg) {
        this._e = getErrorWithStack();
        this.name = name;
        this.message = msg;
    }
    derive(DexieError).from(Error).extend({
        stack: {
            get: function () {
                return this._stack ||
                    (this._stack = this.name + ": " + this.message + prettyStack(this._e, 2));
            }
        },
        toString: function () { return this.name + ": " + this.message; }
    });
    function getMultiErrorMessage(msg, failures) {
        return msg + ". Errors: " + Object.keys(failures)
            .map(function (key) { return failures[key].toString(); })
            .filter(function (v, i, s) { return s.indexOf(v) === i; })
            .join('\n');
    }
    function ModifyError(msg, failures, successCount, failedKeys) {
        this._e = getErrorWithStack();
        this.failures = failures;
        this.failedKeys = failedKeys;
        this.successCount = successCount;
        this.message = getMultiErrorMessage(msg, failures);
    }
    derive(ModifyError).from(DexieError);
    function BulkError(msg, failures) {
        this._e = getErrorWithStack();
        this.name = "BulkError";
        this.failures = Object.keys(failures).map(function (pos) { return failures[pos]; });
        this.failuresByPos = failures;
        this.message = getMultiErrorMessage(msg, failures);
    }
    derive(BulkError).from(DexieError);
    var errnames = errorList.reduce(function (obj, name) { return (obj[name] = name + "Error", obj); }, {});
    var BaseException = DexieError;
    var exceptions = errorList.reduce(function (obj, name) {
        var fullName = name + "Error";
        function DexieError(msgOrInner, inner) {
            this._e = getErrorWithStack();
            this.name = fullName;
            if (!msgOrInner) {
                this.message = defaultTexts[name] || fullName;
                this.inner = null;
            }
            else if (typeof msgOrInner === 'string') {
                this.message = "" + msgOrInner + (!inner ? '' : '\n ' + inner);
                this.inner = inner || null;
            }
            else if (typeof msgOrInner === 'object') {
                this.message = msgOrInner.name + " " + msgOrInner.message;
                this.inner = msgOrInner;
            }
        }
        derive(DexieError).from(BaseException);
        obj[name] = DexieError;
        return obj;
    }, {});
    exceptions.Syntax = SyntaxError;
    exceptions.Type = TypeError;
    exceptions.Range = RangeError;
    var exceptionMap = idbDomErrorNames.reduce(function (obj, name) {
        obj[name + "Error"] = exceptions[name];
        return obj;
    }, {});
    function mapError(domError, message) {
        if (!domError || domError instanceof DexieError || domError instanceof TypeError || domError instanceof SyntaxError || !domError.name || !exceptionMap[domError.name])
            return domError;
        var rv = new exceptionMap[domError.name](message || domError.message, domError);
        if ("stack" in domError) {
            setProp(rv, "stack", { get: function () {
                    return this.inner.stack;
                } });
        }
        return rv;
    }
    var fullNameExceptions = errorList.reduce(function (obj, name) {
        if (["Syntax", "Type", "Range"].indexOf(name) === -1)
            obj[name + "Error"] = exceptions[name];
        return obj;
    }, {});
    fullNameExceptions.ModifyError = ModifyError;
    fullNameExceptions.DexieError = DexieError;
    fullNameExceptions.BulkError = BulkError;

    function nop() { }
    function mirror(val) { return val; }
    function pureFunctionChain(f1, f2) {
        if (f1 == null || f1 === mirror)
            return f2;
        return function (val) {
            return f2(f1(val));
        };
    }
    function callBoth(on1, on2) {
        return function () {
            on1.apply(this, arguments);
            on2.apply(this, arguments);
        };
    }
    function hookCreatingChain(f1, f2) {
        if (f1 === nop)
            return f2;
        return function () {
            var res = f1.apply(this, arguments);
            if (res !== undefined)
                arguments[0] = res;
            var onsuccess = this.onsuccess,
            onerror = this.onerror;
            this.onsuccess = null;
            this.onerror = null;
            var res2 = f2.apply(this, arguments);
            if (onsuccess)
                this.onsuccess = this.onsuccess ? callBoth(onsuccess, this.onsuccess) : onsuccess;
            if (onerror)
                this.onerror = this.onerror ? callBoth(onerror, this.onerror) : onerror;
            return res2 !== undefined ? res2 : res;
        };
    }
    function hookDeletingChain(f1, f2) {
        if (f1 === nop)
            return f2;
        return function () {
            f1.apply(this, arguments);
            var onsuccess = this.onsuccess,
            onerror = this.onerror;
            this.onsuccess = this.onerror = null;
            f2.apply(this, arguments);
            if (onsuccess)
                this.onsuccess = this.onsuccess ? callBoth(onsuccess, this.onsuccess) : onsuccess;
            if (onerror)
                this.onerror = this.onerror ? callBoth(onerror, this.onerror) : onerror;
        };
    }
    function hookUpdatingChain(f1, f2) {
        if (f1 === nop)
            return f2;
        return function (modifications) {
            var res = f1.apply(this, arguments);
            extend(modifications, res);
            var onsuccess = this.onsuccess,
            onerror = this.onerror;
            this.onsuccess = null;
            this.onerror = null;
            var res2 = f2.apply(this, arguments);
            if (onsuccess)
                this.onsuccess = this.onsuccess ? callBoth(onsuccess, this.onsuccess) : onsuccess;
            if (onerror)
                this.onerror = this.onerror ? callBoth(onerror, this.onerror) : onerror;
            return res === undefined ?
                (res2 === undefined ? undefined : res2) :
                (extend(res, res2));
        };
    }
    function reverseStoppableEventChain(f1, f2) {
        if (f1 === nop)
            return f2;
        return function () {
            if (f2.apply(this, arguments) === false)
                return false;
            return f1.apply(this, arguments);
        };
    }
    function promisableChain(f1, f2) {
        if (f1 === nop)
            return f2;
        return function () {
            var res = f1.apply(this, arguments);
            if (res && typeof res.then === 'function') {
                var thiz = this, i = arguments.length, args = new Array(i);
                while (i--)
                    args[i] = arguments[i];
                return res.then(function () {
                    return f2.apply(thiz, args);
                });
            }
            return f2.apply(this, arguments);
        };
    }

    var INTERNAL = {};
    var LONG_STACKS_CLIP_LIMIT = 100,
    MAX_LONG_STACKS = 20, ZONE_ECHO_LIMIT = 100, _a$1 = typeof Promise === 'undefined' ?
        [] :
        (function () {
            var globalP = Promise.resolve();
            if (typeof crypto === 'undefined' || !crypto.subtle)
                return [globalP, getProto(globalP), globalP];
            var nativeP = crypto.subtle.digest("SHA-512", new Uint8Array([0]));
            return [
                nativeP,
                getProto(nativeP),
                globalP
            ];
        })(), resolvedNativePromise = _a$1[0], nativePromiseProto = _a$1[1], resolvedGlobalPromise = _a$1[2], nativePromiseThen = nativePromiseProto && nativePromiseProto.then;
    var NativePromise = resolvedNativePromise && resolvedNativePromise.constructor;
    var patchGlobalPromise = !!resolvedGlobalPromise;
    var stack_being_generated = false;
    var schedulePhysicalTick = resolvedGlobalPromise ?
        function () { resolvedGlobalPromise.then(physicalTick); }
        :
            _global.setImmediate ?
                setImmediate.bind(null, physicalTick) :
                _global.MutationObserver ?
                    function () {
                        var hiddenDiv = document.createElement("div");
                        (new MutationObserver(function () {
                            physicalTick();
                            hiddenDiv = null;
                        })).observe(hiddenDiv, { attributes: true });
                        hiddenDiv.setAttribute('i', '1');
                    } :
                    function () { setTimeout(physicalTick, 0); };
    var asap = function (callback, args) {
        microtickQueue.push([callback, args]);
        if (needsNewPhysicalTick) {
            schedulePhysicalTick();
            needsNewPhysicalTick = false;
        }
    };
    var isOutsideMicroTick = true,
    needsNewPhysicalTick = true,
    unhandledErrors = [],
    rejectingErrors = [],
    currentFulfiller = null, rejectionMapper = mirror;
    var globalPSD = {
        id: 'global',
        global: true,
        ref: 0,
        unhandleds: [],
        onunhandled: globalError,
        pgp: false,
        env: {},
        finalize: function () {
            this.unhandleds.forEach(function (uh) {
                try {
                    globalError(uh[0], uh[1]);
                }
                catch (e) { }
            });
        }
    };
    var PSD = globalPSD;
    var microtickQueue = [];
    var numScheduledCalls = 0;
    var tickFinalizers = [];
    function DexiePromise(fn) {
        if (typeof this !== 'object')
            throw new TypeError('Promises must be constructed via new');
        this._listeners = [];
        this.onuncatched = nop;
        this._lib = false;
        var psd = (this._PSD = PSD);
        if (debug) {
            this._stackHolder = getErrorWithStack();
            this._prev = null;
            this._numPrev = 0;
        }
        if (typeof fn !== 'function') {
            if (fn !== INTERNAL)
                throw new TypeError('Not a function');
            this._state = arguments[1];
            this._value = arguments[2];
            if (this._state === false)
                handleRejection(this, this._value);
            return;
        }
        this._state = null;
        this._value = null;
        ++psd.ref;
        executePromiseTask(this, fn);
    }
    var thenProp = {
        get: function () {
            var psd = PSD, microTaskId = totalEchoes;
            function then(onFulfilled, onRejected) {
                var _this = this;
                var possibleAwait = !psd.global && (psd !== PSD || microTaskId !== totalEchoes);
                var cleanup = possibleAwait && !decrementExpectedAwaits();
                var rv = new DexiePromise(function (resolve, reject) {
                    propagateToListener(_this, new Listener(nativeAwaitCompatibleWrap(onFulfilled, psd, possibleAwait, cleanup), nativeAwaitCompatibleWrap(onRejected, psd, possibleAwait, cleanup), resolve, reject, psd));
                });
                debug && linkToPreviousPromise(rv, this);
                return rv;
            }
            then.prototype = INTERNAL;
            return then;
        },
        set: function (value) {
            setProp(this, 'then', value && value.prototype === INTERNAL ?
                thenProp :
                {
                    get: function () {
                        return value;
                    },
                    set: thenProp.set
                });
        }
    };
    props(DexiePromise.prototype, {
        then: thenProp,
        _then: function (onFulfilled, onRejected) {
            propagateToListener(this, new Listener(null, null, onFulfilled, onRejected, PSD));
        },
        catch: function (onRejected) {
            if (arguments.length === 1)
                return this.then(null, onRejected);
            var type = arguments[0], handler = arguments[1];
            return typeof type === 'function' ? this.then(null, function (err) {
                return err instanceof type ? handler(err) : PromiseReject(err);
            })
                : this.then(null, function (err) {
                    return err && err.name === type ? handler(err) : PromiseReject(err);
                });
        },
        finally: function (onFinally) {
            return this.then(function (value) {
                onFinally();
                return value;
            }, function (err) {
                onFinally();
                return PromiseReject(err);
            });
        },
        stack: {
            get: function () {
                if (this._stack)
                    return this._stack;
                try {
                    stack_being_generated = true;
                    var stacks = getStack(this, [], MAX_LONG_STACKS);
                    var stack = stacks.join("\nFrom previous: ");
                    if (this._state !== null)
                        this._stack = stack;
                    return stack;
                }
                finally {
                    stack_being_generated = false;
                }
            }
        },
        timeout: function (ms, msg) {
            var _this = this;
            return ms < Infinity ?
                new DexiePromise(function (resolve, reject) {
                    var handle = setTimeout(function () { return reject(new exceptions.Timeout(msg)); }, ms);
                    _this.then(resolve, reject).finally(clearTimeout.bind(null, handle));
                }) : this;
        }
    });
    if (typeof Symbol !== 'undefined' && Symbol.toStringTag)
        setProp(DexiePromise.prototype, Symbol.toStringTag, 'Dexie.Promise');
    globalPSD.env = snapShot();
    function Listener(onFulfilled, onRejected, resolve, reject, zone) {
        this.onFulfilled = typeof onFulfilled === 'function' ? onFulfilled : null;
        this.onRejected = typeof onRejected === 'function' ? onRejected : null;
        this.resolve = resolve;
        this.reject = reject;
        this.psd = zone;
    }
    props(DexiePromise, {
        all: function () {
            var values = getArrayOf.apply(null, arguments)
                .map(onPossibleParallellAsync);
            return new DexiePromise(function (resolve, reject) {
                if (values.length === 0)
                    resolve([]);
                var remaining = values.length;
                values.forEach(function (a, i) { return DexiePromise.resolve(a).then(function (x) {
                    values[i] = x;
                    if (!--remaining)
                        resolve(values);
                }, reject); });
            });
        },
        resolve: function (value) {
            if (value instanceof DexiePromise)
                return value;
            if (value && typeof value.then === 'function')
                return new DexiePromise(function (resolve, reject) {
                    value.then(resolve, reject);
                });
            var rv = new DexiePromise(INTERNAL, true, value);
            linkToPreviousPromise(rv, currentFulfiller);
            return rv;
        },
        reject: PromiseReject,
        race: function () {
            var values = getArrayOf.apply(null, arguments).map(onPossibleParallellAsync);
            return new DexiePromise(function (resolve, reject) {
                values.map(function (value) { return DexiePromise.resolve(value).then(resolve, reject); });
            });
        },
        PSD: {
            get: function () { return PSD; },
            set: function (value) { return PSD = value; }
        },
        totalEchoes: { get: function () { return totalEchoes; } },
        newPSD: newScope,
        usePSD: usePSD,
        scheduler: {
            get: function () { return asap; },
            set: function (value) { asap = value; }
        },
        rejectionMapper: {
            get: function () { return rejectionMapper; },
            set: function (value) { rejectionMapper = value; }
        },
        follow: function (fn, zoneProps) {
            return new DexiePromise(function (resolve, reject) {
                return newScope(function (resolve, reject) {
                    var psd = PSD;
                    psd.unhandleds = [];
                    psd.onunhandled = reject;
                    psd.finalize = callBoth(function () {
                        var _this = this;
                        run_at_end_of_this_or_next_physical_tick(function () {
                            _this.unhandleds.length === 0 ? resolve() : reject(_this.unhandleds[0]);
                        });
                    }, psd.finalize);
                    fn();
                }, zoneProps, resolve, reject);
            });
        }
    });
    if (NativePromise) {
        if (NativePromise.allSettled)
            setProp(DexiePromise, "allSettled", function () {
                var possiblePromises = getArrayOf.apply(null, arguments).map(onPossibleParallellAsync);
                return new DexiePromise(function (resolve) {
                    if (possiblePromises.length === 0)
                        resolve([]);
                    var remaining = possiblePromises.length;
                    var results = new Array(remaining);
                    possiblePromises.forEach(function (p, i) { return DexiePromise.resolve(p).then(function (value) { return results[i] = { status: "fulfilled", value: value }; }, function (reason) { return results[i] = { status: "rejected", reason: reason }; })
                        .then(function () { return --remaining || resolve(results); }); });
                });
            });
        if (NativePromise.any && typeof AggregateError !== 'undefined')
            setProp(DexiePromise, "any", function () {
                var possiblePromises = getArrayOf.apply(null, arguments).map(onPossibleParallellAsync);
                return new DexiePromise(function (resolve, reject) {
                    if (possiblePromises.length === 0)
                        reject(new AggregateError([]));
                    var remaining = possiblePromises.length;
                    var failures = new Array(remaining);
                    possiblePromises.forEach(function (p, i) { return DexiePromise.resolve(p).then(function (value) { return resolve(value); }, function (failure) {
                        failures[i] = failure;
                        if (!--remaining)
                            reject(new AggregateError(failures));
                    }); });
                });
            });
    }
    function executePromiseTask(promise, fn) {
        try {
            fn(function (value) {
                if (promise._state !== null)
                    return;
                if (value === promise)
                    throw new TypeError('A promise cannot be resolved with itself.');
                var shouldExecuteTick = promise._lib && beginMicroTickScope();
                if (value && typeof value.then === 'function') {
                    executePromiseTask(promise, function (resolve, reject) {
                        value instanceof DexiePromise ?
                            value._then(resolve, reject) :
                            value.then(resolve, reject);
                    });
                }
                else {
                    promise._state = true;
                    promise._value = value;
                    propagateAllListeners(promise);
                }
                if (shouldExecuteTick)
                    endMicroTickScope();
            }, handleRejection.bind(null, promise));
        }
        catch (ex) {
            handleRejection(promise, ex);
        }
    }
    function handleRejection(promise, reason) {
        rejectingErrors.push(reason);
        if (promise._state !== null)
            return;
        var shouldExecuteTick = promise._lib && beginMicroTickScope();
        reason = rejectionMapper(reason);
        promise._state = false;
        promise._value = reason;
        debug && reason !== null && typeof reason === 'object' && !reason._promise && tryCatch(function () {
            var origProp = getPropertyDescriptor(reason, "stack");
            reason._promise = promise;
            setProp(reason, "stack", {
                get: function () {
                    return stack_being_generated ?
                        origProp && (origProp.get ?
                            origProp.get.apply(reason) :
                            origProp.value) :
                        promise.stack;
                }
            });
        });
        addPossiblyUnhandledError(promise);
        propagateAllListeners(promise);
        if (shouldExecuteTick)
            endMicroTickScope();
    }
    function propagateAllListeners(promise) {
        var listeners = promise._listeners;
        promise._listeners = [];
        for (var i = 0, len = listeners.length; i < len; ++i) {
            propagateToListener(promise, listeners[i]);
        }
        var psd = promise._PSD;
        --psd.ref || psd.finalize();
        if (numScheduledCalls === 0) {
            ++numScheduledCalls;
            asap(function () {
                if (--numScheduledCalls === 0)
                    finalizePhysicalTick();
            }, []);
        }
    }
    function propagateToListener(promise, listener) {
        if (promise._state === null) {
            promise._listeners.push(listener);
            return;
        }
        var cb = promise._state ? listener.onFulfilled : listener.onRejected;
        if (cb === null) {
            return (promise._state ? listener.resolve : listener.reject)(promise._value);
        }
        ++listener.psd.ref;
        ++numScheduledCalls;
        asap(callListener, [cb, promise, listener]);
    }
    function callListener(cb, promise, listener) {
        try {
            currentFulfiller = promise;
            var ret, value = promise._value;
            if (promise._state) {
                ret = cb(value);
            }
            else {
                if (rejectingErrors.length)
                    rejectingErrors = [];
                ret = cb(value);
                if (rejectingErrors.indexOf(value) === -1)
                    markErrorAsHandled(promise);
            }
            listener.resolve(ret);
        }
        catch (e) {
            listener.reject(e);
        }
        finally {
            currentFulfiller = null;
            if (--numScheduledCalls === 0)
                finalizePhysicalTick();
            --listener.psd.ref || listener.psd.finalize();
        }
    }
    function getStack(promise, stacks, limit) {
        if (stacks.length === limit)
            return stacks;
        var stack = "";
        if (promise._state === false) {
            var failure = promise._value, errorName, message;
            if (failure != null) {
                errorName = failure.name || "Error";
                message = failure.message || failure;
                stack = prettyStack(failure, 0);
            }
            else {
                errorName = failure;
                message = "";
            }
            stacks.push(errorName + (message ? ": " + message : "") + stack);
        }
        if (debug) {
            stack = prettyStack(promise._stackHolder, 2);
            if (stack && stacks.indexOf(stack) === -1)
                stacks.push(stack);
            if (promise._prev)
                getStack(promise._prev, stacks, limit);
        }
        return stacks;
    }
    function linkToPreviousPromise(promise, prev) {
        var numPrev = prev ? prev._numPrev + 1 : 0;
        if (numPrev < LONG_STACKS_CLIP_LIMIT) {
            promise._prev = prev;
            promise._numPrev = numPrev;
        }
    }
    function physicalTick() {
        beginMicroTickScope() && endMicroTickScope();
    }
    function beginMicroTickScope() {
        var wasRootExec = isOutsideMicroTick;
        isOutsideMicroTick = false;
        needsNewPhysicalTick = false;
        return wasRootExec;
    }
    function endMicroTickScope() {
        var callbacks, i, l;
        do {
            while (microtickQueue.length > 0) {
                callbacks = microtickQueue;
                microtickQueue = [];
                l = callbacks.length;
                for (i = 0; i < l; ++i) {
                    var item = callbacks[i];
                    item[0].apply(null, item[1]);
                }
            }
        } while (microtickQueue.length > 0);
        isOutsideMicroTick = true;
        needsNewPhysicalTick = true;
    }
    function finalizePhysicalTick() {
        var unhandledErrs = unhandledErrors;
        unhandledErrors = [];
        unhandledErrs.forEach(function (p) {
            p._PSD.onunhandled.call(null, p._value, p);
        });
        var finalizers = tickFinalizers.slice(0);
        var i = finalizers.length;
        while (i)
            finalizers[--i]();
    }
    function run_at_end_of_this_or_next_physical_tick(fn) {
        function finalizer() {
            fn();
            tickFinalizers.splice(tickFinalizers.indexOf(finalizer), 1);
        }
        tickFinalizers.push(finalizer);
        ++numScheduledCalls;
        asap(function () {
            if (--numScheduledCalls === 0)
                finalizePhysicalTick();
        }, []);
    }
    function addPossiblyUnhandledError(promise) {
        if (!unhandledErrors.some(function (p) { return p._value === promise._value; }))
            unhandledErrors.push(promise);
    }
    function markErrorAsHandled(promise) {
        var i = unhandledErrors.length;
        while (i)
            if (unhandledErrors[--i]._value === promise._value) {
                unhandledErrors.splice(i, 1);
                return;
            }
    }
    function PromiseReject(reason) {
        return new DexiePromise(INTERNAL, false, reason);
    }
    function wrap(fn, errorCatcher) {
        var psd = PSD;
        return function () {
            var wasRootExec = beginMicroTickScope(), outerScope = PSD;
            try {
                switchToZone(psd, true);
                return fn.apply(this, arguments);
            }
            catch (e) {
                errorCatcher && errorCatcher(e);
            }
            finally {
                switchToZone(outerScope, false);
                if (wasRootExec)
                    endMicroTickScope();
            }
        };
    }
    var task = { awaits: 0, echoes: 0, id: 0 };
    var taskCounter = 0;
    var zoneStack = [];
    var zoneEchoes = 0;
    var totalEchoes = 0;
    var zone_id_counter = 0;
    function newScope(fn, props, a1, a2) {
        var parent = PSD, psd = Object.create(parent);
        psd.parent = parent;
        psd.ref = 0;
        psd.global = false;
        psd.id = ++zone_id_counter;
        var globalEnv = globalPSD.env;
        psd.env = patchGlobalPromise ? {
            Promise: DexiePromise,
            PromiseProp: { value: DexiePromise, configurable: true, writable: true },
            all: DexiePromise.all,
            race: DexiePromise.race,
            allSettled: DexiePromise.allSettled,
            any: DexiePromise.any,
            resolve: DexiePromise.resolve,
            reject: DexiePromise.reject,
            nthen: getPatchedPromiseThen(globalEnv.nthen, psd),
            gthen: getPatchedPromiseThen(globalEnv.gthen, psd)
        } : {};
        if (props)
            extend(psd, props);
        ++parent.ref;
        psd.finalize = function () {
            --this.parent.ref || this.parent.finalize();
        };
        var rv = usePSD(psd, fn, a1, a2);
        if (psd.ref === 0)
            psd.finalize();
        return rv;
    }
    function incrementExpectedAwaits() {
        if (!task.id)
            task.id = ++taskCounter;
        ++task.awaits;
        task.echoes += ZONE_ECHO_LIMIT;
        return task.id;
    }
    function decrementExpectedAwaits() {
        if (!task.awaits)
            return false;
        if (--task.awaits === 0)
            task.id = 0;
        task.echoes = task.awaits * ZONE_ECHO_LIMIT;
        return true;
    }
    if (('' + nativePromiseThen).indexOf('[native code]') === -1) {
        incrementExpectedAwaits = decrementExpectedAwaits = nop;
    }
    function onPossibleParallellAsync(possiblePromise) {
        if (task.echoes && possiblePromise && possiblePromise.constructor === NativePromise) {
            incrementExpectedAwaits();
            return possiblePromise.then(function (x) {
                decrementExpectedAwaits();
                return x;
            }, function (e) {
                decrementExpectedAwaits();
                return rejection(e);
            });
        }
        return possiblePromise;
    }
    function zoneEnterEcho(targetZone) {
        ++totalEchoes;
        if (!task.echoes || --task.echoes === 0) {
            task.echoes = task.id = 0;
        }
        zoneStack.push(PSD);
        switchToZone(targetZone, true);
    }
    function zoneLeaveEcho() {
        var zone = zoneStack[zoneStack.length - 1];
        zoneStack.pop();
        switchToZone(zone, false);
    }
    function switchToZone(targetZone, bEnteringZone) {
        var currentZone = PSD;
        if (bEnteringZone ? task.echoes && (!zoneEchoes++ || targetZone !== PSD) : zoneEchoes && (!--zoneEchoes || targetZone !== PSD)) {
            enqueueNativeMicroTask(bEnteringZone ? zoneEnterEcho.bind(null, targetZone) : zoneLeaveEcho);
        }
        if (targetZone === PSD)
            return;
        PSD = targetZone;
        if (currentZone === globalPSD)
            globalPSD.env = snapShot();
        if (patchGlobalPromise) {
            var GlobalPromise_1 = globalPSD.env.Promise;
            var targetEnv = targetZone.env;
            nativePromiseProto.then = targetEnv.nthen;
            GlobalPromise_1.prototype.then = targetEnv.gthen;
            if (currentZone.global || targetZone.global) {
                Object.defineProperty(_global, 'Promise', targetEnv.PromiseProp);
                GlobalPromise_1.all = targetEnv.all;
                GlobalPromise_1.race = targetEnv.race;
                GlobalPromise_1.resolve = targetEnv.resolve;
                GlobalPromise_1.reject = targetEnv.reject;
                if (targetEnv.allSettled)
                    GlobalPromise_1.allSettled = targetEnv.allSettled;
                if (targetEnv.any)
                    GlobalPromise_1.any = targetEnv.any;
            }
        }
    }
    function snapShot() {
        var GlobalPromise = _global.Promise;
        return patchGlobalPromise ? {
            Promise: GlobalPromise,
            PromiseProp: Object.getOwnPropertyDescriptor(_global, "Promise"),
            all: GlobalPromise.all,
            race: GlobalPromise.race,
            allSettled: GlobalPromise.allSettled,
            any: GlobalPromise.any,
            resolve: GlobalPromise.resolve,
            reject: GlobalPromise.reject,
            nthen: nativePromiseProto.then,
            gthen: GlobalPromise.prototype.then
        } : {};
    }
    function usePSD(psd, fn, a1, a2, a3) {
        var outerScope = PSD;
        try {
            switchToZone(psd, true);
            return fn(a1, a2, a3);
        }
        finally {
            switchToZone(outerScope, false);
        }
    }
    function enqueueNativeMicroTask(job) {
        nativePromiseThen.call(resolvedNativePromise, job);
    }
    function nativeAwaitCompatibleWrap(fn, zone, possibleAwait, cleanup) {
        return typeof fn !== 'function' ? fn : function () {
            var outerZone = PSD;
            if (possibleAwait)
                incrementExpectedAwaits();
            switchToZone(zone, true);
            try {
                return fn.apply(this, arguments);
            }
            finally {
                switchToZone(outerZone, false);
                if (cleanup)
                    enqueueNativeMicroTask(decrementExpectedAwaits);
            }
        };
    }
    function getPatchedPromiseThen(origThen, zone) {
        return function (onResolved, onRejected) {
            return origThen.call(this, nativeAwaitCompatibleWrap(onResolved, zone), nativeAwaitCompatibleWrap(onRejected, zone));
        };
    }
    var UNHANDLEDREJECTION = "unhandledrejection";
    function globalError(err, promise) {
        var rv;
        try {
            rv = promise.onuncatched(err);
        }
        catch (e) { }
        if (rv !== false)
            try {
                var event, eventData = { promise: promise, reason: err };
                if (_global.document && document.createEvent) {
                    event = document.createEvent('Event');
                    event.initEvent(UNHANDLEDREJECTION, true, true);
                    extend(event, eventData);
                }
                else if (_global.CustomEvent) {
                    event = new CustomEvent(UNHANDLEDREJECTION, { detail: eventData });
                    extend(event, eventData);
                }
                if (event && _global.dispatchEvent) {
                    dispatchEvent(event);
                    if (!_global.PromiseRejectionEvent && _global.onunhandledrejection)
                        try {
                            _global.onunhandledrejection(event);
                        }
                        catch (_) { }
                }
                if (debug && event && !event.defaultPrevented) {
                    console.warn("Unhandled rejection: " + (err.stack || err));
                }
            }
            catch (e) { }
    }
    var rejection = DexiePromise.reject;

    function tempTransaction(db, mode, storeNames, fn) {
        if (!db.idbdb || (!db._state.openComplete && (!PSD.letThrough && !db._vip))) {
            if (db._state.openComplete) {
                return rejection(new exceptions.DatabaseClosed(db._state.dbOpenError));
            }
            if (!db._state.isBeingOpened) {
                if (!db._options.autoOpen)
                    return rejection(new exceptions.DatabaseClosed());
                db.open().catch(nop);
            }
            return db._state.dbReadyPromise.then(function () { return tempTransaction(db, mode, storeNames, fn); });
        }
        else {
            var trans = db._createTransaction(mode, storeNames, db._dbSchema);
            try {
                trans.create();
                db._state.PR1398_maxLoop = 3;
            }
            catch (ex) {
                if (ex.name === errnames.InvalidState && db.isOpen() && --db._state.PR1398_maxLoop > 0) {
                    console.warn('Dexie: Need to reopen db');
                    db._close();
                    return db.open().then(function () { return tempTransaction(db, mode, storeNames, fn); });
                }
                return rejection(ex);
            }
            return trans._promise(mode, function (resolve, reject) {
                return newScope(function () {
                    PSD.trans = trans;
                    return fn(resolve, reject, trans);
                });
            }).then(function (result) {
                return trans._completion.then(function () { return result; });
            });
        }
    }

    var DEXIE_VERSION = '3.2.2';
    var maxString = String.fromCharCode(65535);
    var minKey = -Infinity;
    var INVALID_KEY_ARGUMENT = "Invalid key provided. Keys must be of type string, number, Date or Array<string | number | Date>.";
    var STRING_EXPECTED = "String expected.";
    var connections = [];
    var isIEOrEdge = typeof navigator !== 'undefined' && /(MSIE|Trident|Edge)/.test(navigator.userAgent);
    var hasIEDeleteObjectStoreBug = isIEOrEdge;
    var hangsOnDeleteLargeKeyRange = isIEOrEdge;
    var dexieStackFrameFilter = function (frame) { return !/(dexie\.js|dexie\.min\.js)/.test(frame); };
    var DBNAMES_DB = '__dbnames';
    var READONLY = 'readonly';
    var READWRITE = 'readwrite';

    function combine(filter1, filter2) {
        return filter1 ?
            filter2 ?
                function () { return filter1.apply(this, arguments) && filter2.apply(this, arguments); } :
                filter1 :
            filter2;
    }

    var AnyRange = {
        type: 3 ,
        lower: -Infinity,
        lowerOpen: false,
        upper: [[]],
        upperOpen: false
    };

    function workaroundForUndefinedPrimKey(keyPath) {
        return typeof keyPath === "string" && !/\./.test(keyPath)
            ? function (obj) {
                if (obj[keyPath] === undefined && (keyPath in obj)) {
                    obj = deepClone(obj);
                    delete obj[keyPath];
                }
                return obj;
            }
            : function (obj) { return obj; };
    }

    var Table =  (function () {
        function Table() {
        }
        Table.prototype._trans = function (mode, fn, writeLocked) {
            var trans = this._tx || PSD.trans;
            var tableName = this.name;
            function checkTableInTransaction(resolve, reject, trans) {
                if (!trans.schema[tableName])
                    throw new exceptions.NotFound("Table " + tableName + " not part of transaction");
                return fn(trans.idbtrans, trans);
            }
            var wasRootExec = beginMicroTickScope();
            try {
                return trans && trans.db === this.db ?
                    trans === PSD.trans ?
                        trans._promise(mode, checkTableInTransaction, writeLocked) :
                        newScope(function () { return trans._promise(mode, checkTableInTransaction, writeLocked); }, { trans: trans, transless: PSD.transless || PSD }) :
                    tempTransaction(this.db, mode, [this.name], checkTableInTransaction);
            }
            finally {
                if (wasRootExec)
                    endMicroTickScope();
            }
        };
        Table.prototype.get = function (keyOrCrit, cb) {
            var _this = this;
            if (keyOrCrit && keyOrCrit.constructor === Object)
                return this.where(keyOrCrit).first(cb);
            return this._trans('readonly', function (trans) {
                return _this.core.get({ trans: trans, key: keyOrCrit })
                    .then(function (res) { return _this.hook.reading.fire(res); });
            }).then(cb);
        };
        Table.prototype.where = function (indexOrCrit) {
            if (typeof indexOrCrit === 'string')
                return new this.db.WhereClause(this, indexOrCrit);
            if (isArray(indexOrCrit))
                return new this.db.WhereClause(this, "[" + indexOrCrit.join('+') + "]");
            var keyPaths = keys(indexOrCrit);
            if (keyPaths.length === 1)
                return this
                    .where(keyPaths[0])
                    .equals(indexOrCrit[keyPaths[0]]);
            var compoundIndex = this.schema.indexes.concat(this.schema.primKey).filter(function (ix) {
                return ix.compound &&
                    keyPaths.every(function (keyPath) { return ix.keyPath.indexOf(keyPath) >= 0; }) &&
                    ix.keyPath.every(function (keyPath) { return keyPaths.indexOf(keyPath) >= 0; });
            })[0];
            if (compoundIndex && this.db._maxKey !== maxString)
                return this
                    .where(compoundIndex.name)
                    .equals(compoundIndex.keyPath.map(function (kp) { return indexOrCrit[kp]; }));
            if (!compoundIndex && debug)
                console.warn("The query " + JSON.stringify(indexOrCrit) + " on " + this.name + " would benefit of a " +
                    ("compound index [" + keyPaths.join('+') + "]"));
            var idxByName = this.schema.idxByName;
            var idb = this.db._deps.indexedDB;
            function equals(a, b) {
                try {
                    return idb.cmp(a, b) === 0;
                }
                catch (e) {
                    return false;
                }
            }
            var _a = keyPaths.reduce(function (_a, keyPath) {
                var prevIndex = _a[0], prevFilterFn = _a[1];
                var index = idxByName[keyPath];
                var value = indexOrCrit[keyPath];
                return [
                    prevIndex || index,
                    prevIndex || !index ?
                        combine(prevFilterFn, index && index.multi ?
                            function (x) {
                                var prop = getByKeyPath(x, keyPath);
                                return isArray(prop) && prop.some(function (item) { return equals(value, item); });
                            } : function (x) { return equals(value, getByKeyPath(x, keyPath)); })
                        : prevFilterFn
                ];
            }, [null, null]), idx = _a[0], filterFunction = _a[1];
            return idx ?
                this.where(idx.name).equals(indexOrCrit[idx.keyPath])
                    .filter(filterFunction) :
                compoundIndex ?
                    this.filter(filterFunction) :
                    this.where(keyPaths).equals('');
        };
        Table.prototype.filter = function (filterFunction) {
            return this.toCollection().and(filterFunction);
        };
        Table.prototype.count = function (thenShortcut) {
            return this.toCollection().count(thenShortcut);
        };
        Table.prototype.offset = function (offset) {
            return this.toCollection().offset(offset);
        };
        Table.prototype.limit = function (numRows) {
            return this.toCollection().limit(numRows);
        };
        Table.prototype.each = function (callback) {
            return this.toCollection().each(callback);
        };
        Table.prototype.toArray = function (thenShortcut) {
            return this.toCollection().toArray(thenShortcut);
        };
        Table.prototype.toCollection = function () {
            return new this.db.Collection(new this.db.WhereClause(this));
        };
        Table.prototype.orderBy = function (index) {
            return new this.db.Collection(new this.db.WhereClause(this, isArray(index) ?
                "[" + index.join('+') + "]" :
                index));
        };
        Table.prototype.reverse = function () {
            return this.toCollection().reverse();
        };
        Table.prototype.mapToClass = function (constructor) {
            this.schema.mappedClass = constructor;
            var readHook = function (obj) {
                if (!obj)
                    return obj;
                var res = Object.create(constructor.prototype);
                for (var m in obj)
                    if (hasOwn(obj, m))
                        try {
                            res[m] = obj[m];
                        }
                        catch (_) { }
                return res;
            };
            if (this.schema.readHook) {
                this.hook.reading.unsubscribe(this.schema.readHook);
            }
            this.schema.readHook = readHook;
            this.hook("reading", readHook);
            return constructor;
        };
        Table.prototype.defineClass = function () {
            function Class(content) {
                extend(this, content);
            }
            return this.mapToClass(Class);
        };
        Table.prototype.add = function (obj, key) {
            var _this = this;
            var _a = this.schema.primKey, auto = _a.auto, keyPath = _a.keyPath;
            var objToAdd = obj;
            if (keyPath && auto) {
                objToAdd = workaroundForUndefinedPrimKey(keyPath)(obj);
            }
            return this._trans('readwrite', function (trans) {
                return _this.core.mutate({ trans: trans, type: 'add', keys: key != null ? [key] : null, values: [objToAdd] });
            }).then(function (res) { return res.numFailures ? DexiePromise.reject(res.failures[0]) : res.lastResult; })
                .then(function (lastResult) {
                if (keyPath) {
                    try {
                        setByKeyPath(obj, keyPath, lastResult);
                    }
                    catch (_) { }
                }
                return lastResult;
            });
        };
        Table.prototype.update = function (keyOrObject, modifications) {
            if (typeof keyOrObject === 'object' && !isArray(keyOrObject)) {
                var key = getByKeyPath(keyOrObject, this.schema.primKey.keyPath);
                if (key === undefined)
                    return rejection(new exceptions.InvalidArgument("Given object does not contain its primary key"));
                try {
                    if (typeof modifications !== "function") {
                        keys(modifications).forEach(function (keyPath) {
                            setByKeyPath(keyOrObject, keyPath, modifications[keyPath]);
                        });
                    }
                    else {
                        modifications(keyOrObject, { value: keyOrObject, primKey: key });
                    }
                }
                catch (_a) {
                }
                return this.where(":id").equals(key).modify(modifications);
            }
            else {
                return this.where(":id").equals(keyOrObject).modify(modifications);
            }
        };
        Table.prototype.put = function (obj, key) {
            var _this = this;
            var _a = this.schema.primKey, auto = _a.auto, keyPath = _a.keyPath;
            var objToAdd = obj;
            if (keyPath && auto) {
                objToAdd = workaroundForUndefinedPrimKey(keyPath)(obj);
            }
            return this._trans('readwrite', function (trans) { return _this.core.mutate({ trans: trans, type: 'put', values: [objToAdd], keys: key != null ? [key] : null }); })
                .then(function (res) { return res.numFailures ? DexiePromise.reject(res.failures[0]) : res.lastResult; })
                .then(function (lastResult) {
                if (keyPath) {
                    try {
                        setByKeyPath(obj, keyPath, lastResult);
                    }
                    catch (_) { }
                }
                return lastResult;
            });
        };
        Table.prototype.delete = function (key) {
            var _this = this;
            return this._trans('readwrite', function (trans) { return _this.core.mutate({ trans: trans, type: 'delete', keys: [key] }); })
                .then(function (res) { return res.numFailures ? DexiePromise.reject(res.failures[0]) : undefined; });
        };
        Table.prototype.clear = function () {
            var _this = this;
            return this._trans('readwrite', function (trans) { return _this.core.mutate({ trans: trans, type: 'deleteRange', range: AnyRange }); })
                .then(function (res) { return res.numFailures ? DexiePromise.reject(res.failures[0]) : undefined; });
        };
        Table.prototype.bulkGet = function (keys) {
            var _this = this;
            return this._trans('readonly', function (trans) {
                return _this.core.getMany({
                    keys: keys,
                    trans: trans
                }).then(function (result) { return result.map(function (res) { return _this.hook.reading.fire(res); }); });
            });
        };
        Table.prototype.bulkAdd = function (objects, keysOrOptions, options) {
            var _this = this;
            var keys = Array.isArray(keysOrOptions) ? keysOrOptions : undefined;
            options = options || (keys ? undefined : keysOrOptions);
            var wantResults = options ? options.allKeys : undefined;
            return this._trans('readwrite', function (trans) {
                var _a = _this.schema.primKey, auto = _a.auto, keyPath = _a.keyPath;
                if (keyPath && keys)
                    throw new exceptions.InvalidArgument("bulkAdd(): keys argument invalid on tables with inbound keys");
                if (keys && keys.length !== objects.length)
                    throw new exceptions.InvalidArgument("Arguments objects and keys must have the same length");
                var numObjects = objects.length;
                var objectsToAdd = keyPath && auto ?
                    objects.map(workaroundForUndefinedPrimKey(keyPath)) :
                    objects;
                return _this.core.mutate({ trans: trans, type: 'add', keys: keys, values: objectsToAdd, wantResults: wantResults })
                    .then(function (_a) {
                    var numFailures = _a.numFailures, results = _a.results, lastResult = _a.lastResult, failures = _a.failures;
                    var result = wantResults ? results : lastResult;
                    if (numFailures === 0)
                        return result;
                    throw new BulkError(_this.name + ".bulkAdd(): " + numFailures + " of " + numObjects + " operations failed", failures);
                });
            });
        };
        Table.prototype.bulkPut = function (objects, keysOrOptions, options) {
            var _this = this;
            var keys = Array.isArray(keysOrOptions) ? keysOrOptions : undefined;
            options = options || (keys ? undefined : keysOrOptions);
            var wantResults = options ? options.allKeys : undefined;
            return this._trans('readwrite', function (trans) {
                var _a = _this.schema.primKey, auto = _a.auto, keyPath = _a.keyPath;
                if (keyPath && keys)
                    throw new exceptions.InvalidArgument("bulkPut(): keys argument invalid on tables with inbound keys");
                if (keys && keys.length !== objects.length)
                    throw new exceptions.InvalidArgument("Arguments objects and keys must have the same length");
                var numObjects = objects.length;
                var objectsToPut = keyPath && auto ?
                    objects.map(workaroundForUndefinedPrimKey(keyPath)) :
                    objects;
                return _this.core.mutate({ trans: trans, type: 'put', keys: keys, values: objectsToPut, wantResults: wantResults })
                    .then(function (_a) {
                    var numFailures = _a.numFailures, results = _a.results, lastResult = _a.lastResult, failures = _a.failures;
                    var result = wantResults ? results : lastResult;
                    if (numFailures === 0)
                        return result;
                    throw new BulkError(_this.name + ".bulkPut(): " + numFailures + " of " + numObjects + " operations failed", failures);
                });
            });
        };
        Table.prototype.bulkDelete = function (keys) {
            var _this = this;
            var numKeys = keys.length;
            return this._trans('readwrite', function (trans) {
                return _this.core.mutate({ trans: trans, type: 'delete', keys: keys });
            }).then(function (_a) {
                var numFailures = _a.numFailures, lastResult = _a.lastResult, failures = _a.failures;
                if (numFailures === 0)
                    return lastResult;
                throw new BulkError(_this.name + ".bulkDelete(): " + numFailures + " of " + numKeys + " operations failed", failures);
            });
        };
        return Table;
    }());

    function Events(ctx) {
        var evs = {};
        var rv = function (eventName, subscriber) {
            if (subscriber) {
                var i = arguments.length, args = new Array(i - 1);
                while (--i)
                    args[i - 1] = arguments[i];
                evs[eventName].subscribe.apply(null, args);
                return ctx;
            }
            else if (typeof (eventName) === 'string') {
                return evs[eventName];
            }
        };
        rv.addEventType = add;
        for (var i = 1, l = arguments.length; i < l; ++i) {
            add(arguments[i]);
        }
        return rv;
        function add(eventName, chainFunction, defaultFunction) {
            if (typeof eventName === 'object')
                return addConfiguredEvents(eventName);
            if (!chainFunction)
                chainFunction = reverseStoppableEventChain;
            if (!defaultFunction)
                defaultFunction = nop;
            var context = {
                subscribers: [],
                fire: defaultFunction,
                subscribe: function (cb) {
                    if (context.subscribers.indexOf(cb) === -1) {
                        context.subscribers.push(cb);
                        context.fire = chainFunction(context.fire, cb);
                    }
                },
                unsubscribe: function (cb) {
                    context.subscribers = context.subscribers.filter(function (fn) { return fn !== cb; });
                    context.fire = context.subscribers.reduce(chainFunction, defaultFunction);
                }
            };
            evs[eventName] = rv[eventName] = context;
            return context;
        }
        function addConfiguredEvents(cfg) {
            keys(cfg).forEach(function (eventName) {
                var args = cfg[eventName];
                if (isArray(args)) {
                    add(eventName, cfg[eventName][0], cfg[eventName][1]);
                }
                else if (args === 'asap') {
                    var context = add(eventName, mirror, function fire() {
                        var i = arguments.length, args = new Array(i);
                        while (i--)
                            args[i] = arguments[i];
                        context.subscribers.forEach(function (fn) {
                            asap$1(function fireEvent() {
                                fn.apply(null, args);
                            });
                        });
                    });
                }
                else
                    throw new exceptions.InvalidArgument("Invalid event config");
            });
        }
    }

    function makeClassConstructor(prototype, constructor) {
        derive(constructor).from({ prototype: prototype });
        return constructor;
    }

    function createTableConstructor(db) {
        return makeClassConstructor(Table.prototype, function Table(name, tableSchema, trans) {
            this.db = db;
            this._tx = trans;
            this.name = name;
            this.schema = tableSchema;
            this.hook = db._allTables[name] ? db._allTables[name].hook : Events(null, {
                "creating": [hookCreatingChain, nop],
                "reading": [pureFunctionChain, mirror],
                "updating": [hookUpdatingChain, nop],
                "deleting": [hookDeletingChain, nop]
            });
        });
    }

    function isPlainKeyRange(ctx, ignoreLimitFilter) {
        return !(ctx.filter || ctx.algorithm || ctx.or) &&
            (ignoreLimitFilter ? ctx.justLimit : !ctx.replayFilter);
    }
    function addFilter(ctx, fn) {
        ctx.filter = combine(ctx.filter, fn);
    }
    function addReplayFilter(ctx, factory, isLimitFilter) {
        var curr = ctx.replayFilter;
        ctx.replayFilter = curr ? function () { return combine(curr(), factory()); } : factory;
        ctx.justLimit = isLimitFilter && !curr;
    }
    function addMatchFilter(ctx, fn) {
        ctx.isMatch = combine(ctx.isMatch, fn);
    }
    function getIndexOrStore(ctx, coreSchema) {
        if (ctx.isPrimKey)
            return coreSchema.primaryKey;
        var index = coreSchema.getIndexByKeyPath(ctx.index);
        if (!index)
            throw new exceptions.Schema("KeyPath " + ctx.index + " on object store " + coreSchema.name + " is not indexed");
        return index;
    }
    function openCursor(ctx, coreTable, trans) {
        var index = getIndexOrStore(ctx, coreTable.schema);
        return coreTable.openCursor({
            trans: trans,
            values: !ctx.keysOnly,
            reverse: ctx.dir === 'prev',
            unique: !!ctx.unique,
            query: {
                index: index,
                range: ctx.range
            }
        });
    }
    function iter(ctx, fn, coreTrans, coreTable) {
        var filter = ctx.replayFilter ? combine(ctx.filter, ctx.replayFilter()) : ctx.filter;
        if (!ctx.or) {
            return iterate(openCursor(ctx, coreTable, coreTrans), combine(ctx.algorithm, filter), fn, !ctx.keysOnly && ctx.valueMapper);
        }
        else {
            var set_1 = {};
            var union = function (item, cursor, advance) {
                if (!filter || filter(cursor, advance, function (result) { return cursor.stop(result); }, function (err) { return cursor.fail(err); })) {
                    var primaryKey = cursor.primaryKey;
                    var key = '' + primaryKey;
                    if (key === '[object ArrayBuffer]')
                        key = '' + new Uint8Array(primaryKey);
                    if (!hasOwn(set_1, key)) {
                        set_1[key] = true;
                        fn(item, cursor, advance);
                    }
                }
            };
            return Promise.all([
                ctx.or._iterate(union, coreTrans),
                iterate(openCursor(ctx, coreTable, coreTrans), ctx.algorithm, union, !ctx.keysOnly && ctx.valueMapper)
            ]);
        }
    }
    function iterate(cursorPromise, filter, fn, valueMapper) {
        var mappedFn = valueMapper ? function (x, c, a) { return fn(valueMapper(x), c, a); } : fn;
        var wrappedFn = wrap(mappedFn);
        return cursorPromise.then(function (cursor) {
            if (cursor) {
                return cursor.start(function () {
                    var c = function () { return cursor.continue(); };
                    if (!filter || filter(cursor, function (advancer) { return c = advancer; }, function (val) { cursor.stop(val); c = nop; }, function (e) { cursor.fail(e); c = nop; }))
                        wrappedFn(cursor.value, cursor, function (advancer) { return c = advancer; });
                    c();
                });
            }
        });
    }

    function cmp(a, b) {
        try {
            var ta = type(a);
            var tb = type(b);
            if (ta !== tb) {
                if (ta === 'Array')
                    return 1;
                if (tb === 'Array')
                    return -1;
                if (ta === 'binary')
                    return 1;
                if (tb === 'binary')
                    return -1;
                if (ta === 'string')
                    return 1;
                if (tb === 'string')
                    return -1;
                if (ta === 'Date')
                    return 1;
                if (tb !== 'Date')
                    return NaN;
                return -1;
            }
            switch (ta) {
                case 'number':
                case 'Date':
                case 'string':
                    return a > b ? 1 : a < b ? -1 : 0;
                case 'binary': {
                    return compareUint8Arrays(getUint8Array(a), getUint8Array(b));
                }
                case 'Array':
                    return compareArrays(a, b);
            }
        }
        catch (_a) { }
        return NaN;
    }
    function compareArrays(a, b) {
        var al = a.length;
        var bl = b.length;
        var l = al < bl ? al : bl;
        for (var i = 0; i < l; ++i) {
            var res = cmp(a[i], b[i]);
            if (res !== 0)
                return res;
        }
        return al === bl ? 0 : al < bl ? -1 : 1;
    }
    function compareUint8Arrays(a, b) {
        var al = a.length;
        var bl = b.length;
        var l = al < bl ? al : bl;
        for (var i = 0; i < l; ++i) {
            if (a[i] !== b[i])
                return a[i] < b[i] ? -1 : 1;
        }
        return al === bl ? 0 : al < bl ? -1 : 1;
    }
    function type(x) {
        var t = typeof x;
        if (t !== 'object')
            return t;
        if (ArrayBuffer.isView(x))
            return 'binary';
        var tsTag = toStringTag(x);
        return tsTag === 'ArrayBuffer' ? 'binary' : tsTag;
    }
    function getUint8Array(a) {
        if (a instanceof Uint8Array)
            return a;
        if (ArrayBuffer.isView(a))
            return new Uint8Array(a.buffer, a.byteOffset, a.byteLength);
        return new Uint8Array(a);
    }

    var Collection =  (function () {
        function Collection() {
        }
        Collection.prototype._read = function (fn, cb) {
            var ctx = this._ctx;
            return ctx.error ?
                ctx.table._trans(null, rejection.bind(null, ctx.error)) :
                ctx.table._trans('readonly', fn).then(cb);
        };
        Collection.prototype._write = function (fn) {
            var ctx = this._ctx;
            return ctx.error ?
                ctx.table._trans(null, rejection.bind(null, ctx.error)) :
                ctx.table._trans('readwrite', fn, "locked");
        };
        Collection.prototype._addAlgorithm = function (fn) {
            var ctx = this._ctx;
            ctx.algorithm = combine(ctx.algorithm, fn);
        };
        Collection.prototype._iterate = function (fn, coreTrans) {
            return iter(this._ctx, fn, coreTrans, this._ctx.table.core);
        };
        Collection.prototype.clone = function (props) {
            var rv = Object.create(this.constructor.prototype), ctx = Object.create(this._ctx);
            if (props)
                extend(ctx, props);
            rv._ctx = ctx;
            return rv;
        };
        Collection.prototype.raw = function () {
            this._ctx.valueMapper = null;
            return this;
        };
        Collection.prototype.each = function (fn) {
            var ctx = this._ctx;
            return this._read(function (trans) { return iter(ctx, fn, trans, ctx.table.core); });
        };
        Collection.prototype.count = function (cb) {
            var _this = this;
            return this._read(function (trans) {
                var ctx = _this._ctx;
                var coreTable = ctx.table.core;
                if (isPlainKeyRange(ctx, true)) {
                    return coreTable.count({
                        trans: trans,
                        query: {
                            index: getIndexOrStore(ctx, coreTable.schema),
                            range: ctx.range
                        }
                    }).then(function (count) { return Math.min(count, ctx.limit); });
                }
                else {
                    var count = 0;
                    return iter(ctx, function () { ++count; return false; }, trans, coreTable)
                        .then(function () { return count; });
                }
            }).then(cb);
        };
        Collection.prototype.sortBy = function (keyPath, cb) {
            var parts = keyPath.split('.').reverse(), lastPart = parts[0], lastIndex = parts.length - 1;
            function getval(obj, i) {
                if (i)
                    return getval(obj[parts[i]], i - 1);
                return obj[lastPart];
            }
            var order = this._ctx.dir === "next" ? 1 : -1;
            function sorter(a, b) {
                var aVal = getval(a, lastIndex), bVal = getval(b, lastIndex);
                return aVal < bVal ? -order : aVal > bVal ? order : 0;
            }
            return this.toArray(function (a) {
                return a.sort(sorter);
            }).then(cb);
        };
        Collection.prototype.toArray = function (cb) {
            var _this = this;
            return this._read(function (trans) {
                var ctx = _this._ctx;
                if (ctx.dir === 'next' && isPlainKeyRange(ctx, true) && ctx.limit > 0) {
                    var valueMapper_1 = ctx.valueMapper;
                    var index = getIndexOrStore(ctx, ctx.table.core.schema);
                    return ctx.table.core.query({
                        trans: trans,
                        limit: ctx.limit,
                        values: true,
                        query: {
                            index: index,
                            range: ctx.range
                        }
                    }).then(function (_a) {
                        var result = _a.result;
                        return valueMapper_1 ? result.map(valueMapper_1) : result;
                    });
                }
                else {
                    var a_1 = [];
                    return iter(ctx, function (item) { return a_1.push(item); }, trans, ctx.table.core).then(function () { return a_1; });
                }
            }, cb);
        };
        Collection.prototype.offset = function (offset) {
            var ctx = this._ctx;
            if (offset <= 0)
                return this;
            ctx.offset += offset;
            if (isPlainKeyRange(ctx)) {
                addReplayFilter(ctx, function () {
                    var offsetLeft = offset;
                    return function (cursor, advance) {
                        if (offsetLeft === 0)
                            return true;
                        if (offsetLeft === 1) {
                            --offsetLeft;
                            return false;
                        }
                        advance(function () {
                            cursor.advance(offsetLeft);
                            offsetLeft = 0;
                        });
                        return false;
                    };
                });
            }
            else {
                addReplayFilter(ctx, function () {
                    var offsetLeft = offset;
                    return function () { return (--offsetLeft < 0); };
                });
            }
            return this;
        };
        Collection.prototype.limit = function (numRows) {
            this._ctx.limit = Math.min(this._ctx.limit, numRows);
            addReplayFilter(this._ctx, function () {
                var rowsLeft = numRows;
                return function (cursor, advance, resolve) {
                    if (--rowsLeft <= 0)
                        advance(resolve);
                    return rowsLeft >= 0;
                };
            }, true);
            return this;
        };
        Collection.prototype.until = function (filterFunction, bIncludeStopEntry) {
            addFilter(this._ctx, function (cursor, advance, resolve) {
                if (filterFunction(cursor.value)) {
                    advance(resolve);
                    return bIncludeStopEntry;
                }
                else {
                    return true;
                }
            });
            return this;
        };
        Collection.prototype.first = function (cb) {
            return this.limit(1).toArray(function (a) { return a[0]; }).then(cb);
        };
        Collection.prototype.last = function (cb) {
            return this.reverse().first(cb);
        };
        Collection.prototype.filter = function (filterFunction) {
            addFilter(this._ctx, function (cursor) {
                return filterFunction(cursor.value);
            });
            addMatchFilter(this._ctx, filterFunction);
            return this;
        };
        Collection.prototype.and = function (filter) {
            return this.filter(filter);
        };
        Collection.prototype.or = function (indexName) {
            return new this.db.WhereClause(this._ctx.table, indexName, this);
        };
        Collection.prototype.reverse = function () {
            this._ctx.dir = (this._ctx.dir === "prev" ? "next" : "prev");
            if (this._ondirectionchange)
                this._ondirectionchange(this._ctx.dir);
            return this;
        };
        Collection.prototype.desc = function () {
            return this.reverse();
        };
        Collection.prototype.eachKey = function (cb) {
            var ctx = this._ctx;
            ctx.keysOnly = !ctx.isMatch;
            return this.each(function (val, cursor) { cb(cursor.key, cursor); });
        };
        Collection.prototype.eachUniqueKey = function (cb) {
            this._ctx.unique = "unique";
            return this.eachKey(cb);
        };
        Collection.prototype.eachPrimaryKey = function (cb) {
            var ctx = this._ctx;
            ctx.keysOnly = !ctx.isMatch;
            return this.each(function (val, cursor) { cb(cursor.primaryKey, cursor); });
        };
        Collection.prototype.keys = function (cb) {
            var ctx = this._ctx;
            ctx.keysOnly = !ctx.isMatch;
            var a = [];
            return this.each(function (item, cursor) {
                a.push(cursor.key);
            }).then(function () {
                return a;
            }).then(cb);
        };
        Collection.prototype.primaryKeys = function (cb) {
            var ctx = this._ctx;
            if (ctx.dir === 'next' && isPlainKeyRange(ctx, true) && ctx.limit > 0) {
                return this._read(function (trans) {
                    var index = getIndexOrStore(ctx, ctx.table.core.schema);
                    return ctx.table.core.query({
                        trans: trans,
                        values: false,
                        limit: ctx.limit,
                        query: {
                            index: index,
                            range: ctx.range
                        }
                    });
                }).then(function (_a) {
                    var result = _a.result;
                    return result;
                }).then(cb);
            }
            ctx.keysOnly = !ctx.isMatch;
            var a = [];
            return this.each(function (item, cursor) {
                a.push(cursor.primaryKey);
            }).then(function () {
                return a;
            }).then(cb);
        };
        Collection.prototype.uniqueKeys = function (cb) {
            this._ctx.unique = "unique";
            return this.keys(cb);
        };
        Collection.prototype.firstKey = function (cb) {
            return this.limit(1).keys(function (a) { return a[0]; }).then(cb);
        };
        Collection.prototype.lastKey = function (cb) {
            return this.reverse().firstKey(cb);
        };
        Collection.prototype.distinct = function () {
            var ctx = this._ctx, idx = ctx.index && ctx.table.schema.idxByName[ctx.index];
            if (!idx || !idx.multi)
                return this;
            var set = {};
            addFilter(this._ctx, function (cursor) {
                var strKey = cursor.primaryKey.toString();
                var found = hasOwn(set, strKey);
                set[strKey] = true;
                return !found;
            });
            return this;
        };
        Collection.prototype.modify = function (changes) {
            var _this = this;
            var ctx = this._ctx;
            return this._write(function (trans) {
                var modifyer;
                if (typeof changes === 'function') {
                    modifyer = changes;
                }
                else {
                    var keyPaths = keys(changes);
                    var numKeys = keyPaths.length;
                    modifyer = function (item) {
                        var anythingModified = false;
                        for (var i = 0; i < numKeys; ++i) {
                            var keyPath = keyPaths[i], val = changes[keyPath];
                            if (getByKeyPath(item, keyPath) !== val) {
                                setByKeyPath(item, keyPath, val);
                                anythingModified = true;
                            }
                        }
                        return anythingModified;
                    };
                }
                var coreTable = ctx.table.core;
                var _a = coreTable.schema.primaryKey, outbound = _a.outbound, extractKey = _a.extractKey;
                var limit = _this.db._options.modifyChunkSize || 200;
                var totalFailures = [];
                var successCount = 0;
                var failedKeys = [];
                var applyMutateResult = function (expectedCount, res) {
                    var failures = res.failures, numFailures = res.numFailures;
                    successCount += expectedCount - numFailures;
                    for (var _i = 0, _a = keys(failures); _i < _a.length; _i++) {
                        var pos = _a[_i];
                        totalFailures.push(failures[pos]);
                    }
                };
                return _this.clone().primaryKeys().then(function (keys) {
                    var nextChunk = function (offset) {
                        var count = Math.min(limit, keys.length - offset);
                        return coreTable.getMany({
                            trans: trans,
                            keys: keys.slice(offset, offset + count),
                            cache: "immutable"
                        }).then(function (values) {
                            var addValues = [];
                            var putValues = [];
                            var putKeys = outbound ? [] : null;
                            var deleteKeys = [];
                            for (var i = 0; i < count; ++i) {
                                var origValue = values[i];
                                var ctx_1 = {
                                    value: deepClone(origValue),
                                    primKey: keys[offset + i]
                                };
                                if (modifyer.call(ctx_1, ctx_1.value, ctx_1) !== false) {
                                    if (ctx_1.value == null) {
                                        deleteKeys.push(keys[offset + i]);
                                    }
                                    else if (!outbound && cmp(extractKey(origValue), extractKey(ctx_1.value)) !== 0) {
                                        deleteKeys.push(keys[offset + i]);
                                        addValues.push(ctx_1.value);
                                    }
                                    else {
                                        putValues.push(ctx_1.value);
                                        if (outbound)
                                            putKeys.push(keys[offset + i]);
                                    }
                                }
                            }
                            var criteria = isPlainKeyRange(ctx) &&
                                ctx.limit === Infinity &&
                                (typeof changes !== 'function' || changes === deleteCallback) && {
                                index: ctx.index,
                                range: ctx.range
                            };
                            return Promise.resolve(addValues.length > 0 &&
                                coreTable.mutate({ trans: trans, type: 'add', values: addValues })
                                    .then(function (res) {
                                    for (var pos in res.failures) {
                                        deleteKeys.splice(parseInt(pos), 1);
                                    }
                                    applyMutateResult(addValues.length, res);
                                })).then(function () { return (putValues.length > 0 || (criteria && typeof changes === 'object')) &&
                                coreTable.mutate({
                                    trans: trans,
                                    type: 'put',
                                    keys: putKeys,
                                    values: putValues,
                                    criteria: criteria,
                                    changeSpec: typeof changes !== 'function'
                                        && changes
                                }).then(function (res) { return applyMutateResult(putValues.length, res); }); }).then(function () { return (deleteKeys.length > 0 || (criteria && changes === deleteCallback)) &&
                                coreTable.mutate({
                                    trans: trans,
                                    type: 'delete',
                                    keys: deleteKeys,
                                    criteria: criteria
                                }).then(function (res) { return applyMutateResult(deleteKeys.length, res); }); }).then(function () {
                                return keys.length > offset + count && nextChunk(offset + limit);
                            });
                        });
                    };
                    return nextChunk(0).then(function () {
                        if (totalFailures.length > 0)
                            throw new ModifyError("Error modifying one or more objects", totalFailures, successCount, failedKeys);
                        return keys.length;
                    });
                });
            });
        };
        Collection.prototype.delete = function () {
            var ctx = this._ctx, range = ctx.range;
            if (isPlainKeyRange(ctx) &&
                ((ctx.isPrimKey && !hangsOnDeleteLargeKeyRange) || range.type === 3 ))
             {
                return this._write(function (trans) {
                    var primaryKey = ctx.table.core.schema.primaryKey;
                    var coreRange = range;
                    return ctx.table.core.count({ trans: trans, query: { index: primaryKey, range: coreRange } }).then(function (count) {
                        return ctx.table.core.mutate({ trans: trans, type: 'deleteRange', range: coreRange })
                            .then(function (_a) {
                            var failures = _a.failures; _a.lastResult; _a.results; var numFailures = _a.numFailures;
                            if (numFailures)
                                throw new ModifyError("Could not delete some values", Object.keys(failures).map(function (pos) { return failures[pos]; }), count - numFailures);
                            return count - numFailures;
                        });
                    });
                });
            }
            return this.modify(deleteCallback);
        };
        return Collection;
    }());
    var deleteCallback = function (value, ctx) { return ctx.value = null; };

    function createCollectionConstructor(db) {
        return makeClassConstructor(Collection.prototype, function Collection(whereClause, keyRangeGenerator) {
            this.db = db;
            var keyRange = AnyRange, error = null;
            if (keyRangeGenerator)
                try {
                    keyRange = keyRangeGenerator();
                }
                catch (ex) {
                    error = ex;
                }
            var whereCtx = whereClause._ctx;
            var table = whereCtx.table;
            var readingHook = table.hook.reading.fire;
            this._ctx = {
                table: table,
                index: whereCtx.index,
                isPrimKey: (!whereCtx.index || (table.schema.primKey.keyPath && whereCtx.index === table.schema.primKey.name)),
                range: keyRange,
                keysOnly: false,
                dir: "next",
                unique: "",
                algorithm: null,
                filter: null,
                replayFilter: null,
                justLimit: true,
                isMatch: null,
                offset: 0,
                limit: Infinity,
                error: error,
                or: whereCtx.or,
                valueMapper: readingHook !== mirror ? readingHook : null
            };
        });
    }

    function simpleCompare(a, b) {
        return a < b ? -1 : a === b ? 0 : 1;
    }
    function simpleCompareReverse(a, b) {
        return a > b ? -1 : a === b ? 0 : 1;
    }

    function fail(collectionOrWhereClause, err, T) {
        var collection = collectionOrWhereClause instanceof WhereClause ?
            new collectionOrWhereClause.Collection(collectionOrWhereClause) :
            collectionOrWhereClause;
        collection._ctx.error = T ? new T(err) : new TypeError(err);
        return collection;
    }
    function emptyCollection(whereClause) {
        return new whereClause.Collection(whereClause, function () { return rangeEqual(""); }).limit(0);
    }
    function upperFactory(dir) {
        return dir === "next" ?
            function (s) { return s.toUpperCase(); } :
            function (s) { return s.toLowerCase(); };
    }
    function lowerFactory(dir) {
        return dir === "next" ?
            function (s) { return s.toLowerCase(); } :
            function (s) { return s.toUpperCase(); };
    }
    function nextCasing(key, lowerKey, upperNeedle, lowerNeedle, cmp, dir) {
        var length = Math.min(key.length, lowerNeedle.length);
        var llp = -1;
        for (var i = 0; i < length; ++i) {
            var lwrKeyChar = lowerKey[i];
            if (lwrKeyChar !== lowerNeedle[i]) {
                if (cmp(key[i], upperNeedle[i]) < 0)
                    return key.substr(0, i) + upperNeedle[i] + upperNeedle.substr(i + 1);
                if (cmp(key[i], lowerNeedle[i]) < 0)
                    return key.substr(0, i) + lowerNeedle[i] + upperNeedle.substr(i + 1);
                if (llp >= 0)
                    return key.substr(0, llp) + lowerKey[llp] + upperNeedle.substr(llp + 1);
                return null;
            }
            if (cmp(key[i], lwrKeyChar) < 0)
                llp = i;
        }
        if (length < lowerNeedle.length && dir === "next")
            return key + upperNeedle.substr(key.length);
        if (length < key.length && dir === "prev")
            return key.substr(0, upperNeedle.length);
        return (llp < 0 ? null : key.substr(0, llp) + lowerNeedle[llp] + upperNeedle.substr(llp + 1));
    }
    function addIgnoreCaseAlgorithm(whereClause, match, needles, suffix) {
        var upper, lower, compare, upperNeedles, lowerNeedles, direction, nextKeySuffix, needlesLen = needles.length;
        if (!needles.every(function (s) { return typeof s === 'string'; })) {
            return fail(whereClause, STRING_EXPECTED);
        }
        function initDirection(dir) {
            upper = upperFactory(dir);
            lower = lowerFactory(dir);
            compare = (dir === "next" ? simpleCompare : simpleCompareReverse);
            var needleBounds = needles.map(function (needle) {
                return { lower: lower(needle), upper: upper(needle) };
            }).sort(function (a, b) {
                return compare(a.lower, b.lower);
            });
            upperNeedles = needleBounds.map(function (nb) { return nb.upper; });
            lowerNeedles = needleBounds.map(function (nb) { return nb.lower; });
            direction = dir;
            nextKeySuffix = (dir === "next" ? "" : suffix);
        }
        initDirection("next");
        var c = new whereClause.Collection(whereClause, function () { return createRange(upperNeedles[0], lowerNeedles[needlesLen - 1] + suffix); });
        c._ondirectionchange = function (direction) {
            initDirection(direction);
        };
        var firstPossibleNeedle = 0;
        c._addAlgorithm(function (cursor, advance, resolve) {
            var key = cursor.key;
            if (typeof key !== 'string')
                return false;
            var lowerKey = lower(key);
            if (match(lowerKey, lowerNeedles, firstPossibleNeedle)) {
                return true;
            }
            else {
                var lowestPossibleCasing = null;
                for (var i = firstPossibleNeedle; i < needlesLen; ++i) {
                    var casing = nextCasing(key, lowerKey, upperNeedles[i], lowerNeedles[i], compare, direction);
                    if (casing === null && lowestPossibleCasing === null)
                        firstPossibleNeedle = i + 1;
                    else if (lowestPossibleCasing === null || compare(lowestPossibleCasing, casing) > 0) {
                        lowestPossibleCasing = casing;
                    }
                }
                if (lowestPossibleCasing !== null) {
                    advance(function () { cursor.continue(lowestPossibleCasing + nextKeySuffix); });
                }
                else {
                    advance(resolve);
                }
                return false;
            }
        });
        return c;
    }
    function createRange(lower, upper, lowerOpen, upperOpen) {
        return {
            type: 2 ,
            lower: lower,
            upper: upper,
            lowerOpen: lowerOpen,
            upperOpen: upperOpen
        };
    }
    function rangeEqual(value) {
        return {
            type: 1 ,
            lower: value,
            upper: value
        };
    }

    var WhereClause =  (function () {
        function WhereClause() {
        }
        Object.defineProperty(WhereClause.prototype, "Collection", {
            get: function () {
                return this._ctx.table.db.Collection;
            },
            enumerable: false,
            configurable: true
        });
        WhereClause.prototype.between = function (lower, upper, includeLower, includeUpper) {
            includeLower = includeLower !== false;
            includeUpper = includeUpper === true;
            try {
                if ((this._cmp(lower, upper) > 0) ||
                    (this._cmp(lower, upper) === 0 && (includeLower || includeUpper) && !(includeLower && includeUpper)))
                    return emptyCollection(this);
                return new this.Collection(this, function () { return createRange(lower, upper, !includeLower, !includeUpper); });
            }
            catch (e) {
                return fail(this, INVALID_KEY_ARGUMENT);
            }
        };
        WhereClause.prototype.equals = function (value) {
            if (value == null)
                return fail(this, INVALID_KEY_ARGUMENT);
            return new this.Collection(this, function () { return rangeEqual(value); });
        };
        WhereClause.prototype.above = function (value) {
            if (value == null)
                return fail(this, INVALID_KEY_ARGUMENT);
            return new this.Collection(this, function () { return createRange(value, undefined, true); });
        };
        WhereClause.prototype.aboveOrEqual = function (value) {
            if (value == null)
                return fail(this, INVALID_KEY_ARGUMENT);
            return new this.Collection(this, function () { return createRange(value, undefined, false); });
        };
        WhereClause.prototype.below = function (value) {
            if (value == null)
                return fail(this, INVALID_KEY_ARGUMENT);
            return new this.Collection(this, function () { return createRange(undefined, value, false, true); });
        };
        WhereClause.prototype.belowOrEqual = function (value) {
            if (value == null)
                return fail(this, INVALID_KEY_ARGUMENT);
            return new this.Collection(this, function () { return createRange(undefined, value); });
        };
        WhereClause.prototype.startsWith = function (str) {
            if (typeof str !== 'string')
                return fail(this, STRING_EXPECTED);
            return this.between(str, str + maxString, true, true);
        };
        WhereClause.prototype.startsWithIgnoreCase = function (str) {
            if (str === "")
                return this.startsWith(str);
            return addIgnoreCaseAlgorithm(this, function (x, a) { return x.indexOf(a[0]) === 0; }, [str], maxString);
        };
        WhereClause.prototype.equalsIgnoreCase = function (str) {
            return addIgnoreCaseAlgorithm(this, function (x, a) { return x === a[0]; }, [str], "");
        };
        WhereClause.prototype.anyOfIgnoreCase = function () {
            var set = getArrayOf.apply(NO_CHAR_ARRAY, arguments);
            if (set.length === 0)
                return emptyCollection(this);
            return addIgnoreCaseAlgorithm(this, function (x, a) { return a.indexOf(x) !== -1; }, set, "");
        };
        WhereClause.prototype.startsWithAnyOfIgnoreCase = function () {
            var set = getArrayOf.apply(NO_CHAR_ARRAY, arguments);
            if (set.length === 0)
                return emptyCollection(this);
            return addIgnoreCaseAlgorithm(this, function (x, a) { return a.some(function (n) { return x.indexOf(n) === 0; }); }, set, maxString);
        };
        WhereClause.prototype.anyOf = function () {
            var _this = this;
            var set = getArrayOf.apply(NO_CHAR_ARRAY, arguments);
            var compare = this._cmp;
            try {
                set.sort(compare);
            }
            catch (e) {
                return fail(this, INVALID_KEY_ARGUMENT);
            }
            if (set.length === 0)
                return emptyCollection(this);
            var c = new this.Collection(this, function () { return createRange(set[0], set[set.length - 1]); });
            c._ondirectionchange = function (direction) {
                compare = (direction === "next" ?
                    _this._ascending :
                    _this._descending);
                set.sort(compare);
            };
            var i = 0;
            c._addAlgorithm(function (cursor, advance, resolve) {
                var key = cursor.key;
                while (compare(key, set[i]) > 0) {
                    ++i;
                    if (i === set.length) {
                        advance(resolve);
                        return false;
                    }
                }
                if (compare(key, set[i]) === 0) {
                    return true;
                }
                else {
                    advance(function () { cursor.continue(set[i]); });
                    return false;
                }
            });
            return c;
        };
        WhereClause.prototype.notEqual = function (value) {
            return this.inAnyRange([[minKey, value], [value, this.db._maxKey]], { includeLowers: false, includeUppers: false });
        };
        WhereClause.prototype.noneOf = function () {
            var set = getArrayOf.apply(NO_CHAR_ARRAY, arguments);
            if (set.length === 0)
                return new this.Collection(this);
            try {
                set.sort(this._ascending);
            }
            catch (e) {
                return fail(this, INVALID_KEY_ARGUMENT);
            }
            var ranges = set.reduce(function (res, val) { return res ?
                res.concat([[res[res.length - 1][1], val]]) :
                [[minKey, val]]; }, null);
            ranges.push([set[set.length - 1], this.db._maxKey]);
            return this.inAnyRange(ranges, { includeLowers: false, includeUppers: false });
        };
        WhereClause.prototype.inAnyRange = function (ranges, options) {
            var _this = this;
            var cmp = this._cmp, ascending = this._ascending, descending = this._descending, min = this._min, max = this._max;
            if (ranges.length === 0)
                return emptyCollection(this);
            if (!ranges.every(function (range) {
                return range[0] !== undefined &&
                    range[1] !== undefined &&
                    ascending(range[0], range[1]) <= 0;
            })) {
                return fail(this, "First argument to inAnyRange() must be an Array of two-value Arrays [lower,upper] where upper must not be lower than lower", exceptions.InvalidArgument);
            }
            var includeLowers = !options || options.includeLowers !== false;
            var includeUppers = options && options.includeUppers === true;
            function addRange(ranges, newRange) {
                var i = 0, l = ranges.length;
                for (; i < l; ++i) {
                    var range = ranges[i];
                    if (cmp(newRange[0], range[1]) < 0 && cmp(newRange[1], range[0]) > 0) {
                        range[0] = min(range[0], newRange[0]);
                        range[1] = max(range[1], newRange[1]);
                        break;
                    }
                }
                if (i === l)
                    ranges.push(newRange);
                return ranges;
            }
            var sortDirection = ascending;
            function rangeSorter(a, b) { return sortDirection(a[0], b[0]); }
            var set;
            try {
                set = ranges.reduce(addRange, []);
                set.sort(rangeSorter);
            }
            catch (ex) {
                return fail(this, INVALID_KEY_ARGUMENT);
            }
            var rangePos = 0;
            var keyIsBeyondCurrentEntry = includeUppers ?
                function (key) { return ascending(key, set[rangePos][1]) > 0; } :
                function (key) { return ascending(key, set[rangePos][1]) >= 0; };
            var keyIsBeforeCurrentEntry = includeLowers ?
                function (key) { return descending(key, set[rangePos][0]) > 0; } :
                function (key) { return descending(key, set[rangePos][0]) >= 0; };
            function keyWithinCurrentRange(key) {
                return !keyIsBeyondCurrentEntry(key) && !keyIsBeforeCurrentEntry(key);
            }
            var checkKey = keyIsBeyondCurrentEntry;
            var c = new this.Collection(this, function () { return createRange(set[0][0], set[set.length - 1][1], !includeLowers, !includeUppers); });
            c._ondirectionchange = function (direction) {
                if (direction === "next") {
                    checkKey = keyIsBeyondCurrentEntry;
                    sortDirection = ascending;
                }
                else {
                    checkKey = keyIsBeforeCurrentEntry;
                    sortDirection = descending;
                }
                set.sort(rangeSorter);
            };
            c._addAlgorithm(function (cursor, advance, resolve) {
                var key = cursor.key;
                while (checkKey(key)) {
                    ++rangePos;
                    if (rangePos === set.length) {
                        advance(resolve);
                        return false;
                    }
                }
                if (keyWithinCurrentRange(key)) {
                    return true;
                }
                else if (_this._cmp(key, set[rangePos][1]) === 0 || _this._cmp(key, set[rangePos][0]) === 0) {
                    return false;
                }
                else {
                    advance(function () {
                        if (sortDirection === ascending)
                            cursor.continue(set[rangePos][0]);
                        else
                            cursor.continue(set[rangePos][1]);
                    });
                    return false;
                }
            });
            return c;
        };
        WhereClause.prototype.startsWithAnyOf = function () {
            var set = getArrayOf.apply(NO_CHAR_ARRAY, arguments);
            if (!set.every(function (s) { return typeof s === 'string'; })) {
                return fail(this, "startsWithAnyOf() only works with strings");
            }
            if (set.length === 0)
                return emptyCollection(this);
            return this.inAnyRange(set.map(function (str) { return [str, str + maxString]; }));
        };
        return WhereClause;
    }());

    function createWhereClauseConstructor(db) {
        return makeClassConstructor(WhereClause.prototype, function WhereClause(table, index, orCollection) {
            this.db = db;
            this._ctx = {
                table: table,
                index: index === ":id" ? null : index,
                or: orCollection
            };
            var indexedDB = db._deps.indexedDB;
            if (!indexedDB)
                throw new exceptions.MissingAPI();
            this._cmp = this._ascending = indexedDB.cmp.bind(indexedDB);
            this._descending = function (a, b) { return indexedDB.cmp(b, a); };
            this._max = function (a, b) { return indexedDB.cmp(a, b) > 0 ? a : b; };
            this._min = function (a, b) { return indexedDB.cmp(a, b) < 0 ? a : b; };
            this._IDBKeyRange = db._deps.IDBKeyRange;
        });
    }

    function eventRejectHandler(reject) {
        return wrap(function (event) {
            preventDefault(event);
            reject(event.target.error);
            return false;
        });
    }
    function preventDefault(event) {
        if (event.stopPropagation)
            event.stopPropagation();
        if (event.preventDefault)
            event.preventDefault();
    }

    var DEXIE_STORAGE_MUTATED_EVENT_NAME = 'storagemutated';
    var STORAGE_MUTATED_DOM_EVENT_NAME = 'x-storagemutated-1';
    var globalEvents = Events(null, DEXIE_STORAGE_MUTATED_EVENT_NAME);

    var Transaction =  (function () {
        function Transaction() {
        }
        Transaction.prototype._lock = function () {
            assert(!PSD.global);
            ++this._reculock;
            if (this._reculock === 1 && !PSD.global)
                PSD.lockOwnerFor = this;
            return this;
        };
        Transaction.prototype._unlock = function () {
            assert(!PSD.global);
            if (--this._reculock === 0) {
                if (!PSD.global)
                    PSD.lockOwnerFor = null;
                while (this._blockedFuncs.length > 0 && !this._locked()) {
                    var fnAndPSD = this._blockedFuncs.shift();
                    try {
                        usePSD(fnAndPSD[1], fnAndPSD[0]);
                    }
                    catch (e) { }
                }
            }
            return this;
        };
        Transaction.prototype._locked = function () {
            return this._reculock && PSD.lockOwnerFor !== this;
        };
        Transaction.prototype.create = function (idbtrans) {
            var _this = this;
            if (!this.mode)
                return this;
            var idbdb = this.db.idbdb;
            var dbOpenError = this.db._state.dbOpenError;
            assert(!this.idbtrans);
            if (!idbtrans && !idbdb) {
                switch (dbOpenError && dbOpenError.name) {
                    case "DatabaseClosedError":
                        throw new exceptions.DatabaseClosed(dbOpenError);
                    case "MissingAPIError":
                        throw new exceptions.MissingAPI(dbOpenError.message, dbOpenError);
                    default:
                        throw new exceptions.OpenFailed(dbOpenError);
                }
            }
            if (!this.active)
                throw new exceptions.TransactionInactive();
            assert(this._completion._state === null);
            idbtrans = this.idbtrans = idbtrans ||
                (this.db.core
                    ? this.db.core.transaction(this.storeNames, this.mode, { durability: this.chromeTransactionDurability })
                    : idbdb.transaction(this.storeNames, this.mode, { durability: this.chromeTransactionDurability }));
            idbtrans.onerror = wrap(function (ev) {
                preventDefault(ev);
                _this._reject(idbtrans.error);
            });
            idbtrans.onabort = wrap(function (ev) {
                preventDefault(ev);
                _this.active && _this._reject(new exceptions.Abort(idbtrans.error));
                _this.active = false;
                _this.on("abort").fire(ev);
            });
            idbtrans.oncomplete = wrap(function () {
                _this.active = false;
                _this._resolve();
                if ('mutatedParts' in idbtrans) {
                    globalEvents.storagemutated.fire(idbtrans["mutatedParts"]);
                }
            });
            return this;
        };
        Transaction.prototype._promise = function (mode, fn, bWriteLock) {
            var _this = this;
            if (mode === 'readwrite' && this.mode !== 'readwrite')
                return rejection(new exceptions.ReadOnly("Transaction is readonly"));
            if (!this.active)
                return rejection(new exceptions.TransactionInactive());
            if (this._locked()) {
                return new DexiePromise(function (resolve, reject) {
                    _this._blockedFuncs.push([function () {
                            _this._promise(mode, fn, bWriteLock).then(resolve, reject);
                        }, PSD]);
                });
            }
            else if (bWriteLock) {
                return newScope(function () {
                    var p = new DexiePromise(function (resolve, reject) {
                        _this._lock();
                        var rv = fn(resolve, reject, _this);
                        if (rv && rv.then)
                            rv.then(resolve, reject);
                    });
                    p.finally(function () { return _this._unlock(); });
                    p._lib = true;
                    return p;
                });
            }
            else {
                var p = new DexiePromise(function (resolve, reject) {
                    var rv = fn(resolve, reject, _this);
                    if (rv && rv.then)
                        rv.then(resolve, reject);
                });
                p._lib = true;
                return p;
            }
        };
        Transaction.prototype._root = function () {
            return this.parent ? this.parent._root() : this;
        };
        Transaction.prototype.waitFor = function (promiseLike) {
            var root = this._root();
            var promise = DexiePromise.resolve(promiseLike);
            if (root._waitingFor) {
                root._waitingFor = root._waitingFor.then(function () { return promise; });
            }
            else {
                root._waitingFor = promise;
                root._waitingQueue = [];
                var store = root.idbtrans.objectStore(root.storeNames[0]);
                (function spin() {
                    ++root._spinCount;
                    while (root._waitingQueue.length)
                        (root._waitingQueue.shift())();
                    if (root._waitingFor)
                        store.get(-Infinity).onsuccess = spin;
                }());
            }
            var currentWaitPromise = root._waitingFor;
            return new DexiePromise(function (resolve, reject) {
                promise.then(function (res) { return root._waitingQueue.push(wrap(resolve.bind(null, res))); }, function (err) { return root._waitingQueue.push(wrap(reject.bind(null, err))); }).finally(function () {
                    if (root._waitingFor === currentWaitPromise) {
                        root._waitingFor = null;
                    }
                });
            });
        };
        Transaction.prototype.abort = function () {
            if (this.active) {
                this.active = false;
                if (this.idbtrans)
                    this.idbtrans.abort();
                this._reject(new exceptions.Abort());
            }
        };
        Transaction.prototype.table = function (tableName) {
            var memoizedTables = (this._memoizedTables || (this._memoizedTables = {}));
            if (hasOwn(memoizedTables, tableName))
                return memoizedTables[tableName];
            var tableSchema = this.schema[tableName];
            if (!tableSchema) {
                throw new exceptions.NotFound("Table " + tableName + " not part of transaction");
            }
            var transactionBoundTable = new this.db.Table(tableName, tableSchema, this);
            transactionBoundTable.core = this.db.core.table(tableName);
            memoizedTables[tableName] = transactionBoundTable;
            return transactionBoundTable;
        };
        return Transaction;
    }());

    function createTransactionConstructor(db) {
        return makeClassConstructor(Transaction.prototype, function Transaction(mode, storeNames, dbschema, chromeTransactionDurability, parent) {
            var _this = this;
            this.db = db;
            this.mode = mode;
            this.storeNames = storeNames;
            this.schema = dbschema;
            this.chromeTransactionDurability = chromeTransactionDurability;
            this.idbtrans = null;
            this.on = Events(this, "complete", "error", "abort");
            this.parent = parent || null;
            this.active = true;
            this._reculock = 0;
            this._blockedFuncs = [];
            this._resolve = null;
            this._reject = null;
            this._waitingFor = null;
            this._waitingQueue = null;
            this._spinCount = 0;
            this._completion = new DexiePromise(function (resolve, reject) {
                _this._resolve = resolve;
                _this._reject = reject;
            });
            this._completion.then(function () {
                _this.active = false;
                _this.on.complete.fire();
            }, function (e) {
                var wasActive = _this.active;
                _this.active = false;
                _this.on.error.fire(e);
                _this.parent ?
                    _this.parent._reject(e) :
                    wasActive && _this.idbtrans && _this.idbtrans.abort();
                return rejection(e);
            });
        });
    }

    function createIndexSpec(name, keyPath, unique, multi, auto, compound, isPrimKey) {
        return {
            name: name,
            keyPath: keyPath,
            unique: unique,
            multi: multi,
            auto: auto,
            compound: compound,
            src: (unique && !isPrimKey ? '&' : '') + (multi ? '*' : '') + (auto ? "++" : "") + nameFromKeyPath(keyPath)
        };
    }
    function nameFromKeyPath(keyPath) {
        return typeof keyPath === 'string' ?
            keyPath :
            keyPath ? ('[' + [].join.call(keyPath, '+') + ']') : "";
    }

    function createTableSchema(name, primKey, indexes) {
        return {
            name: name,
            primKey: primKey,
            indexes: indexes,
            mappedClass: null,
            idxByName: arrayToObject(indexes, function (index) { return [index.name, index]; })
        };
    }

    function safariMultiStoreFix(storeNames) {
        return storeNames.length === 1 ? storeNames[0] : storeNames;
    }
    var getMaxKey = function (IdbKeyRange) {
        try {
            IdbKeyRange.only([[]]);
            getMaxKey = function () { return [[]]; };
            return [[]];
        }
        catch (e) {
            getMaxKey = function () { return maxString; };
            return maxString;
        }
    };

    function getKeyExtractor(keyPath) {
        if (keyPath == null) {
            return function () { return undefined; };
        }
        else if (typeof keyPath === 'string') {
            return getSinglePathKeyExtractor(keyPath);
        }
        else {
            return function (obj) { return getByKeyPath(obj, keyPath); };
        }
    }
    function getSinglePathKeyExtractor(keyPath) {
        var split = keyPath.split('.');
        if (split.length === 1) {
            return function (obj) { return obj[keyPath]; };
        }
        else {
            return function (obj) { return getByKeyPath(obj, keyPath); };
        }
    }

    function arrayify(arrayLike) {
        return [].slice.call(arrayLike);
    }
    var _id_counter = 0;
    function getKeyPathAlias(keyPath) {
        return keyPath == null ?
            ":id" :
            typeof keyPath === 'string' ?
                keyPath :
                "[" + keyPath.join('+') + "]";
    }
    function createDBCore(db, IdbKeyRange, tmpTrans) {
        function extractSchema(db, trans) {
            var tables = arrayify(db.objectStoreNames);
            return {
                schema: {
                    name: db.name,
                    tables: tables.map(function (table) { return trans.objectStore(table); }).map(function (store) {
                        var keyPath = store.keyPath, autoIncrement = store.autoIncrement;
                        var compound = isArray(keyPath);
                        var outbound = keyPath == null;
                        var indexByKeyPath = {};
                        var result = {
                            name: store.name,
                            primaryKey: {
                                name: null,
                                isPrimaryKey: true,
                                outbound: outbound,
                                compound: compound,
                                keyPath: keyPath,
                                autoIncrement: autoIncrement,
                                unique: true,
                                extractKey: getKeyExtractor(keyPath)
                            },
                            indexes: arrayify(store.indexNames).map(function (indexName) { return store.index(indexName); })
                                .map(function (index) {
                                var name = index.name, unique = index.unique, multiEntry = index.multiEntry, keyPath = index.keyPath;
                                var compound = isArray(keyPath);
                                var result = {
                                    name: name,
                                    compound: compound,
                                    keyPath: keyPath,
                                    unique: unique,
                                    multiEntry: multiEntry,
                                    extractKey: getKeyExtractor(keyPath)
                                };
                                indexByKeyPath[getKeyPathAlias(keyPath)] = result;
                                return result;
                            }),
                            getIndexByKeyPath: function (keyPath) { return indexByKeyPath[getKeyPathAlias(keyPath)]; }
                        };
                        indexByKeyPath[":id"] = result.primaryKey;
                        if (keyPath != null) {
                            indexByKeyPath[getKeyPathAlias(keyPath)] = result.primaryKey;
                        }
                        return result;
                    })
                },
                hasGetAll: tables.length > 0 && ('getAll' in trans.objectStore(tables[0])) &&
                    !(typeof navigator !== 'undefined' && /Safari/.test(navigator.userAgent) &&
                        !/(Chrome\/|Edge\/)/.test(navigator.userAgent) &&
                        [].concat(navigator.userAgent.match(/Safari\/(\d*)/))[1] < 604)
            };
        }
        function makeIDBKeyRange(range) {
            if (range.type === 3 )
                return null;
            if (range.type === 4 )
                throw new Error("Cannot convert never type to IDBKeyRange");
            var lower = range.lower, upper = range.upper, lowerOpen = range.lowerOpen, upperOpen = range.upperOpen;
            var idbRange = lower === undefined ?
                upper === undefined ?
                    null :
                    IdbKeyRange.upperBound(upper, !!upperOpen) :
                upper === undefined ?
                    IdbKeyRange.lowerBound(lower, !!lowerOpen) :
                    IdbKeyRange.bound(lower, upper, !!lowerOpen, !!upperOpen);
            return idbRange;
        }
        function createDbCoreTable(tableSchema) {
            var tableName = tableSchema.name;
            function mutate(_a) {
                var trans = _a.trans, type = _a.type, keys = _a.keys, values = _a.values, range = _a.range;
                return new Promise(function (resolve, reject) {
                    resolve = wrap(resolve);
                    var store = trans.objectStore(tableName);
                    var outbound = store.keyPath == null;
                    var isAddOrPut = type === "put" || type === "add";
                    if (!isAddOrPut && type !== 'delete' && type !== 'deleteRange')
                        throw new Error("Invalid operation type: " + type);
                    var length = (keys || values || { length: 1 }).length;
                    if (keys && values && keys.length !== values.length) {
                        throw new Error("Given keys array must have same length as given values array.");
                    }
                    if (length === 0)
                        return resolve({ numFailures: 0, failures: {}, results: [], lastResult: undefined });
                    var req;
                    var reqs = [];
                    var failures = [];
                    var numFailures = 0;
                    var errorHandler = function (event) {
                        ++numFailures;
                        preventDefault(event);
                    };
                    if (type === 'deleteRange') {
                        if (range.type === 4 )
                            return resolve({ numFailures: numFailures, failures: failures, results: [], lastResult: undefined });
                        if (range.type === 3 )
                            reqs.push(req = store.clear());
                        else
                            reqs.push(req = store.delete(makeIDBKeyRange(range)));
                    }
                    else {
                        var _a = isAddOrPut ?
                            outbound ?
                                [values, keys] :
                                [values, null] :
                            [keys, null], args1 = _a[0], args2 = _a[1];
                        if (isAddOrPut) {
                            for (var i = 0; i < length; ++i) {
                                reqs.push(req = (args2 && args2[i] !== undefined ?
                                    store[type](args1[i], args2[i]) :
                                    store[type](args1[i])));
                                req.onerror = errorHandler;
                            }
                        }
                        else {
                            for (var i = 0; i < length; ++i) {
                                reqs.push(req = store[type](args1[i]));
                                req.onerror = errorHandler;
                            }
                        }
                    }
                    var done = function (event) {
                        var lastResult = event.target.result;
                        reqs.forEach(function (req, i) { return req.error != null && (failures[i] = req.error); });
                        resolve({
                            numFailures: numFailures,
                            failures: failures,
                            results: type === "delete" ? keys : reqs.map(function (req) { return req.result; }),
                            lastResult: lastResult
                        });
                    };
                    req.onerror = function (event) {
                        errorHandler(event);
                        done(event);
                    };
                    req.onsuccess = done;
                });
            }
            function openCursor(_a) {
                var trans = _a.trans, values = _a.values, query = _a.query, reverse = _a.reverse, unique = _a.unique;
                return new Promise(function (resolve, reject) {
                    resolve = wrap(resolve);
                    var index = query.index, range = query.range;
                    var store = trans.objectStore(tableName);
                    var source = index.isPrimaryKey ?
                        store :
                        store.index(index.name);
                    var direction = reverse ?
                        unique ?
                            "prevunique" :
                            "prev" :
                        unique ?
                            "nextunique" :
                            "next";
                    var req = values || !('openKeyCursor' in source) ?
                        source.openCursor(makeIDBKeyRange(range), direction) :
                        source.openKeyCursor(makeIDBKeyRange(range), direction);
                    req.onerror = eventRejectHandler(reject);
                    req.onsuccess = wrap(function (ev) {
                        var cursor = req.result;
                        if (!cursor) {
                            resolve(null);
                            return;
                        }
                        cursor.___id = ++_id_counter;
                        cursor.done = false;
                        var _cursorContinue = cursor.continue.bind(cursor);
                        var _cursorContinuePrimaryKey = cursor.continuePrimaryKey;
                        if (_cursorContinuePrimaryKey)
                            _cursorContinuePrimaryKey = _cursorContinuePrimaryKey.bind(cursor);
                        var _cursorAdvance = cursor.advance.bind(cursor);
                        var doThrowCursorIsNotStarted = function () { throw new Error("Cursor not started"); };
                        var doThrowCursorIsStopped = function () { throw new Error("Cursor not stopped"); };
                        cursor.trans = trans;
                        cursor.stop = cursor.continue = cursor.continuePrimaryKey = cursor.advance = doThrowCursorIsNotStarted;
                        cursor.fail = wrap(reject);
                        cursor.next = function () {
                            var _this = this;
                            var gotOne = 1;
                            return this.start(function () { return gotOne-- ? _this.continue() : _this.stop(); }).then(function () { return _this; });
                        };
                        cursor.start = function (callback) {
                            var iterationPromise = new Promise(function (resolveIteration, rejectIteration) {
                                resolveIteration = wrap(resolveIteration);
                                req.onerror = eventRejectHandler(rejectIteration);
                                cursor.fail = rejectIteration;
                                cursor.stop = function (value) {
                                    cursor.stop = cursor.continue = cursor.continuePrimaryKey = cursor.advance = doThrowCursorIsStopped;
                                    resolveIteration(value);
                                };
                            });
                            var guardedCallback = function () {
                                if (req.result) {
                                    try {
                                        callback();
                                    }
                                    catch (err) {
                                        cursor.fail(err);
                                    }
                                }
                                else {
                                    cursor.done = true;
                                    cursor.start = function () { throw new Error("Cursor behind last entry"); };
                                    cursor.stop();
                                }
                            };
                            req.onsuccess = wrap(function (ev) {
                                req.onsuccess = guardedCallback;
                                guardedCallback();
                            });
                            cursor.continue = _cursorContinue;
                            cursor.continuePrimaryKey = _cursorContinuePrimaryKey;
                            cursor.advance = _cursorAdvance;
                            guardedCallback();
                            return iterationPromise;
                        };
                        resolve(cursor);
                    }, reject);
                });
            }
            function query(hasGetAll) {
                return function (request) {
                    return new Promise(function (resolve, reject) {
                        resolve = wrap(resolve);
                        var trans = request.trans, values = request.values, limit = request.limit, query = request.query;
                        var nonInfinitLimit = limit === Infinity ? undefined : limit;
                        var index = query.index, range = query.range;
                        var store = trans.objectStore(tableName);
                        var source = index.isPrimaryKey ? store : store.index(index.name);
                        var idbKeyRange = makeIDBKeyRange(range);
                        if (limit === 0)
                            return resolve({ result: [] });
                        if (hasGetAll) {
                            var req = values ?
                                source.getAll(idbKeyRange, nonInfinitLimit) :
                                source.getAllKeys(idbKeyRange, nonInfinitLimit);
                            req.onsuccess = function (event) { return resolve({ result: event.target.result }); };
                            req.onerror = eventRejectHandler(reject);
                        }
                        else {
                            var count_1 = 0;
                            var req_1 = values || !('openKeyCursor' in source) ?
                                source.openCursor(idbKeyRange) :
                                source.openKeyCursor(idbKeyRange);
                            var result_1 = [];
                            req_1.onsuccess = function (event) {
                                var cursor = req_1.result;
                                if (!cursor)
                                    return resolve({ result: result_1 });
                                result_1.push(values ? cursor.value : cursor.primaryKey);
                                if (++count_1 === limit)
                                    return resolve({ result: result_1 });
                                cursor.continue();
                            };
                            req_1.onerror = eventRejectHandler(reject);
                        }
                    });
                };
            }
            return {
                name: tableName,
                schema: tableSchema,
                mutate: mutate,
                getMany: function (_a) {
                    var trans = _a.trans, keys = _a.keys;
                    return new Promise(function (resolve, reject) {
                        resolve = wrap(resolve);
                        var store = trans.objectStore(tableName);
                        var length = keys.length;
                        var result = new Array(length);
                        var keyCount = 0;
                        var callbackCount = 0;
                        var req;
                        var successHandler = function (event) {
                            var req = event.target;
                            if ((result[req._pos] = req.result) != null)
                                ;
                            if (++callbackCount === keyCount)
                                resolve(result);
                        };
                        var errorHandler = eventRejectHandler(reject);
                        for (var i = 0; i < length; ++i) {
                            var key = keys[i];
                            if (key != null) {
                                req = store.get(keys[i]);
                                req._pos = i;
                                req.onsuccess = successHandler;
                                req.onerror = errorHandler;
                                ++keyCount;
                            }
                        }
                        if (keyCount === 0)
                            resolve(result);
                    });
                },
                get: function (_a) {
                    var trans = _a.trans, key = _a.key;
                    return new Promise(function (resolve, reject) {
                        resolve = wrap(resolve);
                        var store = trans.objectStore(tableName);
                        var req = store.get(key);
                        req.onsuccess = function (event) { return resolve(event.target.result); };
                        req.onerror = eventRejectHandler(reject);
                    });
                },
                query: query(hasGetAll),
                openCursor: openCursor,
                count: function (_a) {
                    var query = _a.query, trans = _a.trans;
                    var index = query.index, range = query.range;
                    return new Promise(function (resolve, reject) {
                        var store = trans.objectStore(tableName);
                        var source = index.isPrimaryKey ? store : store.index(index.name);
                        var idbKeyRange = makeIDBKeyRange(range);
                        var req = idbKeyRange ? source.count(idbKeyRange) : source.count();
                        req.onsuccess = wrap(function (ev) { return resolve(ev.target.result); });
                        req.onerror = eventRejectHandler(reject);
                    });
                }
            };
        }
        var _a = extractSchema(db, tmpTrans), schema = _a.schema, hasGetAll = _a.hasGetAll;
        var tables = schema.tables.map(function (tableSchema) { return createDbCoreTable(tableSchema); });
        var tableMap = {};
        tables.forEach(function (table) { return tableMap[table.name] = table; });
        return {
            stack: "dbcore",
            transaction: db.transaction.bind(db),
            table: function (name) {
                var result = tableMap[name];
                if (!result)
                    throw new Error("Table '" + name + "' not found");
                return tableMap[name];
            },
            MIN_KEY: -Infinity,
            MAX_KEY: getMaxKey(IdbKeyRange),
            schema: schema
        };
    }

    function createMiddlewareStack(stackImpl, middlewares) {
        return middlewares.reduce(function (down, _a) {
            var create = _a.create;
            return (__assign(__assign({}, down), create(down)));
        }, stackImpl);
    }
    function createMiddlewareStacks(middlewares, idbdb, _a, tmpTrans) {
        var IDBKeyRange = _a.IDBKeyRange; _a.indexedDB;
        var dbcore = createMiddlewareStack(createDBCore(idbdb, IDBKeyRange, tmpTrans), middlewares.dbcore);
        return {
            dbcore: dbcore
        };
    }
    function generateMiddlewareStacks(_a, tmpTrans) {
        var db = _a._novip;
        var idbdb = tmpTrans.db;
        var stacks = createMiddlewareStacks(db._middlewares, idbdb, db._deps, tmpTrans);
        db.core = stacks.dbcore;
        db.tables.forEach(function (table) {
            var tableName = table.name;
            if (db.core.schema.tables.some(function (tbl) { return tbl.name === tableName; })) {
                table.core = db.core.table(tableName);
                if (db[tableName] instanceof db.Table) {
                    db[tableName].core = table.core;
                }
            }
        });
    }

    function setApiOnPlace(_a, objs, tableNames, dbschema) {
        var db = _a._novip;
        tableNames.forEach(function (tableName) {
            var schema = dbschema[tableName];
            objs.forEach(function (obj) {
                var propDesc = getPropertyDescriptor(obj, tableName);
                if (!propDesc || ("value" in propDesc && propDesc.value === undefined)) {
                    if (obj === db.Transaction.prototype || obj instanceof db.Transaction) {
                        setProp(obj, tableName, {
                            get: function () { return this.table(tableName); },
                            set: function (value) {
                                defineProperty(this, tableName, { value: value, writable: true, configurable: true, enumerable: true });
                            }
                        });
                    }
                    else {
                        obj[tableName] = new db.Table(tableName, schema);
                    }
                }
            });
        });
    }
    function removeTablesApi(_a, objs) {
        var db = _a._novip;
        objs.forEach(function (obj) {
            for (var key in obj) {
                if (obj[key] instanceof db.Table)
                    delete obj[key];
            }
        });
    }
    function lowerVersionFirst(a, b) {
        return a._cfg.version - b._cfg.version;
    }
    function runUpgraders(db, oldVersion, idbUpgradeTrans, reject) {
        var globalSchema = db._dbSchema;
        var trans = db._createTransaction('readwrite', db._storeNames, globalSchema);
        trans.create(idbUpgradeTrans);
        trans._completion.catch(reject);
        var rejectTransaction = trans._reject.bind(trans);
        var transless = PSD.transless || PSD;
        newScope(function () {
            PSD.trans = trans;
            PSD.transless = transless;
            if (oldVersion === 0) {
                keys(globalSchema).forEach(function (tableName) {
                    createTable(idbUpgradeTrans, tableName, globalSchema[tableName].primKey, globalSchema[tableName].indexes);
                });
                generateMiddlewareStacks(db, idbUpgradeTrans);
                DexiePromise.follow(function () { return db.on.populate.fire(trans); }).catch(rejectTransaction);
            }
            else
                updateTablesAndIndexes(db, oldVersion, trans, idbUpgradeTrans).catch(rejectTransaction);
        });
    }
    function updateTablesAndIndexes(_a, oldVersion, trans, idbUpgradeTrans) {
        var db = _a._novip;
        var queue = [];
        var versions = db._versions;
        var globalSchema = db._dbSchema = buildGlobalSchema(db, db.idbdb, idbUpgradeTrans);
        var anyContentUpgraderHasRun = false;
        var versToRun = versions.filter(function (v) { return v._cfg.version >= oldVersion; });
        versToRun.forEach(function (version) {
            queue.push(function () {
                var oldSchema = globalSchema;
                var newSchema = version._cfg.dbschema;
                adjustToExistingIndexNames(db, oldSchema, idbUpgradeTrans);
                adjustToExistingIndexNames(db, newSchema, idbUpgradeTrans);
                globalSchema = db._dbSchema = newSchema;
                var diff = getSchemaDiff(oldSchema, newSchema);
                diff.add.forEach(function (tuple) {
                    createTable(idbUpgradeTrans, tuple[0], tuple[1].primKey, tuple[1].indexes);
                });
                diff.change.forEach(function (change) {
                    if (change.recreate) {
                        throw new exceptions.Upgrade("Not yet support for changing primary key");
                    }
                    else {
                        var store_1 = idbUpgradeTrans.objectStore(change.name);
                        change.add.forEach(function (idx) { return addIndex(store_1, idx); });
                        change.change.forEach(function (idx) {
                            store_1.deleteIndex(idx.name);
                            addIndex(store_1, idx);
                        });
                        change.del.forEach(function (idxName) { return store_1.deleteIndex(idxName); });
                    }
                });
                var contentUpgrade = version._cfg.contentUpgrade;
                if (contentUpgrade && version._cfg.version > oldVersion) {
                    generateMiddlewareStacks(db, idbUpgradeTrans);
                    trans._memoizedTables = {};
                    anyContentUpgraderHasRun = true;
                    var upgradeSchema_1 = shallowClone(newSchema);
                    diff.del.forEach(function (table) {
                        upgradeSchema_1[table] = oldSchema[table];
                    });
                    removeTablesApi(db, [db.Transaction.prototype]);
                    setApiOnPlace(db, [db.Transaction.prototype], keys(upgradeSchema_1), upgradeSchema_1);
                    trans.schema = upgradeSchema_1;
                    var contentUpgradeIsAsync_1 = isAsyncFunction(contentUpgrade);
                    if (contentUpgradeIsAsync_1) {
                        incrementExpectedAwaits();
                    }
                    var returnValue_1;
                    var promiseFollowed = DexiePromise.follow(function () {
                        returnValue_1 = contentUpgrade(trans);
                        if (returnValue_1) {
                            if (contentUpgradeIsAsync_1) {
                                var decrementor = decrementExpectedAwaits.bind(null, null);
                                returnValue_1.then(decrementor, decrementor);
                            }
                        }
                    });
                    return (returnValue_1 && typeof returnValue_1.then === 'function' ?
                        DexiePromise.resolve(returnValue_1) : promiseFollowed.then(function () { return returnValue_1; }));
                }
            });
            queue.push(function (idbtrans) {
                if (!anyContentUpgraderHasRun || !hasIEDeleteObjectStoreBug) {
                    var newSchema = version._cfg.dbschema;
                    deleteRemovedTables(newSchema, idbtrans);
                }
                removeTablesApi(db, [db.Transaction.prototype]);
                setApiOnPlace(db, [db.Transaction.prototype], db._storeNames, db._dbSchema);
                trans.schema = db._dbSchema;
            });
        });
        function runQueue() {
            return queue.length ? DexiePromise.resolve(queue.shift()(trans.idbtrans)).then(runQueue) :
                DexiePromise.resolve();
        }
        return runQueue().then(function () {
            createMissingTables(globalSchema, idbUpgradeTrans);
        });
    }
    function getSchemaDiff(oldSchema, newSchema) {
        var diff = {
            del: [],
            add: [],
            change: []
        };
        var table;
        for (table in oldSchema) {
            if (!newSchema[table])
                diff.del.push(table);
        }
        for (table in newSchema) {
            var oldDef = oldSchema[table], newDef = newSchema[table];
            if (!oldDef) {
                diff.add.push([table, newDef]);
            }
            else {
                var change = {
                    name: table,
                    def: newDef,
                    recreate: false,
                    del: [],
                    add: [],
                    change: []
                };
                if ((
                '' + (oldDef.primKey.keyPath || '')) !== ('' + (newDef.primKey.keyPath || '')) ||
                    (oldDef.primKey.auto !== newDef.primKey.auto && !isIEOrEdge))
                 {
                    change.recreate = true;
                    diff.change.push(change);
                }
                else {
                    var oldIndexes = oldDef.idxByName;
                    var newIndexes = newDef.idxByName;
                    var idxName = void 0;
                    for (idxName in oldIndexes) {
                        if (!newIndexes[idxName])
                            change.del.push(idxName);
                    }
                    for (idxName in newIndexes) {
                        var oldIdx = oldIndexes[idxName], newIdx = newIndexes[idxName];
                        if (!oldIdx)
                            change.add.push(newIdx);
                        else if (oldIdx.src !== newIdx.src)
                            change.change.push(newIdx);
                    }
                    if (change.del.length > 0 || change.add.length > 0 || change.change.length > 0) {
                        diff.change.push(change);
                    }
                }
            }
        }
        return diff;
    }
    function createTable(idbtrans, tableName, primKey, indexes) {
        var store = idbtrans.db.createObjectStore(tableName, primKey.keyPath ?
            { keyPath: primKey.keyPath, autoIncrement: primKey.auto } :
            { autoIncrement: primKey.auto });
        indexes.forEach(function (idx) { return addIndex(store, idx); });
        return store;
    }
    function createMissingTables(newSchema, idbtrans) {
        keys(newSchema).forEach(function (tableName) {
            if (!idbtrans.db.objectStoreNames.contains(tableName)) {
                createTable(idbtrans, tableName, newSchema[tableName].primKey, newSchema[tableName].indexes);
            }
        });
    }
    function deleteRemovedTables(newSchema, idbtrans) {
        [].slice.call(idbtrans.db.objectStoreNames).forEach(function (storeName) {
            return newSchema[storeName] == null && idbtrans.db.deleteObjectStore(storeName);
        });
    }
    function addIndex(store, idx) {
        store.createIndex(idx.name, idx.keyPath, { unique: idx.unique, multiEntry: idx.multi });
    }
    function buildGlobalSchema(db, idbdb, tmpTrans) {
        var globalSchema = {};
        var dbStoreNames = slice(idbdb.objectStoreNames, 0);
        dbStoreNames.forEach(function (storeName) {
            var store = tmpTrans.objectStore(storeName);
            var keyPath = store.keyPath;
            var primKey = createIndexSpec(nameFromKeyPath(keyPath), keyPath || "", false, false, !!store.autoIncrement, keyPath && typeof keyPath !== "string", true);
            var indexes = [];
            for (var j = 0; j < store.indexNames.length; ++j) {
                var idbindex = store.index(store.indexNames[j]);
                keyPath = idbindex.keyPath;
                var index = createIndexSpec(idbindex.name, keyPath, !!idbindex.unique, !!idbindex.multiEntry, false, keyPath && typeof keyPath !== "string", false);
                indexes.push(index);
            }
            globalSchema[storeName] = createTableSchema(storeName, primKey, indexes);
        });
        return globalSchema;
    }
    function readGlobalSchema(_a, idbdb, tmpTrans) {
        var db = _a._novip;
        db.verno = idbdb.version / 10;
        var globalSchema = db._dbSchema = buildGlobalSchema(db, idbdb, tmpTrans);
        db._storeNames = slice(idbdb.objectStoreNames, 0);
        setApiOnPlace(db, [db._allTables], keys(globalSchema), globalSchema);
    }
    function verifyInstalledSchema(db, tmpTrans) {
        var installedSchema = buildGlobalSchema(db, db.idbdb, tmpTrans);
        var diff = getSchemaDiff(installedSchema, db._dbSchema);
        return !(diff.add.length || diff.change.some(function (ch) { return ch.add.length || ch.change.length; }));
    }
    function adjustToExistingIndexNames(_a, schema, idbtrans) {
        var db = _a._novip;
        var storeNames = idbtrans.db.objectStoreNames;
        for (var i = 0; i < storeNames.length; ++i) {
            var storeName = storeNames[i];
            var store = idbtrans.objectStore(storeName);
            db._hasGetAll = 'getAll' in store;
            for (var j = 0; j < store.indexNames.length; ++j) {
                var indexName = store.indexNames[j];
                var keyPath = store.index(indexName).keyPath;
                var dexieName = typeof keyPath === 'string' ? keyPath : "[" + slice(keyPath).join('+') + "]";
                if (schema[storeName]) {
                    var indexSpec = schema[storeName].idxByName[dexieName];
                    if (indexSpec) {
                        indexSpec.name = indexName;
                        delete schema[storeName].idxByName[dexieName];
                        schema[storeName].idxByName[indexName] = indexSpec;
                    }
                }
            }
        }
        if (typeof navigator !== 'undefined' && /Safari/.test(navigator.userAgent) &&
            !/(Chrome\/|Edge\/)/.test(navigator.userAgent) &&
            _global.WorkerGlobalScope && _global instanceof _global.WorkerGlobalScope &&
            [].concat(navigator.userAgent.match(/Safari\/(\d*)/))[1] < 604) {
            db._hasGetAll = false;
        }
    }
    function parseIndexSyntax(primKeyAndIndexes) {
        return primKeyAndIndexes.split(',').map(function (index, indexNum) {
            index = index.trim();
            var name = index.replace(/([&*]|\+\+)/g, "");
            var keyPath = /^\[/.test(name) ? name.match(/^\[(.*)\]$/)[1].split('+') : name;
            return createIndexSpec(name, keyPath || null, /\&/.test(index), /\*/.test(index), /\+\+/.test(index), isArray(keyPath), indexNum === 0);
        });
    }

    var Version =  (function () {
        function Version() {
        }
        Version.prototype._parseStoresSpec = function (stores, outSchema) {
            keys(stores).forEach(function (tableName) {
                if (stores[tableName] !== null) {
                    var indexes = parseIndexSyntax(stores[tableName]);
                    var primKey = indexes.shift();
                    if (primKey.multi)
                        throw new exceptions.Schema("Primary key cannot be multi-valued");
                    indexes.forEach(function (idx) {
                        if (idx.auto)
                            throw new exceptions.Schema("Only primary key can be marked as autoIncrement (++)");
                        if (!idx.keyPath)
                            throw new exceptions.Schema("Index must have a name and cannot be an empty string");
                    });
                    outSchema[tableName] = createTableSchema(tableName, primKey, indexes);
                }
            });
        };
        Version.prototype.stores = function (stores) {
            var db = this.db;
            this._cfg.storesSource = this._cfg.storesSource ?
                extend(this._cfg.storesSource, stores) :
                stores;
            var versions = db._versions;
            var storesSpec = {};
            var dbschema = {};
            versions.forEach(function (version) {
                extend(storesSpec, version._cfg.storesSource);
                dbschema = (version._cfg.dbschema = {});
                version._parseStoresSpec(storesSpec, dbschema);
            });
            db._dbSchema = dbschema;
            removeTablesApi(db, [db._allTables, db, db.Transaction.prototype]);
            setApiOnPlace(db, [db._allTables, db, db.Transaction.prototype, this._cfg.tables], keys(dbschema), dbschema);
            db._storeNames = keys(dbschema);
            return this;
        };
        Version.prototype.upgrade = function (upgradeFunction) {
            this._cfg.contentUpgrade = promisableChain(this._cfg.contentUpgrade || nop, upgradeFunction);
            return this;
        };
        return Version;
    }());

    function createVersionConstructor(db) {
        return makeClassConstructor(Version.prototype, function Version(versionNumber) {
            this.db = db;
            this._cfg = {
                version: versionNumber,
                storesSource: null,
                dbschema: {},
                tables: {},
                contentUpgrade: null
            };
        });
    }

    function getDbNamesTable(indexedDB, IDBKeyRange) {
        var dbNamesDB = indexedDB["_dbNamesDB"];
        if (!dbNamesDB) {
            dbNamesDB = indexedDB["_dbNamesDB"] = new Dexie$1(DBNAMES_DB, {
                addons: [],
                indexedDB: indexedDB,
                IDBKeyRange: IDBKeyRange,
            });
            dbNamesDB.version(1).stores({ dbnames: "name" });
        }
        return dbNamesDB.table("dbnames");
    }
    function hasDatabasesNative(indexedDB) {
        return indexedDB && typeof indexedDB.databases === "function";
    }
    function getDatabaseNames(_a) {
        var indexedDB = _a.indexedDB, IDBKeyRange = _a.IDBKeyRange;
        return hasDatabasesNative(indexedDB)
            ? Promise.resolve(indexedDB.databases()).then(function (infos) {
                return infos
                    .map(function (info) { return info.name; })
                    .filter(function (name) { return name !== DBNAMES_DB; });
            })
            : getDbNamesTable(indexedDB, IDBKeyRange).toCollection().primaryKeys();
    }
    function _onDatabaseCreated(_a, name) {
        var indexedDB = _a.indexedDB, IDBKeyRange = _a.IDBKeyRange;
        !hasDatabasesNative(indexedDB) &&
            name !== DBNAMES_DB &&
            getDbNamesTable(indexedDB, IDBKeyRange).put({ name: name }).catch(nop);
    }
    function _onDatabaseDeleted(_a, name) {
        var indexedDB = _a.indexedDB, IDBKeyRange = _a.IDBKeyRange;
        !hasDatabasesNative(indexedDB) &&
            name !== DBNAMES_DB &&
            getDbNamesTable(indexedDB, IDBKeyRange).delete(name).catch(nop);
    }

    function vip(fn) {
        return newScope(function () {
            PSD.letThrough = true;
            return fn();
        });
    }

    function idbReady() {
        var isSafari = !navigator.userAgentData &&
            /Safari\//.test(navigator.userAgent) &&
            !/Chrom(e|ium)\//.test(navigator.userAgent);
        if (!isSafari || !indexedDB.databases)
            return Promise.resolve();
        var intervalId;
        return new Promise(function (resolve) {
            var tryIdb = function () { return indexedDB.databases().finally(resolve); };
            intervalId = setInterval(tryIdb, 100);
            tryIdb();
        }).finally(function () { return clearInterval(intervalId); });
    }

    function dexieOpen(db) {
        var state = db._state;
        var indexedDB = db._deps.indexedDB;
        if (state.isBeingOpened || db.idbdb)
            return state.dbReadyPromise.then(function () { return state.dbOpenError ?
                rejection(state.dbOpenError) :
                db; });
        debug && (state.openCanceller._stackHolder = getErrorWithStack());
        state.isBeingOpened = true;
        state.dbOpenError = null;
        state.openComplete = false;
        var openCanceller = state.openCanceller;
        function throwIfCancelled() {
            if (state.openCanceller !== openCanceller)
                throw new exceptions.DatabaseClosed('db.open() was cancelled');
        }
        var resolveDbReady = state.dbReadyResolve,
        upgradeTransaction = null, wasCreated = false;
        return DexiePromise.race([openCanceller, (typeof navigator === 'undefined' ? DexiePromise.resolve() : idbReady()).then(function () { return new DexiePromise(function (resolve, reject) {
                throwIfCancelled();
                if (!indexedDB)
                    throw new exceptions.MissingAPI();
                var dbName = db.name;
                var req = state.autoSchema ?
                    indexedDB.open(dbName) :
                    indexedDB.open(dbName, Math.round(db.verno * 10));
                if (!req)
                    throw new exceptions.MissingAPI();
                req.onerror = eventRejectHandler(reject);
                req.onblocked = wrap(db._fireOnBlocked);
                req.onupgradeneeded = wrap(function (e) {
                    upgradeTransaction = req.transaction;
                    if (state.autoSchema && !db._options.allowEmptyDB) {
                        req.onerror = preventDefault;
                        upgradeTransaction.abort();
                        req.result.close();
                        var delreq = indexedDB.deleteDatabase(dbName);
                        delreq.onsuccess = delreq.onerror = wrap(function () {
                            reject(new exceptions.NoSuchDatabase("Database " + dbName + " doesnt exist"));
                        });
                    }
                    else {
                        upgradeTransaction.onerror = eventRejectHandler(reject);
                        var oldVer = e.oldVersion > Math.pow(2, 62) ? 0 : e.oldVersion;
                        wasCreated = oldVer < 1;
                        db._novip.idbdb = req.result;
                        runUpgraders(db, oldVer / 10, upgradeTransaction, reject);
                    }
                }, reject);
                req.onsuccess = wrap(function () {
                    upgradeTransaction = null;
                    var idbdb = db._novip.idbdb = req.result;
                    var objectStoreNames = slice(idbdb.objectStoreNames);
                    if (objectStoreNames.length > 0)
                        try {
                            var tmpTrans = idbdb.transaction(safariMultiStoreFix(objectStoreNames), 'readonly');
                            if (state.autoSchema)
                                readGlobalSchema(db, idbdb, tmpTrans);
                            else {
                                adjustToExistingIndexNames(db, db._dbSchema, tmpTrans);
                                if (!verifyInstalledSchema(db, tmpTrans)) {
                                    console.warn("Dexie SchemaDiff: Schema was extended without increasing the number passed to db.version(). Some queries may fail.");
                                }
                            }
                            generateMiddlewareStacks(db, tmpTrans);
                        }
                        catch (e) {
                        }
                    connections.push(db);
                    idbdb.onversionchange = wrap(function (ev) {
                        state.vcFired = true;
                        db.on("versionchange").fire(ev);
                    });
                    idbdb.onclose = wrap(function (ev) {
                        db.on("close").fire(ev);
                    });
                    if (wasCreated)
                        _onDatabaseCreated(db._deps, dbName);
                    resolve();
                }, reject);
            }); })]).then(function () {
            throwIfCancelled();
            state.onReadyBeingFired = [];
            return DexiePromise.resolve(vip(function () { return db.on.ready.fire(db.vip); })).then(function fireRemainders() {
                if (state.onReadyBeingFired.length > 0) {
                    var remainders_1 = state.onReadyBeingFired.reduce(promisableChain, nop);
                    state.onReadyBeingFired = [];
                    return DexiePromise.resolve(vip(function () { return remainders_1(db.vip); })).then(fireRemainders);
                }
            });
        }).finally(function () {
            state.onReadyBeingFired = null;
            state.isBeingOpened = false;
        }).then(function () {
            return db;
        }).catch(function (err) {
            state.dbOpenError = err;
            try {
                upgradeTransaction && upgradeTransaction.abort();
            }
            catch (_a) { }
            if (openCanceller === state.openCanceller) {
                db._close();
            }
            return rejection(err);
        }).finally(function () {
            state.openComplete = true;
            resolveDbReady();
        });
    }

    function awaitIterator(iterator) {
        var callNext = function (result) { return iterator.next(result); }, doThrow = function (error) { return iterator.throw(error); }, onSuccess = step(callNext), onError = step(doThrow);
        function step(getNext) {
            return function (val) {
                var next = getNext(val), value = next.value;
                return next.done ? value :
                    (!value || typeof value.then !== 'function' ?
                        isArray(value) ? Promise.all(value).then(onSuccess, onError) : onSuccess(value) :
                        value.then(onSuccess, onError));
            };
        }
        return step(callNext)();
    }

    function extractTransactionArgs(mode, _tableArgs_, scopeFunc) {
        var i = arguments.length;
        if (i < 2)
            throw new exceptions.InvalidArgument("Too few arguments");
        var args = new Array(i - 1);
        while (--i)
            args[i - 1] = arguments[i];
        scopeFunc = args.pop();
        var tables = flatten(args);
        return [mode, tables, scopeFunc];
    }
    function enterTransactionScope(db, mode, storeNames, parentTransaction, scopeFunc) {
        return DexiePromise.resolve().then(function () {
            var transless = PSD.transless || PSD;
            var trans = db._createTransaction(mode, storeNames, db._dbSchema, parentTransaction);
            var zoneProps = {
                trans: trans,
                transless: transless
            };
            if (parentTransaction) {
                trans.idbtrans = parentTransaction.idbtrans;
            }
            else {
                try {
                    trans.create();
                    db._state.PR1398_maxLoop = 3;
                }
                catch (ex) {
                    if (ex.name === errnames.InvalidState && db.isOpen() && --db._state.PR1398_maxLoop > 0) {
                        console.warn('Dexie: Need to reopen db');
                        db._close();
                        return db.open().then(function () { return enterTransactionScope(db, mode, storeNames, null, scopeFunc); });
                    }
                    return rejection(ex);
                }
            }
            var scopeFuncIsAsync = isAsyncFunction(scopeFunc);
            if (scopeFuncIsAsync) {
                incrementExpectedAwaits();
            }
            var returnValue;
            var promiseFollowed = DexiePromise.follow(function () {
                returnValue = scopeFunc.call(trans, trans);
                if (returnValue) {
                    if (scopeFuncIsAsync) {
                        var decrementor = decrementExpectedAwaits.bind(null, null);
                        returnValue.then(decrementor, decrementor);
                    }
                    else if (typeof returnValue.next === 'function' && typeof returnValue.throw === 'function') {
                        returnValue = awaitIterator(returnValue);
                    }
                }
            }, zoneProps);
            return (returnValue && typeof returnValue.then === 'function' ?
                DexiePromise.resolve(returnValue).then(function (x) { return trans.active ?
                    x
                    : rejection(new exceptions.PrematureCommit("Transaction committed too early. See http://bit.ly/2kdckMn")); })
                : promiseFollowed.then(function () { return returnValue; })).then(function (x) {
                if (parentTransaction)
                    trans._resolve();
                return trans._completion.then(function () { return x; });
            }).catch(function (e) {
                trans._reject(e);
                return rejection(e);
            });
        });
    }

    function pad(a, value, count) {
        var result = isArray(a) ? a.slice() : [a];
        for (var i = 0; i < count; ++i)
            result.push(value);
        return result;
    }
    function createVirtualIndexMiddleware(down) {
        return __assign(__assign({}, down), { table: function (tableName) {
                var table = down.table(tableName);
                var schema = table.schema;
                var indexLookup = {};
                var allVirtualIndexes = [];
                function addVirtualIndexes(keyPath, keyTail, lowLevelIndex) {
                    var keyPathAlias = getKeyPathAlias(keyPath);
                    var indexList = (indexLookup[keyPathAlias] = indexLookup[keyPathAlias] || []);
                    var keyLength = keyPath == null ? 0 : typeof keyPath === 'string' ? 1 : keyPath.length;
                    var isVirtual = keyTail > 0;
                    var virtualIndex = __assign(__assign({}, lowLevelIndex), { isVirtual: isVirtual, keyTail: keyTail, keyLength: keyLength, extractKey: getKeyExtractor(keyPath), unique: !isVirtual && lowLevelIndex.unique });
                    indexList.push(virtualIndex);
                    if (!virtualIndex.isPrimaryKey) {
                        allVirtualIndexes.push(virtualIndex);
                    }
                    if (keyLength > 1) {
                        var virtualKeyPath = keyLength === 2 ?
                            keyPath[0] :
                            keyPath.slice(0, keyLength - 1);
                        addVirtualIndexes(virtualKeyPath, keyTail + 1, lowLevelIndex);
                    }
                    indexList.sort(function (a, b) { return a.keyTail - b.keyTail; });
                    return virtualIndex;
                }
                var primaryKey = addVirtualIndexes(schema.primaryKey.keyPath, 0, schema.primaryKey);
                indexLookup[":id"] = [primaryKey];
                for (var _i = 0, _a = schema.indexes; _i < _a.length; _i++) {
                    var index = _a[_i];
                    addVirtualIndexes(index.keyPath, 0, index);
                }
                function findBestIndex(keyPath) {
                    var result = indexLookup[getKeyPathAlias(keyPath)];
                    return result && result[0];
                }
                function translateRange(range, keyTail) {
                    return {
                        type: range.type === 1  ?
                            2  :
                            range.type,
                        lower: pad(range.lower, range.lowerOpen ? down.MAX_KEY : down.MIN_KEY, keyTail),
                        lowerOpen: true,
                        upper: pad(range.upper, range.upperOpen ? down.MIN_KEY : down.MAX_KEY, keyTail),
                        upperOpen: true
                    };
                }
                function translateRequest(req) {
                    var index = req.query.index;
                    return index.isVirtual ? __assign(__assign({}, req), { query: {
                            index: index,
                            range: translateRange(req.query.range, index.keyTail)
                        } }) : req;
                }
                var result = __assign(__assign({}, table), { schema: __assign(__assign({}, schema), { primaryKey: primaryKey, indexes: allVirtualIndexes, getIndexByKeyPath: findBestIndex }), count: function (req) {
                        return table.count(translateRequest(req));
                    }, query: function (req) {
                        return table.query(translateRequest(req));
                    }, openCursor: function (req) {
                        var _a = req.query.index, keyTail = _a.keyTail, isVirtual = _a.isVirtual, keyLength = _a.keyLength;
                        if (!isVirtual)
                            return table.openCursor(req);
                        function createVirtualCursor(cursor) {
                            function _continue(key) {
                                key != null ?
                                    cursor.continue(pad(key, req.reverse ? down.MAX_KEY : down.MIN_KEY, keyTail)) :
                                    req.unique ?
                                        cursor.continue(cursor.key.slice(0, keyLength)
                                            .concat(req.reverse
                                            ? down.MIN_KEY
                                            : down.MAX_KEY, keyTail)) :
                                        cursor.continue();
                            }
                            var virtualCursor = Object.create(cursor, {
                                continue: { value: _continue },
                                continuePrimaryKey: {
                                    value: function (key, primaryKey) {
                                        cursor.continuePrimaryKey(pad(key, down.MAX_KEY, keyTail), primaryKey);
                                    }
                                },
                                primaryKey: {
                                    get: function () {
                                        return cursor.primaryKey;
                                    }
                                },
                                key: {
                                    get: function () {
                                        var key = cursor.key;
                                        return keyLength === 1 ?
                                            key[0] :
                                            key.slice(0, keyLength);
                                    }
                                },
                                value: {
                                    get: function () {
                                        return cursor.value;
                                    }
                                }
                            });
                            return virtualCursor;
                        }
                        return table.openCursor(translateRequest(req))
                            .then(function (cursor) { return cursor && createVirtualCursor(cursor); });
                    } });
                return result;
            } });
    }
    var virtualIndexMiddleware = {
        stack: "dbcore",
        name: "VirtualIndexMiddleware",
        level: 1,
        create: createVirtualIndexMiddleware
    };

    function getObjectDiff(a, b, rv, prfx) {
        rv = rv || {};
        prfx = prfx || '';
        keys(a).forEach(function (prop) {
            if (!hasOwn(b, prop)) {
                rv[prfx + prop] = undefined;
            }
            else {
                var ap = a[prop], bp = b[prop];
                if (typeof ap === 'object' && typeof bp === 'object' && ap && bp) {
                    var apTypeName = toStringTag(ap);
                    var bpTypeName = toStringTag(bp);
                    if (apTypeName !== bpTypeName) {
                        rv[prfx + prop] = b[prop];
                    }
                    else if (apTypeName === 'Object') {
                        getObjectDiff(ap, bp, rv, prfx + prop + '.');
                    }
                    else if (ap !== bp) {
                        rv[prfx + prop] = b[prop];
                    }
                }
                else if (ap !== bp)
                    rv[prfx + prop] = b[prop];
            }
        });
        keys(b).forEach(function (prop) {
            if (!hasOwn(a, prop)) {
                rv[prfx + prop] = b[prop];
            }
        });
        return rv;
    }

    function getEffectiveKeys(primaryKey, req) {
        if (req.type === 'delete')
            return req.keys;
        return req.keys || req.values.map(primaryKey.extractKey);
    }

    var hooksMiddleware = {
        stack: "dbcore",
        name: "HooksMiddleware",
        level: 2,
        create: function (downCore) { return (__assign(__assign({}, downCore), { table: function (tableName) {
                var downTable = downCore.table(tableName);
                var primaryKey = downTable.schema.primaryKey;
                var tableMiddleware = __assign(__assign({}, downTable), { mutate: function (req) {
                        var dxTrans = PSD.trans;
                        var _a = dxTrans.table(tableName).hook, deleting = _a.deleting, creating = _a.creating, updating = _a.updating;
                        switch (req.type) {
                            case 'add':
                                if (creating.fire === nop)
                                    break;
                                return dxTrans._promise('readwrite', function () { return addPutOrDelete(req); }, true);
                            case 'put':
                                if (creating.fire === nop && updating.fire === nop)
                                    break;
                                return dxTrans._promise('readwrite', function () { return addPutOrDelete(req); }, true);
                            case 'delete':
                                if (deleting.fire === nop)
                                    break;
                                return dxTrans._promise('readwrite', function () { return addPutOrDelete(req); }, true);
                            case 'deleteRange':
                                if (deleting.fire === nop)
                                    break;
                                return dxTrans._promise('readwrite', function () { return deleteRange(req); }, true);
                        }
                        return downTable.mutate(req);
                        function addPutOrDelete(req) {
                            var dxTrans = PSD.trans;
                            var keys = req.keys || getEffectiveKeys(primaryKey, req);
                            if (!keys)
                                throw new Error("Keys missing");
                            req = req.type === 'add' || req.type === 'put' ? __assign(__assign({}, req), { keys: keys }) : __assign({}, req);
                            if (req.type !== 'delete')
                                req.values = __spreadArray([], req.values, true);
                            if (req.keys)
                                req.keys = __spreadArray([], req.keys, true);
                            return getExistingValues(downTable, req, keys).then(function (existingValues) {
                                var contexts = keys.map(function (key, i) {
                                    var existingValue = existingValues[i];
                                    var ctx = { onerror: null, onsuccess: null };
                                    if (req.type === 'delete') {
                                        deleting.fire.call(ctx, key, existingValue, dxTrans);
                                    }
                                    else if (req.type === 'add' || existingValue === undefined) {
                                        var generatedPrimaryKey = creating.fire.call(ctx, key, req.values[i], dxTrans);
                                        if (key == null && generatedPrimaryKey != null) {
                                            key = generatedPrimaryKey;
                                            req.keys[i] = key;
                                            if (!primaryKey.outbound) {
                                                setByKeyPath(req.values[i], primaryKey.keyPath, key);
                                            }
                                        }
                                    }
                                    else {
                                        var objectDiff = getObjectDiff(existingValue, req.values[i]);
                                        var additionalChanges_1 = updating.fire.call(ctx, objectDiff, key, existingValue, dxTrans);
                                        if (additionalChanges_1) {
                                            var requestedValue_1 = req.values[i];
                                            Object.keys(additionalChanges_1).forEach(function (keyPath) {
                                                if (hasOwn(requestedValue_1, keyPath)) {
                                                    requestedValue_1[keyPath] = additionalChanges_1[keyPath];
                                                }
                                                else {
                                                    setByKeyPath(requestedValue_1, keyPath, additionalChanges_1[keyPath]);
                                                }
                                            });
                                        }
                                    }
                                    return ctx;
                                });
                                return downTable.mutate(req).then(function (_a) {
                                    var failures = _a.failures, results = _a.results, numFailures = _a.numFailures, lastResult = _a.lastResult;
                                    for (var i = 0; i < keys.length; ++i) {
                                        var primKey = results ? results[i] : keys[i];
                                        var ctx = contexts[i];
                                        if (primKey == null) {
                                            ctx.onerror && ctx.onerror(failures[i]);
                                        }
                                        else {
                                            ctx.onsuccess && ctx.onsuccess(req.type === 'put' && existingValues[i] ?
                                                req.values[i] :
                                                primKey
                                            );
                                        }
                                    }
                                    return { failures: failures, results: results, numFailures: numFailures, lastResult: lastResult };
                                }).catch(function (error) {
                                    contexts.forEach(function (ctx) { return ctx.onerror && ctx.onerror(error); });
                                    return Promise.reject(error);
                                });
                            });
                        }
                        function deleteRange(req) {
                            return deleteNextChunk(req.trans, req.range, 10000);
                        }
                        function deleteNextChunk(trans, range, limit) {
                            return downTable.query({ trans: trans, values: false, query: { index: primaryKey, range: range }, limit: limit })
                                .then(function (_a) {
                                var result = _a.result;
                                return addPutOrDelete({ type: 'delete', keys: result, trans: trans }).then(function (res) {
                                    if (res.numFailures > 0)
                                        return Promise.reject(res.failures[0]);
                                    if (result.length < limit) {
                                        return { failures: [], numFailures: 0, lastResult: undefined };
                                    }
                                    else {
                                        return deleteNextChunk(trans, __assign(__assign({}, range), { lower: result[result.length - 1], lowerOpen: true }), limit);
                                    }
                                });
                            });
                        }
                    } });
                return tableMiddleware;
            } })); }
    };
    function getExistingValues(table, req, effectiveKeys) {
        return req.type === "add"
            ? Promise.resolve([])
            : table.getMany({ trans: req.trans, keys: effectiveKeys, cache: "immutable" });
    }

    function getFromTransactionCache(keys, cache, clone) {
        try {
            if (!cache)
                return null;
            if (cache.keys.length < keys.length)
                return null;
            var result = [];
            for (var i = 0, j = 0; i < cache.keys.length && j < keys.length; ++i) {
                if (cmp(cache.keys[i], keys[j]) !== 0)
                    continue;
                result.push(clone ? deepClone(cache.values[i]) : cache.values[i]);
                ++j;
            }
            return result.length === keys.length ? result : null;
        }
        catch (_a) {
            return null;
        }
    }
    var cacheExistingValuesMiddleware = {
        stack: "dbcore",
        level: -1,
        create: function (core) {
            return {
                table: function (tableName) {
                    var table = core.table(tableName);
                    return __assign(__assign({}, table), { getMany: function (req) {
                            if (!req.cache) {
                                return table.getMany(req);
                            }
                            var cachedResult = getFromTransactionCache(req.keys, req.trans["_cache"], req.cache === "clone");
                            if (cachedResult) {
                                return DexiePromise.resolve(cachedResult);
                            }
                            return table.getMany(req).then(function (res) {
                                req.trans["_cache"] = {
                                    keys: req.keys,
                                    values: req.cache === "clone" ? deepClone(res) : res,
                                };
                                return res;
                            });
                        }, mutate: function (req) {
                            if (req.type !== "add")
                                req.trans["_cache"] = null;
                            return table.mutate(req);
                        } });
                },
            };
        },
    };

    var _a;
    function isEmptyRange(node) {
        return !("from" in node);
    }
    var RangeSet = function (fromOrTree, to) {
        if (this) {
            extend(this, arguments.length ? { d: 1, from: fromOrTree, to: arguments.length > 1 ? to : fromOrTree } : { d: 0 });
        }
        else {
            var rv = new RangeSet();
            if (fromOrTree && ("d" in fromOrTree)) {
                extend(rv, fromOrTree);
            }
            return rv;
        }
    };
    props(RangeSet.prototype, (_a = {
            add: function (rangeSet) {
                mergeRanges(this, rangeSet);
                return this;
            },
            addKey: function (key) {
                addRange(this, key, key);
                return this;
            },
            addKeys: function (keys) {
                var _this = this;
                keys.forEach(function (key) { return addRange(_this, key, key); });
                return this;
            }
        },
        _a[iteratorSymbol] = function () {
            return getRangeSetIterator(this);
        },
        _a));
    function addRange(target, from, to) {
        var diff = cmp(from, to);
        if (isNaN(diff))
            return;
        if (diff > 0)
            throw RangeError();
        if (isEmptyRange(target))
            return extend(target, { from: from, to: to, d: 1 });
        var left = target.l;
        var right = target.r;
        if (cmp(to, target.from) < 0) {
            left
                ? addRange(left, from, to)
                : (target.l = { from: from, to: to, d: 1, l: null, r: null });
            return rebalance(target);
        }
        if (cmp(from, target.to) > 0) {
            right
                ? addRange(right, from, to)
                : (target.r = { from: from, to: to, d: 1, l: null, r: null });
            return rebalance(target);
        }
        if (cmp(from, target.from) < 0) {
            target.from = from;
            target.l = null;
            target.d = right ? right.d + 1 : 1;
        }
        if (cmp(to, target.to) > 0) {
            target.to = to;
            target.r = null;
            target.d = target.l ? target.l.d + 1 : 1;
        }
        var rightWasCutOff = !target.r;
        if (left && !target.l) {
            mergeRanges(target, left);
        }
        if (right && rightWasCutOff) {
            mergeRanges(target, right);
        }
    }
    function mergeRanges(target, newSet) {
        function _addRangeSet(target, _a) {
            var from = _a.from, to = _a.to, l = _a.l, r = _a.r;
            addRange(target, from, to);
            if (l)
                _addRangeSet(target, l);
            if (r)
                _addRangeSet(target, r);
        }
        if (!isEmptyRange(newSet))
            _addRangeSet(target, newSet);
    }
    function rangesOverlap(rangeSet1, rangeSet2) {
        var i1 = getRangeSetIterator(rangeSet2);
        var nextResult1 = i1.next();
        if (nextResult1.done)
            return false;
        var a = nextResult1.value;
        var i2 = getRangeSetIterator(rangeSet1);
        var nextResult2 = i2.next(a.from);
        var b = nextResult2.value;
        while (!nextResult1.done && !nextResult2.done) {
            if (cmp(b.from, a.to) <= 0 && cmp(b.to, a.from) >= 0)
                return true;
            cmp(a.from, b.from) < 0
                ? (a = (nextResult1 = i1.next(b.from)).value)
                : (b = (nextResult2 = i2.next(a.from)).value);
        }
        return false;
    }
    function getRangeSetIterator(node) {
        var state = isEmptyRange(node) ? null : { s: 0, n: node };
        return {
            next: function (key) {
                var keyProvided = arguments.length > 0;
                while (state) {
                    switch (state.s) {
                        case 0:
                            state.s = 1;
                            if (keyProvided) {
                                while (state.n.l && cmp(key, state.n.from) < 0)
                                    state = { up: state, n: state.n.l, s: 1 };
                            }
                            else {
                                while (state.n.l)
                                    state = { up: state, n: state.n.l, s: 1 };
                            }
                        case 1:
                            state.s = 2;
                            if (!keyProvided || cmp(key, state.n.to) <= 0)
                                return { value: state.n, done: false };
                        case 2:
                            if (state.n.r) {
                                state.s = 3;
                                state = { up: state, n: state.n.r, s: 0 };
                                continue;
                            }
                        case 3:
                            state = state.up;
                    }
                }
                return { done: true };
            },
        };
    }
    function rebalance(target) {
        var _a, _b;
        var diff = (((_a = target.r) === null || _a === void 0 ? void 0 : _a.d) || 0) - (((_b = target.l) === null || _b === void 0 ? void 0 : _b.d) || 0);
        var r = diff > 1 ? "r" : diff < -1 ? "l" : "";
        if (r) {
            var l = r === "r" ? "l" : "r";
            var rootClone = __assign({}, target);
            var oldRootRight = target[r];
            target.from = oldRootRight.from;
            target.to = oldRootRight.to;
            target[r] = oldRootRight[r];
            rootClone[r] = oldRootRight[l];
            target[l] = rootClone;
            rootClone.d = computeDepth(rootClone);
        }
        target.d = computeDepth(target);
    }
    function computeDepth(_a) {
        var r = _a.r, l = _a.l;
        return (r ? (l ? Math.max(r.d, l.d) : r.d) : l ? l.d : 0) + 1;
    }

    var observabilityMiddleware = {
        stack: "dbcore",
        level: 0,
        create: function (core) {
            var dbName = core.schema.name;
            var FULL_RANGE = new RangeSet(core.MIN_KEY, core.MAX_KEY);
            return __assign(__assign({}, core), { table: function (tableName) {
                    var table = core.table(tableName);
                    var schema = table.schema;
                    var primaryKey = schema.primaryKey;
                    var extractKey = primaryKey.extractKey, outbound = primaryKey.outbound;
                    var tableClone = __assign(__assign({}, table), { mutate: function (req) {
                            var trans = req.trans;
                            var mutatedParts = trans.mutatedParts || (trans.mutatedParts = {});
                            var getRangeSet = function (indexName) {
                                var part = "idb://" + dbName + "/" + tableName + "/" + indexName;
                                return (mutatedParts[part] ||
                                    (mutatedParts[part] = new RangeSet()));
                            };
                            var pkRangeSet = getRangeSet("");
                            var delsRangeSet = getRangeSet(":dels");
                            var type = req.type;
                            var _a = req.type === "deleteRange"
                                ? [req.range]
                                : req.type === "delete"
                                    ? [req.keys]
                                    : req.values.length < 50
                                        ? [[], req.values]
                                        : [], keys = _a[0], newObjs = _a[1];
                            var oldCache = req.trans["_cache"];
                            return table.mutate(req).then(function (res) {
                                if (isArray(keys)) {
                                    if (type !== "delete")
                                        keys = res.results;
                                    pkRangeSet.addKeys(keys);
                                    var oldObjs = getFromTransactionCache(keys, oldCache);
                                    if (!oldObjs && type !== "add") {
                                        delsRangeSet.addKeys(keys);
                                    }
                                    if (oldObjs || newObjs) {
                                        trackAffectedIndexes(getRangeSet, schema, oldObjs, newObjs);
                                    }
                                }
                                else if (keys) {
                                    var range = { from: keys.lower, to: keys.upper };
                                    delsRangeSet.add(range);
                                    pkRangeSet.add(range);
                                }
                                else {
                                    pkRangeSet.add(FULL_RANGE);
                                    delsRangeSet.add(FULL_RANGE);
                                    schema.indexes.forEach(function (idx) { return getRangeSet(idx.name).add(FULL_RANGE); });
                                }
                                return res;
                            });
                        } });
                    var getRange = function (_a) {
                        var _b, _c;
                        var _d = _a.query, index = _d.index, range = _d.range;
                        return [
                            index,
                            new RangeSet((_b = range.lower) !== null && _b !== void 0 ? _b : core.MIN_KEY, (_c = range.upper) !== null && _c !== void 0 ? _c : core.MAX_KEY),
                        ];
                    };
                    var readSubscribers = {
                        get: function (req) { return [primaryKey, new RangeSet(req.key)]; },
                        getMany: function (req) { return [primaryKey, new RangeSet().addKeys(req.keys)]; },
                        count: getRange,
                        query: getRange,
                        openCursor: getRange,
                    };
                    keys(readSubscribers).forEach(function (method) {
                        tableClone[method] = function (req) {
                            var subscr = PSD.subscr;
                            if (subscr) {
                                var getRangeSet = function (indexName) {
                                    var part = "idb://" + dbName + "/" + tableName + "/" + indexName;
                                    return (subscr[part] ||
                                        (subscr[part] = new RangeSet()));
                                };
                                var pkRangeSet_1 = getRangeSet("");
                                var delsRangeSet_1 = getRangeSet(":dels");
                                var _a = readSubscribers[method](req), queriedIndex = _a[0], queriedRanges = _a[1];
                                getRangeSet(queriedIndex.name || "").add(queriedRanges);
                                if (!queriedIndex.isPrimaryKey) {
                                    if (method === "count") {
                                        delsRangeSet_1.add(FULL_RANGE);
                                    }
                                    else {
                                        var keysPromise_1 = method === "query" &&
                                            outbound &&
                                            req.values &&
                                            table.query(__assign(__assign({}, req), { values: false }));
                                        return table[method].apply(this, arguments).then(function (res) {
                                            if (method === "query") {
                                                if (outbound && req.values) {
                                                    return keysPromise_1.then(function (_a) {
                                                        var resultingKeys = _a.result;
                                                        pkRangeSet_1.addKeys(resultingKeys);
                                                        return res;
                                                    });
                                                }
                                                var pKeys = req.values
                                                    ? res.result.map(extractKey)
                                                    : res.result;
                                                if (req.values) {
                                                    pkRangeSet_1.addKeys(pKeys);
                                                }
                                                else {
                                                    delsRangeSet_1.addKeys(pKeys);
                                                }
                                            }
                                            else if (method === "openCursor") {
                                                var cursor_1 = res;
                                                var wantValues_1 = req.values;
                                                return (cursor_1 &&
                                                    Object.create(cursor_1, {
                                                        key: {
                                                            get: function () {
                                                                delsRangeSet_1.addKey(cursor_1.primaryKey);
                                                                return cursor_1.key;
                                                            },
                                                        },
                                                        primaryKey: {
                                                            get: function () {
                                                                var pkey = cursor_1.primaryKey;
                                                                delsRangeSet_1.addKey(pkey);
                                                                return pkey;
                                                            },
                                                        },
                                                        value: {
                                                            get: function () {
                                                                wantValues_1 && pkRangeSet_1.addKey(cursor_1.primaryKey);
                                                                return cursor_1.value;
                                                            },
                                                        },
                                                    }));
                                            }
                                            return res;
                                        });
                                    }
                                }
                            }
                            return table[method].apply(this, arguments);
                        };
                    });
                    return tableClone;
                } });
        },
    };
    function trackAffectedIndexes(getRangeSet, schema, oldObjs, newObjs) {
        function addAffectedIndex(ix) {
            var rangeSet = getRangeSet(ix.name || "");
            function extractKey(obj) {
                return obj != null ? ix.extractKey(obj) : null;
            }
            var addKeyOrKeys = function (key) { return ix.multiEntry && isArray(key)
                ? key.forEach(function (key) { return rangeSet.addKey(key); })
                : rangeSet.addKey(key); };
            (oldObjs || newObjs).forEach(function (_, i) {
                var oldKey = oldObjs && extractKey(oldObjs[i]);
                var newKey = newObjs && extractKey(newObjs[i]);
                if (cmp(oldKey, newKey) !== 0) {
                    if (oldKey != null)
                        addKeyOrKeys(oldKey);
                    if (newKey != null)
                        addKeyOrKeys(newKey);
                }
            });
        }
        schema.indexes.forEach(addAffectedIndex);
    }

    var Dexie$1 =  (function () {
        function Dexie(name, options) {
            var _this = this;
            this._middlewares = {};
            this.verno = 0;
            var deps = Dexie.dependencies;
            this._options = options = __assign({
                addons: Dexie.addons, autoOpen: true,
                indexedDB: deps.indexedDB, IDBKeyRange: deps.IDBKeyRange }, options);
            this._deps = {
                indexedDB: options.indexedDB,
                IDBKeyRange: options.IDBKeyRange
            };
            var addons = options.addons;
            this._dbSchema = {};
            this._versions = [];
            this._storeNames = [];
            this._allTables = {};
            this.idbdb = null;
            this._novip = this;
            var state = {
                dbOpenError: null,
                isBeingOpened: false,
                onReadyBeingFired: null,
                openComplete: false,
                dbReadyResolve: nop,
                dbReadyPromise: null,
                cancelOpen: nop,
                openCanceller: null,
                autoSchema: true,
                PR1398_maxLoop: 3
            };
            state.dbReadyPromise = new DexiePromise(function (resolve) {
                state.dbReadyResolve = resolve;
            });
            state.openCanceller = new DexiePromise(function (_, reject) {
                state.cancelOpen = reject;
            });
            this._state = state;
            this.name = name;
            this.on = Events(this, "populate", "blocked", "versionchange", "close", { ready: [promisableChain, nop] });
            this.on.ready.subscribe = override(this.on.ready.subscribe, function (subscribe) {
                return function (subscriber, bSticky) {
                    Dexie.vip(function () {
                        var state = _this._state;
                        if (state.openComplete) {
                            if (!state.dbOpenError)
                                DexiePromise.resolve().then(subscriber);
                            if (bSticky)
                                subscribe(subscriber);
                        }
                        else if (state.onReadyBeingFired) {
                            state.onReadyBeingFired.push(subscriber);
                            if (bSticky)
                                subscribe(subscriber);
                        }
                        else {
                            subscribe(subscriber);
                            var db_1 = _this;
                            if (!bSticky)
                                subscribe(function unsubscribe() {
                                    db_1.on.ready.unsubscribe(subscriber);
                                    db_1.on.ready.unsubscribe(unsubscribe);
                                });
                        }
                    });
                };
            });
            this.Collection = createCollectionConstructor(this);
            this.Table = createTableConstructor(this);
            this.Transaction = createTransactionConstructor(this);
            this.Version = createVersionConstructor(this);
            this.WhereClause = createWhereClauseConstructor(this);
            this.on("versionchange", function (ev) {
                if (ev.newVersion > 0)
                    console.warn("Another connection wants to upgrade database '" + _this.name + "'. Closing db now to resume the upgrade.");
                else
                    console.warn("Another connection wants to delete database '" + _this.name + "'. Closing db now to resume the delete request.");
                _this.close();
            });
            this.on("blocked", function (ev) {
                if (!ev.newVersion || ev.newVersion < ev.oldVersion)
                    console.warn("Dexie.delete('" + _this.name + "') was blocked");
                else
                    console.warn("Upgrade '" + _this.name + "' blocked by other connection holding version " + ev.oldVersion / 10);
            });
            this._maxKey = getMaxKey(options.IDBKeyRange);
            this._createTransaction = function (mode, storeNames, dbschema, parentTransaction) { return new _this.Transaction(mode, storeNames, dbschema, _this._options.chromeTransactionDurability, parentTransaction); };
            this._fireOnBlocked = function (ev) {
                _this.on("blocked").fire(ev);
                connections
                    .filter(function (c) { return c.name === _this.name && c !== _this && !c._state.vcFired; })
                    .map(function (c) { return c.on("versionchange").fire(ev); });
            };
            this.use(virtualIndexMiddleware);
            this.use(hooksMiddleware);
            this.use(observabilityMiddleware);
            this.use(cacheExistingValuesMiddleware);
            this.vip = Object.create(this, { _vip: { value: true } });
            addons.forEach(function (addon) { return addon(_this); });
        }
        Dexie.prototype.version = function (versionNumber) {
            if (isNaN(versionNumber) || versionNumber < 0.1)
                throw new exceptions.Type("Given version is not a positive number");
            versionNumber = Math.round(versionNumber * 10) / 10;
            if (this.idbdb || this._state.isBeingOpened)
                throw new exceptions.Schema("Cannot add version when database is open");
            this.verno = Math.max(this.verno, versionNumber);
            var versions = this._versions;
            var versionInstance = versions.filter(function (v) { return v._cfg.version === versionNumber; })[0];
            if (versionInstance)
                return versionInstance;
            versionInstance = new this.Version(versionNumber);
            versions.push(versionInstance);
            versions.sort(lowerVersionFirst);
            versionInstance.stores({});
            this._state.autoSchema = false;
            return versionInstance;
        };
        Dexie.prototype._whenReady = function (fn) {
            var _this = this;
            return (this.idbdb && (this._state.openComplete || PSD.letThrough || this._vip)) ? fn() : new DexiePromise(function (resolve, reject) {
                if (_this._state.openComplete) {
                    return reject(new exceptions.DatabaseClosed(_this._state.dbOpenError));
                }
                if (!_this._state.isBeingOpened) {
                    if (!_this._options.autoOpen) {
                        reject(new exceptions.DatabaseClosed());
                        return;
                    }
                    _this.open().catch(nop);
                }
                _this._state.dbReadyPromise.then(resolve, reject);
            }).then(fn);
        };
        Dexie.prototype.use = function (_a) {
            var stack = _a.stack, create = _a.create, level = _a.level, name = _a.name;
            if (name)
                this.unuse({ stack: stack, name: name });
            var middlewares = this._middlewares[stack] || (this._middlewares[stack] = []);
            middlewares.push({ stack: stack, create: create, level: level == null ? 10 : level, name: name });
            middlewares.sort(function (a, b) { return a.level - b.level; });
            return this;
        };
        Dexie.prototype.unuse = function (_a) {
            var stack = _a.stack, name = _a.name, create = _a.create;
            if (stack && this._middlewares[stack]) {
                this._middlewares[stack] = this._middlewares[stack].filter(function (mw) {
                    return create ? mw.create !== create :
                        name ? mw.name !== name :
                            false;
                });
            }
            return this;
        };
        Dexie.prototype.open = function () {
            return dexieOpen(this);
        };
        Dexie.prototype._close = function () {
            var state = this._state;
            var idx = connections.indexOf(this);
            if (idx >= 0)
                connections.splice(idx, 1);
            if (this.idbdb) {
                try {
                    this.idbdb.close();
                }
                catch (e) { }
                this._novip.idbdb = null;
            }
            state.dbReadyPromise = new DexiePromise(function (resolve) {
                state.dbReadyResolve = resolve;
            });
            state.openCanceller = new DexiePromise(function (_, reject) {
                state.cancelOpen = reject;
            });
        };
        Dexie.prototype.close = function () {
            this._close();
            var state = this._state;
            this._options.autoOpen = false;
            state.dbOpenError = new exceptions.DatabaseClosed();
            if (state.isBeingOpened)
                state.cancelOpen(state.dbOpenError);
        };
        Dexie.prototype.delete = function () {
            var _this = this;
            var hasArguments = arguments.length > 0;
            var state = this._state;
            return new DexiePromise(function (resolve, reject) {
                var doDelete = function () {
                    _this.close();
                    var req = _this._deps.indexedDB.deleteDatabase(_this.name);
                    req.onsuccess = wrap(function () {
                        _onDatabaseDeleted(_this._deps, _this.name);
                        resolve();
                    });
                    req.onerror = eventRejectHandler(reject);
                    req.onblocked = _this._fireOnBlocked;
                };
                if (hasArguments)
                    throw new exceptions.InvalidArgument("Arguments not allowed in db.delete()");
                if (state.isBeingOpened) {
                    state.dbReadyPromise.then(doDelete);
                }
                else {
                    doDelete();
                }
            });
        };
        Dexie.prototype.backendDB = function () {
            return this.idbdb;
        };
        Dexie.prototype.isOpen = function () {
            return this.idbdb !== null;
        };
        Dexie.prototype.hasBeenClosed = function () {
            var dbOpenError = this._state.dbOpenError;
            return dbOpenError && (dbOpenError.name === 'DatabaseClosed');
        };
        Dexie.prototype.hasFailed = function () {
            return this._state.dbOpenError !== null;
        };
        Dexie.prototype.dynamicallyOpened = function () {
            return this._state.autoSchema;
        };
        Object.defineProperty(Dexie.prototype, "tables", {
            get: function () {
                var _this = this;
                return keys(this._allTables).map(function (name) { return _this._allTables[name]; });
            },
            enumerable: false,
            configurable: true
        });
        Dexie.prototype.transaction = function () {
            var args = extractTransactionArgs.apply(this, arguments);
            return this._transaction.apply(this, args);
        };
        Dexie.prototype._transaction = function (mode, tables, scopeFunc) {
            var _this = this;
            var parentTransaction = PSD.trans;
            if (!parentTransaction || parentTransaction.db !== this || mode.indexOf('!') !== -1)
                parentTransaction = null;
            var onlyIfCompatible = mode.indexOf('?') !== -1;
            mode = mode.replace('!', '').replace('?', '');
            var idbMode, storeNames;
            try {
                storeNames = tables.map(function (table) {
                    var storeName = table instanceof _this.Table ? table.name : table;
                    if (typeof storeName !== 'string')
                        throw new TypeError("Invalid table argument to Dexie.transaction(). Only Table or String are allowed");
                    return storeName;
                });
                if (mode == "r" || mode === READONLY)
                    idbMode = READONLY;
                else if (mode == "rw" || mode == READWRITE)
                    idbMode = READWRITE;
                else
                    throw new exceptions.InvalidArgument("Invalid transaction mode: " + mode);
                if (parentTransaction) {
                    if (parentTransaction.mode === READONLY && idbMode === READWRITE) {
                        if (onlyIfCompatible) {
                            parentTransaction = null;
                        }
                        else
                            throw new exceptions.SubTransaction("Cannot enter a sub-transaction with READWRITE mode when parent transaction is READONLY");
                    }
                    if (parentTransaction) {
                        storeNames.forEach(function (storeName) {
                            if (parentTransaction && parentTransaction.storeNames.indexOf(storeName) === -1) {
                                if (onlyIfCompatible) {
                                    parentTransaction = null;
                                }
                                else
                                    throw new exceptions.SubTransaction("Table " + storeName +
                                        " not included in parent transaction.");
                            }
                        });
                    }
                    if (onlyIfCompatible && parentTransaction && !parentTransaction.active) {
                        parentTransaction = null;
                    }
                }
            }
            catch (e) {
                return parentTransaction ?
                    parentTransaction._promise(null, function (_, reject) { reject(e); }) :
                    rejection(e);
            }
            var enterTransaction = enterTransactionScope.bind(null, this, idbMode, storeNames, parentTransaction, scopeFunc);
            return (parentTransaction ?
                parentTransaction._promise(idbMode, enterTransaction, "lock") :
                PSD.trans ?
                    usePSD(PSD.transless, function () { return _this._whenReady(enterTransaction); }) :
                    this._whenReady(enterTransaction));
        };
        Dexie.prototype.table = function (tableName) {
            if (!hasOwn(this._allTables, tableName)) {
                throw new exceptions.InvalidTable("Table " + tableName + " does not exist");
            }
            return this._allTables[tableName];
        };
        return Dexie;
    }());

    var symbolObservable = typeof Symbol !== "undefined" && "observable" in Symbol
        ? Symbol.observable
        : "@@observable";
    var Observable =  (function () {
        function Observable(subscribe) {
            this._subscribe = subscribe;
        }
        Observable.prototype.subscribe = function (x, error, complete) {
            return this._subscribe(!x || typeof x === "function" ? { next: x, error: error, complete: complete } : x);
        };
        Observable.prototype[symbolObservable] = function () {
            return this;
        };
        return Observable;
    }());

    function extendObservabilitySet(target, newSet) {
        keys(newSet).forEach(function (part) {
            var rangeSet = target[part] || (target[part] = new RangeSet());
            mergeRanges(rangeSet, newSet[part]);
        });
        return target;
    }

    function liveQuery(querier) {
        return new Observable(function (observer) {
            var scopeFuncIsAsync = isAsyncFunction(querier);
            function execute(subscr) {
                if (scopeFuncIsAsync) {
                    incrementExpectedAwaits();
                }
                var exec = function () { return newScope(querier, { subscr: subscr, trans: null }); };
                var rv = PSD.trans
                    ?
                        usePSD(PSD.transless, exec)
                    : exec();
                if (scopeFuncIsAsync) {
                    rv.then(decrementExpectedAwaits, decrementExpectedAwaits);
                }
                return rv;
            }
            var closed = false;
            var accumMuts = {};
            var currentObs = {};
            var subscription = {
                get closed() {
                    return closed;
                },
                unsubscribe: function () {
                    closed = true;
                    globalEvents.storagemutated.unsubscribe(mutationListener);
                },
            };
            observer.start && observer.start(subscription);
            var querying = false, startedListening = false;
            function shouldNotify() {
                return keys(currentObs).some(function (key) {
                    return accumMuts[key] && rangesOverlap(accumMuts[key], currentObs[key]);
                });
            }
            var mutationListener = function (parts) {
                extendObservabilitySet(accumMuts, parts);
                if (shouldNotify()) {
                    doQuery();
                }
            };
            var doQuery = function () {
                if (querying || closed)
                    return;
                accumMuts = {};
                var subscr = {};
                var ret = execute(subscr);
                if (!startedListening) {
                    globalEvents(DEXIE_STORAGE_MUTATED_EVENT_NAME, mutationListener);
                    startedListening = true;
                }
                querying = true;
                Promise.resolve(ret).then(function (result) {
                    querying = false;
                    if (closed)
                        return;
                    if (shouldNotify()) {
                        doQuery();
                    }
                    else {
                        accumMuts = {};
                        currentObs = subscr;
                        observer.next && observer.next(result);
                    }
                }, function (err) {
                    querying = false;
                    observer.error && observer.error(err);
                    subscription.unsubscribe();
                });
            };
            doQuery();
            return subscription;
        });
    }

    var domDeps;
    try {
        domDeps = {
            indexedDB: _global.indexedDB || _global.mozIndexedDB || _global.webkitIndexedDB || _global.msIndexedDB,
            IDBKeyRange: _global.IDBKeyRange || _global.webkitIDBKeyRange
        };
    }
    catch (e) {
        domDeps = { indexedDB: null, IDBKeyRange: null };
    }

    var Dexie = Dexie$1;
    props(Dexie, __assign(__assign({}, fullNameExceptions), {
        delete: function (databaseName) {
            var db = new Dexie(databaseName, { addons: [] });
            return db.delete();
        },
        exists: function (name) {
            return new Dexie(name, { addons: [] }).open().then(function (db) {
                db.close();
                return true;
            }).catch('NoSuchDatabaseError', function () { return false; });
        },
        getDatabaseNames: function (cb) {
            try {
                return getDatabaseNames(Dexie.dependencies).then(cb);
            }
            catch (_a) {
                return rejection(new exceptions.MissingAPI());
            }
        },
        defineClass: function () {
            function Class(content) {
                extend(this, content);
            }
            return Class;
        }, ignoreTransaction: function (scopeFunc) {
            return PSD.trans ?
                usePSD(PSD.transless, scopeFunc) :
                scopeFunc();
        }, vip: vip, async: function (generatorFn) {
            return function () {
                try {
                    var rv = awaitIterator(generatorFn.apply(this, arguments));
                    if (!rv || typeof rv.then !== 'function')
                        return DexiePromise.resolve(rv);
                    return rv;
                }
                catch (e) {
                    return rejection(e);
                }
            };
        }, spawn: function (generatorFn, args, thiz) {
            try {
                var rv = awaitIterator(generatorFn.apply(thiz, args || []));
                if (!rv || typeof rv.then !== 'function')
                    return DexiePromise.resolve(rv);
                return rv;
            }
            catch (e) {
                return rejection(e);
            }
        },
        currentTransaction: {
            get: function () { return PSD.trans || null; }
        }, waitFor: function (promiseOrFunction, optionalTimeout) {
            var promise = DexiePromise.resolve(typeof promiseOrFunction === 'function' ?
                Dexie.ignoreTransaction(promiseOrFunction) :
                promiseOrFunction)
                .timeout(optionalTimeout || 60000);
            return PSD.trans ?
                PSD.trans.waitFor(promise) :
                promise;
        },
        Promise: DexiePromise,
        debug: {
            get: function () { return debug; },
            set: function (value) {
                setDebug(value, value === 'dexie' ? function () { return true; } : dexieStackFrameFilter);
            }
        },
        derive: derive, extend: extend, props: props, override: override,
        Events: Events, on: globalEvents, liveQuery: liveQuery, extendObservabilitySet: extendObservabilitySet,
        getByKeyPath: getByKeyPath, setByKeyPath: setByKeyPath, delByKeyPath: delByKeyPath, shallowClone: shallowClone, deepClone: deepClone, getObjectDiff: getObjectDiff, cmp: cmp, asap: asap$1,
        minKey: minKey,
        addons: [],
        connections: connections,
        errnames: errnames,
        dependencies: domDeps,
        semVer: DEXIE_VERSION, version: DEXIE_VERSION.split('.')
            .map(function (n) { return parseInt(n); })
            .reduce(function (p, c, i) { return p + (c / Math.pow(10, i * 2)); }) }));
    Dexie.maxKey = getMaxKey(Dexie.dependencies.IDBKeyRange);

    if (typeof dispatchEvent !== 'undefined' && typeof addEventListener !== 'undefined') {
        globalEvents(DEXIE_STORAGE_MUTATED_EVENT_NAME, function (updatedParts) {
            if (!propagatingLocally) {
                var event_1;
                if (isIEOrEdge) {
                    event_1 = document.createEvent('CustomEvent');
                    event_1.initCustomEvent(STORAGE_MUTATED_DOM_EVENT_NAME, true, true, updatedParts);
                }
                else {
                    event_1 = new CustomEvent(STORAGE_MUTATED_DOM_EVENT_NAME, {
                        detail: updatedParts
                    });
                }
                propagatingLocally = true;
                dispatchEvent(event_1);
                propagatingLocally = false;
            }
        });
        addEventListener(STORAGE_MUTATED_DOM_EVENT_NAME, function (_a) {
            var detail = _a.detail;
            if (!propagatingLocally) {
                propagateLocally(detail);
            }
        });
    }
    function propagateLocally(updateParts) {
        var wasMe = propagatingLocally;
        try {
            propagatingLocally = true;
            globalEvents.storagemutated.fire(updateParts);
        }
        finally {
            propagatingLocally = wasMe;
        }
    }
    var propagatingLocally = false;

    if (typeof BroadcastChannel !== 'undefined') {
        var bc_1 = new BroadcastChannel(STORAGE_MUTATED_DOM_EVENT_NAME);
        globalEvents(DEXIE_STORAGE_MUTATED_EVENT_NAME, function (changedParts) {
            if (!propagatingLocally) {
                bc_1.postMessage(changedParts);
            }
        });
        bc_1.onmessage = function (ev) {
            if (ev.data)
                propagateLocally(ev.data);
        };
    }
    else if (typeof self !== 'undefined' && typeof navigator !== 'undefined') {
        globalEvents(DEXIE_STORAGE_MUTATED_EVENT_NAME, function (changedParts) {
            try {
                if (!propagatingLocally) {
                    if (typeof localStorage !== 'undefined') {
                        localStorage.setItem(STORAGE_MUTATED_DOM_EVENT_NAME, JSON.stringify({
                            trig: Math.random(),
                            changedParts: changedParts,
                        }));
                    }
                    if (typeof self['clients'] === 'object') {
                        __spreadArray([], self['clients'].matchAll({ includeUncontrolled: true }), true).forEach(function (client) {
                            return client.postMessage({
                                type: STORAGE_MUTATED_DOM_EVENT_NAME,
                                changedParts: changedParts,
                            });
                        });
                    }
                }
            }
            catch (_a) { }
        });
        if (typeof addEventListener !== 'undefined') {
            addEventListener('storage', function (ev) {
                if (ev.key === STORAGE_MUTATED_DOM_EVENT_NAME) {
                    var data = JSON.parse(ev.newValue);
                    if (data)
                        propagateLocally(data.changedParts);
                }
            });
        }
        var swContainer = self.document && navigator.serviceWorker;
        if (swContainer) {
            swContainer.addEventListener('message', propagateMessageLocally);
        }
    }
    function propagateMessageLocally(_a) {
        var data = _a.data;
        if (data && data.type === STORAGE_MUTATED_DOM_EVENT_NAME) {
            propagateLocally(data.changedParts);
        }
    }

    DexiePromise.rejectionMapper = mapError;
    setDebug(debug, dexieStackFrameFilter);

    var namedExports = /*#__PURE__*/Object.freeze({
        __proto__: null,
        Dexie: Dexie$1,
        liveQuery: liveQuery,
        'default': Dexie$1,
        RangeSet: RangeSet,
        mergeRanges: mergeRanges,
        rangesOverlap: rangesOverlap
    });

    __assign(Dexie$1, namedExports, { default: Dexie$1 });

    return Dexie$1;

}));


}).call(this)}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {},require("timers").setImmediate)
},{"timers":2}],5:[function(require,module,exports){
// db.js
const Dexie = require('dexie');

const db = new Dexie('workspace');;
db.version(1).stores({
  files: '++id, workspaceName, filePath, fileResult', // Primary key and indexed props
});

module.exports.db = db;
},{"dexie":4}],6:[function(require,module,exports){
const PrismFetchThrottler = function($fetch,throughput,waitInMS) {
    const ratelimiter = {
        throttleQueue: [],
        intervalId: null,
        throughput: throughput||50,
        activeQueue: [],
        waitInMS: waitInMS || 1000,
        executeBundle: function() {
            if (!this.intervalId) {
                var $ratelimiter = this;
                $ratelimiter.intervalId = setTimeout(function() {
                    var activePromises = $ratelimiter.throttleQueue.splice(0, $ratelimiter.throughput).map(itemParams=>itemParams());
                    if (activePromises.length) {
                        $ratelimiter.activeQueue.splice(0, 0, ...activePromises);
                        Promise.all(activePromises).then(function(dn) {
                            $ratelimiter.intervalId = null;
                            $ratelimiter.executeBundle();
                        });
                    } else {
                        console.debug('[PrismFetchThrottler]','Queue Done.');
                        $ratelimiter.intervalId = null;
                    }
                }, $ratelimiter.activeQueue <= $ratelimiter.throughput ? 0 : $ratelimiter.waitInMS);
            }
            
        }
    }

     function executeQueued() {
        var $arguments = arguments;
        const isPriority = Object.values($arguments).some(function(item) {
            return /priority/i.test(item);
        });

        return new Promise(function(resolve, reject) {
            const fnCB = function() {
                return $fetch.apply(null, $arguments).then(resolve).catch(reject);
            };

            if (isPriority) {
                ratelimiter.throttleQueue.splice(0, 0, fnCB);
            } else {
                ratelimiter.throttleQueue.push(fnCB);
            }
            ratelimiter.executeBundle();
        }
        );

    }
    executeQueued.config = {
        
    }

    Object.defineProperty(executeQueued.config, 'throughput',{
        get: function getThroughput() {
            return ratelimiter.throughput;
        },
        set: function setThroughput(val){
            ratelimiter.throughput = val;
        }
    });
    
    
    return executeQueued;
}

module.exports = PrismFetchThrottler;
},{}],7:[function(require,module,exports){
const PENDING = "pending";
const FULFILLED = "fulfilled";
const REJECTED = "rejected";

function prismpromise(executor) {
    var state=PENDING;
    var value = null;
    var handlers=[];
    var catchers = [];
	var $this = this;
  	function updateValue(result) {
      	if (result && result.constructor.name != "prismpromise") {
           	value = result; 
        }
      	else if (result && result.constructor.name == "prismpromise"){
          	$this = result;
        }
      
    }
  
    function resolve(result) {
        if(state!==PENDING) return;

        state=FULFILLED;
        value = result;

        handlers.forEach(function(h){ 
          updateValue(h(value));
          
          return result;
		 });
    }

    function reject(error) {
        if(state!==PENDING)return;

        state=REJECTED;
        value=error;
        catchers.forEach(function(c) { return c(value);});
    }

    this.then = function(successCallback) {
        if(state === FULFILLED) {
          	updateValue(successCallback(value));
        }else {
            handlers.push(successCallback);
        }
        return $this;
    }

    this.catch = function(failureCallback) {
        if(state===REJECTED){
            failureCallback(value)
        } else {
            catchers.push(value);
        }
      	return $this;
    }
    executor(resolve,reject);
}

prismpromise.all = function(promises) {
    
    return new prismpromise(function (resolve, reject) {
        var cnt = 0;
        var res = [];
        if (promises.length === 0) {
            resolve(promises);
            return;
        }
        promises.forEach(function(prom,i){
            prom.then(function(val) {
                done(val, i);
            }).catch(function(err) {
                reject(err);
                
            });
        })
        
        
        function done(val, i) {
            res[i] = val;
            cnt++;
            if (promises.length === cnt) {
                resolve(res);
            }
        }
    });
}

module.exports = prismpromise;
},{}],"prism_api_base":[function(require,module,exports){
const prismworker = require("./prism_worker");
const prismpromise = require("./prism_promise");

const prism_api_base = function($prism) {

    var $prism_api_base = this;
    const configParams = {
        auth: {},
        kurmiHost: '..',
        apiPath: "/Kurmi/restAPI.do",
        substitutitionTenantOrTemplate: null,
        throughput: 50
    }
    
    const mergeParams = function(newConfigParams) {
        $configParams = newConfigParams || {
            auth: {},
            kurmiHost: '..',
            apiPath: "/Kurmi/restAPI.do",
            substitutitionTenantOrTemplate: null
        };
        Object.keys($configParams).reduce(function(p,c){
            p[c] = $configParams[c];
            return p;
        },configParams);
    }
    this.initParams = mergeParams;

    if (sessionStorage){
        mergeParams(JSON.parse(sessionStorage.getItem('configParams')));
    }

    this.kurmiRequestPath = function() {
        return configParams.kurmiHost + configParams.apiPath;
    }

    this.kurmiWSDLPath = function() {
        return this.kurmiRequestPath().replace("restAPI.do","APIService?wsdl");
    }

    this.configParams = configParams;

    function getAuth() {
        return JSON.parse(JSON.stringify(configParams.auth));
    }

    function setAuth(login,password,kurmiHost){
        configParams.auth.login = login;
        configParams.auth.password = password;
        if (window.hasOwnProperty('sessionStorage')){
            sessionStorage.setItem('configParams', JSON.stringify(configParams));
        }
        if (kurmiHost){
            configParams.kurmiHost = kurmiHost;
        }
    }

    this.execute = function(functionName,params,tenantDbidOrName){
        params.auth = getAuth();
        if (tenantDbidOrName){
            params.auth.substitutionTenantOrTemplate  = getSubstitutionTenantOrTemplate(tenantDbidOrName);
        }
        else if (configParams.substitutionTenantOrTemplate) {
            params.auth.substitutionTenantOrTemplate  = configParams.substitutionTenantOrTemplate ;
        }
        
        if (typeof Promise === "function"){
            return $prism.executeKurmiAPI(functionName,params);
        }

        return new prismpromise(function(resolve,reject){
            var rawresult = $prism.executeKurmiAPI(functionName,params);
            
            var result = typeof rawresult === "object" ? rawresult : JSON.parse(rawresult);
            if (result.status && result.status != "SUCCESS"){
                reject(result);
            }
            else {
                resolve(result);
            }

        });
        
        //return new prismworker([functionName,params],$prism.executeKurmiAPI);
    }

    this.execute.priority = function(){
        var args = Object.values(arguments).concat('priority');
        this.execute.apply(this,  args)
    }

    const connectedEvents = [function(p){
        return p.getTenants().then(function(res){
            p.tenants = res;
            //$prism.debug("loaded tenants",JSON.stringify(p.tenants));
        });
        
    }];
    this.addConnectedEvent = function(callback){
        connectedEvents.push(callback);
    }

    const proxypromise = typeof Promise === "function" ? Promise : prismpromise;

    this.isConnected = false;
    this.isSubstituted = false;
    this.login = setAuth;
    this.connect = function() {
        
        return this.execute("getKurmiVersion",{}).then(function(res){
            return proxypromise.all(connectedEvents.map(function(cb){
                return cb($prism_api_base);
            })).then(function(done){
                $prism.debug("connect",true);
                $prism_api_base.connected = true;
            });
            
            
        }).catch(function(e){
            $prism.debug("connect",false,e);
            $prism_api_base.connected = false;
        });
    }

    var tenantList = null; 
    this.tenants = tenantList;
    this.getTenants = function() {
        return $prism.component.searchComponentList({type:"TENANT"});
    }

    function getSubstitutionTenantOrTemplate(tenantDbidOrName){
        var result = null;
        switch(typeof tenantDbidOrName){
            
            case typeof "tenantname": {
                try {
                    if (/SYSTEM/i.test(tenantDbidOrName)){
                        result = null;
                    }
                    //var numberDbid = tenantDbidOrName.replace(/\D/g,"");
                    //if (numberDbid && parseInt(numberDbid)){
                    //    result = getSubstituteTenantOrTemplate(parseInt(numberDbid));
                    //}
                    var tenant = $prism_api_base.tenants.filter(function(tenant){
                        return (new RegExp(tenantDbidOrName,"i")).test(tenant.identifier);
                    })[0];
                    result = {"@type": "TENANT", "@dbid": tenant["dbid"]};
                }
                catch(e){

                }

                break;
            }
            case typeof 1234: {

                break;
            }

        }
        $prism.debug("getSubstitutionTenantOrTemplate",JSON.stringify([tenantDbidOrName,result]));
        return result;
    }

    this.substitute = function(tenantDbidOrName){
        configParams.substitutionTenantOrTemplate = getSubstitutionTenantOrTemplate(tenantDbidOrName);
        console.info("Substituted to ",tenantDbidOrName);
        this.isSubstituted = configParams.substitutionTenantOrTemplate;
        return true;
    }

    this.exitSubtitution = function() {
        delete configParams.auth.substitutionTenantOrTemplate;
        configParams.substitutionTenantOrTemplate = null;
        console.info("Exited Substitution");
        this.isSubstituted = configParams.substitutionTenantOrTemplate;
        return true;
    }

}

module.exports = prism_api_base;

},{"./prism_promise":7,"./prism_worker":"prism_worker"}],"prism_api_wsdl":[function(require,module,exports){
const localCache = {
    get: function () {

    },
    set: function () { }

}
const prism_api_wsdl = async function ($prism) {
    $prism.api.apiMethods = {};
    $prism.api.apiMethodNames = [];
    $prism.api.wsdlStorage = {};


    function parseWsdl(data) {
        let doc = document.implementation.createHTMLDocument("Kurmi WSDL Document");
        doc.write(data);
        $prism.api.wsdlStorage["main"] = data;
        doc.querySelectorAll('message').forEach(function (item, i) {
            $prism.api.apiMethodNames.push(item.getAttribute('name'));
            var name = item.getAttribute('name');
            /*
            $kurmiapimodule.apiMethods[name] = function (params, tenantName) {
                return $kurmiapimodule.execute(name, params || {}, tenantName).toJSON();
            }

            $kurmiapimodule.apiMethods[name].element = (item.querySelector('part').getAttribute('element') || ":").split(':')[1];
            $kurmiapimodule.apiMethods[name].getParams = function () {
                    return $kurmiapimodule.getParamsForAPIMethod(name);
                }
            $kurmiapimodule.apiMethods[name].execute = function (params, tenantName) {
                return $kurmiapimodule.execute(name, params || {}, tenantName).toJSON();
            }
            */
            if (!/.*Response$/i.test(name)) {

                $prism.api[name] = function (params, tenantName) {
                    return $prism.api.execute(name, params || {}, tenantName);
                }

                $prism.api[name].element = (item.querySelector('part').getAttribute('element') || ":").split(':')[1];
                $prism.api[name].getParams = function () {
                    return getParamsForAPIMethod(name, $prism.api[name].element);
                }
                $prism.api[name].getPrototype = function () {
                    return getFunctionForParams(name, $prism.api[name].element);
                }


                $prism.api[name].execute = function (params, tenantName) {
                    return $prism.api.execute(name, params || {}, tenantName);
                }
            }
        });
        $prism.log("Loaded API Methods, ready!");

    };

    function wsdlResolver(wsdl, queryselector, objectIn) {

        (wsdl).querySelectorAll(queryselector).forEach(function (i, item) {
            jQuery(item).querySelectorAll('xs\\:element').forEach(function (iitem, ii) {
                var name = (iitem).getAttribute('name');
                var type = (iitem).getAttribute('type');
                var objectToPass = {};
                if (objectIn[name] && typeof objectIn[name] === typeof {}) {
                    objectIn[name] = [objectIn[name]];
                }
                if (objectIn[name] && typeof objectIn[name] === typeof []) {
                    objectToPass = {};
                    objectIn[name][objectIn[name].length] = objectToPass;

                } else {
                    objectIn[name] = {};
                    objectToPass = objectIn[name];
                }
                $prism.api.wsdlResolver(wsdl, `xs\\:complexType[name="${type}"]`, objectToPass);
            });
        });
    }

    async function getParamsForAPIMethod(methodName, elementName) {
        methodName = ($prism.api.apiMethods[methodName] || {}).element || methodName;
        if (!$prism.api.wsdlStorage["main"]) {
            $prism.api.loadApiMethods();
        }
        let doc = document.implementation.createHTMLDocument("New Document");
        doc.write($prism.api.wsdlStorage["main"])
        var schemaLocation = doc.querySelector('types [namespace=\"http://www.nates.fr/API/1.0\"]').getAttribute('schemaLocation');
        var params = [];
        var data = await (await (fetch(schemaLocation))).text();
        let doc2 = document.implementation.createHTMLDocument("New Document");
        doc2.write(data);
        $prism.log(doc2.querySelector(`xs\\:element[name="${elementName}"]`).outerHTML);
        doc2.querySelectorAll(`xs\\:element[name="${elementName}"]`).forEach(function (item, i) {
            item.querySelectorAll('xs\\:element').forEach(function (iitem, ii) {
                var name = (iitem).getAttribute('name');
                var xmltype = ((iitem).getAttribute('type') || "").replace("tns:", "");
                if (xmltype) {
                    //var typp = (doc2).querySelector(`xs\\:complexType[name="${xmltype}"]`);
                    //console.log("type", (typp).outerHTML);
                    params.push((iitem).getAttribute('name'));
                }
                else {
                    params.push((iitem).getAttribute('name'));
                }
            });
            item.querySelectorAll('xs\\:extension').forEach(function (iitem, ii) {
                var base = (iitem).getAttribute('base').split(':')[1];
                params.push(base);
                $prism.log(doc2.querySelector(`xs\\:extension[base="${(iitem).getAttribute('base')}"]`).outerHTML);
            });
        });
        $prism.log(params);
        return params;
    }

    $prism.api.wsdlTypes = {};
    var wsdlTypes = $prism.api.wsdlTypes;

    function processParam(item) {
        var name = item.getAttribute('name');
        var minOccurs = parseInt(item.getAttribute('minOccurs') || "-1");
        var prefix = "o";
        switch (item.getAttribute('type')) {
            case "xs:string": {
                prefix = "s";
                break;
            }
            case "xs:boolean": {
                prefix = "b";
                break;
            }
            case "xs:int": {
                prefix = "i";
                break;
            }

        }
        var paramName = prefix + name.replace(/^(.)/, function (r, fm) { return fm.toUpperCase(); })
        paramName = minOccurs == 0 ? "optional_" + paramName : paramName;
        var functionLogic = minOccurs == 0 ? `\tif (${paramName}) this["${paramName}"] = ${paramName};` : `\tthis["${paramName}"] = ${paramName};`;
        paramName = paramName.replace(/^(.)/, function (r, fm) { return fm.toLowerCase(); });
        return [paramName, functionLogic];
    }

    async function getFunctionForParams(methodName, elementName) {
        methodName = ($prism.api.apiMethods[methodName] || {}).element || methodName;
        if (!$prism.api.wsdlStorage["main"]) {
            $prism.api.loadApiMethods();
        }
        let doc = document.implementation.createHTMLDocument("New Document");
        doc.write($prism.api.wsdlStorage["main"])
        var schemaLocation = doc.querySelector('types [namespace=\"http://www.nates.fr/API/1.0\"]').getAttribute('schemaLocation');
        var params = [];
        var functionLines = [];
        var data = await (await (fetch(schemaLocation))).text();
        let doc2 = document.implementation.createHTMLDocument("New Document");
        doc2.write(data);
        $prism.log(doc2.querySelector(`xs\\:element[name="${elementName}"]`).outerHTML);


        doc2.querySelectorAll(`xs\\:element[name="${elementName}"]`).forEach(function (item, i) {

            item.querySelectorAll('xs\\:element').forEach(function (iitem, ii) {
                var [paramName, functionLogic] = processParam(iitem);
                params.push(paramName);
                functionLines.push(functionLines);
            });
            item.querySelectorAll('xs\\:extension').forEach(function (iitem, ii) {
                var base = (iitem).getAttribute('base').split(':')[1];

                doc2.querySelectorAll(`xs\\:complexType[name="${base}"] xs\\:element`).forEach(function (iiitem) {
                    var [paramName, functionLogic] = processParam(iiitem);
                    params.push(paramName);
                    functionLines.push(functionLines);
                });

            });
        });
        $prism.log(params);

        return new Function(params, functionLines.join('\r\n'));
        //return params;
    }

    var wsdl = localCache.get("wsdl", "main");
    if (wsdl) {
        return parseWsdl(wsdl);
    }


    return fetch($prism.api.kurmiWSDLPath()).then(function (data) {
        return data.text();
    }).then(parseWsdl).catch(function (e) {
        $prism.error("Failed to load SOAP API METHODS!");
        $prism.api.ready = false;
    });


}

module.exports = prism_api_wsdl;


},{}],"prism_base":[function(require,module,exports){
const prism_query = function(selector) {
    this.selector = selector;
}


const prism_base = {
    version: 0,
    logParams: {},
    moduleMap: {
        "api": function($prism) { return new (require("./prism_api_base"))($prism); },
        "sql": function($prism) { return new (require("./prism_sql_base"))($prism); },
        "component": function($prism) { return new (require("./prism_component_base"))($prism); },
        "workspace": function($prism) { return new (require("./prism_workspace_base"))($prism); },
        "sunrise": function($prism) { return new (require("./prism_sunrise_base"))($prism); }
    },
    init: function() {
        var $prismparent = prism_query;
        var $prisminstance = {
            executeKurmiAPI: typeof executeKurmiAPI === "function" ? executeKurmiAPI : null
        };
        $prismparent.__proto__ = $prisminstance;

        const prism_logger = require("./prism_logger");
        prism_logger($prisminstance);
        var $prism_base = this;
        Object.keys($prism_base.moduleMap).forEach(function(moduleName){
            $prisminstance[moduleName] = $prism_base.moduleMap[moduleName]($prismparent);
        });
        return $prismparent;       
    }
}

module.exports = prism_base;

},{"./prism_api_base":"prism_api_base","./prism_component_base":"prism_component_base","./prism_logger":"prism_logger","./prism_sql_base":"prism_sql_base","./prism_sunrise_base":"prism_sunrise_base","./prism_workspace_base":"prism_workspace_base"}],"prism_component_base":[function(require,module,exports){
const prismworker = require("./prism_worker");
const prism_component_base = function($prism) {
    var $prism_component_base = this;
    if (typeof searchExistingComponent != "undefined") {
        this.searchExistingComponent = searchExistingComponent && function() { 
            var args = arguments;
            return new prismworker(Object.keys(args).map(function(k){return args[k]; }),searchExistingComponent);
            
        }
    }
    else {
        this.searchExistingComponent = function(searchFilter) { 
            return this.searchComponentList(searchFilter,0,false);
        }
    }
    if (typeof searchComponentList != "undefined"){
        this.searchComponentList = searchComponentList && function() { 
            var args = arguments; 
            return new prismworker(Object.keys(args).map(function(k){return args[k]; }),searchComponentList); 
        }
    }
    else {
        this.searchComponentList = function (filter, startingIndex, returnLimit) {
            var params = { filter: filter, detail: true };
            if (filter["type"]) {
                filter["@type"] = filter["type"];
                delete filter["type"];
            }
            if (startingIndex) params.startingIndex = startingIndex;
            if (returnLimit) params.returnLimit = returnLimit;
            var componentWorker = {}
            return $prism.api.execute("searchQuery", params).then(function(componentResult){
                componentWorker.totalNumberOfResults = componentResult.totalNumberOfResults;
                if (componentResult && componentResult.componentDetail && componentResult.componentDetail[0]) {
                    return componentResult.componentDetail.map(function ($component) {
                        var _component = { "dbid": $component["@dbid"], "type": $component["@type"] };
                        if ($component.fields && $component.fields.field) {
                            $prism_component_base.fieldDeserialize($component.fields.field, _component);
                        }
                        if ($component.fields && $component.fields.fieldMult) {
                            //$this.api.component.fieldDeserialize($component.fields.field, _component);
                        }
                        return _component;
                    });
                }
                else if (componentResult && !componentResult.componentDetail) {
                    $prism.warn("[kQuery] searchQuery returned no components.", "componentResult.totalNumberOfResults: ", JSON.stringify(componentResult.totalNumberOfResults));
                    return null;
                }
                else {
                    return undefined;
                }
            });
        }

    }

    $prism_component_base.setConnectorVirtual = function (tenant, equipmentId, virtual) {
        return $prism.api.execute("searchConfig", { query: "/*/*[@equipmentId=" + equipmentId + "]" }, tenant).then(function(searchConfigQuery){
            if (!searchConfigQuery.path) {
                throw "[kQuery] No equipment found with equipmentId \"" + equipmentId + "\"" + (tenant ? " on tenant \"" + tenant + "\"" : "");
            }
            var query = virtual === true ? "*[not(boolean(@virtual))]" : "*[boolean(@virtual)]";
            return $prism.api.execute("searchConfig", { path: searchConfigQuery.path[0], query: query }, tenant).then(function(searchConfig){
                var setConfig = { configVersion: searchConfig.configVersion };

                setConfig.update = [searchConfigQuery.path].concat(searchConfig.path).map(function (path) {
                    return { "@path": path, key: { "@id": "virtual", "#text": virtual === true ? "true" : "false" } };
                });
                return $prism.api.execute("setConfig", setConfig, tenant);
            });
        });
    }
    $prism_component_base.processConfigNode = function processNode(_node, mergeWith) {
        if (_node.node) {
            if (/^\d+$/.test(_node.node[0]["@id"])) {
                mergeWith = _node.node.map(function (__node) {
                    return processNode(__node, {});
                });
            }
            else {
                _node.node.forEach(function (__node) {
                    mergeWith[__node["@id"]] = processNode(__node, {});
                });
            }
        }
        if (_node.key) {
            return _node.key.reduce(function (p, c) {
                p[c["@id"]] = c["#text"];
                return p;
            }, mergeWith);
        }
        else {
            return mergeWith;
        }
    }
    
    $prism_component_base.getEquipmentConfiguration = function (equipmentId, tenant) {
        var params = { query: "/*/*[@equipmentId=" + equipmentId + "]" };
        return $prism.api.execute("searchConfig", params, tenant).then(function (searchConfigQuery) {
            if (!searchConfigQuery.path) {
                $prism.error(JSON.stringify([params, searchConfigQuery]));
                throw "[prism] No equipment found with equipmentId \"" + equipmentId + "\"" + (tenant ? " on tenant \"" + tenant + "\"" : "");
            } else {
                return $prism.api.execute("getConfigTree", { path: searchConfigQuery.path[0], query: "[@equipmentId=" + equipmentId + "]" }, tenant).then(function (equipmentConfigurationRequest) {
                    return equipmentConfigurationRequest.configTree && $prism_component_base.processConfigNode(equipmentConfigurationRequest.configTree, {});
                });
            }
        });
    }

    $prism_component_base.fieldSerialize = function (fieldObject) {
        return Object.keys(fieldObject).map(function (key) {
            return {
                "@name": key,
                "#text": fieldObject[key]
            };
        });
    }

    $prism_component_base.fieldDeserialize = function (fieldsetList, baseObject, bFavorTextValueOverTechnicalValue) {
        return fieldsetList.reduce(function (p, fieldset, i, a) {
            if (bFavorTextValueOverTechnicalValue){
                p[fieldset["@name"]] = fieldset["#text"] || fieldset["@technicalValue"];
            }
            else {
                p[fieldset["@name"]] = fieldset["@technicalValue"] || fieldset["#text"];
            }
            
            return p;
        }, baseObject || {});
    }

    
}

module.exports = prism_component_base;
},{"./prism_worker":"prism_worker"}],"prism_connector_base":[function(require,module,exports){
const prismworker = require("./prism_worker");
const prism_connector_base = function($prism) {
    var $prism_connector_base = this;
    
    $prism_connector_base.setConnectorVirtual = function (tenant, equipmentId, virtual) {
        return $prism.api.execute("searchConfig", { query: "/*/*[@equipmentId=" + equipmentId + "]" }, tenant).then(function(searchConfigQuery){
            if (!searchConfigQuery.path) {
                throw "[kQuery] No equipment found with equipmentId \"" + equipmentId + "\"" + (tenant ? " on tenant \"" + tenant + "\"" : "");
            }
            var query = virtual === true ? "*[not(boolean(@virtual))]" : "*[boolean(@virtual)]";
            return $prism.api.execute("searchConfig", { path: searchConfigQuery.path[0], query: query }, tenant).then(function(searchConfig){
                var setConfig = { configVersion: searchConfig.configVersion };

                setConfig.update = [searchConfigQuery.path].concat(searchConfig.path).map(function (path) {
                    return { "@path": path, key: { "@id": "virtual", "#text": virtual === true ? "true" : "false" } };
                });
                return $prism.api.execute("setConfig", setConfig, tenant);
            });
        });
    }
    $prism_connector_base.processConfigNode = function processNode(_node, mergeWith) {
        mergeWith = mergeWith || {};
        if (_node.node) {
            if (/^\d+$/.test(_node.node[0]["@id"])) {
                mergeWith = _node.node.map(function (__node) {
                    return processNode(__node, {});
                });
            }
            else {
                _node.node.forEach(function (__node) {
                    mergeWith[__node["@id"]] = processNode(__node, {});
                });
            }
        }
        if (_node.key) {
            return _node.key.reduce(function (p, c) {
                p[c["@id"]] = c["#text"];
                return p;
            }, mergeWith);
        }
        else {
            return mergeWith;
        }
    }
    
    $prism_connector_base.getEquipmentConfiguration = function (equipmentId, tenant) {
        var params = { query: "/*/*[@equipmentId=" + equipmentId + "]" };
        return $prism.api.execute("searchConfig", params, tenant).then(function(searchConfigQuery){
            if (!searchConfigQuery.path) {
                $prism.error(JSON.stringify([params, searchConfigQuery]));
                throw "[prism] No equipment found with equipmentId \"" + equipmentId + "\"" + (tenant ? " on tenant \"" + tenant + "\"" : "");
            } else {
                return $prism.api.execute("getConfigTree", { path: searchConfigQuery.path[0], query: "[@equipmentId=" + equipmentId + "]" }, tenant).then(function (equipmentConfigurationRequest) {
                    return equipmentConfigurationRequest.configTree && $prism_connector_base.processConfigNode(equipmentConfigurationRequest.configTree, {});
                });
            }
        });        
    }

    $prism_connector_base.fieldSerialize = function (fieldObject) {
        return Object.keys(fieldObject).map(function (key) {
            return {
                "@name": key,
                "#text": fieldObject[key]
            };
        });
    }

    $prism_connector_base.fieldDeserialize = function (fieldsetList, baseObject, bFavorTextValueOverTechnicalValue) {
        return fieldsetList.reduce(function (p, fieldset, i, a) {
            if (bFavorTextValueOverTechnicalValue){
                p[fieldset["@name"]] = fieldset["#text"] || fieldset["@technicalValue"];
            }
            else {
                p[fieldset["@name"]] = fieldset["@technicalValue"] || fieldset["#text"];
            }
            
            return p;
        }, baseObject || {});
    }

    $prism_connector_base.getRootConfiguration = function(tenantName) {
        return $prism.api.execute('getConfigTree',{path:'/'}, tenantName).then(function(res){
            return $prism_connector_base.processConfigNode(res.configTree,{});
        });        
    }

    $prism_connector_base.getReference = async function getReference(moduleName, tenantName){
        var tenant = $prism.api.tenants.filter(function(tenant){
            return (new RegExp(tenantName,"i")).test(tenant.identifier);
        })[0];
        var workspaceId = 2;
        if (tenant){
            workspaceId = await $prism.workspace.getWorkspaceId(tenant.configBranch.split('/').slice(-2,-1)[0]);
        }

        return await prism.api.getReferenceConfigTree({workspace:workspaceId}).then(function({configTree}){
            return configTree.node.find(n=>n['@id'] == moduleName);
        }).then(function({node}){
           return node[0]; 
        }).then(function(node){
            return $prism_connector_base.processConfigNode(node,{}); 
        });
    }

    function genericConnectorEquipment() {


    }


    function AvayaCM(){
        this.__proto__ = new genericConnectorEquipment();
    }
    
    
}

module.exports = prism_connector_base;
},{"./prism_worker":"prism_worker"}],"prism_executeKurmiAPI_async":[function(require,module,exports){
const FetchThrottler = require('./prism_fetch_throttler');


module.exports = function ($prism) {
    return function (fnFetch, FormData) {
        const fetch = new FetchThrottler(fnFetch,$prism.api.configParams.throughput,1000);

        

        const executeKurmiAPI = function executeKurmiAPI(functionName, params) {
            params = params || {};
            var $this = this;
            if ($prism.api.configParams.throughput){
                fetch.throughput = $prism.api.configParams.throughput;
            }

            var $promise = new Promise(function (resolve, reject) {
                try {
                    var formData = new FormData();
                    formData.append("jsonData", JSON.stringify({
                        "function": functionName,
                        "parameters": params
                    }));
                    formData.append("action", "apiKurmi");
                    fetch($prism.api.kurmiRequestPath(), {
                        method: 'POST',
                        //credentials: "include",
                        //mode: 'no-cors',
                        body: formData
                    }).then(function (resp) {
                        return resp.json();
                    }).then(function (jResp) {
                        if (jResp.status && jResp.errorDetail) {
                            reject(jResp);
                        }
                        else if (jResp.status && jResp.status == "SUCCESS") {
                            resolve(jResp);
                        } else if (jResp.status && jResp.status != "SUCCESS") {
                            reject(jResp);
                        } else {
                            throw "[prism][unexpected json result] " + JSON.stringify(jResp);
                        }

                    }).catch(function (e) {
                        reject(e);
                    });

                } catch (e) {
                    reject({
                        status: "FAIL",
                        message: e
                    });
                }
            });

            return $promise;
        }



        

        return executeKurmiAPI;
    };
};

},{"./prism_fetch_throttler":6}],"prism_executeKurmiAPI_browsersync":[function(require,module,exports){
module.exports = function ($prism) {
    return function (functionName, params) {
		params = params || {};
		var xhttp = new XMLHttpRequest();
		xhttp.open("POST", $prism.api.kurmiRequestPath(), false);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		var formData = new FormData();
		formData.append("jsonData", JSON.stringify({
			"function": functionName,
			"parameters": params
		}));
		formData.append("action", "apiKurmi");
		xhttp.send(new URLSearchParams(formData).toString());
		return xhttp.responseText;
	};
};

/*
module.exports = function (functionName, params) {
    params = params || {};
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", $prism.api.config.restAPIpath, false);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    var formData = new FormData();
    formData.append("jsonData", JSON.stringify({
        "function": functionName,
        "parameters": params
    }));
    formData.append("action", "apiKurmi");
    xhttp.send(new URLSearchParams(formData).toString());
    return xhttp.responseText;
};
*/
},{}],"prism_logger":[function(require,module,exports){
function prism_logger($prism) {
    $prism.logParams = $prism.logParams || {};
    $prism.logParams.title = $prism.logParams.title || "prism";
    ["log","debug","error","warn","info"].forEach(function(key){
        $prism[key] = function() {
            var $arguments = arguments;
            var arrayOfArgs = Object.keys($arguments).map(function(a){ return $arguments[a];});
            if ($prism.logParams.concatWithChar) {
                arrayOfArgs = [arrayOfArgs.join($prism.logParams.concatWithChar)]
            }
            arrayOfArgs.splice(0,0,"["+$prism.logParams.title+"]");
            
            console[key].apply(null,arrayOfArgs);
        }

    })
}

module.exports = prism_logger;
},{}],"prism_reporting_base":[function(require,module,exports){
const prism_reporting_base = function ($prism) {
    var $prism_reporting_base = this;
    function getSegments(path){
        var segments = [];
        while(path.length > 0) {
            var dot = path.indexOf('.');
            var bracketOpen = path.indexOf('[');
            var bracketClose = path.indexOf(']');
            var splitCaptureStart = -1;
            var splitCaptureEnd = 1;
  
            if (dot == -1 && bracketOpen == -1) {
                splitCaptureStart = 0;
                splitCaptureEnd = path.length;
            }
            else if (dot == -1 && bracketOpen != -1){
                dot = 1000000
            }
            else if (dot != -1 && bracketOpen == -1){
                bracketOpen = 100000000;
            }
  
            if (dot < bracketOpen){
                splitCaptureStart = dot;
                splitCaptureEnd = dot+1;
            }
            else if (bracketOpen < dot){
                splitCaptureStart = bracketOpen;
                splitCaptureEnd = bracketClose+1;
                if (path[splitCaptureEnd] == ".") {
                    splitCaptureEnd++;
                }
            }
            if (splitCaptureStart > 0) {
                segments.push(path.substring(0,splitCaptureStart));
            }
            if (splitCaptureEnd > 0) {
                segments.push(path.substring(splitCaptureStart,splitCaptureEnd).replace(/(.)\.$/,"$1"));
            }
            path = path.slice(splitCaptureEnd);
  
        }
        return segments;
    }
  
    function evalSegments(withObject,segments){
        if (segments.length == 0){
            return withObject;
        }
        else if (/\{/.test(segments[0])){
            var newWithObject = withObject;
        }
        else {
            var newWithObject = withObject[segments[0]] ;
        }
  
        if (Array.isArray(newWithObject)){
            var dotBracket = segments[1];
            var props = dotBracket.match(/\[(.+?)\]/)[1].split(/&&|\|\|/);
  
            if (props && props.indexOf('*') == -1){
                newWithObject = newWithObject.filter(function(item){
                    with(item){
                        return props.every(function(prop){ return eval(prop);});
                    }
                });
            }
            return newWithObject.map(function(item){
                return evalSegments(item, segments.slice(2));
            });
        }
        else if (typeof newWithObject === "object"){
            var matchForProps = segments[Math.min(segments.length-1,1)];
            var objProps = matchForProps.match(/\{(.+?)\}/);
            objProps = objProps && objProps[1].split(',');
            if (objProps){
                return JSON.parse(JSON.stringify(newWithObject,objProps))
            }
            return evalSegments(newWithObject,segments.slice(2));
        }
        else {
            return newWithObject;
        }
  
    }
    $prism_reporting_base.evalPath = function(path){
        var segments = getSegments(path);
        return evalSegments(this,segments);
    }

    $prism_reporting_base.listAdministrators = function(){
        return prism.api.searchAdministrators({filter:{}}).then(function(result) { 
            return Promise.all(result.login.map(function(adminLogin) { 
                return prism.api.execute("getAdministratorContent",{adminLogin:adminLogin}).then(function(result){
                    return prism.component.fieldDeserialize(result.field);
                });
            })); 
        });
    }

}

module.exports = prism_reporting_base;
},{}],"prism_sql_base":[function(require,module,exports){
const prism_sql_base = function ($prism) {
    var $prism_sql_base = this;

    $prism_sql_base.read = function (sqlScript) {
        return $prism.api.execute("executeReadSQL", { sqlScript: sqlScript }).then(function (sqlResult) {
            return (sqlResult.sqlLineResult || []).map(function(row) { return row.item;});
        });
    }
    $prism_sql_base.getObject = function (columns, table) {
        return $prism_sql_base.read("select " + columns.join(', ') + " from " + table + ";").then(function (sqlResult) {
            return sqlResult.reduce(function (p, c) {
                var obj = columns.reduce(function (agg, colName, colIndex) { agg[colName] = c[colIndex]; return agg; }, {});
                p.push(obj);
                return p;
            }, []);
        });
    }

    $prism_sql_base.listComponentTypes = function() {
        return $prism_sql_base.getObject(["name"],"componentkind").then(function(results){
            return results.reduce(function(p,c){
                return [c.name].concat(p);
            },[]);
        });
    }

    $prism_sql_base.listServiceTypes = function() {
        return $prism_sql_base.getObject(["serviceid"],"service_type").then(function(results){
            return results.reduce(function(p,c){
                return [c.serviceid].concat(p);
            },[]);
        });
    }


}

module.exports = prism_sql_base;
},{}],"prism_sunrise_base":[function(require,module,exports){
const prism_sunrise_base = function($prism){
    $prism_sunrise_base = this;
    
    $prism_sunrise_base.sunriseGetTenantDocuments = function (tenantName,wantedDocuments) {
        return $prism.api.execute("sunriseGetTenantDocuments",{tenant:tenantName,wantedDocument:[].concat(wantedDocuments)}).then(function(results){
            return results.document.reduce(function(res,doc){
                res[doc["@documentType"]] = JSON.parse(doc["#text"]);    
                return res;
            },{});
        });
    }       
    $prism_sunrise_base.getTenantDocument = function(tenantName){
        return $prism_sunrise_base.sunriseGetTenantDocuments(tenantName,"tenant")["tenant"];
    }
    $prism_sunrise_base.getProvisioningDocument = function(tenantName){
        return $prism_sunrise_base.sunriseGetTenantDocuments(tenantName,"provisioning")["provisioning"];
    }
    $prism_sunrise_base.getSynchroDocument = function(tenantName){
        return $prism_sunrise_base.sunriseGetTenantDocuments(tenantName,"synchro")["synchro"];
    }
    $prism_sunrise_base.releaseLockForScenario = function(scenarioDBId){
        return $prism.api.execute("releaseLockForScenario",{scenarioDBId:scenarioDBId});
    }
    $prism_sunrise_base.terminateSunriseScenario = function(scenarioDBId){
        return $prism.api.execute("releaseLockForScenario",{scenarioDBId:scenarioDBId});
    }

    $prism_sunrise_base.createEngineeringRuleFromDiscoveredComponent = function(tenant,component,links) {
        component;
    }

}

module.exports = prism_sunrise_base;
},{}],"prism_worker":[function(require,module,exports){
function prism_worker(arrayOfParams,functionToExecute,desc){
    this.msg = "im a prism worker: "+ desc;
  var $prism_worker = this;
    this.thenCounter = 0;
  try {
      var args = arguments;
      var rawResponse = functionToExecute.apply(null,arrayOfParams);
        
      if ( typeof rawResponse === 'object' && typeof rawResponse.then === 'function' && typeof rawResponse.catch === 'function'){
          //return promise
          return rawResponse;
      }
      $prism_worker.response = JSON.parse(rawResponse);
  }
  catch(e){
      $prism_worker.response = {errorMessage: e};
  }
  $prism_worker.data = $prism_worker.response;
  
  this.then = function(cb){
        this.thenCounter++;
      if (cb && $prism_worker.response.status && $prism_worker.response.status == "SUCCESS"){
          $prism_worker.data = cb($prism_worker.data);
      }
        
      return $prism_worker;
  }
  this.catch = function(ecb){
      if (ecb && (!$prism_worker.response || !$prism_worker.response.status || $prism_worker.response.status != "SUCCESS")){
          console.error("catch statement",JSON.stringify([ecb]));
          ecb($prism_worker.data);
      }
      return $prism_worker;
  }
  
}

module.exports = prism_worker;



},{}],"prism_workspace_base":[function(require,module,exports){
const prismpromise = require("./prism_promise");
const kurmifieldlist = require("./kurmi_fields");
const prism_workspace_base = function ($prism) {
    var $prism_workspace_base = this;

    const proxypromise = typeof Promise === "function" ? Promise : prismpromise;
    $prism_workspace_base.getList = function () {
        function flattenWorkspaceList(masterList, currentWorkspace) {
            masterList.push(currentWorkspace.name);
            (currentWorkspace.childs || []).reduce(flattenWorkspaceList, masterList);
            return masterList;
        }
        return $prism.api.execute("listWorkspaces", {}).then(function (listWorkspaceResult) {
            return listWorkspaceResult && listWorkspaceResult.workspaces && JSON.parse(listWorkspaceResult.workspaces).reduce(flattenWorkspaceList, []);
        });
    };
    $prism_workspace_base.listWorkspaceFiles = function (workspaceName, fileType) {
        return $prism.api.execute("listWorkspaceFiles", { workspace: workspaceName }).then(function (listWorkspaceFilesResult) {
            var files = JSON.parse(listWorkspaceFilesResult.files).map(function (file) { return file.path });
            if (fileType) {
                var rg = typeof fileType == typeof /.*/ ? fileType : new RegExp(fileType, "i");
                files = files.filter(function (f) { return rg.test(f); });
            }
            return files.sort();
        });
    },
        $prism_workspace_base.getWorkspaceFile = function (workspaceName, filePath) {
            return $prism.api.execute("getWorkspaceFile", { workspace: workspaceName, path: filePath }).then(function (getWorkspaceFileResult) {
                return getWorkspaceFileResult;
            });
        }

    $prism_workspace_base.listOfMailFileRelations = function (workspaceName) {
        return $prism.workspace.listWorkspaceFiles(workspaceName, "mail.js").then(function (mailfilenames) {
            var mailFileResultsPromise = prismpromise.all(mailfilenames.map(function (name) {
                return $prism.workspace.getWorkspaceFile(workspaceName, name);
            }));
            return mailFileResultsPromise.then(function (mailFileResults) {
                var mailFileIncludes = mailFileResults.map(function (item) { return { path: item.path, includes: (item.content.match(/(?=\#include )(.*\.mail.js)/g)  || []).map(function(mtch) { return mtch.replace("#include ","");   }) }; });
                return mailFileIncludes.reduce(function (p, c, i, a) {
                    c.mentions = a.filter(function (mfc) { return mfc.includes.indexOf(c.path) != -1; });
                    p.push(c);
                    return p;
                }, []);
            });
        });
    }
    $prism_workspace_base.mapOfMailfiles = function (workspaceName) {
        return $prism_workspace_base.listOfMailFileRelations(workspaceName).then(function (mailFileIncludes) {
            var map = {};

            function reduceMailFiles($item, list) {
                var item = JSON.parse(JSON.stringify($item));
                if (item.includes) {
                    item.includes.forEach(function (includedItemPath) {

                        var includedItem = list.filter(function(f) { return f.path == includedItemPath;})[0];
                        if (includedItem) {
                            item[includedItemPath] = reduceMailFiles(includedItem, list);
                        }
                    });

                }
                if (item.mentions && item.mentions.length == 0) {
                    map[item.path] = item;


                }
                delete item.path;
                delete item.includes;
                delete item.mentions;

                return item;
            }
            var mailTemplates = {};
            Object.keys(mailTemplates).reduce(function(p,k) {
                var v = mailTemplates[k];
              	p[k] = {};
                p[k][v] = tp1[v];
                
                return p;
            },{});

            mailFileIncludes.forEach(function (item, i, arr) { reduceMailFiles(item, arr); });
            return map;
        });
    }
    $prism_workspace_base.mapConfiguredMailFiles = function(optionalTenantName){
        //prism.api.getConfigTree({path:"/MailManagement"},"trainingpod1").then(function(res) { return prism.component.processConfigNode(res.configTree,{}); });
        
        function _mapConfiguredMailFiles(mapOfMailfiles,configuration_MailManagement){
            var templateKeys = Object.keys(configuration_MailManagement || {}).filter(function(key){ return /^template/i.test(key); });
            
            return templateKeys.reduce(function(p,c){
                p[c] = {};
                var templateFilePath = configuration_MailManagement[c];
                p[c][templateFilePath] = mapOfMailfiles[templateFilePath] || {};


                return p;
            },{});

        }
        if (!optionalTenantName){
            return $prism.api.execute("getConfigTree",{path:"/MailManagement"}).then(function(res) { return prism.component.processConfigNode(res.configTree,{}); }).then(function(configuration_MailModule){
                return $prism_workspace_base.getSystemWorkspaceName().then(function(workspaceName){
                    return $prism_workspace_base.mapOfMailfiles(workspaceName).then(function(momf){
                        return _mapConfiguredMailFiles(momf,configuration_MailModule);
                    });
                })
            });
            
        }
        else {
            return $prism.api.execute("getConfigTree",{path:"/MailManagement"},optionalTenantName).then(function(res) { 
                return $prism.component.processConfigNode(res.configTree,{}); 
            }).then(function(configuration_MailModule){
                return $prism_workspace_base.getTenantWorkspaceName(optionalTenantName).then(function(workspaceName){
                    return $prism_workspace_base.mapOfMailfiles(workspaceName).then(function(momf){
                        return _mapConfiguredMailFiles(momf,configuration_MailModule);
                    });
                });
            });
        }
    }

    $prism_workspace_base.getTenantWorkspaceName = function(tenantIdentifier){
        return $prism.api.getTenants().then(function(tenants){
            return tenants.filter(function(t){ return t.identifier == tenantIdentifier; })[0].configBranch.split('/').slice(-2,-1)[0];
        });
    }

    $prism_workspace_base.getSystemWorkspaceName = function(){
        return $prism.sql.read("select name from cfg_branch join cfg_main_branch on cfg_main_branch.cfg_branch_id = cfg_branch.cfg_branch_id;").then(function(result) {
            return result[0][0];
            });
    }

    $prism_workspace_base.getWorkspaceId = function(workspaceName){
        return $prism.sql.read("select cfg_branch_id from cfg_branch where name like '" + workspaceName + "';").then(function(result) {
            return result[0][0];
            });
    }

    $prism_workspace_base.getWorkspaceServicesAndQuickfeaturesDocument = function(workspaceName) {
        return $prism_workspace_base.listWorkspaceFiles(workspaceName,/(service|quickfeature).xml/).then(function(listServices){
            return listServices.map(function(filePath){
                return $prism_workspace_base.getWorkspaceFile(workspaceName,filePath)
            })
        }).then(function(fileContentPromises){
            return Promise.all(fileContentPromises).then(function(fileResults) { return $prism_workspace_base.parseFileResultstoDocument(fileResults); });
        });

    }

    $prism_workspace_base.Workspace = function(workspaceDocument){
        this.xmlDocument = workspaceDocument;
        function getParentsUntil(ele,tagName) {
            return ele.tagName != tagName ? getParentsUntil(ele.parentElement,tagName) : ele;
        }
		const $xmlDocument = this.xmlDocument;

        Object.defineProperty(this,
            "serviceQuickFeatureMapping", {
                get() {
                    return [...$xmlDocument.querySelectorAll('service')].reduce(function(list, service) { 
                        var qfs = [...$xmlDocument.querySelectorAll(`quickchange service-addon[identifier="${service.getAttribute('uid')}"], quickchange service[identifier="${service.getAttribute('uid')}"]`)].map(el=>getParentsUntil(el,'quickchange')).filter(function(item,i,arr) { return arr.findIndex(it=>it.getAttribute('uid')==item.getAttribute('uid')) == i;});
                        if (qfs.length){
                            list[service.getAttribute('identifier')] = qfs;
                        }
                        return list;
                    },{});
                }
            }
        );
        
    }

    $prism_workspace_base.parseFileResultstoDocument = function(arrFileResults) {
        var dp = new DOMParser();
        var doc = dp.parseFromString("<workspace></workspace>","application/xml");
        var workspace = doc.querySelector('workspace');
        arrFileResults.forEach(function(fileResult){
            var docparsed = dp.parseFromString(fileResult.content,"application/xml");
            var fileResultElement = document.createElement('file-result');
            ['id','path','revisionAuthor','revisionComment','revisionDate','title'].forEach(function(attribute){
                fileResultElement.setAttribute(attribute,fileResult[attribute]);
                
            });
            fileResultElement.appendChild(docparsed.documentElement);    
            workspace.appendChild(fileResultElement);         
        });
        return doc;
    }

    $prism_workspace_base.parseXMLtoDocument = function(stringXML) {
        return (new DOMParser()).parseFromString(stringXML, "application/xml");
    }

    

    $prism_workspace_base.Service = function(stringXMLOrElement){
        this.xmlElement = typeof stringXMLOrElement == "string" ? $prism_workspace_base.parseXMLtoDocument(stringXMLOrElement) : stringXMLOrElement ;

        var modules = [...this.xmlElement.querySelectorAll('module')].map(f=>f.textContent);
        var modulesAndFields = Object.entries(kurmifieldlist[0].modules).filter(([moduleName,fields])=>{ return modules.some(mod=> moduleName.indexOf(mod) != -1); });





        this.getAnchorPoint = function() {
            var modifyVariable = this.xmlElement.querySelector('modifyVariable[logic="anchorPoint"]');
            var addVariable = this.xmlElement.querySelector('addVariable[logic="anchorPoint"]');
            var variable = this.xmlElement.querySelector('variable[logic="anchorPoint"]');
            return modifyVariable || addVariable || variable;
        }

        function scoreTag(element){
            //scores the rule or variable based on whether it's an add, modify, delete or builtin rule;
            if (!element) return -2;
            var tagName = element.tagName;
            
            if (/add/i.test(tagName)) {
                return 1;
            }
            else if (/modify/i.test(tagName)){
                return 2;
            }
            else if (/remove/i.test(tagName)){
                return -1;
            }
            else { 
                return 0;
            }
        }
        function weightTag(elementA,elementB){
            //favors the more recent rule or variable and returns it;
            if (scoreTag(elementA)>= scoreTag(elementB)){
                return elementA;
            }
            else {
                return elementB;
            }
        }

        this.getBlocks = function(){
            var blocks = [...this.xmlElement.querySelectorAll('variable:not([logic="field"]),addVariable:not([logic="field"]),modifyVariable:not([logic="field"])')];

            return Object.values(blocks.reduce(function(p,c){
                var id = c.getAttribute('id');
                p[id] = weightTag(p[id], c);
                return p;
            },[]));
        }
        this.getServiceVariables = function(){
            return [...this.xmlElement.querySelectorAll('variable[logic="field"], addVariable[logic="field"],modifyVariable[logic="field"]')];
        }
        this.getFields = function(){
            
            var fieldMap = {};
            this.xmlElement.querySelectorAll('rule, addRule').forEach(function (rule) {
              rule.querySelectorAll('fields field').forEach(function (field) {
                var mappedField = fieldMap[`${rule.querySelector('expression').textContent}:${field.textContent}`] = fieldMap[`${rule.querySelector('expression').textContent}:${field.textContent}`] || {};
                mappedField.rules = mappedField.rules || [];
                mappedField.rules.push(rule.getAttribute('uid'))
                mappedField.expression = rule.querySelector('expression').textContent;
                mappedField.name = field.textContent;
              });
      
            });
            window.fieldMap = fieldMap;
            return Object.values(fieldMap);
        }

        

        this.getRulesByBlockAndField = function(block,fieldName){
			var blockIdentifier = typeof block == 'string' ? block : variable.getAttribute('logic') == "field" ? 'self' : block.getAttribute('id');
			return [...this.xmlElement.querySelectorAll('rule,addRule,modifyRule')].filter(function(rule) { return rule.querySelector('expression').textContent == blockIdentifier && [...rule.querySelectorAll('fields field')].find(f=>f.textContent == fieldName);  });
		}

        this.getRuleById = function(uid){
            var rules = this.getRulesById(uid);
            return rules.modifyRule || rules.addRule || rules.rule;
        }
        this.getRulesById = function(uid) {
            var rule = this.xmlElement.querySelector(`rule[uid="${uid}"]`);
            var addRule = this.xmlElement.querySelector(`addRule[uid="${uid}"]`);
            var modifyRule = this.xmlElement.querySelector(`modifyRule[uid="${uid}"]`);
            var removeRule = this.xmlElement.querySelector(`removeRule[uid="${uid}"]`);
            return {addRule,modifyRule,removeRule,rule};
          }
        this.getFieldsOnVariable = function(variable) {
            if (variable.getAttribute('logic') != "field"){
                var componentType = variable.querySelector('componentType') && variable.querySelector('componentType').getAttribute('id');
                var specificModule = modulesAndFields.find(m=> new RegExp(componentType,'i').test(m[0]));
                if (!specificModule) {
                    console.error("couldn't find module by name");
                }
                var moduleFields = specificModule && specificModule[1].map(function(field){ return {name:field,expression:variable.getAttribute('id'),rules:[]} }) || [];
                var knownFields = this.getFields().filter(function (field) { return  variable.getAttribute('logic') == "field" ? field.expression == 'self' : field.expression == variable.getAttribute('id'); })

                return moduleFields.reduce(function(p,c){
                    if (!p.find(field=>field.name == c.name)){
                        p.push(c);
                    }
                    return p;
                },knownFields);
                
            }
            if (!variable) {
              console.error("no variable");
              return [];
            }
            return this.getFields().filter(function (field) { return  variable.getAttribute('logic') == "field" ? field.expression == 'self' : field.expression == variable.getAttribute('id'); });
          }
    }
}

module.exports = prism_workspace_base;

},{"./kurmi_fields":3,"./prism_promise":7}],"prism_workspace_files_addons":[function(require,module,exports){
const kurmifieldlist = require('./kurmi_fields');
const XMLReference = {
    xmlDocument: null,
    parseXmlDocument: function (xmlContent) {
        $this = this;
        $this.xmlDocument = typeof xmlContent == "string" ? (new DOMParser()).parseFromString(xmlContent, "application/xml") : xmlContent;
        $this.ready = true;

    }
}

function getParentsUntil(ele,tagName) {
    return ele.tagName == tagName ? ele : getParentsUntil(ele.parentNode, tagName);
}

module.exports.QuickFeatureReference = QuickFeatureReference = function (xmlContent) {
    $QuickFeatureReference = this;
    $QuickFeatureReference.__proto__ = XMLReference;
    $QuickFeatureReference.parseXmlDocument(xmlContent);
    var quickfeatureId = $QuickFeatureReference.id;
    
    Object.defineProperty($QuickFeatureReference, 'id', {
        get() {
            if ($QuickFeatureReference.xmlDocument.querySelector('quickchange id')) {
                return $QuickFeatureReference.xmlDocument.querySelector('quickchange id').textContent;
            }
            else {
                console.error('Didnt find quickchange id field', $WorkspaceFileReference.path);
                return null;
            }
        },
        set(val) {
            $QuickFeatureReference.xmlDocument.querySelector('quickchange id').textContent = val;
        }
    });

    Object.defineProperty($QuickFeatureReference, 'authorization', {
        get() {
            if ($QuickFeatureReference.xmlDocument.querySelector('authorization')) {
                return [...$QuickFeatureReference.xmlDocument.querySelector('authorization').children].map(c=>c.tagName);
            }
            else {
           
                return null;
            }
        },
        set(vals) {
            $QuickFeatureReference.xmlDocument.querySelector('authorization').replaceChildren(...vals.map(c=>$QuickFeatureReference.xmlDocument.createElement(c)));
        }
    });

    

}



module.exports.ServiceReference = function ServiceReference(xmlContent) {
    
    $ServiceReference = this;
    $ServiceReference.__proto__ = XMLReference;

    
    $ServiceReference.parseXmlDocument(xmlContent);
    var service = $ServiceReference.xmlDocument.querySelector('service');
    var serviceIdentifier = service && service.getAttribute('identifier');
    //const globalmapindex = JSON.stringify({ workspaceName, serviceIdentifier });
    //globalmap.set(globalmapindex, $ServiceReference);
    
    this.getModules = function () {
        var modules = [...this.xmlDocument.querySelectorAll('module')].map(f => f.textContent);
        var modulesAndFields = Object.entries(kurmifieldlist[0].modules).filter(([moduleName, fields]) => { return modules.some(mod => moduleName.indexOf(mod) != -1); });
        return modulesAndFields;
    }
    this.getAnchorPoint = function () {
        var modifyVariable = this.xmlDocument.querySelector('modifyVariable[logic="anchorPoint"]');
        var addVariable = this.xmlDocument.querySelector('addVariable[logic="anchorPoint"]');
        var variable = this.xmlDocument.querySelector('variable[logic="anchorPoint"]');
        return modifyVariable || addVariable || variable;
    }

    function scoreTag(element) {
        //scores the rule or variable based on whether it's an add, modify, delete or builtin rule;
        if (!element) return -2;
        var tagName = element.tagName;

        if (/add/i.test(tagName)) {
            return 1;
        }
        else if (/modify/i.test(tagName)) {
            return 2;
        }
        else if (/remove/i.test(tagName)) {
            return -1;
        }
        else {
            return 0;
        }
    }
    function weightTag(elementA, elementB) {
        //favors the more recent rule or variable and returns it;
        if (scoreTag(elementA) >= scoreTag(elementB)) {
            return elementA;
        }
        else {
            return elementB;
        }
    }

    this.getBlocks = function () {
        var blocks = [...this.xmlDocument.querySelectorAll('variable:not([logic="field"]),addVariable:not([logic="field"]),modifyVariable:not([logic="field"])')];

        return Object.values(blocks.reduce(function (p, c) {
            var id = c.getAttribute('id');
            p[id] = weightTag(p[id], c);
            return p;
        }, []));
    }
    this.getServiceVariables = function () {
        return [...this.xmlDocument.querySelectorAll('variable[logic="field"], addVariable[logic="field"],modifyVariable[logic="field"]')];
    }
    this.getFields = function () {

        var fieldMap = {};
        this.xmlDocument.querySelectorAll('rule, addRule').forEach(function (rule) {
            rule.querySelectorAll('fields field').forEach(function (field) {
                var mappedField = fieldMap[`${rule.querySelector('expression').textContent}:${field.textContent}`] = fieldMap[`${rule.querySelector('expression').textContent}:${field.textContent}`] || {};
                mappedField.rules = mappedField.rules || [];
                mappedField.rules.push(rule.getAttribute('uid'))
                mappedField.expression = rule.querySelector('expression').textContent;
                mappedField.name = field.textContent;
            });
            //globalmap.set(globalmapindex+`&rule='${rule.getAttribute('uid')}'`, rule);
        });
        window.fieldMap = fieldMap;
        return Object.values(fieldMap);
    }



    this.getRulesByBlockAndField = function (block, fieldName) {
        var blockIdentifier = typeof block == 'string' ? block : variable.getAttribute('logic') == "field" ? 'self' : block.getAttribute('id');
        return [...this.xmlDocument.querySelectorAll('rule,addRule,modifyRule')].filter(function (rule) { return rule.querySelector('expression').textContent == blockIdentifier && [...rule.querySelectorAll('fields field')].find(f => f.textContent == fieldName); });
    }

    this.getRuleById = function (uid) {
        var rules = this.getRulesById(uid);
        return rules.modifyRule || rules.addRule || rules.rule;
    }
    this.getRulesById = function (uid) {
        var rule = this.xmlDocument.querySelector(`rule[uid="${uid}"]`);
        var addRule = this.xmlDocument.querySelector(`addRule[uid="${uid}"]`);
        var modifyRule = this.xmlDocument.querySelector(`modifyRule[uid="${uid}"]`);
        var removeRule = this.xmlDocument.querySelector(`removeRule[uid="${uid}"]`);
        return { addRule, modifyRule, removeRule, rule };
    }
    this.getFieldsOnVariable = function (variable) {
        if (variable.getAttribute('logic') != "field") {
            var componentType = variable.querySelector('componentType') && variable.querySelector('componentType').getAttribute('id');
            var specificModule = this.getModules().find(m => new RegExp(componentType, 'i').test(m[0]));
            if (!specificModule) {
                console.error("couldn't find module by name");
            }
            var moduleFields = specificModule && specificModule[1].map(function (field) { return { name: field, expression: variable.getAttribute('id'), rules: [] } }) || [];
            var knownFields = this.getFields().filter(function (field) { return variable.getAttribute('logic') == "field" ? field.expression == 'self' : field.expression == variable.getAttribute('id'); })

            return moduleFields.reduce(function (p, c) {
                if (!p.find(field => field.name == c.name)) {
                    p.push(c);
                }
                return p;
            }, knownFields);

        }
        if (!variable) {
            console.error("no variable");
            return [];
        }
        return this.getFields().filter(function (field) { return variable.getAttribute('logic') == "field" ? field.expression == 'self' : field.expression == variable.getAttribute('id'); });
    }
}
},{"./kurmi_fields":3}],"prism_workspace_files":[function(require,module,exports){

const db = require('./prism_db');

const { ServiceReference, QuickFeatureReference} = require('./prism_workspace_files_addons');
module.exports = function prism_workspace_files($prism) {
    const $prism_workspace_base = $prism.workspace;
    $prism_workspace_base.ServiceReference = ServiceReference;
    $prism_workspace_base.QuickFeatureReference = QuickFeatureReference;

    $prism_workspace_base.getWorkspaceServicesAndQuickfeaturesDocument = function (workspaceName) {
        const $prism_workspace_base = this;
        return $prism_workspace_base.listWorkspaceFiles(workspaceName, /(service|quickfeature).xml/).then(function (listServices) {
            return listServices.map(function (filePath) {
                return $prism_workspace_base.getWorkspaceFile(workspaceName, filePath)
            })
        }).then(function (fileContentPromises) {
            return Promise.all(fileContentPromises).then(function (fileResults) { return $prism_workspace_base.parseFileResultstoDocument(fileResults); });
        });

    }

    $prism_workspace_base.Workspace = function (workspaceDocument) {
        this.xmlDocument = workspaceDocument;
        function getParentsUntil(ele, tagName) {
            return ele.tagName != tagName ? getParentsUntil(ele.parentElement, tagName) : ele;
        }
        const $xmlDocument = this.xmlDocument;

        Object.defineProperty(this,
            "serviceQuickFeatureMapping", {
            get() {
                return [...$xmlDocument.querySelectorAll('service')].reduce(function (list, service) {
                    var qfs = [...$xmlDocument.querySelectorAll(`quickchange service-addon[identifier="${service.getAttribute('uid')}"], quickchange service[identifier="${service.getAttribute('uid')}"]`)].map(el => getParentsUntil(el, 'quickchange')).filter(function (item, i, arr) { return arr.findIndex(it => it.getAttribute('uid') == item.getAttribute('uid')) == i; });
                    if (qfs.length) {
                        list[service.getAttribute('identifier')] = qfs;
                    }
                    return list;
                }, {});
            }
        }
        );

    }

    $prism_workspace_base.parseFileResultstoDocument = function (arrFileResults) {
        var dp = new DOMParser();
        var doc = dp.parseFromString("<workspace></workspace>", "application/xml");
        var workspace = doc.querySelector('workspace');
        arrFileResults.forEach(function (fileResult) {
            var docparsed = dp.parseFromString(fileResult.content, "application/xml");
            var fileResultElement = document.createElement('file-result');
            ['id', 'path', 'revisionAuthor', 'revisionComment', 'revisionDate', 'title'].forEach(function (attribute) {
                fileResultElement.setAttribute(attribute, fileResult[attribute]);

            });
            fileResultElement.appendChild(docparsed.documentElement);
            workspace.appendChild(fileResultElement);
        });
        return doc;
    }

    $prism_workspace_base.parseXMLtoDocument = function (stringXML) {
        return (new DOMParser()).parseFromString(stringXML, "application/xml");
    }

    function getParentsUntil(ele,tagName) {
        return ele.tagName == tagName ? ele : getParentsUntil(ele.parentNode, tagName);
    }

    $prism_workspace_base.WorkspaceReference = function (workspaceName, globalmap) {
        const $WorkspaceReference = this;
        this.workspaceName = workspaceName;
        this.workspacePath = null;
        const _files = [];
        const _fileList = [];
        this.listReady = false;

        globalmap = globalmap || new Map();

        var $WorkspaceReferenceResolver, $WorkspaceReferenceRejecter;

        this.onReady = new Promise(function (resolve, reject) {
            $WorkspaceReferenceResolver = resolve;
            $WorkspaceReferenceRejecter = reject;
        });



        this.getFileListAsync = function (nocache) {
            $WorkspaceReferenceResolver.listReady = false;

            return new Promise(function (resolve, reject) {

                if (nocache != true && _fileList.length) {
                    resolve(_fileList);
                }

                $prism_workspace_base.listWorkspaceFiles(workspaceName).then(function (listFiles) {
                    resolve(listFiles);
                }).catch(reject);

            }).then(function (listFiles) {
                _fileList.splice(0, _fileList.length, ...listFiles);
                _files.splice(0, _files.length);
                listFiles.forEach(function (filePath) { _files.push(new $WorkspaceReference.WorkspaceFileReference(filePath)) });
                $WorkspaceReference.listReady = true;
                $WorkspaceReferenceResolver(_files);
                return _files;
            });
            //.then($WorkspaceReferenceResolver).catch($WorkspaceReferenceRejecter);
        }

        Object.defineProperty(this, "files", {
            get: function () {
                if (_files.length == 0) {
                    $WorkspaceReference.getFileListAsync();
                }
                return _files;
            }
        });

        Object.defineProperty(this, "cachedFileList", {
            get() {
                var cachedWS = JSON.parse(localStorage.getItem(`workspace=${workspaceName}`));
                if (cachedWS && cachedWS.files) return cachedWS.files;
            },
            set(val) {
                
                localStorage.setItem(`workspace=${workspaceName}`, JSON.stringify({ files: val }));
            }
        })


        if ($WorkspaceReference.cachedFiles) {

            _files.splice(0, _files.length, ...$WorkspaceReference.cachedFiles);
            $WorkspaceReferenceResolver.listReady = true;
            $WorkspaceReferenceResolver();
        }

        $WorkspaceReference.WorkspaceFileReference = function (filePath) {

            const $WorkspaceFileReference = this;
            const globalmapindex = JSON.stringify({ workspaceName, filePath });
            globalmap.set(globalmapindex, $WorkspaceFileReference);
            $WorkspaceFileReference.loaded = -1;
            $WorkspaceFileReference._lastContent = null;
            $WorkspaceFileReference._fileResult = $WorkspaceFileReference.cachedFileResult;
            $WorkspaceFileReference.isSaved = false;
            $WorkspaceFileReference.path = filePath;
            $WorkspaceFileReference.title = '';
            $WorkspaceFileReference.error;
            $WorkspaceFileReference.success;
            var loadedResolve;
            var loadedReject;
            $WorkspaceFileReference.onLoad = new Promise(function (good, bad) {
                loadedResolve = good;
                loadedReject = bad;
            });

            $WorkspaceFileReference.getFileResultAsync = function () {
                $WorkspaceFileReference.loaded = 0;

                return $prism_workspace_base.getWorkspaceFile(workspaceName, filePath).then(function (fileResult) {
                    $WorkspaceFileReference._fileResult = fileResult;
                    $WorkspaceFileReference.loaded = 1;
                    $WorkspaceFileReference.cachedFileResult = fileResult;
                }).then(loadedResolve).catch(loadedReject).catch(function (e) {
                    $WorkspaceFileReference.loaded = -1;
                    $WorkspaceFileReference.error = e;
                });
            }



            Object.defineProperty($WorkspaceFileReference, 'fileResult', {
                get: function () {
                    if (!$WorkspaceFileReference._fileResult) {
                        $WorkspaceFileReference.getFileResultAsync();
                    }
                    return $WorkspaceFileReference._fileResult;
                },
                set: function (val) {
                    $WorkspaceFileReference._fileResult = val;
                    $WorkspaceFileReference.cachedFileResult = val;
                }
            });

            Object.defineProperty($WorkspaceFileReference, 'content', {
                get: function () {
                    return $WorkspaceFileReference._fileResult && $WorkspaceFileReference._fileResult.content;
                },
                set: function (val) {
                    $WorkspaceFileReference.lastContent = $WorkspaceFileReference._fileResult.content;
                    if (!$WorkspaceFileReference._fileResult) {
                        $WorkspaceFileReference._fileResult = { path: filePath };
                    }
                    $WorkspaceFileReference._fileResult.content = val;
                    $WorkspaceFileReference.isSaved = false;
                }
            });



            this.save = function () {
                $WorkspaceFileReference.isSaved = false;
                $WorkspaceFileReference.error = undefined;
                $WorkspaceFileReference.success = undefined;
                return $prism_workspace_base.addWorkspaceFile(workspaceName, $WorkspaceFileReference.fileResult).then(function (fileResult) {
                    $WorkspaceFileReference.isSaved = true;
                    $WorkspaceFileReference.success = "Saved on " + (new Date()).toString();

                }).catch(function (e) {
                    $WorkspaceFileReference.error = e;
                    console.error("Did not save workspace file ", e);
                })
            }



            Object.defineProperty($WorkspaceFileReference, 'cachedFileResult', {
                get() {
                    return localStorage.getItem(`workspace=${workspaceName},file-path=${filePath}`);
                },
                set(fileResult) {
                    //db.files.add({workspaceName,filePath,fileResult})
                    //localStorage.setItem(`workspace=${workspaceName},file-path=${filePath}`, JSON.stringify($WorkspaceFileReference._fileResult));
                }
            })

            if ($WorkspaceFileReference._fileResult) {

            }

            

            


            $WorkspaceFileReference.onLoad.then(function() { 
                if (/service.xml/i.test($WorkspaceFileReference.path)) {
                    const $ServiceReference = new ServiceReference($WorkspaceFileReference.content);
                }

                if (/quickfeature.xml/i.test($WorkspaceFileReference.path)) {
                    const $QuickFeatureReference = new QuickFeatureReference($WorkspaceFileReference.content);
                }
            });

            $WorkspaceFileReference.toService = function () {
                return $WorkspaceFileReference.loaded == 1 ? $ServiceReference : new Error('Not Loaded');
            }

            $WorkspaceFileReference.toQuickfeature = function () {
                return $WorkspaceFileReference.loaded == 1 ? $QuickFeatureReference : new Error('Not Loaded');
            }



        }

        var dp = new DOMParser();
        $WorkspaceReference.doc = dp.parseFromString("<workspace></workspace>", "application/xml");
        $WorkspaceReference.getWorkspaceServicesAndQuickfeaturesDocumentAsync = function () {
            const getFileQueue = [];
            const getFileQueueCounts = {};
            const $interval = setInterval(function () {
                getFileQueue.splice(0, 5).forEach(function (fileRef) {
                    getFileQueueCounts[fileRef.path] = getFileQueueCounts[fileRef.path] || 0;
                    getFileQueueCounts[fileRef.path]++;
                    if (fileRef.loaded == -1) {
                        fileRef.getFileResultAsync().catch(e => {
                            if (getFileQueueCounts[fileRef.path] < 3) {
                                getFileQueue.push(fileRef);
                            }
                            else {
                                console.error("Failed to load file", fileRef);
                            }
                        });
                    }

                });

            }, 2000);

            const workspace = $WorkspaceReference.doc.querySelector('workspace');

            $WorkspaceReference.onReady.then(function () {
                return $WorkspaceReference.files;
            }).then(function (files) {
                files.filter(f => /(service|quickfeature).xml$/.test(f.path)).forEach(function (fileRef) {
                    if (fileRef.loaded == -1) {
                        fileRef.getFileResultAsync();
                    }
                    fileRef.onLoad.then(function () {
                        console.log("parsing file", fileRef.path);
                        var fileResult = fileRef.fileResult;
                        var fileResultElement = document.createElement('file-result');
                        ['id', 'path', 'revisionAuthor', 'revisionComment', 'revisionDate', 'title'].forEach(function (attribute) {
                            fileResultElement.setAttribute(attribute, fileResult[attribute]);
                        });
                        if (/service.xml/i.test(fileRef.path)) {
                            var service = fileRef.toService();

                            if (!!service.xmlDocument.querySelector('service')) {
                                fileResultElement.appendChild(service.xmlDocument.querySelector('service'));
                            }
                            else {
                                fileResultElement.textContent = fileRef.content;
                                debugger;
                            }
                        }
                        else if (/quickfeature.xml/i.test(fileRef.path)) {
                            var quickchange = fileRef.toQuickfeature();

                            if (!!quickchange.xmlDocument.querySelector('quickchange')) {
                                fileResultElement.appendChild(quickchange.xmlDocument.querySelector('quickchange'));
                            }
                            else {
                                fileResultElement.textContent = fileRef.content;
                                debugger;
                            }
                        }
                        workspace.appendChild(fileResultElement);

                    });

                });

            });
            return workspace;
        }
        Object.defineProperty($WorkspaceReference,
            "serviceQuickFeatureMapping", {
            get() {
                return [...$WorkspaceReference.doc.querySelectorAll('service')].reduce(function (list, service) {
                    var qfs = [...$WorkspaceReference.doc.querySelectorAll(`quickchange service-addon[identifier="${service.getAttribute('uid')}"], quickchange service[identifier="${service.getAttribute('uid')}"]`)].map(el => getParentsUntil(el, 'quickchange')).filter(function (item, i, arr) { return arr.findIndex(it => it.getAttribute('uid') == item.getAttribute('uid')) == i; });
                    if (qfs.length) {
                        list[service.getAttribute('identifier')] = qfs;
                    }
                    return list;
                }, {});
            }
        });
    }
}
},{"./prism_db":5,"./prism_workspace_files_addons":"prism_workspace_files_addons"}],"prism":[function(require,module,exports){
const prism_base = require('./prism_base');


prism_base.moduleMap.executeKurmiAPI = function($prism) {
    var getExecuteKurmiAPI = require("./prism_executeKurmiAPI_async");
    return (new getExecuteKurmiAPI($prism))(fetch,FormData);
}

prism_base.moduleMap.reporting = function($prism){
    return (new (require('./prism_reporting_base'))($prism));
}

prism_base.moduleMap.connector = function($prism){
    return (new (require('./prism_connector_base'))($prism));
}

var $prism = prism_base.init();
require('./prism_api_wsdl')($prism);
require('./prism_workspace_files')($prism);

module.exports = $prism;

},{"./prism_api_wsdl":"prism_api_wsdl","./prism_base":"prism_base","./prism_connector_base":"prism_connector_base","./prism_executeKurmiAPI_async":"prism_executeKurmiAPI_async","./prism_reporting_base":"prism_reporting_base","./prism_workspace_files":"prism_workspace_files"}]},{},[]);
