const prism_sunrise_base = function($prism){
    $prism_sunrise_base = this;
    
    $prism_sunrise_base.sunriseGetTenantDocuments = function (tenantName,wantedDocuments) {
        return $prism.api.execute("sunriseGetTenantDocuments",{tenant:tenantName,wantedDocument:[].concat(wantedDocuments)}).then(function(results){
            return results.document.reduce(function(res,doc){
                res[doc["@documentType"]] = JSON.parse(doc["#text"]);    
                return res;
            },{});
        });
    }       
    $prism_sunrise_base.getTenantDocument = function(tenantName){
        return $prism_sunrise_base.sunriseGetTenantDocuments(tenantName,"tenant")["tenant"];
    }
    $prism_sunrise_base.getProvisioningDocument = function(tenantName){
        return $prism_sunrise_base.sunriseGetTenantDocuments(tenantName,"provisioning")["provisioning"];
    }
    $prism_sunrise_base.getSynchroDocument = function(tenantName){
        return $prism_sunrise_base.sunriseGetTenantDocuments(tenantName,"synchro")["synchro"];
    }
    $prism_sunrise_base.releaseLockForScenario = function(scenarioDBId){
        return $prism.api.execute("releaseLockForScenario",{scenarioDBId:scenarioDBId});
    }
    $prism_sunrise_base.terminateSunriseScenario = function(scenarioDBId){
        return $prism.api.execute("releaseLockForScenario",{scenarioDBId:scenarioDBId});
    }

    $prism_sunrise_base.createEngineeringRuleFromDiscoveredComponent = function(tenant,component,links) {
        component;
    }

}

module.exports = prism_sunrise_base;