var prismConstants = require('./prism-constants.js');
var prismworker = require('./prism-worker.js');

var _localCache = {};

var localCache = {
    get: function (table, key) {
        if (localStorage) {
            localStorage["prism"] = localStorage["prism"] || "{}";
            var kquerystorage = JSON.parse(localStorage["prism"]);
            var _table = kquerystorage;
        } else {
            var _table = (_localCache || {});
        }

        table.split('.').forEach(function (pathSplit, pathSplitI) {
            _table = _table[pathSplit] || {};
        });

        if (key) {
            return _table[key];
        }
        return _table;
    },
    store: function (table, key, value) {
        if (localStorage) {
            localStorage["prism"] = localStorage["prism"] || "{}";
            var kquerystorage = JSON.parse(localStorage["prism"]);

            var _table = kquerystorage;
            table.split('.').forEach(function (pathSplit, pathSplitI) {
                if (!_table[pathSplit]) {
                    _table[pathSplit] = {};
                }
                _table = _table[pathSplit];
            });
            _table[key] = value;

            localStorage["prism"] = JSON.stringify(kquerystorage);
        } else {
            _localCache[table] = _localCache[table] || {};
            _localCache[table][key] = value;
        }
    }
}


const prismbase = function() {
    this.initDate = null
    this.version = 1;
    this.modules = [];
    this.loadModule = function(name,$module){
        this[name] = new $module(this);
        this[name].prisminstance = this;
        
    }
}

module.exports = prismbase;

