const PENDING = "pending";
const FULFILLED = "fulfilled";
const REJECTED = "rejected";

function prismpromise(executor) {
    var state=PENDING;
    var value = null;
    var handlers=[];
    var catchers = [];
	var $this = this;
  	function updateValue(result) {
      	if (result && result.constructor.name != "prismpromise") {
           	value = result; 
        }
      	else if (result && result.constructor.name == "prismpromise"){
          	$this = result;
        }
      
    }
  
    function resolve(result) {
        if(state!==PENDING) return;

        state=FULFILLED;
        value = result;

        handlers.forEach(function(h){ 
          updateValue(h(value));
          
          return result;
		 });
    }

    function reject(error) {
        if(state!==PENDING)return;

        state=REJECTED;
        value=error;
        catchers.forEach(function(c) { return c(value);});
    }

    this.then = function(successCallback) {
        if(state === FULFILLED) {
          	updateValue(successCallback(value));
        }else {
            handlers.push(successCallback);
        }
        return $this;
    }

    this.catch = function(failureCallback) {
        if(state===REJECTED){
            failureCallback(value)
        } else {
            catchers.push(value);
        }
      	return $this;
    }
    executor(resolve,reject);
}

prismpromise.all = function(promises) {
    
    return new prismpromise(function (resolve, reject) {
        var cnt = 0;
        var res = [];
        if (promises.length === 0) {
            resolve(promises);
            return;
        }
        promises.forEach(function(prom,i){
            prom.then(function(val) {
                done(val, i);
            }).catch(function(err) {
                reject(err);
                
            });
        })
        
        
        function done(val, i) {
            res[i] = val;
            cnt++;
            if (promises.length === cnt) {
                resolve(res);
            }
        }
    });
}


const prism_query = function(selector) {
    this.selector = selector;
}


const prism_base = {
    version: 0,
    logParams: {},
    moduleMap: {
        "api": function($prism) { return new prism_api_base($prism); },
        "sql": function($prism) { return new prism_sql_base($prism); },
        "component": function($prism) { return new prism_component_base($prism); },
        "workspace": function($prism) { return new prism_workspace_base($prism); },
        "sunrise": function($prism) { return new prism_sunrise_base($prism); }
    },
    init: function() {
        var $prismparent = prism_query;
        var $prisminstance = {
            executeKurmiAPI: typeof executeKurmiAPI === "function" ? executeKurmiAPI : null
        };
        $prismparent.__proto__ = $prisminstance;

        
        prism_logger($prisminstance);
        var $prism_base = this;
        Object.keys($prism_base.moduleMap).forEach(function(moduleName){
            $prisminstance[moduleName] = $prism_base.moduleMap[moduleName]($prismparent);
        });
        return $prismparent;       
    }
}



const prism_quickfeature = function ($prism) {
    var $prism_quickfeature = this;


    $prism_quickfeature.askCredentialField = function (comp,credFields,idPrefixTranslation) {
      idPrefixTranslation = idPrefixTranslation || comp.getFullTitle();
      credFields = credFields || comp.fieldList.filter(function(fieldName) { return comp.isCredential(fieldName);   });
      credFields.forEach(function(fieldName) {
        askField({idPrefix:"cred_"+comp.dbid,idTranslation:idPrefixTranslation, component: comp, fieldName: fieldName});
      });
    }
  
    $prism_quickfeature.reduceChildServices = function (list,kuOrService) {
     (kuOrService.getChildServicesFields && kuOrService.getChildServicesFields() || []).forEach(function(csfn) {
         var childServices = kuOrService[csfn].getValue();
         [].concat(childServices).forEach(function(cs) {
  
             list.push(cs);
             reduceChildServices(list,cs);
         });
     });
     return list;
  }
  
  $prism_quickfeature.mapOfBlocks = function(comp){

    return prism_quickfeature.reduceChildServices([],comp).map(function(svc) { return [svc.getTypeName(), Object.keys(svc).filter(function(key) { return svc[key].logic == "Create"; }).map(function(key) { return askCredentialField(svc[key].getValue()); }) ]; });
  }

  

  $prism_quickfeature.QuickFeatureFormFromSchema = function (jsonSchema, propName) {
        var qfForm = this;
      var askParams = {};
      var askFunction = ask;
      askParams.id = propName || jsonSchema.title;
        askParams.idTranslation = jsonSchema.title || propName;
        var condition = (jsonSchema.dependencies || []).every(function(dep) { 
          if (typeof dep === "string") {
               return qfForm[dep]; 
          }
            else if (typeof dep === "object" && dep.id){
            return qfForm[dep.id] == dep.value;
          }
            return true;
      });
      if (jsonSchema.enums) {
          askParams.possibleValues = jsonSchema.enums;
      }
      if (jsonSchema.type == "object") {
            qfForm[propName] = {};
            var childcontext = qfForm[propName];
          Object.keys(jsonSchema.properties).forEach(function (propName) {
                
              $prism_quickfeature.QuickFeatureFormFromSchema.call(childcontext, jsonSchema.properties[propName], propName);
          });

      }
      else {
          var askType = "string";
          if (componentList.indexOf(jsonSchema.type) != -1) {
              askType = "component";
          }
          if (servicesList.indexOf(jsonSchema.type) != -1) {
              askType = "service";
          }

          if (condition != false || (typeof condition === "function" && condition() != false)) {
              switch (askType) {
                  case "string": {
                      break;
                  }
                case "number": {
                     break; 
                }
                  case "component": {
                      askFunction = askComponent;
                      askParams.componentType = jsonSchema.type;
                      break;
                  }
                  case "service": {
                      askFunction = askService;
                      askParams.serviceName = jsonSchema.type;
                      break;
                  }


              }
                qfForm["rules"] = qfForm["rules"] || 0;
                qfForm["rules"]++;
              
                
                try {
                    
                    var answer = askFunction(askParams);
                    forgetOnReload(askParams.id);
                    qfForm[askParams.id] = answer;
              }
                catch(e) {
                
              }
              
          }
      }

      return qfForm;
  }



}




//
const prism_api_base = function($prism) {

    var $prism_api_base = this;
    const configParams = {
        auth: {},
        kurmiHost: '..',
        apiPath: "/Kurmi/restAPI.do",
        substitutitionTenantOrTemplate: null,
        throughput: 50
    }
    
    const mergeParams = function(newConfigParams) {
        $configParams = newConfigParams || {
            auth: {},
            kurmiHost: '..',
            apiPath: "/Kurmi/restAPI.do",
            substitutitionTenantOrTemplate: null
        };
        Object.keys($configParams).reduce(function(p,c){
            p[c] = $configParams[c];
            return p;
        },configParams);
    }
    this.initParams = mergeParams;

    if (this.sessionStorage){
        mergeParams(JSON.parse(sessionStorage.getItem('configParams')));
    }

    this.kurmiRequestPath = function() {
        return configParams.kurmiHost + configParams.apiPath;
    }

    this.kurmiWSDLPath = function() {
        return this.kurmiRequestPath().replace("restAPI.do","APIService?wsdl");
    }

    this.configParams = configParams;

    function getAuth() {
        return JSON.parse(JSON.stringify(configParams.auth));
    }

    function setAuth(login,password,kurmiHost){
        configParams.auth.login = login;
        configParams.auth.password = password;
        if (this.window && this.this.window && this.window.hasOwnProperty('sessionStorage')){
            sessionStorage.setItem('configParams', JSON.stringify(configParams));
        }
        if (kurmiHost){
            configParams.kurmiHost = kurmiHost;
        }
    }

    this.execute = function(functionName,params,tenantDbidOrName){
        params.auth = getAuth();
        if (tenantDbidOrName){
            params.auth.substitutionTenantOrTemplate  = getSubstitutionTenantOrTemplate(tenantDbidOrName);
        }
        else if (configParams.substitutionTenantOrTemplate) {
            params.auth.substitutionTenantOrTemplate  = configParams.substitutionTenantOrTemplate ;
        }
        
        if (typeof Promise === "function"){
            return $prism.executeKurmiAPI(functionName,params);
        }

        return new prismpromise(function(resolve,reject){
            var rawresult = $prism.executeKurmiAPI(functionName,params);
            
            var result = typeof rawresult === "object" ? rawresult : JSON.parse(rawresult);
            if (result.status && result.status != "SUCCESS"){
                reject(result);
            }
            else {
                resolve(result);
            }

        });
        
        //return new prismworker([functionName,params],$prism.executeKurmiAPI);
    }

    this.execute.priority = function(){
        var args = Object.values(arguments).concat('priority');
        this.execute.apply(this,  args)
    }

    const connectedEvents = [function(p){
        return p.getTenants().then(function(res){
            p.tenants = res;
            //$prism.debug("loaded tenants",JSON.stringify(p.tenants));
        });
        
    }];
    this.addConnectedEvent = function(callback){
        connectedEvents.push(callback);
    }

    const proxypromise = typeof Promise === "function" ? Promise : prismpromise;

    this.isConnected = false;
    this.isSubstituted = false;
    this.login = setAuth;
    this.connect = function () {

        return this.execute("getKurmiVersion", {}).then(function (res) {
            if (res.errorDetail) throw res.errorDetail;
            
            return proxypromise.all(connectedEvents.map(function (cb) {
                return cb($prism_api_base);
            })).then(function (done) {

                $prism.debug("connect", true);
                $prism_api_base.isConnected = true;
                return res;
            });


        }).catch(function (e) {
            $prism.debug("connect", false, e);
            $prism_api_base.isConnected = false;
            throw e;
        });
    }

    var tenantList = null; 
    this.tenants = tenantList;
    this.getTenants = function() {
        return $prism.component.searchComponentList({type:"TENANT"});
    }

    function getSubstitutionTenantOrTemplate(tenantDbidOrName){
        var result = null;
        switch(typeof tenantDbidOrName){
            
            case typeof "tenantname": {
                try {
                    if (/SYSTEM/i.test(tenantDbidOrName)){
                        result = null;
                    }
                    //var numberDbid = tenantDbidOrName.replace(/\D/g,"");
                    //if (numberDbid && parseInt(numberDbid)){
                    //    result = getSubstituteTenantOrTemplate(parseInt(numberDbid));
                    //}
                    var tenant = $prism_api_base.tenants.filter(function(tenant){
                        return (new RegExp(tenantDbidOrName,"i")).test(tenant.identifier);
                    })[0];
                    result = {"@type": "TENANT", "@dbid": tenant["dbid"]};
                }
                catch(e){

                }

                break;
            }
            case typeof 1234: {

                break;
            }

        }
        $prism.debug("getSubstitutionTenantOrTemplate",JSON.stringify([tenantDbidOrName,result]));
        return result;
    }

    this.substitute = function(tenantDbidOrName){
        configParams.substitutionTenantOrTemplate = getSubstitutionTenantOrTemplate(tenantDbidOrName);
        console.info("Substituted to ",tenantDbidOrName);
        this.isSubstituted = configParams.substitutionTenantOrTemplate;
        return true;
    }

    this.exitSubtitution = function() {
        delete configParams.auth.substitutionTenantOrTemplate;
        configParams.substitutionTenantOrTemplate = null;
        console.info("Exited Substitution");
        this.isSubstituted = configParams.substitutionTenantOrTemplate;
        return true;
    }

}



function prism_worker(arrayOfParams,functionToExecute,desc){
    this.msg = "im a prism worker: "+ desc;
  var $prism_worker = this;
    this.thenCounter = 0;
  try {
      var args = arguments;
      var rawResponse = functionToExecute.apply(null,arrayOfParams);
        
      if ( typeof rawResponse === 'object' && typeof rawResponse.then === 'function' && typeof rawResponse.catch === 'function'){
          //return promise
          return rawResponse;
      }
      $prism_worker.response = JSON.parse(rawResponse);
  }
  catch(e){
      $prism_worker.response = {errorMessage: e};
  }
  $prism_worker.data = $prism_worker.response;
  
  this.then = function(cb){
        this.thenCounter++;
      if (cb && $prism_worker.response.status && $prism_worker.response.status == "SUCCESS"){
          $prism_worker.data = cb($prism_worker.data);
      }
        
      return $prism_worker;
  }
  this.catch = function(ecb){
      if (ecb && (!$prism_worker.response || !$prism_worker.response.status || $prism_worker.response.status != "SUCCESS")){
          console.error("catch statement",JSON.stringify([ecb]));
          ecb($prism_worker.data);
      }
      return $prism_worker;
  }
  
}






const prism_component_base = function($prism) {
    var $prism_component_base = this;
    if (typeof searchExistingComponent != "undefined") {
        this.searchExistingComponent = searchExistingComponent && function() { 
            var args = arguments;
            return new prismworker(Object.keys(args).map(function(k){return args[k]; }),searchExistingComponent);
            
        }
    }
    else {
        this.searchExistingComponent = function(searchFilter) { 
            return this.searchComponentList(searchFilter,0,false);
        }
    }
    if (typeof searchComponentList != "undefined"){
        this.searchComponentList = searchComponentList && function() { 
            var args = arguments; 
            return new prismworker(Object.keys(args).map(function(k){return args[k]; }),searchComponentList); 
        }
    }
    else {
        this.searchComponentList = function (filter, startingIndex, returnLimit) {
            var params = { filter: filter, detail: true };
            if (filter["type"]) {
                filter["@type"] = filter["type"];
                delete filter["type"];
            }
            if (startingIndex) params.startingIndex = startingIndex;
            if (returnLimit) params.returnLimit = returnLimit;
            var componentWorker = {}
            return $prism.api.execute("searchQuery", params).then(function(componentResult){
                componentWorker.totalNumberOfResults = componentResult.totalNumberOfResults;
                if (componentResult && componentResult.componentDetail && componentResult.componentDetail[0]) {
                    return componentResult.componentDetail.map(function ($component) {
                        var _component = { "dbid": $component["@dbid"], "type": $component["@type"] };
                        if ($component.fields && $component.fields.field) {
                            $prism_component_base.fieldDeserialize($component.fields.field, _component);
                        }
                        if ($component.fields && $component.fields.fieldMult) {
                            //$this.api.component.fieldDeserialize($component.fields.field, _component);
                        }
                        return _component;
                    });
                }
                else if (componentResult && !componentResult.componentDetail) {
                    $prism.warn("[kQuery] searchQuery returned no components.", "componentResult.totalNumberOfResults: ", JSON.stringify(componentResult.totalNumberOfResults));
                    return null;
                }
                else {
                    return undefined;
                }
            });
        }

    }

    $prism_component_base.setConnectorVirtual = function (tenant, equipmentId, virtual) {
        return $prism.api.execute("searchConfig", { query: "/*/*[@equipmentId=" + equipmentId + "]" }, tenant).then(function(searchConfigQuery){
            if (!searchConfigQuery.path) {
                throw "[kQuery] No equipment found with equipmentId \"" + equipmentId + "\"" + (tenant ? " on tenant \"" + tenant + "\"" : "");
            }
            var query = virtual === true ? "*[not(boolean(@virtual))]" : "*[boolean(@virtual)]";
            return $prism.api.execute("searchConfig", { path: searchConfigQuery.path[0], query: query }, tenant).then(function(searchConfig){
                var setConfig = { configVersion: searchConfig.configVersion };

                setConfig.update = [searchConfigQuery.path].concat(searchConfig.path).map(function (path) {
                    return { "@path": path, key: { "@id": "virtual", "#text": virtual === true ? "true" : "false" } };
                });
                return $prism.api.execute("setConfig", setConfig, tenant);
            });
        });
    }
    $prism_component_base.processConfigNode = function processNode(_node, mergeWith) {
        if (_node.node) {
            if (/^\d+$/.test(_node.node[0]["@id"])) {
                mergeWith = _node.node.map(function (__node) {
                    return processNode(__node, {});
                });
            }
            else {
                _node.node.forEach(function (__node) {
                    mergeWith[__node["@id"]] = processNode(__node, {});
                });
            }
        }
        if (_node.key) {
            return _node.key.reduce(function (p, c) {
                p[c["@id"]] = c["#text"];
                return p;
            }, mergeWith);
        }
        else {
            return mergeWith;
        }
    }
    
    $prism_component_base.getEquipmentConfiguration = function (equipmentId, tenant) {
        var params = { query: "/*/*[@equipmentId=" + equipmentId + "]" };
        return $prism.api.execute("searchConfig", params, tenant).then(function (searchConfigQuery) {
            if (!searchConfigQuery.path) {
                $prism.error(JSON.stringify([params, searchConfigQuery]));
                throw "[prism] No equipment found with equipmentId \"" + equipmentId + "\"" + (tenant ? " on tenant \"" + tenant + "\"" : "");
            } else {
                return $prism.api.execute("getConfigTree", { path: searchConfigQuery.path[0], query: "[@equipmentId=" + equipmentId + "]" }, tenant).then(function (equipmentConfigurationRequest) {
                    return equipmentConfigurationRequest.configTree && $prism_component_base.processConfigNode(equipmentConfigurationRequest.configTree, {});
                });
            }
        });
    }

    $prism_component_base.fieldSerialize = function (fieldObject) {
        return Object.keys(fieldObject).map(function (key) {
            return {
                "@name": key,
                "#text": fieldObject[key]
            };
        });
    }

    $prism_component_base.fieldDeserialize = function (fieldsetList, baseObject, bFavorTextValueOverTechnicalValue) {
        return fieldsetList.reduce(function (p, fieldset, i, a) {
            if (bFavorTextValueOverTechnicalValue){
                p[fieldset["@name"]] = fieldset["#text"] || fieldset["@technicalValue"];
            }
            else {
                p[fieldset["@name"]] = fieldset["@technicalValue"] || fieldset["#text"];
            }
            
            return p;
        }, baseObject || {});
    }

    
}


const prism_sql_base = function ($prism) {
    var $prism_sql_base = this;

    $prism_sql_base.read = function (sqlScript) {
        return $prism.api.execute("executeReadSQL", { sqlScript: sqlScript }).then(function (sqlResult) {
            return (sqlResult.sqlLineResult || []).map(function(row) { return row.item;});
        });
    }
    $prism_sql_base.getObject = function (columns, table) {
        return $prism_sql_base.read("select " + columns.join(', ') + " from " + table + ";").then(function (sqlResult) {
            return sqlResult.reduce(function (p, c) {
                var obj = columns.reduce(function (agg, colName, colIndex) { agg[colName] = c[colIndex]; return agg; }, {});
                p.push(obj);
                return p;
            }, []);
        });
    }

    $prism_sql_base.listComponentTypes = function() {
        return $prism_sql_base.getObject(["name"],"componentkind").then(function(results){
            return results.reduce(function(p,c){
                return [c.name].concat(p);
            },[]);
        });
    }

    $prism_sql_base.listServiceTypes = function() {
        return $prism_sql_base.getObject(["serviceid"],"service_type").then(function(results){
            return results.reduce(function(p,c){
                return [c.serviceid].concat(p);
            },[]);
        });
    }


}


const prism_sunrise_base = function($prism){
    $prism_sunrise_base = this;
    
    $prism_sunrise_base.sunriseGetTenantDocuments = function (tenantName,wantedDocuments) {
        return $prism.api.execute("sunriseGetTenantDocuments",{tenant:tenantName,wantedDocument:[].concat(wantedDocuments)}).then(function(results){
            return results.document.reduce(function(res,doc){
                res[doc["@documentType"]] = JSON.parse(doc["#text"]);    
                return res;
            },{});
        });
    }       
    $prism_sunrise_base.getTenantDocument = function(tenantName){
        return $prism_sunrise_base.sunriseGetTenantDocuments(tenantName,"tenant")["tenant"];
    }
    $prism_sunrise_base.getProvisioningDocument = function(tenantName){
        return $prism_sunrise_base.sunriseGetTenantDocuments(tenantName,"provisioning")["provisioning"];
    }
    $prism_sunrise_base.getSynchroDocument = function(tenantName){
        return $prism_sunrise_base.sunriseGetTenantDocuments(tenantName,"synchro")["synchro"];
    }
    $prism_sunrise_base.releaseLockForScenario = function(scenarioDBId){
        return $prism.api.execute("releaseLockForScenario",{scenarioDBId:scenarioDBId});
    }
    $prism_sunrise_base.terminateSunriseScenario = function(scenarioDBId){
        return $prism.api.execute("releaseLockForScenario",{scenarioDBId:scenarioDBId});
    }

    $prism_sunrise_base.createEngineeringRuleFromDiscoveredComponent = function(tenant,component,links) {
        component;
    }

}




const prism_workspace_base = function ($prism) {
    var $prism_workspace_base = this;

    const proxypromise = typeof Promise === "function" ? Promise : prismpromise;

    $prism_workspace_base.getDetailList = function () {
        return $prism.sql.getObject(["cfg_branch_id", "name","frozen","location","parent","revision"],"cfg_branch").then(function (listWorkspaceSqlResult) {
            return listWorkspaceSqlResult;
        });
    };

    $prism_workspace_base.getList = function () {
        function flattenWorkspaceList(masterList, currentWorkspace) {
            masterList.push(currentWorkspace.name);
            (currentWorkspace.childs || []).reduce(flattenWorkspaceList, masterList);
            return masterList;
        }
        return $prism.api.execute("listWorkspaces", {}).then(function (listWorkspaceResult) {
            return listWorkspaceResult && listWorkspaceResult.workspaces && JSON.parse(listWorkspaceResult.workspaces).reduce(flattenWorkspaceList, []);
        });
    };
    $prism_workspace_base.getDetailList = function(){
        return $prism.sql.getObject(["name","location","frozen","cfg_branch_id"],'cfg_branch');
    }

    $prism_workspace_base.createBranch = function createBranch(branchName,parentName){
        return $prism_workspace_base.getDetailList().then(function(list){
            var _workspace = list.filter(function(ws){
                return ws.name == parentName;
            })[0];
            if (_workspace){
                return $prism.api.execute('addWorkspace',{name:branchName,parent:parentName});
            }
            else {
                return $prism.api.execute('addWorkspace',{name:branchName});
            }
        });
    }

    $prism_workspace_base.listWorkspaceFiles = function (workspaceName, fileType) {
        return $prism.api.execute("listWorkspaceFiles", { workspace: workspaceName }).then(function (listWorkspaceFilesResult) {
            var files = JSON.parse(listWorkspaceFilesResult.files).map(function (file) { return file.path });
            if (fileType) {
                var rg = typeof fileType == typeof /.*/ ? fileType : new RegExp(fileType, "i");
                files = files.filter(function (f) { return rg.test(f); });
            }
            return files.sort();
        });
    },
        $prism_workspace_base.getWorkspaceFile = function (workspaceName, filePath) {
            return $prism.api.execute("getWorkspaceFile", { workspace: workspaceName, path: filePath }).then(function (getWorkspaceFileResult) {
                return getWorkspaceFileResult;
            });
        }

    $prism_workspace_base.listOfMailFileRelations = function (workspaceName) {
        return $prism.workspace.listWorkspaceFiles(workspaceName, "mail.js").then(function (mailfilenames) {
            var mailFileResultsPromise = prismpromise.all(mailfilenames.map(function (name) {
                return $prism.workspace.getWorkspaceFile(workspaceName, name);
            }));
            return mailFileResultsPromise.then(function (mailFileResults) {
                var mailFileIncludes = mailFileResults.map(function (item) { return { path: item.path, includes: (item.content.match(/(?=\#include )(.*\.mail.js)/g)  || []).map(function(mtch) { return mtch.replace("#include ","");   }) }; });
                return mailFileIncludes.reduce(function (p, c, i, a) {
                    c.mentions = a.filter(function (mfc) { return mfc.includes.indexOf(c.path) != -1; });
                    p.push(c);
                    return p;
                }, []);
            });
        });
    }
    $prism_workspace_base.mapOfMailfiles = function (workspaceName) {
        return $prism_workspace_base.listOfMailFileRelations(workspaceName).then(function (mailFileIncludes) {
            var map = {};

            function reduceMailFiles($item, list) {
                var item = JSON.parse(JSON.stringify($item));
                if (item.includes) {
                    item.includes.forEach(function (includedItemPath) {

                        var includedItem = list.filter(function(f) { return f.path == includedItemPath;})[0];
                        if (includedItem) {
                            item[includedItemPath] = reduceMailFiles(includedItem, list);
                        }
                    });

                }
                if (item.mentions && item.mentions.length == 0) {
                    map[item.path] = item;


                }
                delete item.path;
                delete item.includes;
                delete item.mentions;

                return item;
            }
            var mailTemplates = {};
            Object.keys(mailTemplates).reduce(function(p,k) {
                var v = mailTemplates[k];
              	p[k] = {};
                p[k][v] = tp1[v];
                
                return p;
            },{});

            mailFileIncludes.forEach(function (item, i, arr) { reduceMailFiles(item, arr); });
            return map;
        });
    }
    $prism_workspace_base.mapConfiguredMailFiles = function(optionalTenantName){
        //prism.api.getConfigTree({path:"/MailManagement"},"trainingpod1").then(function(res) { return prism.component.processConfigNode(res.configTree,{}); });
        
        function _mapConfiguredMailFiles(mapOfMailfiles,configuration_MailManagement){
            var templateKeys = Object.keys(configuration_MailManagement || {}).filter(function(key){ return /^template/i.test(key); });
            
            return templateKeys.reduce(function(p,c){
                p[c] = {};
                var templateFilePath = configuration_MailManagement[c];
                p[c][templateFilePath] = mapOfMailfiles[templateFilePath] || {};


                return p;
            },{});

        }
        if (!optionalTenantName){
            return $prism.api.execute("getConfigTree",{path:"/MailManagement"}).then(function(res) { return prism.component.processConfigNode(res.configTree,{}); }).then(function(configuration_MailModule){
                return $prism_workspace_base.getSystemWorkspaceName().then(function(workspaceName){
                    return $prism_workspace_base.mapOfMailfiles(workspaceName).then(function(momf){
                        return _mapConfiguredMailFiles(momf,configuration_MailModule);
                    });
                })
            });
            
        }
        else {
            return $prism.api.execute("getConfigTree",{path:"/MailManagement"},optionalTenantName).then(function(res) { 
                return $prism.component.processConfigNode(res.configTree,{}); 
            }).then(function(configuration_MailModule){
                return $prism_workspace_base.getTenantWorkspaceName(optionalTenantName).then(function(workspaceName){
                    return $prism_workspace_base.mapOfMailfiles(workspaceName).then(function(momf){
                        return _mapConfiguredMailFiles(momf,configuration_MailModule);
                    });
                });
            });
        }
    }

    $prism_workspace_base.getTenantWorkspaceName = function(tenantIdentifier){
        return $prism.api.getTenants().then(function(tenants){
            return tenants.filter(function(t){ return t.identifier == tenantIdentifier; })[0].configBranch.split('/').slice(-2,-1)[0];
        });
    }

    $prism_workspace_base.getSystemWorkspaceName = function(){
        return $prism.sql.read("select name from cfg_branch join cfg_main_branch on cfg_main_branch.cfg_branch_id = cfg_branch.cfg_branch_id;").then(function(result) {
            return result[0][0];
            });
    }

    $prism_workspace_base.getWorkspaceId = function(workspaceName){
        return $prism.sql.read("select cfg_branch_id from cfg_branch where name like '" + workspaceName + "';").then(function(result) {
            return result[0][0];
            });
    }

    $prism_workspace_base.getWorkspaceServicesAndQuickfeaturesDocument = function(workspaceName) {
        return $prism_workspace_base.listWorkspaceFiles(workspaceName,/(service|quickfeature).xml/).then(function(listServices){
            return listServices.map(function(filePath){
                return $prism_workspace_base.getWorkspaceFile(workspaceName,filePath)
            })
        }).then(function(fileContentPromises){
            return Promise.all(fileContentPromises).then(function(fileResults) { return $prism_workspace_base.parseFileResultstoDocument(fileResults); });
        });

    }

    $prism_workspace_base.Workspace = function(workspaceDocument){
        this.xmlDocument = workspaceDocument;
        function getParentsUntil(ele,tagName) {
            return ele.tagName != tagName ? getParentsUntil(ele.parentElement,tagName) : ele;
        }
		const $xmlDocument = this.xmlDocument;

        Object.defineProperty(this,
            "serviceQuickFeatureMapping", {
                get() {
                    return [...$xmlDocument.querySelectorAll('service')].reduce(function(list, service) { 
                        var qfs = [...$xmlDocument.querySelectorAll(`quickchange service-addon[identifier="${service.getAttribute('uid')}"], quickchange service[identifier="${service.getAttribute('uid')}"]`)].map(el=>getParentsUntil(el,'quickchange')).filter(function(item,i,arr) { return arr.findIndex(it=>it.getAttribute('uid')==item.getAttribute('uid')) == i;});
                        if (qfs.length){
                            list[service.getAttribute('identifier')] = qfs;
                        }
                        return list;
                    },{});
                }
            }
        );
        
    }

    $prism_workspace_base.parseFileResultstoDocument = function(arrFileResults) {
        var dp = new DOMParser();
        var doc = dp.parseFromString("<workspace></workspace>","application/xml");
        var workspace = doc.querySelector('workspace');
        arrFileResults.forEach(function(fileResult){
            var docparsed = dp.parseFromString(fileResult.content,"application/xml");
            var fileResultElement = document.createElement('file-result');
            ['id','path','revisionAuthor','revisionComment','revisionDate','title'].forEach(function(attribute){
                fileResultElement.setAttribute(attribute,fileResult[attribute]);
                
            });
            fileResultElement.appendChild(docparsed.documentElement);    
            workspace.appendChild(fileResultElement);         
        });
        return doc;
    }

    $prism_workspace_base.parseXMLtoDocument = function(stringXML) {
        return (new DOMParser()).parseFromString(stringXML, "application/xml");
    }

    

    $prism_workspace_base.Service = function(stringXMLOrElement){
        this.xmlElement = typeof stringXMLOrElement == "string" ? $prism_workspace_base.parseXMLtoDocument(stringXMLOrElement) : stringXMLOrElement ;

        var modules = [...this.xmlElement.querySelectorAll('module')].map(f=>f.textContent);
        var modulesAndFields = Object.entries(kurmifieldlist[0].modules).filter(([moduleName,fields])=>{ return modules.some(mod=> moduleName.indexOf(mod) != -1); });





        this.getAnchorPoint = function() {
            var modifyVariable = this.xmlElement.querySelector('modifyVariable[logic="anchorPoint"]');
            var addVariable = this.xmlElement.querySelector('addVariable[logic="anchorPoint"]');
            var variable = this.xmlElement.querySelector('variable[logic="anchorPoint"]');
            return modifyVariable || addVariable || variable;
        }

        function scoreTag(element){
            //scores the rule or variable based on whether it's an add, modify, delete or builtin rule;
            if (!element) return -2;
            var tagName = element.tagName;
            
            if (/add/i.test(tagName)) {
                return 1;
            }
            else if (/modify/i.test(tagName)){
                return 2;
            }
            else if (/remove/i.test(tagName)){
                return -1;
            }
            else { 
                return 0;
            }
        }
        function weightTag(elementA,elementB){
            //favors the more recent rule or variable and returns it;
            if (scoreTag(elementA)>= scoreTag(elementB)){
                return elementA;
            }
            else {
                return elementB;
            }
        }

        this.getBlocks = function(){
            var blocks = [...this.xmlElement.querySelectorAll('variable:not([logic="field"]),addVariable:not([logic="field"]),modifyVariable:not([logic="field"])')];

            return Object.values(blocks.reduce(function(p,c){
                var id = c.getAttribute('id');
                p[id] = weightTag(p[id], c);
                return p;
            },[]));
        }
        this.getServiceVariables = function(){
            return [...this.xmlElement.querySelectorAll('variable[logic="field"], addVariable[logic="field"],modifyVariable[logic="field"]')];
        }
        this.getFields = function(){
            
            var fieldMap = {};
            this.xmlElement.querySelectorAll('rule, addRule').forEach(function (rule) {
              rule.querySelectorAll('fields field').forEach(function (field) {
                var mappedField = fieldMap[`${rule.querySelector('expression').textContent}:${field.textContent}`] = fieldMap[`${rule.querySelector('expression').textContent}:${field.textContent}`] || {};
                mappedField.rules = mappedField.rules || [];
                mappedField.rules.push(rule.getAttribute('uid'))
                mappedField.expression = rule.querySelector('expression').textContent;
                mappedField.name = field.textContent;
              });
      
            });
            window.fieldMap = fieldMap;
            return Object.values(fieldMap);
        }

        

        this.getRulesByBlockAndField = function(block,fieldName){
			var blockIdentifier = typeof block == 'string' ? block : variable.getAttribute('logic') == "field" ? 'self' : block.getAttribute('id');
			return [...this.xmlElement.querySelectorAll('rule,addRule,modifyRule')].filter(function(rule) { return rule.querySelector('expression').textContent == blockIdentifier && [...rule.querySelectorAll('fields field')].find(f=>f.textContent == fieldName);  });
		}

        this.getRuleById = function(uid){
            var rules = this.getRulesById(uid);
            return rules.modifyRule || rules.addRule || rules.rule;
        }
        this.getRulesById = function(uid) {
            var rule = this.xmlElement.querySelector(`rule[uid="${uid}"]`);
            var addRule = this.xmlElement.querySelector(`addRule[uid="${uid}"]`);
            var modifyRule = this.xmlElement.querySelector(`modifyRule[uid="${uid}"]`);
            var removeRule = this.xmlElement.querySelector(`removeRule[uid="${uid}"]`);
            return {addRule,modifyRule,removeRule,rule};
          }
        this.getFieldsOnVariable = function(variable) {
            if (variable.getAttribute('logic') != "field"){
                var componentType = variable.querySelector('componentType') && variable.querySelector('componentType').getAttribute('id');
                var specificModule = modulesAndFields.find(m=> new RegExp(componentType,'i').test(m[0]));
                if (!specificModule) {
                    console.error("couldn't find module by name");
                }
                var moduleFields = specificModule && specificModule[1].map(function(field){ return {name:field,expression:variable.getAttribute('id'),rules:[]} }) || [];
                var knownFields = this.getFields().filter(function (field) { return  variable.getAttribute('logic') == "field" ? field.expression == 'self' : field.expression == variable.getAttribute('id'); })

                return moduleFields.reduce(function(p,c){
                    if (!p.find(field=>field.name == c.name)){
                        p.push(c);
                    }
                    return p;
                },knownFields);
                
            }
            if (!variable) {
              console.error("no variable");
              return [];
            }
            return this.getFields().filter(function (field) { return  variable.getAttribute('logic') == "field" ? field.expression == 'self' : field.expression == variable.getAttribute('id'); });
          }
    }
}



function prism_logger($prism) {
    $prism.logParams = $prism.logParams || {};
    $prism.logParams.title = $prism.logParams.title || "prism";
    ["log","debug","error","warn","info"].forEach(function(key){
        $prism[key] = function() {
            var $arguments = arguments;
            var arrayOfArgs = Object.keys($arguments).map(function(a){ return $arguments[a];});
            if ($prism.logParams.concatWithChar) {
                arrayOfArgs = [arrayOfArgs.join($prism.logParams.concatWithChar)]
            }
            arrayOfArgs.splice(0,0,"["+$prism.logParams.title+"]");
            
            console[key].apply(null,arrayOfArgs);
        }

    })
}



prism_base.moduleMap.quickfeature = function($prism) {
    return (new prism_quickfeature($prism));
}

function prism_module(doConnect) {

    var $prism = prism_base.init();
    if (doConnect){
        $prism.api.connect();
    }
    return $prism;
}


