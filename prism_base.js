const prism_query = function(selector) {
    this.selector = selector;
}


const prism_base = {
    version: 0,
    logParams: {},
    moduleMap: {
        "api": function($prism) { return new (require("./prism_api_base"))($prism); },
        "sql": function($prism) { return new (require("./prism_sql_base"))($prism); },
        "component": function($prism) { return new (require("./prism_component_base"))($prism); },
        "workspace": function($prism) { return new (require("./prism_workspace_base"))($prism); },
        "sunrise": function($prism) { return new (require("./prism_sunrise_base"))($prism); }
    },
    init: function() {
        var $prismparent = prism_query;
        var $prisminstance = {
            executeKurmiAPI: typeof executeKurmiAPI === "function" ? executeKurmiAPI : null
        };
        $prismparent.__proto__ = $prisminstance;

        const prism_logger = require("./prism_logger");
        prism_logger($prisminstance);
        var $prism_base = this;
        Object.keys($prism_base.moduleMap).forEach(function(moduleName){
            $prisminstance[moduleName] = $prism_base.moduleMap[moduleName]($prismparent);
        });
        return $prismparent;       
    }
}

module.exports = prism_base;
