module.exports = function ($prism) {
    return function (functionName, params) {
		params = params || {};
		var xhttp = new XMLHttpRequest();
		xhttp.open("POST", $prism.api.kurmiRequestPath(), false);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		var formData = new FormData();
		formData.append("jsonData", JSON.stringify({
			"function": functionName,
			"parameters": params
		}));
		formData.append("action", "apiKurmi");
		xhttp.send(new URLSearchParams(formData).toString());
		return xhttp.responseText;
	};
};

/*
module.exports = function (functionName, params) {
    params = params || {};
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", $prism.api.config.restAPIpath, false);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    var formData = new FormData();
    formData.append("jsonData", JSON.stringify({
        "function": functionName,
        "parameters": params
    }));
    formData.append("action", "apiKurmi");
    xhttp.send(new URLSearchParams(formData).toString());
    return xhttp.responseText;
};
*/