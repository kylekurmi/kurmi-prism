const FetchThrottler = require('./prism_fetch_throttler');


module.exports = function ($prism) {
    return function (fnFetch, FormData) {
        const fetch = new FetchThrottler(fnFetch,$prism.api.configParams.throughput,1000);

        

        const executeKurmiAPI = function executeKurmiAPI(functionName, params) {
            params = params || {};
            var $this = this;
            if ($prism.api.configParams.throughput){
                fetch.throughput = $prism.api.configParams.throughput;
            }

            var $promise = new Promise(function (resolve, reject) {
                try {
                    var formData = new FormData();
                    formData.append("jsonData", JSON.stringify({
                        "function": functionName,
                        "parameters": params
                    }));
                    formData.append("action", "apiKurmi");
                    fetch($prism.api.kurmiRequestPath(), {
                        method: 'POST',
                        //credentials: "include",
                        //mode: 'no-cors',
                        body: formData
                    }).then(function (resp) {
                        return resp.json();
                    }).then(function (jResp) {
                        if (jResp.status && jResp.errorDetail) {
                            reject(jResp);
                        }
                        else if (jResp.status && jResp.status == "SUCCESS") {
                            resolve(jResp);
                        } else if (jResp.status && jResp.status != "SUCCESS") {
                            reject(jResp);
                        } else {
                            throw "[prism][unexpected json result] " + JSON.stringify(jResp);
                        }

                    }).catch(function (e) {
                        reject(e);
                    });

                } catch (e) {
                    reject({
                        status: "FAIL",
                        message: e
                    });
                }
            });

            return $promise;
        }



        

        return executeKurmiAPI;
    };
};
