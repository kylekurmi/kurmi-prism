const prism_quickfeature = function ($prism) {
    var $prism_quickfeature = this;


    $prism_quickfeature.askCredentialField = function (comp,credFields,idPrefixTranslation) {
      idPrefixTranslation = idPrefixTranslation || comp.getFullTitle();
      credFields = credFields || comp.fieldList.filter(function(fieldName) { return comp.isCredential(fieldName);   });
      credFields.forEach(function(fieldName) {
        askField({idPrefix:"cred_"+comp.dbid,idTranslation:idPrefixTranslation, component: comp, fieldName: fieldName});
      });
    }
  
    $prism_quickfeature.reduceChildServices = function (list,kuOrService) {
     (kuOrService.getChildServicesFields && kuOrService.getChildServicesFields() || []).forEach(function(csfn) {
         var childServices = kuOrService[csfn].getValue();
         [].concat(childServices).forEach(function(cs) {
  
             list.push(cs);
             reduceChildServices(list,cs);
         });
     });
     return list;
  }
  
  $prism_quickfeature.mapOfBlocks = function(comp){

    return prism_quickfeature.reduceChildServices([],comp).map(function(svc) { return [svc.getTypeName(), Object.keys(svc).filter(function(key) { return svc[key].logic == "Create"; }).map(function(key) { return askCredentialField(svc[key].getValue()); }) ]; });
  }

  

  $prism_quickfeature.QuickFeatureFormFromSchema = function (jsonSchema, propName) {
        var qfForm = this;
      var askParams = {};
      var askFunction = ask;
      askParams.id = propName || jsonSchema.title;
        askParams.idTranslation = jsonSchema.title || propName;
        var condition = (jsonSchema.dependencies || []).every(function(dep) { 
          if (typeof dep === "string") {
               return qfForm[dep]; 
          }
            else if (typeof dep === "object" && dep.id){
            return qfForm[dep.id] == dep.value;
          }
            return true;
      });
      if (jsonSchema.enums) {
          askParams.possibleValues = jsonSchema.enums;
      }
      if (jsonSchema.type == "object") {
            qfForm[propName] = {};
            var childcontext = qfForm[propName];
          Object.keys(jsonSchema.properties).forEach(function (propName) {
                
              $prism_quickfeature.QuickFeatureFormFromSchema.call(childcontext, jsonSchema.properties[propName], propName);
          });

      }
      else {
          var askType = "string";
          if (componentList.indexOf(jsonSchema.type) != -1) {
              askType = "component";
          }
          if (servicesList.indexOf(jsonSchema.type) != -1) {
              askType = "service";
          }

          if (condition != false || (typeof condition === "function" && condition() != false)) {
              switch (askType) {
                  case "string": {
                      break;
                  }
                case "number": {
                     break; 
                }
                  case "component": {
                      askFunction = askComponent;
                      askParams.componentType = jsonSchema.type;
                      break;
                  }
                  case "service": {
                      askFunction = askService;
                      askParams.serviceName = jsonSchema.type;
                      break;
                  }


              }
                qfForm["rules"] = qfForm["rules"] || 0;
                qfForm["rules"]++;
              
                
                try {
                    
                    var answer = askFunction(askParams);
                    forgetOnReload(askParams.id);
                    qfForm[askParams.id] = answer;
              }
                catch(e) {
                
              }
              
          }
      }

      return qfForm;
  }



}

module.exports = prism_quickfeature;