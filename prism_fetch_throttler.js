const PrismFetchThrottler = function($fetch,throughput,waitInMS) {
    const ratelimiter = {
        throttleQueue: [],
        intervalId: null,
        throughput: throughput||50,
        activeQueue: [],
        waitInMS: waitInMS || 1000,
        executeBundle: function() {
            if (!this.intervalId) {
                var $ratelimiter = this;
                $ratelimiter.intervalId = setTimeout(function() {
                    var activePromises = $ratelimiter.throttleQueue.splice(0, $ratelimiter.throughput).map(itemParams=>itemParams());
                    if (activePromises.length) {
                        $ratelimiter.activeQueue.splice(0, 0, ...activePromises);
                        Promise.all(activePromises).then(function(dn) {
                            $ratelimiter.intervalId = null;
                            $ratelimiter.executeBundle();
                        });
                    } else {
                        console.debug('[PrismFetchThrottler]','Queue Done.');
                        $ratelimiter.intervalId = null;
                    }
                }, $ratelimiter.activeQueue <= $ratelimiter.throughput ? 0 : $ratelimiter.waitInMS);
            }
            
        }
    }

     function executeQueued() {
        var $arguments = arguments;
        const isPriority = Object.values($arguments).some(function(item) {
            return /priority/i.test(item);
        });

        return new Promise(function(resolve, reject) {
            const fnCB = function() {
                return $fetch.apply(null, $arguments).then(resolve).catch(reject);
            };

            if (isPriority) {
                ratelimiter.throttleQueue.splice(0, 0, fnCB);
            } else {
                ratelimiter.throttleQueue.push(fnCB);
            }
            ratelimiter.executeBundle();
        }
        );

    }
    executeQueued.config = {
        
    }

    Object.defineProperty(executeQueued.config, 'throughput',{
        get: function getThroughput() {
            return ratelimiter.throughput;
        },
        set: function setThroughput(val){
            ratelimiter.throughput = val;
        }
    });
    
    
    return executeQueued;
}

module.exports = PrismFetchThrottler;