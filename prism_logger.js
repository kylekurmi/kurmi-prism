function prism_logger($prism) {
    $prism.logParams = $prism.logParams || {};
    $prism.logParams.title = $prism.logParams.title || "prism";
    ["log","debug","error","warn","info"].forEach(function(key){
        $prism[key] = function() {
            var $arguments = arguments;
            var arrayOfArgs = Object.keys($arguments).map(function(a){ return $arguments[a];});
            if ($prism.logParams.concatWithChar) {
                arrayOfArgs = [arrayOfArgs.join($prism.logParams.concatWithChar)]
            }
            arrayOfArgs.splice(0,0,"["+$prism.logParams.title+"]");
            
            console[key].apply(null,arrayOfArgs);
        }

    })
}

module.exports = prism_logger;