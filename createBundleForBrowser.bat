browserify ^
-r ./prism_onbrowser.js:prism ^
-r ./prism_base.js:prism_base ^
-r ./prism_api_base.js:prism_api_base ^
-r ./prism_worker.js:prism_worker ^
-r ./prism_component_base.js:prism_component_base ^
-r ./prism_sql_base.js:prism_sql_base ^
-r ./prism_sunrise_base.js:prism_sunrise_base ^
-r ./prism_workspace_base.js:prism_workspace_base ^
-r ./prism_workspace_files.js:prism_workspace_files ^
-r ./prism_workspace_files_addons.js:prism_workspace_files_addons ^
-r ./prism_logger.js:prism_logger ^
-r ./prism_api_wsdl.js:prism_api_wsdl ^
-r ./prism_executeKurmiAPI_browsersync:prism_executeKurmiAPI_browsersync ^
-r ./prism_executeKurmiAPI_async:prism_executeKurmiAPI_async ^
-r ./prism_reporting_base:prism_reporting_base ^
-r ./prism_connector_base.js:prism_connector_base ^
-o prismOnBrowser.js
exit
