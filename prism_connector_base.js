const prismworker = require("./prism_worker");
const prism_connector_base = function($prism) {
    var $prism_connector_base = this;
    
    $prism_connector_base.setConnectorVirtual = function (tenant, equipmentId, virtual) {
        return $prism.api.execute("searchConfig", { query: "/*/*[@equipmentId=" + equipmentId + "]" }, tenant).then(function(searchConfigQuery){
            if (!searchConfigQuery.path) {
                throw "[kQuery] No equipment found with equipmentId \"" + equipmentId + "\"" + (tenant ? " on tenant \"" + tenant + "\"" : "");
            }
            var query = virtual === true ? "*[not(boolean(@virtual))]" : "*[boolean(@virtual)]";
            return $prism.api.execute("searchConfig", { path: searchConfigQuery.path[0], query: query }, tenant).then(function(searchConfig){
                var setConfig = { configVersion: searchConfig.configVersion };

                setConfig.update = [searchConfigQuery.path].concat(searchConfig.path).map(function (path) {
                    return { "@path": path, key: { "@id": "virtual", "#text": virtual === true ? "true" : "false" } };
                });
                return $prism.api.execute("setConfig", setConfig, tenant);
            });
        });
    }
    $prism_connector_base.processConfigNode = function processNode(_node, mergeWith) {
        mergeWith = mergeWith || {};
        if (_node.node) {
            if (/^\d+$/.test(_node.node[0]["@id"])) {
                mergeWith = _node.node.map(function (__node) {
                    return processNode(__node, {});
                });
            }
            else {
                _node.node.forEach(function (__node) {
                    mergeWith[__node["@id"]] = processNode(__node, {});
                });
            }
        }
        if (_node.key) {
            return _node.key.reduce(function (p, c) {
                p[c["@id"]] = c["#text"];
                return p;
            }, mergeWith);
        }
        else {
            return mergeWith;
        }
    }
    
    $prism_connector_base.getEquipmentConfiguration = function (equipmentId, tenant) {
        var params = { query: "/*/*[@equipmentId=" + equipmentId + "]" };
        return $prism.api.execute("searchConfig", params, tenant).then(function(searchConfigQuery){
            if (!searchConfigQuery.path) {
                $prism.error(JSON.stringify([params, searchConfigQuery]));
                throw "[prism] No equipment found with equipmentId \"" + equipmentId + "\"" + (tenant ? " on tenant \"" + tenant + "\"" : "");
            } else {
                return $prism.api.execute("getConfigTree", { path: searchConfigQuery.path[0], query: "[@equipmentId=" + equipmentId + "]" }, tenant).then(function (equipmentConfigurationRequest) {
                    return equipmentConfigurationRequest.configTree && $prism_connector_base.processConfigNode(equipmentConfigurationRequest.configTree, {});
                });
            }
        });        
    }

    $prism_connector_base.fieldSerialize = function (fieldObject) {
        return Object.keys(fieldObject).map(function (key) {
            return {
                "@name": key,
                "#text": fieldObject[key]
            };
        });
    }

    $prism_connector_base.fieldDeserialize = function (fieldsetList, baseObject, bFavorTextValueOverTechnicalValue) {
        return fieldsetList.reduce(function (p, fieldset, i, a) {
            if (bFavorTextValueOverTechnicalValue){
                p[fieldset["@name"]] = fieldset["#text"] || fieldset["@technicalValue"];
            }
            else {
                p[fieldset["@name"]] = fieldset["@technicalValue"] || fieldset["#text"];
            }
            
            return p;
        }, baseObject || {});
    }

    $prism_connector_base.getRootConfiguration = function(tenantName) {
        return $prism.api.execute('getConfigTree',{path:'/'}, tenantName).then(function(res){
            return $prism_connector_base.processConfigNode(res.configTree,{});
        });        
    }

    $prism_connector_base.getReference = async function getReference(moduleName, tenantName){
        var tenant = $prism.api.tenants.filter(function(tenant){
            return (new RegExp(tenantName,"i")).test(tenant.identifier);
        })[0];
        var workspaceId = 2;
        if (tenant){
            workspaceId = await $prism.workspace.getWorkspaceId(tenant.configBranch.split('/').slice(-2,-1)[0]);
        }

        return await prism.api.getReferenceConfigTree({workspace:workspaceId}).then(function({configTree}){
            return configTree.node.find(n=>n['@id'] == moduleName);
        }).then(function({node}){
           return node[0]; 
        }).then(function(node){
            return node;
            //return $prism_connector_base.processConfigNode(node,{}); 
        });
    }


    $prism_connector_base.getRegistry = async function getRegistry(tenantName){
        const setConfig = {
            add: [],
            update: [],
            delete: []
        }
        
        function CheckObjectdefineProperty(obj,key,getSetObj){
            if (!obj.hasOwnProperty(key)){
                Object.defineProperty(obj,key,getSetObj);
            }
            
        }
        
        class Node {
            constructor(id, keys, nodes, parentNode, isNew) {
                this['@id'] = id;
                this.key = keys || [];
                this.node = nodes || [];
        
                this.setParentNode(parentNode, isNew);
                this.loadFromSource();
            }
        
            findNodeByPath(path) {
                return path.split('/').reduce(function(p, c) {
                    if (c == "" || !p)
                        return p;
        
                    var foundNode = p.node.find(n=>n['@id'] == c);
                    if (!foundNode && /\d+/.test(c)) {
                        foundNode = p.node[0];
                    }
                    return foundNode;
        
                }, this);
            }
        
            getIdInParent() {
                return this['@id'];
            }
        
            loadFromSource() {
                var $this = this;
                if (!$this.key) {
                    $this.key = [];
                }
                if (!$this.node) {
                    $this.node = [];
                }
        
                ($this.node || []).forEach(function(nodeItem) {
                    Object.setPrototypeOf(nodeItem, Node.prototype);
                    nodeItem.setParentNode($this, false);
        
                    
                    CheckObjectdefineProperty($this, nodeItem['@id'], {
                        get() {
                            var foundNode = $this.node.find(n=>n['@id'] == nodeItem['@id']);
                            if (!foundNode.key || foundNode.key.length == 0) {
                                if (!foundNode.node['createAnonymousNode']) {
                                    CheckObjectdefineProperty(foundNode.node, 'createAnonymousNode', {
                                        get() {
                                            return function() {
                                                return foundNode.createAnonymousNode();
                                            }
                                            ;
                                        }
                                    });
        
                                }
                                return foundNode.node;
                            }
                            return foundNode;
                        },
                        set(nodeSetValue) {
                            var foundNodeIndex = $this.node.findIndex(n=>n['@id'] == nodeItem['@id']);
                            $this.node[foundNodeIndex] = nodeSetValue;
                        }
                    })
                    nodeItem.loadFromSource();
        
                });
                ($this.key || []).forEach(function(keyItem) {
                    var keyId = keyItem['@id'];
                    if (keyId != "key") {
                        $this.defineKey(keyId);
                    }
        
                });
            }
        
            getKey(keyId) {
                if (this.key) {
                    for (let i = 0; i < this.key.length; i++) {
                        if (this.key[i]['@id'] === keyId) {
                            return this.key[i]['#text'];
                        }
                    }
                }
                return null;
            }
        
            defineKey(keyId) {
                var $this = this;
                CheckObjectdefineProperty(this, keyId, {
                    get() {
                        return $this.getKey(keyId);
                    },
                    set(keyValue) {
                        $this.updateKey(keyId, keyValue);
                    }
                });
            }
        
            defineNode(nodeId) {
                if (this[nodeId])
                    return;
                CheckObjectdefineProperty(this, nodeId, {
                    get() {
                        return this.getNode(nodeId);
                    },
                    set(nodeValue) {
                        this.updateNode(nodeId, nodeValue);
                    }
                })
            }
        
            getNode(nodeId) {
                return this.node.find(n=>n['@id'] == nodeId);
            }
        
            updateNode(nodeId, nodeValue) {
                for (var n = 0; n < this.node.length; n++) {
                    if (this.node[n]['@id'] == nodeId) {
                        this.node[n] = nodeValue;
                        break;
                    }
                }
            }
        
            updateKey(keyId, keyValue) {
                if (keyId == "key")
                    return;
                var foundKey = this.key.find(k=>k['@id'] == keyId);
                if (!foundKey) {
                    foundKey = {
                        '@id': keyId,
                        '#text': keyValue
                    };
                    this.key.push(foundKey);
                }
                foundKey['#text'] = keyValue;
                if (this.isNew) {} else {
                    if (!this.updateTarget) {
                        this.updateTarget = {
                            node: [],
                            key: []
                        };
                        this.updateTarget['@path'] = this.getPath();
                        setConfig.update.push(this.updateTarget);
                    }
                    this.updateTarget.key.push(foundKey);
                }
        
            }
        
            createAnonymousNode() {
                return this.createNode.call(this, '000000');
            }
        
            createNode(id) {
                var refId = id || "123456";
                var parentRef = this.parentNode.getPath();
                var refPath = this.getPath();
                if (refPath == parentRef) {
                    refPath += "/1234567890";
                }
                var referenceNode = referenceRegistry.findNodeByPath(refPath + "/" + refId);
                if (!referenceNode || !referenceNode.key) {
                    debugger ;
                }
                var referenceKeys = referenceNode ? referenceNode.key : [];
                const newNode = new Node(id,referenceKeys,[],this,true);
                this.node.push(newNode);
                this.defineNode(id);
        
                if (newNode.isNew && newNode.parentNode.isNew) {} else if (newNode.isNew && !newNode.parentNode.isNew) {
                    newNode['@path'] = this.getPath();
        
                    setConfig.add.push(newNode);
                } else {
                    newNode.updateTarget = {
                        key: [],
                        node: []
                    };
                    newNode.updateTarget['@path'] = newNode.getPath();
                    setConfig.update.push(newNode.updateTarget);
                }
                return newNode;
            }
        
            getPath() {
                var parentPath = this.parentNode && this.parentNode.getPath();
                parentPath = parentPath || "";
                var currentSegment = this['@id'] ? "/" + this['@id'] : "";
                return parentPath + currentSegment;
            }
        
            save() {
                var $this = this;
                Object.entries(this).filter(([k,v])=>!/key|node|@\w+/gi.test(k)).forEach(function([k,v]) {
        
                    $this.updateKey(k, v);
                    delete $this[k];
                });
                this.node.forEach(function(subnode) {
                    subnode.save();
                });
            }
        
            setParentNode($parentNode, $isNew) {
                var parentNode = $parentNode;
                var isNew = $isNew !== undefined ? $isNew : true;
                var updateTarget = null;
                
                    CheckObjectdefineProperty(this, 'parentNode', {
                        get() {
                            return parentNode;
                        },
                        set(parentNodeValue) {
                            parentNode = parentNodeValue;
                        }
                    });
                
                
                    CheckObjectdefineProperty(this, 'isNew', {
                        get() {
                            return isNew;
                        },
                        set(isNewValue) {
                            isNew = isNewValue;
                        }
                    });
                
                
                    CheckObjectdefineProperty(this, 'updateTarget', {
                        get() {
                            return updateTarget;
                        },
                        set(updateTargetValue) {
                            updateTarget = updateTargetValue;
                        }
                    });
                
            }
        }
        
        var {configTree, configVersion} = (await prism.api.getConfigTree({}, 'Disney'));
        var rawregistry = configTree;
        setConfig.configVersion = configVersion;
        
        var rawReferenceRegistry = await (async function getReference(tenantName) {
            var tenant = prism.api.tenants.filter(function(tenant) {
                return (new RegExp(tenantName,"i")).test(tenant.identifier);
            })[0];
            var workspaceId = 2;
            if (tenant) {
                workspaceId = await prism.workspace.getWorkspaceId(tenant.configBranch.split('/').slice(-2, -1)[0]);
            }
        
            return await prism.api.getReferenceConfigTree({
                workspace: workspaceId
            }).then(function({configTree}) {
                return configTree
            })
        }
        )();
        
        var registry = new Node('',rawregistry.key,rawregistry.node,null,false);
        registry.loadFromSource();
        
        var referenceRegistry = new Node('',rawReferenceRegistry.key,rawReferenceRegistry.node,null,false);
        referenceRegistry.loadFromSource();

        CheckObjectdefineProperty(registry,'setConfig',{
            get() {
                return setConfig;
            }
        });

        return registry;

    }

    function genericConnectorEquipment() {


    }


    function AvayaCM(){
        this.__proto__ = new genericConnectorEquipment();
    }
    
    
}

module.exports = prism_connector_base;