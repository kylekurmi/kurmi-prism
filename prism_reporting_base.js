const prism_reporting_base = function ($prism) {
    var $prism_reporting_base = this;
    function getSegments(path){
        var segments = [];
        while(path.length > 0) {
            var dot = path.indexOf('.');
            var bracketOpen = path.indexOf('[');
            var bracketClose = path.indexOf(']');
            var splitCaptureStart = -1;
            var splitCaptureEnd = 1;
  
            if (dot == -1 && bracketOpen == -1) {
                splitCaptureStart = 0;
                splitCaptureEnd = path.length;
            }
            else if (dot == -1 && bracketOpen != -1){
                dot = 1000000
            }
            else if (dot != -1 && bracketOpen == -1){
                bracketOpen = 100000000;
            }
  
            if (dot < bracketOpen){
                splitCaptureStart = dot;
                splitCaptureEnd = dot+1;
            }
            else if (bracketOpen < dot){
                splitCaptureStart = bracketOpen;
                splitCaptureEnd = bracketClose+1;
                if (path[splitCaptureEnd] == ".") {
                    splitCaptureEnd++;
                }
            }
            if (splitCaptureStart > 0) {
                segments.push(path.substring(0,splitCaptureStart));
            }
            if (splitCaptureEnd > 0) {
                segments.push(path.substring(splitCaptureStart,splitCaptureEnd).replace(/(.)\.$/,"$1"));
            }
            path = path.slice(splitCaptureEnd);
  
        }
        return segments;
    }
  
    function evalSegments(withObject,segments){
        if (segments.length == 0){
            return withObject;
        }
        else if (/\{/.test(segments[0])){
            var newWithObject = withObject;
        }
        else {
            var newWithObject = withObject[segments[0]] ;
        }
  
        if (Array.isArray(newWithObject)){
            var dotBracket = segments[1];
            var props = dotBracket.match(/\[(.+?)\]/)[1].split(/&&|\|\|/);
  
            if (props && props.indexOf('*') == -1){
                newWithObject = newWithObject.filter(function(item){
                    with(item){
                        return props.every(function(prop){ return eval(prop);});
                    }
                });
            }
            return newWithObject.map(function(item){
                return evalSegments(item, segments.slice(2));
            });
        }
        else if (typeof newWithObject === "object"){
            var matchForProps = segments[Math.min(segments.length-1,1)];
            var objProps = matchForProps.match(/\{(.+?)\}/);
            objProps = objProps && objProps[1].split(',');
            if (objProps){
                return JSON.parse(JSON.stringify(newWithObject,objProps))
            }
            return evalSegments(newWithObject,segments.slice(2));
        }
        else {
            return newWithObject;
        }
  
    }
    $prism_reporting_base.evalPath = function(path){
        var segments = getSegments(path);
        return evalSegments(this,segments);
    }

    $prism_reporting_base.listAdministrators = function(){
        return prism.api.searchAdministrators({filter:{}}).then(function(result) { 
            return Promise.all(result.login.map(function(adminLogin) { 
                return prism.api.execute("getAdministratorContent",{adminLogin:adminLogin}).then(function(result){
                    return prism.component.fieldDeserialize(result.field);
                });
            })); 
        });
    }

}

module.exports = prism_reporting_base;