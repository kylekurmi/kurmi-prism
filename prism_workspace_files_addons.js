const kurmifieldlist = require('./kurmi_fields');
const XMLReference = {
    xmlDocument: null,
    parseXmlDocument: function (xmlContent) {
        $this = this;
        $this.xmlDocument = typeof xmlContent == "string" ? (new DOMParser()).parseFromString(xmlContent, "application/xml") : xmlContent;
        $this.ready = true;

    }
}

function getParentsUntil(ele,tagName) {
    return ele.tagName == tagName ? ele : getParentsUntil(ele.parentNode, tagName);
}

module.exports.QuickFeatureReference = QuickFeatureReference = function (xmlContent) {
    $QuickFeatureReference = this;
    $QuickFeatureReference.__proto__ = XMLReference;
    $QuickFeatureReference.parseXmlDocument(xmlContent);
    var quickfeatureId = $QuickFeatureReference.id;
    
    Object.defineProperty($QuickFeatureReference, 'id', {
        get() {
            if ($QuickFeatureReference.xmlDocument.querySelector('quickchange id')) {
                return $QuickFeatureReference.xmlDocument.querySelector('quickchange id').textContent;
            }
            else {
                console.error('Didnt find quickchange id field', $WorkspaceFileReference.path);
                return null;
            }
        },
        set(val) {
            $QuickFeatureReference.xmlDocument.querySelector('quickchange id').textContent = val;
        }
    });

    Object.defineProperty($QuickFeatureReference, 'authorization', {
        get() {
            if ($QuickFeatureReference.xmlDocument.querySelector('authorization')) {
                return [...$QuickFeatureReference.xmlDocument.querySelector('authorization').children].map(c=>c.tagName);
            }
            else {
           
                return null;
            }
        },
        set(vals) {
            $QuickFeatureReference.xmlDocument.querySelector('authorization').replaceChildren(...vals.map(c=>$QuickFeatureReference.xmlDocument.createElement(c)));
        }
    });

    

}



module.exports.ServiceReference = function ServiceReference(xmlContent) {
    
    $ServiceReference = this;
    $ServiceReference.__proto__ = XMLReference;

    
    $ServiceReference.parseXmlDocument(xmlContent);
    var service = $ServiceReference.xmlDocument.querySelector('service');
    var serviceIdentifier = service && service.getAttribute('identifier');
    //const globalmapindex = JSON.stringify({ workspaceName, serviceIdentifier });
    //globalmap.set(globalmapindex, $ServiceReference);
    
    this.getModules = function () {
        var modules = [...this.xmlDocument.querySelectorAll('module')].map(f => f.textContent);
        var modulesAndFields = Object.entries(kurmifieldlist[0].modules).filter(([moduleName, fields]) => { return modules.some(mod => moduleName.indexOf(mod) != -1); });
        return modulesAndFields;
    }
    this.getAnchorPoint = function () {
        var modifyVariable = this.xmlDocument.querySelector('modifyVariable[logic="anchorPoint"]');
        var addVariable = this.xmlDocument.querySelector('addVariable[logic="anchorPoint"]');
        var variable = this.xmlDocument.querySelector('variable[logic="anchorPoint"]');
        return modifyVariable || addVariable || variable;
    }

    function scoreTag(element) {
        //scores the rule or variable based on whether it's an add, modify, delete or builtin rule;
        if (!element) return -2;
        var tagName = element.tagName;

        if (/add/i.test(tagName)) {
            return 1;
        }
        else if (/modify/i.test(tagName)) {
            return 2;
        }
        else if (/remove/i.test(tagName)) {
            return -1;
        }
        else {
            return 0;
        }
    }
    function weightTag(elementA, elementB) {
        //favors the more recent rule or variable and returns it;
        if (scoreTag(elementA) >= scoreTag(elementB)) {
            return elementA;
        }
        else {
            return elementB;
        }
    }

    this.getBlocks = function () {
        var blocks = [...this.xmlDocument.querySelectorAll('variable:not([logic="field"]),addVariable:not([logic="field"]),modifyVariable:not([logic="field"])')];

        return Object.values(blocks.reduce(function (p, c) {
            var id = c.getAttribute('id');
            p[id] = weightTag(p[id], c);
            return p;
        }, []));
    }
    this.getServiceVariables = function () {
        return [...this.xmlDocument.querySelectorAll('variable[logic="field"], addVariable[logic="field"],modifyVariable[logic="field"]')];
    }
    this.getFields = function () {

        var fieldMap = {};
        this.xmlDocument.querySelectorAll('rule, addRule').forEach(function (rule) {
            rule.querySelectorAll('fields field').forEach(function (field) {
                var mappedField = fieldMap[`${rule.querySelector('expression').textContent}:${field.textContent}`] = fieldMap[`${rule.querySelector('expression').textContent}:${field.textContent}`] || {};
                mappedField.rules = mappedField.rules || [];
                mappedField.rules.push(rule.getAttribute('uid'))
                mappedField.expression = rule.querySelector('expression').textContent;
                mappedField.name = field.textContent;
            });
            //globalmap.set(globalmapindex+`&rule='${rule.getAttribute('uid')}'`, rule);
        });
        window.fieldMap = fieldMap;
        return Object.values(fieldMap);
    }



    this.getRulesByBlockAndField = function (block, fieldName) {
        var blockIdentifier = typeof block == 'string' ? block : variable.getAttribute('logic') == "field" ? 'self' : block.getAttribute('id');
        return [...this.xmlDocument.querySelectorAll('rule,addRule,modifyRule')].filter(function (rule) { return rule.querySelector('expression').textContent == blockIdentifier && [...rule.querySelectorAll('fields field')].find(f => f.textContent == fieldName); });
    }

    this.getRuleById = function (uid) {
        var rules = this.getRulesById(uid);
        return rules.modifyRule || rules.addRule || rules.rule;
    }
    this.getRulesById = function (uid) {
        var rule = this.xmlDocument.querySelector(`rule[uid="${uid}"]`);
        var addRule = this.xmlDocument.querySelector(`addRule[uid="${uid}"]`);
        var modifyRule = this.xmlDocument.querySelector(`modifyRule[uid="${uid}"]`);
        var removeRule = this.xmlDocument.querySelector(`removeRule[uid="${uid}"]`);
        return { addRule, modifyRule, removeRule, rule };
    }
    this.getFieldsOnVariable = function (variable) {
        if (variable.getAttribute('logic') != "field") {
            var componentType = variable.querySelector('componentType') && variable.querySelector('componentType').getAttribute('id');
            var specificModule = this.getModules().find(m => new RegExp(componentType, 'i').test(m[0]));
            if (!specificModule) {
                console.error("couldn't find module by name");
            }
            var moduleFields = specificModule && specificModule[1].map(function (field) { return { name: field, expression: variable.getAttribute('id'), rules: [] } }) || [];
            var knownFields = this.getFields().filter(function (field) { return variable.getAttribute('logic') == "field" ? field.expression == 'self' : field.expression == variable.getAttribute('id'); })

            return moduleFields.reduce(function (p, c) {
                if (!p.find(field => field.name == c.name)) {
                    p.push(c);
                }
                return p;
            }, knownFields);

        }
        if (!variable) {
            console.error("no variable");
            return [];
        }
        return this.getFields().filter(function (field) { return variable.getAttribute('logic') == "field" ? field.expression == 'self' : field.expression == variable.getAttribute('id'); });
    }
}