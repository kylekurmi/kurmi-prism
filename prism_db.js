// db.js
const Dexie = require('dexie');

const db = new Dexie('workspace');;
db.version(1).stores({
  files: '++id, workspaceName, filePath, fileResult', // Primary key and indexed props
});

module.exports.db = db;