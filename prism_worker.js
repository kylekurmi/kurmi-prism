function prism_worker(arrayOfParams,functionToExecute,desc){
    this.msg = "im a prism worker: "+ desc;
  var $prism_worker = this;
    this.thenCounter = 0;
  try {
      var args = arguments;
      var rawResponse = functionToExecute.apply(null,arrayOfParams);
        
      if ( typeof rawResponse === 'object' && typeof rawResponse.then === 'function' && typeof rawResponse.catch === 'function'){
          //return promise
          return rawResponse;
      }
      $prism_worker.response = JSON.parse(rawResponse);
  }
  catch(e){
      $prism_worker.response = {errorMessage: e};
  }
  $prism_worker.data = $prism_worker.response;
  
  this.then = function(cb){
        this.thenCounter++;
      if (cb && $prism_worker.response.status && $prism_worker.response.status == "SUCCESS"){
          $prism_worker.data = cb($prism_worker.data);
      }
        
      return $prism_worker;
  }
  this.catch = function(ecb){
      if (ecb && (!$prism_worker.response || !$prism_worker.response.status || $prism_worker.response.status != "SUCCESS")){
          console.error("catch statement",JSON.stringify([ecb]));
          ecb($prism_worker.data);
      }
      return $prism_worker;
  }
  
}

module.exports = prism_worker;


