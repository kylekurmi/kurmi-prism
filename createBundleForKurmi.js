const fs = require('fs');
//const Promise = require("Promise");

var filesPath = ["prism_promise.js", "prism_base.js","prism_quickfeature.js","prism_api_base.js","prism_worker.js","prism_component_base.js","prism_sql_base.js","prism_sunrise_base.js","prism_workspace_base.js",
"prism_logger.js","prism_onkurmi.js"];

var filesContents = [];
Promise.all(filesPath.map(function(path){
    return new Promise(function(resolve,reject){
        fs.readFile(path, 'utf8', (err, data) => {
            if (err) {
              reject(err);
              return;
            }
            var content = data;
            content = content.replace(/module\.exports.*/g,"");
            content = content.replace(/\Srequire[\("'\.\/]+(.+?)["'\)]+/g,"$1");
            content = content.replace(/const .* require[\("'\.\/]+(.+?)["'\);]+/g,"");
            resolve(content);
          });

    });
    
})).then(function(res){
    
    fs.writeFile("prism.util.js",res.join('\r\n'),err=>{console.error(err)});
});







