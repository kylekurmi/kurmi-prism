const prismworker = require("./prism_worker");
const prismpromise = require("./prism_promise");
//const url = require('url');
const prism_api_base = function($prism) {

    var $prism_api_base = this;
    const configParams = {
        auth: {},
        kurmiHost: '..',
        apiPath: "/Kurmi/restAPI.do",
        substitutitionTenantOrTemplate: null,
        throughput: 50
    }
    
    const mergeParams = function(newConfigParams) {
        $configParams = newConfigParams || {
            auth: {},
            kurmiHost: '..',
            apiPath: "/Kurmi/restAPI.do",
            substitutitionTenantOrTemplate: null
        };
        Object.keys($configParams).reduce(function(p,c){
            p[c] = $configParams[c];
            return p;
        },configParams);
    }
    this.initParams = mergeParams;

    if (this.sessionStorage){
        mergeParams(JSON.parse(sessionStorage.getItem('configParams')));
    }

    this.kurmiRequestPath = function() {
        return configParams.kurmiHost + configParams.apiPath;
    }

    this.kurmiWSDLPath = function() {
        return this.kurmiRequestPath().replace("restAPI.do","APIService?wsdl");
    }

    this.configParams = configParams;

    function getAuth() {
        return JSON.parse(JSON.stringify(configParams.auth));
    }

    function setAuth(login,password,kurmiHost){
        configParams.auth.login = login;
        configParams.auth.password = password;
        if (this.window && this.this.window && this.window.hasOwnProperty('sessionStorage')){
            sessionStorage.setItem('configParams', JSON.stringify(configParams));
        }
        if (kurmiHost){
            configParams.kurmiHost = kurmiHost;
        }
    }

    this.execute = function(functionName,params,tenantDbidOrName){
        params.auth = getAuth();
        if (tenantDbidOrName){
            params.auth.substitutionTenantOrTemplate  = getSubstitutionTenantOrTemplate(tenantDbidOrName);
        }
        else if (configParams.substitutionTenantOrTemplate) {
            params.auth.substitutionTenantOrTemplate  = configParams.substitutionTenantOrTemplate ;
        }
        
        if (typeof Promise === "function"){
            return $prism.executeKurmiAPI(functionName,params);
        }

        return new prismpromise(function(resolve,reject){
            var rawresult = $prism.executeKurmiAPI(functionName,params);
            
            var result = typeof rawresult === "object" ? rawresult : JSON.parse(rawresult);
            if (result.status && result.status != "SUCCESS"){
                reject(result);
            }
            else {
                resolve(result);
            }

        });
        
        //return new prismworker([functionName,params],$prism.executeKurmiAPI);
    }

    this.execute.priority = function(){
        var args = Object.values(arguments).concat('priority');
        this.execute.apply(this,  args)
    }

    const connectedEvents = [function(p){
        return p.getTenants().then(function(res){
            p.tenants = res;
            //$prism.debug("loaded tenants",JSON.stringify(p.tenants));
        });
        
    }];
    this.addConnectedEvent = function(callback){
        connectedEvents.push(callback);
    }

    const proxypromise = typeof Promise === "function" ? Promise : prismpromise;

    this.isConnected = false;
    this.isSubstituted = false;
    this.login = setAuth;
    this.connect = function () {

        return this.execute("getKurmiVersion", {}).then(function (res) {
            if (res.errorDetail) throw res.errorDetail;
            
            return proxypromise.all(connectedEvents.map(function (cb) {
                return cb($prism_api_base);
            })).then(function (done) {

                $prism.debug("connect", true);
                $prism_api_base.isConnected = true;
                return res;
            });


        }).catch(function (e) {
            $prism.debug("connect", false, e);
            $prism_api_base.isConnected = false;
            throw e;
        });
    }

    var tenantList = null; 
    this.tenants = tenantList;
    this.getTenants = function() {
        return $prism.component.searchComponentList({type:"TENANT"});
    }

    function getSubstitutionTenantOrTemplate(tenantDbidOrName){
        var result = null;
        switch(typeof tenantDbidOrName){
            
            case typeof "tenantname": {
                try {
                    if (/SYSTEM/i.test(tenantDbidOrName)){
                        result = null;
                    }
                    //var numberDbid = tenantDbidOrName.replace(/\D/g,"");
                    //if (numberDbid && parseInt(numberDbid)){
                    //    result = getSubstituteTenantOrTemplate(parseInt(numberDbid));
                    //}
                    var tenant = $prism_api_base.tenants.filter(function(tenant){
                        return (new RegExp(tenantDbidOrName,"i")).test(tenant.identifier);
                    })[0];
                    result = {"@type": "TENANT", "@dbid": tenant["dbid"]};
                }
                catch(e){

                }

                break;
            }
            case typeof 1234: {

                break;
            }

        }
        $prism.debug("getSubstitutionTenantOrTemplate",JSON.stringify([tenantDbidOrName,result]));
        return result;
    }

    this.substitute = function(tenantDbidOrName){
        configParams.substitutionTenantOrTemplate = getSubstitutionTenantOrTemplate(tenantDbidOrName);
        console.info("Substituted to ",tenantDbidOrName);
        this.isSubstituted = configParams.substitutionTenantOrTemplate;
        return true;
    }

    this.exitSubtitution = function() {
        delete configParams.auth.substitutionTenantOrTemplate;
        configParams.substitutionTenantOrTemplate = null;
        console.info("Exited Substitution");
        this.isSubstituted = configParams.substitutionTenantOrTemplate;
        return true;
    }

}

module.exports = prism_api_base;
