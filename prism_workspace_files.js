
const db = require('./prism_db');

const { ServiceReference, QuickFeatureReference} = require('./prism_workspace_files_addons');
module.exports = function prism_workspace_files($prism) {
    const $prism_workspace_base = $prism.workspace;
    $prism_workspace_base.ServiceReference = ServiceReference;
    $prism_workspace_base.QuickFeatureReference = QuickFeatureReference;

    $prism_workspace_base.getWorkspaceServicesAndQuickfeaturesDocument = function (workspaceName) {
        const $prism_workspace_base = this;
        return $prism_workspace_base.listWorkspaceFiles(workspaceName, /(service|quickfeature).xml/).then(function (listServices) {
            return listServices.map(function (filePath) {
                return $prism_workspace_base.getWorkspaceFile(workspaceName, filePath)
            })
        }).then(function (fileContentPromises) {
            return Promise.all(fileContentPromises).then(function (fileResults) { return $prism_workspace_base.parseFileResultstoDocument(fileResults); });
        });

    }

    $prism_workspace_base.Workspace = function (workspaceDocument) {
        this.xmlDocument = workspaceDocument;
        function getParentsUntil(ele, tagName) {
            return ele.tagName != tagName ? getParentsUntil(ele.parentElement, tagName) : ele;
        }
        const $xmlDocument = this.xmlDocument;

        Object.defineProperty(this,
            "serviceQuickFeatureMapping", {
            get() {
                return [...$xmlDocument.querySelectorAll('service')].reduce(function (list, service) {
                    var qfs = [...$xmlDocument.querySelectorAll(`quickchange service-addon[identifier="${service.getAttribute('uid')}"], quickchange service[identifier="${service.getAttribute('uid')}"]`)].map(el => getParentsUntil(el, 'quickchange')).filter(function (item, i, arr) { return arr.findIndex(it => it.getAttribute('uid') == item.getAttribute('uid')) == i; });
                    if (qfs.length) {
                        list[service.getAttribute('identifier')] = qfs;
                    }
                    return list;
                }, {});
            }
        }
        );

    }

    $prism_workspace_base.parseFileResultstoDocument = function (arrFileResults) {
        var dp = new DOMParser();
        var doc = dp.parseFromString("<workspace></workspace>", "application/xml");
        var workspace = doc.querySelector('workspace');
        arrFileResults.forEach(function (fileResult) {
            var docparsed = dp.parseFromString(fileResult.content, "application/xml");
            var fileResultElement = document.createElement('file-result');
            ['id', 'path', 'revisionAuthor', 'revisionComment', 'revisionDate', 'title'].forEach(function (attribute) {
                fileResultElement.setAttribute(attribute, fileResult[attribute]);

            });
            fileResultElement.appendChild(docparsed.documentElement);
            workspace.appendChild(fileResultElement);
        });
        return doc;
    }

    $prism_workspace_base.parseXMLtoDocument = function (stringXML) {
        return (new DOMParser()).parseFromString(stringXML, "application/xml");
    }

    function getParentsUntil(ele,tagName) {
        return ele.tagName == tagName ? ele : getParentsUntil(ele.parentNode, tagName);
    }

    $prism_workspace_base.WorkspaceReference = function (workspaceName, globalmap) {
        const $WorkspaceReference = this;
        this.workspaceName = workspaceName;
        this.workspacePath = null;
        const _files = [];
        const _fileList = [];
        this.listReady = false;

        globalmap = globalmap || new Map();

        var $WorkspaceReferenceResolver, $WorkspaceReferenceRejecter;

        this.onReady = new Promise(function (resolve, reject) {
            $WorkspaceReferenceResolver = resolve;
            $WorkspaceReferenceRejecter = reject;
        });



        this.getFileListAsync = function (nocache) {
            $WorkspaceReferenceResolver.listReady = false;

            return new Promise(function (resolve, reject) {

                if (nocache != true && _fileList.length) {
                    resolve(_fileList);
                }

                $prism_workspace_base.listWorkspaceFiles(workspaceName).then(function (listFiles) {
                    resolve(listFiles);
                }).catch(reject);

            }).then(function (listFiles) {
                _fileList.splice(0, _fileList.length, ...listFiles);
                _files.splice(0, _files.length);
                listFiles.forEach(function (filePath) { _files.push(new $WorkspaceReference.WorkspaceFileReference(filePath)) });
                $WorkspaceReference.listReady = true;
                $WorkspaceReferenceResolver(_files);
                return _files;
            });
            //.then($WorkspaceReferenceResolver).catch($WorkspaceReferenceRejecter);
        }

        Object.defineProperty(this, "files", {
            get: function () {
                if (_files.length == 0) {
                    $WorkspaceReference.getFileListAsync();
                }
                return _files;
            }
        });

        Object.defineProperty(this, "cachedFileList", {
            get() {
                var cachedWS = JSON.parse(localStorage.getItem(`workspace=${workspaceName}`));
                if (cachedWS && cachedWS.files) return cachedWS.files;
            },
            set(val) {
                
                localStorage.setItem(`workspace=${workspaceName}`, JSON.stringify({ files: val }));
            }
        })


        if ($WorkspaceReference.cachedFiles) {

            _files.splice(0, _files.length, ...$WorkspaceReference.cachedFiles);
            $WorkspaceReferenceResolver.listReady = true;
            $WorkspaceReferenceResolver();
        }

        $WorkspaceReference.WorkspaceFileReference = function (filePath) {

            const $WorkspaceFileReference = this;
            const globalmapindex = JSON.stringify({ workspaceName, filePath });
            globalmap.set(globalmapindex, $WorkspaceFileReference);
            $WorkspaceFileReference.loaded = -1;
            $WorkspaceFileReference._lastContent = null;
            $WorkspaceFileReference._fileResult = $WorkspaceFileReference.cachedFileResult;
            $WorkspaceFileReference.isSaved = false;
            $WorkspaceFileReference.path = filePath;
            $WorkspaceFileReference.title = '';
            $WorkspaceFileReference.error;
            $WorkspaceFileReference.success;
            var loadedResolve;
            var loadedReject;
            $WorkspaceFileReference.onLoad = new Promise(function (good, bad) {
                loadedResolve = good;
                loadedReject = bad;
            });

            $WorkspaceFileReference.getFileResultAsync = function () {
                $WorkspaceFileReference.loaded = 0;

                return $prism_workspace_base.getWorkspaceFile(workspaceName, filePath).then(function (fileResult) {
                    $WorkspaceFileReference._fileResult = fileResult;
                    $WorkspaceFileReference.loaded = 1;
                    $WorkspaceFileReference.cachedFileResult = fileResult;
                }).then(loadedResolve).catch(loadedReject).catch(function (e) {
                    $WorkspaceFileReference.loaded = -1;
                    $WorkspaceFileReference.error = e;
                });
            }



            Object.defineProperty($WorkspaceFileReference, 'fileResult', {
                get: function () {
                    if (!$WorkspaceFileReference._fileResult) {
                        $WorkspaceFileReference.getFileResultAsync();
                    }
                    return $WorkspaceFileReference._fileResult;
                },
                set: function (val) {
                    $WorkspaceFileReference._fileResult = val;
                    $WorkspaceFileReference.cachedFileResult = val;
                }
            });

            Object.defineProperty($WorkspaceFileReference, 'content', {
                get: function () {
                    return $WorkspaceFileReference._fileResult && $WorkspaceFileReference._fileResult.content;
                },
                set: function (val) {
                    $WorkspaceFileReference.lastContent = $WorkspaceFileReference._fileResult.content;
                    if (!$WorkspaceFileReference._fileResult) {
                        $WorkspaceFileReference._fileResult = { path: filePath };
                    }
                    $WorkspaceFileReference._fileResult.content = val;
                    $WorkspaceFileReference.isSaved = false;
                }
            });



            this.save = function () {
                $WorkspaceFileReference.isSaved = false;
                $WorkspaceFileReference.error = undefined;
                $WorkspaceFileReference.success = undefined;
                return $prism_workspace_base.addWorkspaceFile(workspaceName, $WorkspaceFileReference.fileResult).then(function (fileResult) {
                    $WorkspaceFileReference.isSaved = true;
                    $WorkspaceFileReference.success = "Saved on " + (new Date()).toString();

                }).catch(function (e) {
                    $WorkspaceFileReference.error = e;
                    console.error("Did not save workspace file ", e);
                })
            }



            Object.defineProperty($WorkspaceFileReference, 'cachedFileResult', {
                get() {
                    return localStorage.getItem(`workspace=${workspaceName},file-path=${filePath}`);
                },
                set(fileResult) {
                    //db.files.add({workspaceName,filePath,fileResult})
                    //localStorage.setItem(`workspace=${workspaceName},file-path=${filePath}`, JSON.stringify($WorkspaceFileReference._fileResult));
                }
            })

            if ($WorkspaceFileReference._fileResult) {

            }

            

            


            $WorkspaceFileReference.onLoad.then(function() { 
                if (/service.xml/i.test($WorkspaceFileReference.path)) {
                    const $ServiceReference = new ServiceReference($WorkspaceFileReference.content);
                }

                if (/quickfeature.xml/i.test($WorkspaceFileReference.path)) {
                    const $QuickFeatureReference = new QuickFeatureReference($WorkspaceFileReference.content);
                }
            });

            $WorkspaceFileReference.toService = function () {
                return $WorkspaceFileReference.loaded == 1 ? $ServiceReference : new Error('Not Loaded');
            }

            $WorkspaceFileReference.toQuickfeature = function () {
                return $WorkspaceFileReference.loaded == 1 ? $QuickFeatureReference : new Error('Not Loaded');
            }



        }

        var dp = new DOMParser();
        $WorkspaceReference.doc = dp.parseFromString("<workspace></workspace>", "application/xml");
        $WorkspaceReference.getWorkspaceServicesAndQuickfeaturesDocumentAsync = function () {
            const getFileQueue = [];
            const getFileQueueCounts = {};
            const $interval = setInterval(function () {
                getFileQueue.splice(0, 5).forEach(function (fileRef) {
                    getFileQueueCounts[fileRef.path] = getFileQueueCounts[fileRef.path] || 0;
                    getFileQueueCounts[fileRef.path]++;
                    if (fileRef.loaded == -1) {
                        fileRef.getFileResultAsync().catch(e => {
                            if (getFileQueueCounts[fileRef.path] < 3) {
                                getFileQueue.push(fileRef);
                            }
                            else {
                                console.error("Failed to load file", fileRef);
                            }
                        });
                    }

                });

            }, 2000);

            const workspace = $WorkspaceReference.doc.querySelector('workspace');

            $WorkspaceReference.onReady.then(function () {
                return $WorkspaceReference.files;
            }).then(function (files) {
                files.filter(f => /(service|quickfeature).xml$/.test(f.path)).forEach(function (fileRef) {
                    if (fileRef.loaded == -1) {
                        fileRef.getFileResultAsync();
                    }
                    fileRef.onLoad.then(function () {
                        console.log("parsing file", fileRef.path);
                        var fileResult = fileRef.fileResult;
                        var fileResultElement = document.createElement('file-result');
                        ['id', 'path', 'revisionAuthor', 'revisionComment', 'revisionDate', 'title'].forEach(function (attribute) {
                            fileResultElement.setAttribute(attribute, fileResult[attribute]);
                        });
                        if (/service.xml/i.test(fileRef.path)) {
                            var service = fileRef.toService();

                            if (!!service.xmlDocument.querySelector('service')) {
                                fileResultElement.appendChild(service.xmlDocument.querySelector('service'));
                            }
                            else {
                                fileResultElement.textContent = fileRef.content;
                                debugger;
                            }
                        }
                        else if (/quickfeature.xml/i.test(fileRef.path)) {
                            var quickchange = fileRef.toQuickfeature();

                            if (!!quickchange.xmlDocument.querySelector('quickchange')) {
                                fileResultElement.appendChild(quickchange.xmlDocument.querySelector('quickchange'));
                            }
                            else {
                                fileResultElement.textContent = fileRef.content;
                                debugger;
                            }
                        }
                        workspace.appendChild(fileResultElement);

                    });

                });

            });
            return workspace;
        }
        Object.defineProperty($WorkspaceReference,
            "serviceQuickFeatureMapping", {
            get() {
                return [...$WorkspaceReference.doc.querySelectorAll('service')].reduce(function (list, service) {
                    var qfs = [...$WorkspaceReference.doc.querySelectorAll(`quickchange service-addon[identifier="${service.getAttribute('uid')}"], quickchange service[identifier="${service.getAttribute('uid')}"]`)].map(el => getParentsUntil(el, 'quickchange')).filter(function (item, i, arr) { return arr.findIndex(it => it.getAttribute('uid') == item.getAttribute('uid')) == i; });
                    if (qfs.length) {
                        list[service.getAttribute('identifier')] = qfs;
                    }
                    return list;
                }, {});
            }
        });
    }
}