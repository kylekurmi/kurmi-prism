const prism_base = require('./prism_base');
const fetch = require('node-fetch');
const FormData = require('form-data');
prism_base.moduleMap.executeKurmiAPI = function($prism) {
    var getExecuteKurmiAPI = require("./prism_executeKurmiAPI_async");
    return (new getExecuteKurmiAPI($prism))(fetch,FormData);
}
var $prism = prism_base.init();
module.exports = $prism;

