const prism_base = require('./prism_base');


prism_base.moduleMap.executeKurmiAPI = function($prism) {
    var getExecuteKurmiAPI = require("./prism_executeKurmiAPI_async");
    return (new getExecuteKurmiAPI($prism))(fetch,FormData);
}

prism_base.moduleMap.reporting = function($prism){
    return (new (require('./prism_reporting_base'))($prism));
}

prism_base.moduleMap.connector = function($prism){
    return (new (require('./prism_connector_base'))($prism));
}

var $prism = prism_base.init();
require('./prism_api_wsdl')($prism);
require('./prism_workspace_files')($prism);

module.exports = $prism;
